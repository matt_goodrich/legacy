<%-- 
/*******************************************************************************
* Name
*   StartLogout
*
* Identifier
*   Filename: index.jsp
*   
* Purpose
*   Since the Workday Service Provider application requires the logout URL to
*   be an HTTPS URL and the project requirements state that the Logout URL should
*   be Passport, this will be the location of the Logout URL configured in Workday
*   (https://<SSOSERVER>/StartLogout/), and this will perform a redirect to 
*   Passport.
*
******************************************************************************
*/
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%
    //Redirect to Passport.
    response.sendRedirect("http://passport");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Start Logout</title>
    </head>
    <body>
        
    </body>
</html>
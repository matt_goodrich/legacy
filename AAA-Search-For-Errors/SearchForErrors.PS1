# ---------------Start --- Send Email Message ----------------
                $smtpServer = "smtp.csaa.com"
                $msg = new-object Net.Mail.MailMessage
                $smtp = new-object Net.Mail.SmtpClient($smtpServer)
                $msg.From = "qa-sso1@tent.trt.csaa.pri"
               
                $msg.to.add("matt.goodrich@goaaa.com, ryan.tollis@goaaa.com, cody.cook@goaaa.com, eric.uythoven@goaaa.com")
   
                $msg.Subject = 'Errors: ' + (get-date).ToString()
		$errors = Get-ChildItem "C:\pingfederate-6.4.0_engine\pingfederate\log" -filter "server.log*" -Recurse | Select-String "ERROR"
                $msg.Body = $errors -replace "C:", "`r`n`r`nC:" 

                $msg.Priority = [System.Net.Mail.MailPriority]::High
                $smtp.Send($msg)
 # ---------------End --- Send Email Message ----------------
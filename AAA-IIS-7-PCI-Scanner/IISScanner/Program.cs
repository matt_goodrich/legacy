﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

using Microsoft.Web.Administration;
using Microsoft.Web.Management;
using Microsoft.Win32;


namespace IISScanner
{
    class Program
    {
        public static TextWriter tw = new StreamWriter("IISScanOutput.txt");
        public static TextWriter ew = new StreamWriter("IISScanErrorOutput.txt");
        public static TextWriter rw = new StreamWriter("IISScanResults.txt");

        public static Dictionary<string, Dictionary<string, bool>> TestResults = new Dictionary<string, Dictionary<string, bool>>();


        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            tw.WriteLine("-------------------------------------------");
            tw.WriteLine("SCANNING IIS on " + DateTime.Now);
            tw.WriteLine("-------------------------------------------\r\n\r\n");

            Dictionary<string, bool> addDir = new Dictionary<string, bool>();
            
            ServerManager manager = new ServerManager();

            SiteCollection sites = manager.Sites;

            #region 1.1.1

            tw.WriteLine("1.1.1 Ensure Web Content Is on Non-System Partition\r\n");

            foreach (Site site in sites)
            {
                tw.WriteLine("Site Name:\t" + site.Name);
                tw.WriteLine("============================================\r\n");
                foreach (Application app in site.Applications)
                {
                    tw.WriteLine("Application Path:\t" + app.Path);
                    foreach (VirtualDirectory vDir in app.VirtualDirectories)
                    {
                        //addDir = new Dictionary<string,bool>();
                        tw.WriteLine("Virtual Directory Path:\t" + vDir.PhysicalPath);
                        tw.WriteLine("----------\r\n");
                        if (vDir.PhysicalPath.Contains("C:"))
                        {
                            addDir.Add(vDir.PhysicalPath, false);
                        }
                        else
                        {
                            addDir.Add(vDir.PhysicalPath, true);
                        }
                    }
                }
            }
            TestResults.Add("1.1.1 Ensure Web Content Is on Non-System Partition", addDir);

            tw.WriteLine("\r\n-------------------------------------------");

            #endregion

            #region 1.1.2

            tw.WriteLine("\r\n1.1.2 Remove Or Rename Well-Known URLs\r\n");

            List<string> URLs = new List<string>() { "http://localhost/iissamples", "http://localhost/iisadmpwd", "http://localhost/IISHelp", "http://localhost/Printers" };
            addDir = new Dictionary<string, bool>();

            foreach (string str in URLs)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(str);
                request.AllowAutoRedirect = false;
                try
                {
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    tw.WriteLine("URL: " + str + "\tStatusCode: " + (int)response.StatusCode);
                    addDir.Add(str, ((int)response.StatusCode != 404 ? false : true));
                }
                catch (WebException ex)
                {
                    tw.WriteLine("URL: " + str + "\tStatusCode: " + ((int)((HttpWebResponse)ex.Response).StatusCode));
                    addDir.Add(str, true);
                }
            }

            List<string> Folders = new List<string>() { @"C:\inetpub\AdminScripts", @"C:\inetpub\scripts\IISSamples", @"C:\Program Files\Common Files\System\msadc" };

            foreach (string str in Folders)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(str);
                if (!dirInfo.Exists)
                {
                    tw.WriteLine("Directory: " + str + "\tDOES NOT EXIST");
                    addDir.Add(str, true);
                }
                else if (dirInfo.GetFiles().Length > 0)
                {
                    tw.WriteLine("Directory: " + str + "\tFILES FOUND");
                    addDir.Add(str, false);
                }
                else
                {
                    tw.WriteLine("Directory: " + str + "\tNO FILES IN DIRECTORY");
                    addDir.Add(str, true);
                }

            }

            TestResults.Add("1.1.2 Remove Or Rename Well-Known URLs", addDir);

            tw.WriteLine("\r\n-------------------------------------------");

            #endregion

            #region 1.1.3

            tw.WriteLine("\r\n1.1.3 Require Host Headers on all Sites\r\n");

            addDir = new Dictionary<string, bool>();

            foreach (Site site in sites)
            {
                tw.WriteLine("Site Name:\t" + site.Name);
                tw.WriteLine("============================================\r\n");

                foreach (Binding binding in site.Bindings)
                {
                    tw.WriteLine("Protocol: " + binding.Protocol);
                    tw.WriteLine("Host: " + binding.Host);
                    tw.WriteLine("Endpoint: " + binding.EndPoint);
                    tw.WriteLine("----------\r\n");
                    if (binding.Host.ToString().Length < 1)
                    {
                        addDir.Add(site.Name + ":" + binding.Protocol + "://*", false); 
                    }
                    else
                    {
                        addDir.Add(site.Name + ":" + binding.Protocol + "://" + binding.Host, true);
                    }
                }
            }

            TestResults.Add("1.1.3 Require Host Headers on all Sites", addDir);

            tw.WriteLine("\r\n-------------------------------------------");

            #endregion

            #region 1.1.4

            tw.WriteLine("\r\n1.1.4 Disable Directory Browsing\r\n");

            addDir = new Dictionary<string, bool>();

            foreach (Site site in sites)
            {
                tw.WriteLine("Site Name:\t" + site.Name);
                tw.WriteLine("============================================\r\n");

                Configuration config = manager.GetWebConfiguration(site.Name);

                ConfigurationSection directoryBrowseSection = config.GetSection("system.webServer/directoryBrowse");

                bool isEnabled = (bool)directoryBrowseSection.Attributes["enabled"].Value;
                tw.WriteLine("<system.webServer>\r\n\t<directoryBrowse enabled=\"" + isEnabled + "\" />\r\n</system.webServer>");
                tw.WriteLine("----------\r\n");

                addDir.Add(site.Name, !isEnabled);
            }

            TestResults.Add("1.1.4 Disable Directory Browsing", addDir);

            tw.WriteLine("\r\n-------------------------------------------");

            #endregion

            #region 1.1.5

            tw.WriteLine("\r\n1.1.5 Set Default Application Pool Identity To Least Privilege Principal\r\n");

            addDir = new Dictionary<string, bool>();

            tw.WriteLine("DefaultAppPool Identity: " + manager.ApplicationPools["DefaultAppPool"].ProcessModel.IdentityType.ToString());

            if (manager.ApplicationPools["DefaultAppPool"].ProcessModel.IdentityType == ProcessModelIdentityType.ApplicationPoolIdentity)
                addDir.Add("DefaultAppPool", true);
            else
                addDir.Add("DefaultAppPool", false);

            TestResults.Add("1.1.5 Set Default Application Pool Identity To Least Privilege Principal", addDir);

            tw.WriteLine("\r\n-------------------------------------------");

            #endregion

            #region 1.1.6

            tw.WriteLine("\r\n1.1.6 Ensure Application Pools Run Under Unique Identities\r\n");

            addDir = new Dictionary<string, bool>();

            foreach (ApplicationPool pool in manager.ApplicationPools)
            {
                tw.WriteLine(pool.Name + " : " + pool.ProcessModel.IdentityType.ToString());
                if (pool.ProcessModel.IdentityType == ProcessModelIdentityType.ApplicationPoolIdentity)
                    addDir.Add(pool.Name, true);
                else
                    addDir.Add(pool.Name, false);
            }

            TestResults.Add("1.1.6 Ensure Application Pools Run Under Unique Identities", addDir);

            tw.WriteLine("\r\n-------------------------------------------");

            #endregion

            #region 1.1.7

            tw.WriteLine("\r\n1.1.7 Ensure Unique Application Pools for Sites\r\n");

            addDir = new Dictionary<string, bool>();

            List<string> appPools = new List<string>();

            foreach (Site site in sites)
            {
                tw.WriteLine("Site Name:\t" + site.Name);
                tw.WriteLine("============================================\r\n");
                foreach (Application app in site.Applications)
                {
                    tw.WriteLine("Application:\t" + app.Path);
                    tw.WriteLine("Application Pool:\t" + app.Attributes["applicationPool"].Value);
                    tw.WriteLine("----------\r\n");
                    if(!appPools.Contains(app.Attributes["applicationPool"].Value.ToString()))
                    {
                        appPools.Add(app.Attributes["applicationPool"].Value.ToString());
                        addDir.Add(site.Name + app.Path, true);
                    }
                    else
                        addDir.Add(site.Name + app.Path, false);
                }
            }

            TestResults.Add("1.1.7 Ensure Unique Application Pools for Sites", addDir);

            tw.WriteLine("\r\n-------------------------------------------");

            #endregion

            #region 1.1.8

            tw.WriteLine("\r\n1.1.8 Configure Anonymous User Identity To Use Application Pool Identity\r\n");

            addDir = new Dictionary<string, bool>();

            foreach (Site site in sites)
            {
                tw.WriteLine("Site Name:\t" + site.Name);
                tw.WriteLine("============================================\r\n");

                Configuration config = manager.GetWebConfiguration(site.Name);

                ConfigurationSection directoryBrowseSection = config.GetSection("system.webServer/security/authentication/anonymousAuthentication");

                string userName = directoryBrowseSection.Attributes["userName"].Value.ToString();
                tw.WriteLine("<system.webServer>\r\n\t<security>\r\n\t\t<authentication>\r\n\t\t\t<anonymousAuthentication userName=\"" + userName + "\" />\r\n\t\t<authentication>\r\n\t</security>\r\n</system.webServer>");
                tw.WriteLine("----------\r\n");

                addDir.Add(site.Name, String.IsNullOrEmpty(userName));
            }

            TestResults.Add("1.1.8 Configure Anonymous User Identity To Use Application Pool Identity", addDir);

            tw.WriteLine("\r\n-------------------------------------------");

            #endregion

            #region 1.1.9

            tw.WriteLine("\r\n1.1.9 Configure Application Pools to Run As Application Pool Identity\r\n");

            addDir = new Dictionary<string, bool>();

            foreach (ApplicationPool pool in manager.ApplicationPools)
            {
                tw.WriteLine(pool.Name + " : " + pool.ProcessModel.IdentityType.ToString());
                if (pool.ProcessModel.IdentityType == ProcessModelIdentityType.ApplicationPoolIdentity)
                    addDir.Add(pool.Name, true);
                else
                    addDir.Add(pool.Name, false);
            }

            TestResults.Add("1.1.9 Configure Application Pools to Run As Application Pool Identity", addDir);

            tw.WriteLine("\r\n-------------------------------------------");

            #endregion

            tw.Close();
            ew.Close();

            #region WriteResults

            rw.WriteLine("-------------------------------------------");
            rw.WriteLine("SCANNING IIS Completed on " + DateTime.Now);
            rw.WriteLine("Output of processing in: IISScanOutput.txt");
            rw.WriteLine("Output of errors in: IISScanErrorOutput.txt");
            rw.WriteLine("-------------------------------------------\r\n\r\n");

            bool SectionPassed = true;

            foreach (KeyValuePair<string, Dictionary<string, bool>> kvp in TestResults)
            {
                SectionPassed = true;
                rw.WriteLine("\r\n\r\n" + kvp.Key);
                rw.WriteLine("-------------------------------------------");
                foreach (KeyValuePair<string, bool> ikvp in kvp.Value)
                {
                    rw.WriteLine(ikvp.Key + " : " + ikvp.Value.ToString());
                    if (!ikvp.Value)
                        SectionPassed = false;
                }
                rw.WriteLine("\r\nSECTION PASSED:" + SectionPassed.ToString());
                rw.WriteLine("-------------------------------------------");
            }

            rw.Close();

            #endregion

            Environment.Exit(0);
        }


        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            ew.WriteLine(e.ExceptionObject.ToString());
            ew.Close();
            tw.Close();
            rw.Close();
            Environment.Exit(1);
        }
    }
}

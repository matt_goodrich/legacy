﻿using System;
using System.DirectoryServices;

namespace ADManager
{
    class OUManager
    {
        private DirectoryEntry objADAM;  // Binding object.
        private string Host;
        private string OrgString;
        /// <summary>
        /// Initialize the LDAP connection
        /// </summary>
        public void Initialize(string LDAPConnectionString)
        {
            Console.WriteLine("Bind to: {0}", LDAPConnectionString);

            Host = LDAPConnectionString.Substring(0, LDAPConnectionString.IndexOf('/'));
            OrgString = LDAPConnectionString.Substring(LDAPConnectionString.IndexOf('/') + 1);

            // Get AD LDS object.
            try
            {
                objADAM = new DirectoryEntry("LDAP://" + LDAPConnectionString);
                objADAM.RefreshCache();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error:   Bind failed.");
                Console.WriteLine("         {0}", e.Message);
                return;
            }
        }

        /// <summary>
        /// Create AD LDS Organizational Unit.
        /// </summary>
        public void AddParentOU(string name, string description)
        {
            DirectoryEntry objOU;    // Organizational unit.
            string strDescription;   // Description of OU.
            string strOU;            // Organiztional unit.           

            // Specify Organizational Unit.
            strOU = "OU=" + name;
            strDescription = description;
            Console.WriteLine("Create:  {0}", strOU);

            // Create Organizational Unit.
            try
            {
                if (!DirectoryEntry.Exists("LDAP://" + strOU + "," + OrgString))
                {
                    objOU = objADAM.Children.Add(strOU,
                        "OrganizationalUnit");
                    objOU.Properties["description"].Add(strDescription);
                    objOU.CommitChanges();

                    // Output Organizational Unit attributes.
                    Console.WriteLine("Success: Create succeeded.");
                    Console.WriteLine("Name:    {0}", objOU.Name);
                    Console.WriteLine("         {0}",
                        objOU.Properties["description"].Value);
                    return;
                }

                // Output Organizational Unit attributes.
                Console.WriteLine("Success: OU Already Exists.");
                Console.WriteLine("Name:    {0}", strOU);
                Console.WriteLine("         {0}", strDescription);
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error:   Create failed.");
                Console.WriteLine("         {0}", e.Message);
                return;
            }
        }

        public void AddChildOU(string name, string description, string parentName)
        {
            DirectoryEntry objOU;    // Organizational unit.
            string strDescription;   // Description of OU.
            string strOU;            // Organiztional unit.           

            // Specify Organizational Unit.
            strOU = "OU=" + name;
            strDescription = description;
            Console.WriteLine("Create:  {0}", strOU);

            // Create Organizational Unit.
            try
            {
                if (!DirectoryEntry.Exists("LDAP://" + strOU + "," + "OU=" + parentName + "," + OrgString))
                {
                    objOU = objADAM.Children.Find("OU=" + parentName).Children.Add(strOU,
                        "OrganizationalUnit");
                    objOU.Properties["description"].Add(strDescription);
                    objOU.CommitChanges();

                    // Output Organizational Unit attributes.
                    Console.WriteLine("Success: Create succeeded.");
                    Console.WriteLine("Name:    {0}", objOU.Name);
                    Console.WriteLine("         {0}",
                        objOU.Properties["description"].Value);
                    return;
                }

                // Output Organizational Unit attributes.
                Console.WriteLine("Success: OU Already Exists.");
                Console.WriteLine("Name:    {0}", strOU);
                Console.WriteLine("         {0}", strDescription);
                return;

            }
            catch (Exception e)
            {
                Console.WriteLine("Error:   Create failed.");
                Console.WriteLine("         {0}", e.Message);
                return;
            }
        }

        public void AddGrandChildOU(string name, string description, string topParentName, string parentName)
        {
            DirectoryEntry objOU;    // Organizational unit.
            string strDescription;   // Description of OU.
            string strOU;            // Organiztional unit.           

            // Specify Organizational Unit.
            strOU = "OU=" + name;
            strDescription = description;
            Console.WriteLine("Create:  {0}", strOU);

            // Create Organizational Unit.
            try
            {
                if (!DirectoryEntry.Exists("LDAP://" + strOU + "," + "OU=" + topParentName + "," + "OU=" + parentName + "," + OrgString))
                {
                    objOU = objADAM.Children.Find("OU=" + topParentName).Children.Find("OU=" + parentName).Children.Add(strOU,
                        "OrganizationalUnit");
                    objOU.Properties["description"].Add(strDescription);
                    objOU.CommitChanges();

                    // Output Organizational Unit attributes.
                    Console.WriteLine("Success: Create succeeded.");
                    Console.WriteLine("Name:    {0}", objOU.Name);
                    Console.WriteLine("         {0}",
                        objOU.Properties["description"].Value);
                    return;
                }

                // Output Organizational Unit attributes.
                Console.WriteLine("Success: OU Already Exists.");
                Console.WriteLine("Name:    {0}", strOU);
                Console.WriteLine("         {0}", strDescription);
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error:   Create failed.");
                Console.WriteLine("         {0}", e.Message);
                return;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADManager
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            //Create OU's
            OUManager OUMgr = new OUManager();
            OUMgr.Initialize("localhost:389/o=Seros,c=US");
            
            OUMgr.AddParentOU("CSAA", "CSAA");
            OUMgr.AddChildOU("Security Groups", "Security Groups", "CSAA");
            OUMgr.AddChildOU("Distribution Lists", "Distribution Lists", "CSAA");

            OUMgr.AddParentOU("Active Directory Services", "Active Directory Services");
            OUMgr.AddChildOU("Application Security Groups", "Application Security Groups", "Active Directory Services");

            OUMgr.AddParentOU("Partner Clubs", "Partner Clubs");
            OUMgr.AddChildOU("ACP", "ACP", "Partner Clubs");
            OUMgr.AddGrandChildOU("AZ", "Arizona", "Partner Clubs", "ACP");
            OUMgr.AddGrandChildOU("CO", "Colorado", "Partner Clubs", "ACP");
            OUMgr.AddGrandChildOU("IN", "Indiana", "Partner Clubs", "ACP");
            OUMgr.AddChildOU("MAIG", "MAIG", "Partner Clubs");
            OUMgr.AddGrandChildOU("MD", "Maryland", "Partner Clubs", "MAIG");
            OUMgr.AddGrandChildOU("NJ", "New Jersey", "Partner Clubs", "MAIG");
            OUMgr.AddGrandChildOU("NY", "New York", "Partner Clubs", "MAIG");

            //Create Security Groups
            GroupManagement.CreateSecurityGroup("localhost:389", "OU=Security Groups,OU=CSAA,o=Seros,c=US", "Service Accounts");
            
            //Setup our LDAP Service Account
            OUMgr.AddParentOU("Users", "Misc Users");
            UserManagement.CreateUserAccount("localhost:389/ou=Users,o=Seros,c=US", "LDAP_svc", "LD4PSvC");
            UserManagement.AddUserToGroup("localhost:389", "cn=LDAP_svc,ou=Users,o=Seros,c=US", "cn=Service Accounts,OU=Security Groups,OU=CSAA");

            //Create Test Users
            UserManagement.CreateUserAccount("localhost:389/ou=AZ,ou=ACP,ou=Partner Clubs,o=Seros,c=US", "MattAZ", "tr!qwSSe4tgewch899");
            UserManagement.CreateUserAccount("localhost:389/ou=CO,ou=ACP,ou=Partner Clubs,o=Seros,c=US", "MattCO", "tr!qwSSe4tgewch899");
            UserManagement.CreateUserAccount("localhost:389/ou=IN,ou=ACP,ou=Partner Clubs,o=Seros,c=US", "MattIN", "tr!qwSSe4tgewch899");
            UserManagement.CreateUserAccount("localhost:389/ou=MD,ou=MAIG,ou=Partner Clubs,o=Seros,c=US", "MattMD", "tr!qwSSe4tgewch899");
            UserManagement.CreateUserAccount("localhost:389/ou=NJ,ou=MAIG,ou=Partner Clubs,o=Seros,c=US", "MattNJ", "tr!qwSSe4tgewch899");
            UserManagement.CreateUserAccount("localhost:389/ou=NY,ou=MAIG,ou=Partner Clubs,o=Seros,c=US", "MattNY", "tr!qwSSe4tgewch899");

            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            Environment.Exit(0);
        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.ToString());
            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            Environment.Exit(1);
        }

    }
}

﻿using System;
using System.DirectoryServices;

namespace ADManager
{
    class GroupManagement
    {
        public static void CreateSecurityGroup(string host, string ouPath, string name)
        {
            if (!DirectoryEntry.Exists("LDAP://" + host + "/CN=" + name + "," + ouPath))
            {
                try
                {
                    DirectoryEntry entry = new DirectoryEntry("LDAP://" + ouPath);
                    DirectoryEntry group = entry.Children.Add("CN=" + name, "group");
                    group.Properties["sAmAccountName"].Value = name;
                    group.CommitChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message.ToString());
                }
            }
            else { Console.WriteLine(ouPath + " already exists"); }
        }
    }
}

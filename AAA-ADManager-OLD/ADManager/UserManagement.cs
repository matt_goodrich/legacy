﻿using System;
using System.DirectoryServices;
using System.Text;

namespace ADManager
{
    class UserManagement
    {
        public static string CreateUserAccount(string ldapPath, string userName, string userPassword)
        {
            string oGUID = string.Empty;
            try
            {
                string connectionPrefix = "LDAP://" + ldapPath;
                DirectoryEntry dirEntry = new DirectoryEntry(connectionPrefix);
                DirectoryEntry newUser = dirEntry.Children.Add("CN=" + userName, "user");
                newUser.Properties["userPrincipalName"].Value = userName;
                //newUser.Properties["unicodePwd"].Value = GetPasswordData(userPassword);
                newUser.Properties["msDS-UserAccountDisabled"].Value = false;
                newUser.CommitChanges();
                oGUID = newUser.Guid.ToString();

                SetUserPassword(ldapPath, userName, userPassword);

                //DirectoryEntry newUserEntry = new DirectoryEntry(newUser.Path);

                //newUserEntry.Invoke("SetPassword", new object[] { userPassword });
                //newUserEntry.CommitChanges();
                dirEntry.Close();
                newUser.Close();
                //newUserEntry.Close();
            }
            catch (System.DirectoryServices.DirectoryServicesCOMException e)
            {
                Console.WriteLine("Error:   Create User failed.");
                Console.WriteLine("         {0}", e.Message);
                return "FAILURE";
            }
            Console.WriteLine("Success: Create User succeeded.");
            Console.WriteLine("Name:    {0}", userName);
            Console.WriteLine("         {0}", oGUID);
            return oGUID;
        }

        public static void SetUserPassword(string ldapPath, string userName, string password)
        {
            const long ADS_OPTION_PASSWORD_PORTNUMBER = 6;
            const long ADS_OPTION_PASSWORD_METHOD = 7;

            const int ADS_PASSWORD_ENCODE_REQUIRE_SSL = 0;
            const int ADS_PASSWORD_ENCODE_CLEAR = 1;

            AuthenticationTypes AuthTypes;  // Authentication flags.
            int intPort;                    // Port for instance.
            DirectoryEntry objUser;         // User object.
            string strPath;                 // Binding path.
            string strPort;                 // Port for instance.
            string strServer;               // DNS name of the computer with
            //   the AD LDS installation.
            string strUser;                 // User DN.

            // Construct the binding string.
            strPort = ldapPath.Substring(ldapPath.IndexOf(':')+1, 3);
            strPath = String.Concat("LDAP://", ldapPath.Substring(0, ldapPath.IndexOf('/')), '/', "CN=", userName, ",", ldapPath.Substring(ldapPath.IndexOf('/')+1));
            Console.WriteLine("Bind to:  {0}", strPath);

            // Set authentication flags.
            // For non-secure connection, use LDAP port and
            //  ADS_USE_SIGNING |
            //  ADS_USE_SEALING |
            //  ADS_SECURE_AUTHENTICATION
            // For secure connection, use SSL port and
            //  ADS_USE_SSL | ADS_SECURE_AUTHENTICATION
            AuthTypes = AuthenticationTypes.Signing |
                AuthenticationTypes.Sealing |
                AuthenticationTypes.Secure;

            // Bind to user object using LDAP port.
            try
            {
                objUser = new DirectoryEntry(
                    strPath, null, null, AuthTypes);
                objUser.RefreshCache();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error:   Bind failed.");
                Console.WriteLine("         {0}.", e.Message);
                return;
            }

            // Set port number, method, and password.
            intPort = Int32.Parse(strPort);
            try
            {
                //  Be aware that, for security, a password should
                //  not be entered in code, but should be obtained
                //  from the user interface.
                objUser.Invoke("SetOption", new object[] { ADS_OPTION_PASSWORD_PORTNUMBER, intPort });
                objUser.Invoke("SetOption", new object[]
                    {ADS_OPTION_PASSWORD_METHOD,
                     ADS_PASSWORD_ENCODE_CLEAR});
                objUser.Invoke("SetPassword", new object[] { password });
            }
            catch (Exception e)
            {
                Console.WriteLine("Error:   Set password failed.");
                Console.WriteLine("         {0}.", e.Message);
                return;
            }

            Console.WriteLine("Success: Password set.");
            return;
        }

        public static void AddUserToGroup(string host, string userDn, string groupDn)
        {
            try
            {
                DirectoryEntry dirEntry = new DirectoryEntry("LDAP://" + host + "/" + groupDn);
                dirEntry.Properties["member"].Add(userDn);
                dirEntry.CommitChanges();
                dirEntry.Close();
            }
            catch (System.DirectoryServices.DirectoryServicesCOMException E)
            {
                Console.WriteLine("Error:   Create failed.");
                Console.WriteLine("         {0}", E.Message);
                return;

            }
        }
    }
}

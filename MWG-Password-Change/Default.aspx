﻿<%@ Page Language="C#" Debug="true" AutoEventWireup="true" CodeFile="Default.aspx.cs"
    Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
    <title>wgPass</title>
<link href="css/wgPass_Login_styles.css" rel="stylesheet" type="text/css" media="screen" />
    <script language='javascript'>
    
    function submitForm()
    {
    		document.getElementById('loginButton').style.display = 'none';
		    document.getElementById('loginButtonWorking').style.display = 'block';
		    document.form1.submit();
    }
    
    function entsub(event)
        {
            event = event || window.event;
            if(event.which == 13) 
            {
                submitForm();
                return false;
            }
            else if (event.keyCode == 13)
            {
                submitForm();
                return false;
            }
            else return true;
        }
    
    </script>

</head>
<body class="thrColEls" onload="javascript:document.getElementById('emailAddress').focus();">
    <!-- container -->
    <div id="container" align="center">
    <%if (successfulChange) {%>
<table cellspacing="0" cellpadding="0" border="0">
  <tr>
    <td colspan="4" style="padding-top:50px; padding-left:50px" align="left">
      <img src="images/mw_sitelogo.jpg"  alt="McCann Worldgroup"/>
    </td>
  </tr>
  <tr>
    <td>
      <img src="images/blank.png" width="200" />
    </td>
    <td>
      <img src="images/blank.png" />
    </td>
    <td style="padding-top:100px;">
      <img src="images/LogoWgPass.png" width="255" height="94" alt="wg PASS" />
    </td>
    <td>
      <img src="images/blank.png" />
    </td>
  </tr>
  <tr>
    <td>
      <img src="images/blank.png" />
    </td>
    <td colspan="3">
      <div id="sidebar1">
      <h3>Congratulations!</h3>
      <p>Your password has been successfully changed.  Please return to the application you were attempting to access</p>
      </div>
   </td>
   </tr>
 </table>
 <% } else {%>
<form id="form1" runat="server"> 
<table cellspacing="0" cellpadding="0" border="0">
  <tr>
    <td colspan="4" style="padding-top:50px; padding-left:50px" align="left">
      <img src="images/mw_sitelogo.jpg"  alt="McCann Worldgroup"/>
    </td>
  </tr>
  <tr>
    <td>
      <img src="images/blank.png" width="200" />
    </td>
    <td>
      <img src="images/blank.png" />
    </td>
    <td style="padding-top:10px;">
      <img src="images/LogoWgPass.png" width="255" height="94" alt="wg PASS" />
    </td>
    <td>
      <img src="images/blank.png" />
    </td>
  </tr>
  <tr>
    <td>
      <img src="images/blank.png" />
    </td>
    <td width="191" valign="top">
      <br/><br/>
      <div id="sidebar1">
        <h3>Your password has expired!</h3>
        <p>Please select a new password using this form:</p>
      </div>
    </td>
    <td width="256" valign="top">
      <div class="login">
      <div class="loginText">Email Address</div>
      <div><input runat="server" class="input" name="EmailAddress" type="text" id="emailAddress" onkeypress="entsub(event);" /></div>
      </div>

      <div class="login">
      <div class="loginText" style="margin-top:20px">Old Password</div>
      <div><input runat="server" class="input" name="OldPassword" type="password" id="oldPassword" onkeypress="entsub(event);" /></div>
      </div>
      
      <div class="login">
      <div class="loginText" style="margin-top:20px">New Password</div>
      <div> <input runat="server" name="NewPassword1" type="password" class="input" id="newPassword1" onkeypress="entsub(event);" /></div>
      </div>
      
      <div class="login">
      <div class="loginText" style="margin-top:20px">Repeat Password</div>
      <div> <input runat="server" name="NewPassword2" type="password" class="input" id="newPassword2" onkeypress="entsub(event);" /></div>
      </div>


      <div class="login">
      <div class="loginText"></div>
      <div align="right"><input name="Login" type="button" id="loginButton" class="loginButton" value="" onclick="javascript:submitForm();" />
                         <input name="Working" style="display:none;" type="button"  id="loginButtonWorking" class="loginButtonWorking" value="" onclick="javascript:submitForm();" /></div>
      </div>

      <div class="login" style="margin-top:10px">
      <div class="contact" align="right"><a href="mailto:Helpdesk@your-accounts.com?subject=Portal Login support issue">Need Help?</a></div>
      </div>
    </td>
    <td width="191" valign="top">
    <div id="sidebar2">
       <br/><br/>
      <table border="0">
      <tr>
        <td valign="top" style="padding-right:5px; padding-top:5px">
         <%if (!successfulChange)
          {%>
          
        <%if (errorMessage != null)
          { %>
        <img src="images/ErrorBullet.png" width="5" height="18" alt="Error" / >
            <%= errorMessage%>
        
        <%} %>
        <% if (showComplexityRequirements)
           { %>
        <div class="error" align="center">
            <div align="left">
                The password you entered doesn't meet IPG security policy. All passwords...
                <ul>
                    <li>Must be at least 8 characters in length</li>
                    <li>Cannot repeat any of the past 12 passwords</li>
                    <li>Cannot contain any part of your name or username.</li>
                    <li>Must contain characters from three of the following five categories:
                        <ul>
                            <li>English uppercase characters (A through Z)</li>
                            <li>English lowercase characters (a through z)</li>
                            <li>Numbers (0 through 9)</li>
                            <li>Non-alphabetic characters(for example, !, $, #, %)</li>
                            <li>Non-English characters</li>
                        </ul>
                    </li>
                </ul>
                <br />
                Please try again...<br />
                If you are unable to create a password successfully, please contact the Helpdesk at help@mccannneuralnetwork.com using Reference Code 16.
            </div>
            
        </div>
        <%} %>
        <%} %>
          </td>
     </tr>
     </table> 
     </div>
    </td>
  </tr>
</table>


</form>   
        
<%} %>
    </div>
    <!-- End contatiner -->
    <%if (privateError != null)
      { %>
    <div style="visibility: hidden">
        <%=privateError %>
    </div>
    <%} %>
</body>
</html>

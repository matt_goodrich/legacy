using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.IO;



public partial class _Default : System.Web.UI.Page
{
    public string errorMessage = null;
    public bool successfulChange = false;
    public bool showComplexityRequirements = false;
    public string privateError = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        errorMessage = null;
        successfulChange = false;
        showComplexityRequirements = false;
        privateError = null;

        if (Request.QueryString["email"] != null) emailAddress.Value = Request.QueryString["email"];

        if (IsPostBack)
        {
            //Validate Email Address Format
            string inputEmail = emailAddress.Value;
            string strRegex = @"^([a-zA-Z0-9_\-\.\']+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (!re.IsMatch(inputEmail))
            {
                errorMessage = "Your email seems to be incorrectly formatted.  Please try again...";
                return;
            }
            if (oldPassword.Value.Length == 0)
            {
                errorMessage = "You must supply your old password...";
                return;
            }

            if (newPassword1.Value.Length == 0)
            {
                errorMessage = "You must supply a new password...";
                return;
            }
            if (newPassword1.Value != newPassword2.Value)
            {
                errorMessage = "The two new passwords you typed do not match.  Please try again...";
                return;
            }
            string v_result = "";
            try
            {
                pwws.Service1 v_pws = new pwws.Service1();
                v_result = v_pws.ChangePassword(emailAddress.Value, oldPassword.Value, newPassword1.Value);
            }
            catch (Exception ex)
            {
                errorMessage = "There was a problem executing your password change.  Please email the helpdesk for further assistance: <a href='mailto:helpdesk@youraccounts.com'>helpdesk@youraccounts.com</a>";
                privateError = ex.Message + "\n" + ex.StackTrace;
            }
            
            switch(v_result)
            {
                case "UnknownUser":
                    errorMessage = "We were unable to find this email address in our records.  Please try again.  If the issue persists, please contact the Helpdesk at help@mccannneuralnetwork.com using Reference Code 14.";
                    return;
                case "BadPassword":
                    errorMessage = "The old password you entered doesn't match our records.  Please try again.  If the issue persists, please contact the Helpdesk at help@mccannneuralnetwork.com using Reference Code 15.";
                    return;
                case "PolicyViolated":
                    showComplexityRequirements = true;
                    return;
                case "Success":
                    successfulChange = true;
                    return;
                default:
                    errorMessage = "There was a problem executing your password change.  Please contact the Helpdesk at help@mccannneuralnetwork.com using Reference Code 17.";
                    privateError = v_result;
                    return;
                    
            }
            
        }
    }

    private string ChangePassword(string email, string oldPassword, string newPassword)
    {
        DirectoryEntry v_searchRoot = new DirectoryEntry(ConfigurationManager.AppSettings["LDAPConnectionString_CORP"], ConfigurationManager.AppSettings["CorpLDAPUser"], ConfigurationManager.AppSettings["CorpLDAPPassword"]);
        DirectorySearcher v_findUser = new DirectorySearcher(v_searchRoot, "(|(mail=" + email + ")(proxyAddresses=smtp:" + email + "))");
        v_findUser.PropertiesToLoad.Add("mail");
        v_findUser.PropertiesToLoad.Add("displayName");
        v_findUser.PropertiesToLoad.Add("samAccountName");
        SearchResult v_result = v_findUser.FindOne();
        if (v_result == null)
        {
            v_searchRoot.Path = ConfigurationManager.AppSettings["LDAPConnectionString_ExternalUsers"];
            v_result = v_findUser.FindOne();
            if (v_result == null) return "UnknownUser";
        }

        PrincipalContext v_pc = new PrincipalContext(ContextType.Domain, "IPGNA", ConfigurationManager.AppSettings["CorpLDAPUser"], ConfigurationManager.AppSettings["CorpLDAPPassword"]);
        UserPrincipal v_u = new UserPrincipal(v_pc);
        v_u.SamAccountName = (string)v_result.Properties["samAccountName"][0];

        PrincipalSearcher v_ps = new PrincipalSearcher(v_u);

        UserPrincipal v_user = (UserPrincipal) v_ps.FindOne();

        int v_error = 0;

        try
        {
            v_user.ChangePassword(oldPassword, newPassword);
            v_user.Save();
        }
        catch (Exception ex)
        {
            v_error = Marshal.GetLastWin32Error();
            if (ex.Message.Contains("The specified network password is not correct."))
            {
                return "BadPassword";
            }
            else if (ex.Message.Contains("The password does not meet the password policy requirements.")) return "PolicyViolated";
            
            else return ("(" + v_error + ")" + ex.Message);
        }
        finally
        {
            v_findUser.Dispose();
            v_searchRoot.Dispose();
            v_user.Dispose();
        }

        return "Success";
        
    }
}

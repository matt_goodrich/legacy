﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;

namespace GetPASUsers
{
    [DirectoryObjectClass("user")]
    [DirectoryRdnPrefix("CN")]

    public class UserPrincipalsEx : UserPrincipal
    {
        public UserPrincipalsEx(PrincipalContext context) : base(context) { }


        [DirectoryProperty("cSAAAgentId")]
        public string cSAAAgentId
        {
            get
            {
                if (ExtensionGet("cSAAAgentId").Length != 1)
                    return null;

                return (string)ExtensionGet("cSAAAgentId")[0];

            }
            set { this.ExtensionSet("cSAAAgentId", value); }
        }

        [DirectoryProperty("cSAAAgentType")]
        public string cSAAAgentType
        {
            get
            {
                if (ExtensionGet("cSAAAgentType").Length != 1)
                    return null;

                return (string)ExtensionGet("cSAAAgentType")[0];

            }
            set { this.ExtensionSet("cSAAAgentType", value); }
        }

        [DirectoryProperty("cSAAMapId2")]
        public string cSAAMapId2
        {
            get
            {
                if (ExtensionGet("cSAAMapId2").Length != 1)
                    return null;

                return (string)ExtensionGet("cSAAMapId2")[0];

            }
            set { this.ExtensionSet("cSAAMapId2", value); }
        }

        [DirectoryProperty("mobile")]
        public string mobile
        {
            get
            {
                if (ExtensionGet("mobile").Length != 1)
                    return null;

                return (string)ExtensionGet("mobile")[0];

            }
            set { this.ExtensionSet("mobile", value); }
        }

        [DirectoryProperty("facsimileTelephoneNumber")]
        public string facsimileTelephoneNumber
        {
            get
            {
                if (ExtensionGet("facsimileTelephoneNumber").Length != 1)
                    return null;

                return (string)ExtensionGet("facsimileTelephoneNumber")[0];

            }
            set { this.ExtensionSet("facsimileTelephoneNumber", value); }
        }

        [DirectoryProperty("streetAddress")]
        public string streetAddress
        {
            get
            {
                if (ExtensionGet("streetAddress").Length != 1)
                    return null;

                return (string)ExtensionGet("streetAddress")[0];

            }
            set { this.ExtensionSet("streetAddress", value); }
        }

        [DirectoryProperty("l")]
        public string l
        {
            get
            {
                if (ExtensionGet("l").Length != 1)
                    return null;

                return (string)ExtensionGet("l")[0];

            }
            set { this.ExtensionSet("l", value); }
        }

        [DirectoryProperty("st")]
        public string st
        {
            get
            {
                if (ExtensionGet("st").Length != 1)
                    return null;

                return (string)ExtensionGet("st")[0];

            }
            set { this.ExtensionSet("st", value); }
        }

        [DirectoryProperty("postalCode")]
        public string postalCode
        {
            get
            {
                if (ExtensionGet("postalCode").Length != 1)
                    return null;

                return (string)ExtensionGet("postalCode")[0];

            }
            set { this.ExtensionSet("postalCode", value); }
        }

        [DirectoryProperty("employeeNumber")]
        public string employeeNumber
        {
            get
            {
                if (ExtensionGet("employeeNumber").Length != 1)
                    return null;

                return (string)ExtensionGet("employeeNumber")[0];

            }
            set { this.ExtensionSet("employeeNumber", value); }
        }
    }
}

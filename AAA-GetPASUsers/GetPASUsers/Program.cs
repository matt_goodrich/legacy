﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Text;

namespace GetPASUsers
{
    class Program
    {
        static void Main(string[] args)
        {
            GetAllMembers();
            Console.WriteLine("END");
            Console.ReadLine();
        }

        public static void GetAllMembers()
        {
            List<string> users = new List<string>();
            users.Add("g1apiet");
            users.Add("g1ephan");
            users.Add("g1qdura");
            users.Add("g26raki");
            users.Add("g2ychun");
            users.Add("g6klofo");
            users.Add("g7fwalk");
            users.Add("gc3vira");
            users.Add("gd3holl");
            users.Add("ggatuma");
            users.Add("ggnrent");
            users.Add("gj3math");
            users.Add("gk1barc");
            users.Add("gk6harr");
            users.Add("gkecana");
            users.Add("gkjwebb");
            users.Add("gkkalva");
            users.Add("gltgofo");
            users.Add("gn7lile");
            users.Add("gnrmoy");
            users.Add("gqolivi");
            users.Add("gt7zsag");
            users.Add("gx6stan");
            users.Add("gkikest");
            users.Add("gcqevan");

            using (StreamWriter file = new StreamWriter(@"C:\PASUsers.csv"))
            {
                Console.WriteLine("UserID, RiskState, R1, R2, R5, R7, R8, R9, R11, R12, R14, R15, R16, R20, R21, R22, R23, R24, R25, R26, R28, R29, R30, R31, R32, R33, R34, R35, R36, R37, R38, R39, R40, R41");
                file.WriteLine("UserID, RiskState, R1, R2, R5, R7, R8, R9, R11, R12, R14, R15, R16, R20, R21, R22, R23, R24, R25, R26, R28, R29, R30, R31, R32, R33, R34, R35, R36, R37, R38, R39, R40, R41");
                using (var domainContext = new PrincipalContext(ContextType.Domain, "ENT"))
                {
                    foreach(string incuser in users)
                    {
                        using (var user = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, incuser))
                        {
                            var groupR1 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R1");
                            var groupR2 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R2");
                            var groupR5 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R5");
                            var groupR7 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R7");
                            var groupR8 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R8");
                            var groupR9 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R9");
                            var groupR11 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R11");
                            var groupR12 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R12");
                            var groupR14 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R14");
                            var groupR15 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R15");
                            var groupR16 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R16");
                            var groupR20 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R20");
                            var groupR21 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R21");
                            var groupR22 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R22");
                            var groupR23 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R23");
                            var groupR24 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R24");
                            var groupR25 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R25");
                            var groupR26 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R26");
                            var groupR28 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R28");
                            var groupR29 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R29");
                            var groupR30 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R30");
                            var groupR31 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R31");
                            var groupR32 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R32");
                            var groupR33 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R33");
                            var groupR34 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R34");
                            var groupR35 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R35");
                            var groupR36 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R36");
                            var groupR37 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R37");
                            var groupR38 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R38");
                            var groupR39 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R39");
                            var groupR40 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R40");
                            var groupR41 = GroupPrincipal.FindByIdentity(domainContext, "GrpMemberPoint_R41");

                            Console.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33}",
                                incuser,
                                GetProperty(user, "cSAARiskState"),
                                user.IsMemberOf(groupR1),
                                user.IsMemberOf(groupR2),
                                user.IsMemberOf(groupR5),
                                user.IsMemberOf(groupR7),
                                user.IsMemberOf(groupR8),
                                user.IsMemberOf(groupR9),
                                user.IsMemberOf(groupR11),
                                user.IsMemberOf(groupR12),
                                user.IsMemberOf(groupR14),
                                user.IsMemberOf(groupR15),
                                user.IsMemberOf(groupR16),
                                user.IsMemberOf(groupR20),
                                user.IsMemberOf(groupR21),
                                user.IsMemberOf(groupR22),
                                user.IsMemberOf(groupR23),
                                user.IsMemberOf(groupR24),
                                user.IsMemberOf(groupR25),
                                user.IsMemberOf(groupR26),
                                user.IsMemberOf(groupR28),
                                user.IsMemberOf(groupR29),
                                user.IsMemberOf(groupR30),
                                user.IsMemberOf(groupR31),
                                user.IsMemberOf(groupR32),
                                user.IsMemberOf(groupR33),
                                user.IsMemberOf(groupR34),
                                user.IsMemberOf(groupR35),
                                user.IsMemberOf(groupR36),
                                user.IsMemberOf(groupR37),
                                user.IsMemberOf(groupR38),
                                user.IsMemberOf(groupR39),
                                user.IsMemberOf(groupR40),
                                user.IsMemberOf(groupR41)));
                            file.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33}", 
                                incuser,
                                GetProperty(user, "cSAARiskState"),
                                user.IsMemberOf(groupR1), 
                                user.IsMemberOf(groupR2), 
                                user.IsMemberOf(groupR5), 
                                user.IsMemberOf(groupR7), 
                                user.IsMemberOf(groupR8), 
                                user.IsMemberOf(groupR9), 
                                user.IsMemberOf(groupR11), 
                                user.IsMemberOf(groupR12), 
                                user.IsMemberOf(groupR14), 
                                user.IsMemberOf(groupR15), 
                                user.IsMemberOf(groupR16), 
                                user.IsMemberOf(groupR20), 
                                user.IsMemberOf(groupR21), 
                                user.IsMemberOf(groupR22), 
                                user.IsMemberOf(groupR23), 
                                user.IsMemberOf(groupR24), 
                                user.IsMemberOf(groupR25), 
                                user.IsMemberOf(groupR26), 
                                user.IsMemberOf(groupR28), 
                                user.IsMemberOf(groupR29), 
                                user.IsMemberOf(groupR30), 
                                user.IsMemberOf(groupR31), 
                                user.IsMemberOf(groupR32), 
                                user.IsMemberOf(groupR33), 
                                user.IsMemberOf(groupR34), 
                                user.IsMemberOf(groupR35), 
                                user.IsMemberOf(groupR36), 
                                user.IsMemberOf(groupR37), 
                                user.IsMemberOf(groupR38), 
                                user.IsMemberOf(groupR39), 
                                user.IsMemberOf(groupR40), 
                                user.IsMemberOf(groupR41)));
                        }
                    }
                }
            }
            //var groupPrincipal = GroupPrincipal.FindByIdentity(domainContext, IdentityType.Name, group);
            
        }

        public static String GetProperty(Principal principal, String property)
        {
            try
            {
                DirectoryEntry directoryEntry = principal.GetUnderlyingObject() as DirectoryEntry;
                if (directoryEntry.Properties.Contains(property))
                    return directoryEntry.Properties[property].Value.ToString();
                else
                    return String.Empty;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return String.Empty;
            }
        }
    }
}

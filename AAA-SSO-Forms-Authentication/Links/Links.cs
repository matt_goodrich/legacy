﻿/*******************************************************************************
* Name
*   Login Links
*
* Identifier
*   Filename: Links.cs
*   
* Purpose
*   Class to retrieve links used in the Login Application that are stored as
*   configuration items.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Utilities;

#endregion

namespace LoginUtils
{
    /// <summary>
    /// Class to retrieve links used in the Login Application that are stored as configuration items.
    /// </summary>
    public class Links
    {
        //Get the path of the config file in the calling application.
        //It is assumed the config file is in the App_Data/Links directory and is named Links.xml
        protected string ConfigFile = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\Links\\Links.xml");

        //Create a new XmlDocument
        protected XmlDocument doc = new XmlDocument();

        /// <summary>
        /// Initializes a new instance of the <see cref="Links"/> class.
        /// </summary>
        public Links()
        {
            doc.PreserveWhitespace = true;
            doc.Load(ConfigFile);
        }

        #region Getters/Setters

        /// <summary>
        /// Gets or sets the privacy policy URL.
        /// </summary>
        public string PrivacyPolicy
        {
            get { return XMLHelper.GetString(doc, "PrivacyPolicy"); }
            set { XMLHelper.SetString(doc, ConfigFile, "PrivacyPolicy", value); }
        }

        /// <summary>
        /// Gets or sets the terms of service URL.
        /// </summary>
        public string TermsOfService
        {
            get { return XMLHelper.GetString(doc, "TermsOfService"); }
            set { XMLHelper.SetString(doc, ConfigFile, "TermsOfService", value); }
        }

        /// <summary>
        /// Gets or sets the External Password Reset URL.
        /// </summary>
        public string PasswordResetExternal
        {
            get { return XMLHelper.GetString(doc, "PasswordReset-External"); }
            set { XMLHelper.SetString(doc, ConfigFile, "PasswordReset-External", value); }
        }

        /// <summary>
        /// Gets or sets the Internal Password Reset URL.
        /// </summary>
        public string PasswordResetInternal
        {
            get { return XMLHelper.GetString(doc, "PasswordReset-Internal"); }
            set { XMLHelper.SetString(doc, ConfigFile, "PasswordReset-Internal", value); }
        }

        #endregion
    }
}

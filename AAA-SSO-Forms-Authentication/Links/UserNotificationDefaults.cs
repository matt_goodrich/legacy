﻿/*******************************************************************************
* Name
*   Login Links
*
* Identifier
*   Filename: Links.cs
*   
* Purpose
*   Class to retrieve parameters related to notification of the user used in the 
*   Login Application that are stored as configuration items.
*
******************************************************************************
*/
#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Utilities;

#endregion

namespace LoginUtils
{
    /// <summary>
    /// Class to retrieve parameters related to notification of the user used in the 
    /// Login Application that are stored as configuration items.
    /// </summary>
    public class UserNotificationDefaults
    {
        //Get the path of the config file in the calling application.
        //It is assumed the config file is in the App_Data/SystemDefaults directory and is named UserNotificationDefaults.xml
        protected string ConfigFile = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\SystemDefaults\\UserNotificationDefaults.xml");

        //Create a new XmlDocument
        protected XmlDocument doc = new XmlDocument();

        /// <summary>
        /// Initializes a new instance of the <see cref="UserNotificationDefaults"/> class.
        /// </summary>
        public UserNotificationDefaults()
        {
            doc.PreserveWhitespace = true;
            doc.Load(ConfigFile);
        }

        #region Getters/Setters

        /// <summary>
        /// Gets or sets the Password Expiration Reminder value
        /// </summary>
        public int PasswordExpirationReminder
        {
            get { return Convert.ToInt32(XMLHelper.GetString(doc, "PasswordExpirationReminder")); }
            set { XMLHelper.SetString(doc, ConfigFile, "PasswordExpirationReminder", value.ToString()); }
        }

        #endregion
    }
}

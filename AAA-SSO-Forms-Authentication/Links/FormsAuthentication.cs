﻿/*******************************************************************************
* Name
*   Forms Authentication
*
* Identifier
*   Filename: FormsAuthentication.cs
*   
* Purpose
*   Class used to retrieve the timeout value for Forms Authentication from
*   the web.config file
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;
    using log4net;

#endregion

namespace LoginUtils
{
    /// <summary>
    /// Class used to retrieve the timeout value for Forms Authentication from the web.config file
    /// </summary>
    public class FormsAuth
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        /// <summary>
        /// Gets the timeout from the web.config file.
        /// </summary>
        /// <returns>An integer that is the timeout in minutes for the Forms Authentication</returns>
        public static int GetTimeout()
        {
            ServerLog.Info("GetTimeout called");
            XmlDocument doc = new XmlDocument();
            doc.Load(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "web.config"));
            XmlNode node = doc.SelectSingleNode("/configuration/system.web/authentication/forms");
            ServerLog.DebugFormat("Returning timeout: \"{0}\"", node.Attributes["timeout"].Value);
            return int.Parse(node.Attributes["timeout"].Value);
        }
    }
}

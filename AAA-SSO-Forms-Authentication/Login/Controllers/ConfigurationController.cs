﻿/*******************************************************************************
* Name
*   Configuration Controller
*
* Identifier
*   Filename: ConfigurationController.cs
*   
* Purpose
*   This class is the controller class that provides and process data for 
*   the Configuration Views.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Principal;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using System.Xml;
    using LoginApp.ActionFilters;
    using LoginApp.ViewModels;
    using ADIntegration;
    using PingIntegration;
    using LoginUtils;
    using Utilities;
    using log4net;

#endregion

namespace LoginApp.Controllers
{
    /// <summary>
    /// This class is the controller class that provides and process data for 
    /// the Configuration Views.
    /// </summary>
    [SEOCanonicalize]
    public class ConfigurationController : BaseController
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        AuthStrings AS;
        ADDefaults ADD;
        LDAPConfiguration ADLDAPConfig;
        LDAPConfiguration ADLDSLDAPConfig;

        LoginUtils.Links LoginLinks;
        UserNotificationDefaults UND;

        PingIntegration.Links PingLinks;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationController"/> class.
        /// </summary>
        public ConfigurationController()
        {
            ServerLog.Info("Configuration Controller Constructor Called");

            AS = new AuthStrings();
            ADD = new ADDefaults();
            ADLDAPConfig = new LDAPConfiguration("ActiveDirectory");
            ADLDSLDAPConfig = new LDAPConfiguration("ADLDS");
            LoginLinks = new LoginUtils.Links();
            UND = new UserNotificationDefaults();
            PingLinks = new PingIntegration.Links();
        }

        /// <summary>
        /// Controller for the Index View.
        /// </summary>
        /// <returns>The Index View</returns>
        [Authorize(Roles = "GrpSSO_Admin")]
        public ActionResult Index()
        {
            ServerLog.Info("Configuration Controller Index Called");
            
            //Create and load data into the ViewModel
            
            var viewModel = new ConfigurationIndexViewModel();

            viewModel.PingConfigForm = new PingConfigForm {
                PingHost = PingLinks.PFHost,
                TokenType = PingLinks.TokenType,
                UseSSL = PingLinks.UseSSL,
                HTTPSPort = PingLinks.HTTPSPort
            };

            WindowsIdentity newId = WindowsIdentity.GetCurrent();
            WindowsImpersonationContext impersonatedUser = newId.Impersonate();

            viewModel.ADLDAPModuleForm = new ADLDAPModuleForm
            {
                LDAPForm = new LDAPModuleForm
                {
                    ID = "ActiveDirectory",
                    ConnectionString = ADLDAPConfig.ConnectionString,
                    HasDomainPolicy = ADLDAPConfig.HasDomainPolicy,
                    Path = ADLDAPConfig.Path,
                    Impersonate = ADLDAPConfig.Impersonate,
                    ImpersonateUser = newId.Name,
                    UseSSL = ADLDAPConfig.UseSSL,
                    LoginAttribute = ADLDAPConfig.LoginAttribute
                }
            };

            viewModel.ADLDSLDAPModuleForm = new ADLDSLDAPModuleForm
            {
                LDAPForm = new LDAPModuleForm
                {
                    ID = "ADLDS",
                    ConnectionString = ADLDSLDAPConfig.ConnectionString,
                    HasDomainPolicy = ADLDSLDAPConfig.HasDomainPolicy,
                    Impersonate = ADLDSLDAPConfig.Impersonate,
                    ImpersonateUser = newId.Name,
                    UseSSL = ADLDSLDAPConfig.UseSSL,
                    LoginAttribute = ADLDSLDAPConfig.LoginAttribute
                }
            };

            viewModel.AuthStringsForm = new AuthStringsForm {
                NoResource = AS.NoResource,
                NoUsername = AS.NoUsername,
                NoPassword = AS.NoPassword,
                AccountExpired = AS.AccountExpired,
                PasswordExpired = AS.PasswordExpired,
                PasswordExpiresToday = AS.PasswordExpiresToday,
                PasswordExpiresSoon = AS.PasswordExpiresSoon,
                AccountDisabled = AS.AccountDisabled,
                AccountLocked = AS.AccountLocked,
                InvalidUsername = AS.InvalidUsername,
                InvalidPassword = AS.InvalidPassword,
            };

            viewModel.ADDefaultsForm = new ADDefaultsForm {
                AccountExpiration = ADD.AccountExpiration,
                PasswordExpiration = ADD.PasswordExpiration
            };

            viewModel.UserNotificationsDefaultsForm = new UserNotificationsDefaultsForm {
                PasswordExpirationReminder = UND.PasswordExpirationReminder
            };

            viewModel.LinksForm = new LinksForm {
                PrivacyPolicyURL = LoginLinks.PrivacyPolicy,
                TermsOfServiceURL = LoginLinks.TermsOfService,
                PasswordResetURLExternal = LoginLinks.PasswordResetExternal,
                PasswordResetURLInternal = LoginLinks.PasswordResetInternal
            };

            return View(viewModel);
        }

        /// <summary>
        /// Controller that consumes the POST from the Index View
        /// </summary>
        /// <param name="ViewModel">The populated model</param>
        /// <returns>The Index View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        [Authorize(Roles = "GrpSSO_Admin")]
        public ActionResult Index(ConfigurationIndexViewModel ViewModel, HttpPostedFileBase AgentConfig)
        {
            ServerLog.Info("Configuration Controller Index has been POSTed to");
            //Regex for a positive non-decimal number
            string strRegex = "^\\+?\\d+$";

            //Create our new regex object using the regular expression just defined for email addresses
            Regex re = new Regex(strRegex);

            //Validate the PingFederate Configuration
            if (ViewModel.PingConfigForm.PingHost == null)
            {
                ModelState.AddModelError("PingHost", "No PingFederate host specified");
                ServerLog.Debug("Adding Error to Model: No PingFederate host specified");
            }
            if (!re.IsMatch(ViewModel.PingConfigForm.HTTPSPort.ToString()))
            {
                ModelState.AddModelError("HTTPSPort", "No HTTPS port specified");
                ServerLog.Debug("Adding Error to Model: No HTTPS port specified");
            }

            //Validate the Active Directory Configuration
            if (ViewModel.ADLDAPModuleForm.LDAPForm.ConnectionString == null)
            {
                ModelState.AddModelError("ConnectionString", "No Active Directory Connection String specified");
                ServerLog.Debug("Adding Error to Model: No Active Directory Connection String specified");
            }
            if (!ViewModel.ADLDAPModuleForm.LDAPForm.ConnectionString.Contains("LDAP://"))
            {
                ModelState.AddModelError("ConnectionString", "Invalid Active Directory Connection String specified");
                ServerLog.Debug("Adding Error to Model: Invalid Active Directory Connection String specified");
            }
            if (ViewModel.ADLDAPModuleForm.LDAPForm.Path == null)
            {
                ModelState.AddModelError("Path", "No Active Directory Path specified");
                ServerLog.Debug("Adding Error to Model: No Active Directory Path specified");
            }

            //Validate the ADLDS Configuration
            if (ViewModel.ADLDSLDAPModuleForm.LDAPForm.ConnectionString == null)
            {
                ModelState.AddModelError("ConnectionString", "No AD-LDS Connection String specified");
                ServerLog.Debug("Adding Error to Model: No AD-LDS Connection String specified");
            }
            if (!ViewModel.ADLDSLDAPModuleForm.LDAPForm.ConnectionString.Contains("LDAP://"))
            {
                ModelState.AddModelError("ConnectionString", "Invalid AD-LDS Connection String specified");
                ServerLog.Debug("Adding Error to Model: Invalid AD-LDS Connection String specified");
            }

            //Validate the Authentication Error Messages Configuration
            if (ViewModel.AuthStringsForm.NoResource == null)
            {
                ModelState.AddModelError("NoResource", "No No Resource string specified");
                ServerLog.Debug("Adding Error to Model: No No Resource string specified");
            }
            if (ViewModel.AuthStringsForm.NoUsername == null)
            {
                ModelState.AddModelError("NoUsername", "No No Username string specified");
                ServerLog.Debug("Adding Error to Model: No No Username string specified");
            }
            if (ViewModel.AuthStringsForm.NoPassword == null)
            {
                ModelState.AddModelError("NoPassword", "No No Password string specified");
                ServerLog.Debug("Adding Error to Model: No No Password string specified");
            }
            if (ViewModel.AuthStringsForm.AccountExpired == null)
            {
                ModelState.AddModelError("AccountExpired", "No Account Expired string specified");
                ServerLog.Debug("Adding Error to Model: No Account Expired string specified");
            }
            if (ViewModel.AuthStringsForm.PasswordExpired == null)
            {
                ModelState.AddModelError("PasswordExpired", "No Password Expired string specified");
                ServerLog.Debug("Adding Error to Model: No Password Expired string specified");
            }
            if (ViewModel.AuthStringsForm.PasswordExpiresToday == null)
            {
                ModelState.AddModelError("PasswordExpiresToday", "No Password Expires Today string specified");
                ServerLog.Debug("Adding Error to Model: No Password Expires Today string specified");
            }
            if (ViewModel.AuthStringsForm.PasswordExpiresSoon == null)
            {
                ModelState.AddModelError("PasswordExpiresSoon", "No Password Expires Soon string specified");
                ServerLog.Debug("Adding Error to Model: No Password Expires Soon string specified");
            }
            if (!ViewModel.AuthStringsForm.PasswordExpiresSoon.Contains("{0}"))
            {
                ModelState.AddModelError("PasswordExpiresSoon", "Invalid Password Expires Soon string specified");
                ServerLog.Debug("Adding Error to Model: Invalid Password Expires Soon string specified");
            }
            if (ViewModel.AuthStringsForm.AccountDisabled == null)
            {
                ModelState.AddModelError("AccountDisabled", "No Account Disabled string specified");
                ServerLog.Debug("Adding Error to Model: No Account Disabled string specified");
            }
            if (ViewModel.AuthStringsForm.AccountLocked == null)
            {
                ModelState.AddModelError("AccountLocked", "No Account Locked string specified");
                ServerLog.Debug("Adding Error to Model: No Account Locked string specified");
            }
            if (ViewModel.AuthStringsForm.InvalidUsername == null)
            {
                ModelState.AddModelError("InvalidUsername", "No Invalid Username string specified");
                ServerLog.Debug("Adding Error to Model: No Invalid Username string specified");
            }
            if (ViewModel.AuthStringsForm.InvalidPassword == null)
            {
                ModelState.AddModelError("InvalidPassword", "No Invalid Password string specified");
                ServerLog.Debug("Adding Error to Model: No Invalid Password string specified");
            }

            //Validate the LDAP Policy Defaults Configuration
            if (!re.IsMatch(ViewModel.ADDefaultsForm.AccountExpiration.ToString()))
            {
                ModelState.AddModelError("AccountExpiration", "Invalid Default Account Expiration specified");
                ServerLog.Debug("Adding Error to Model: Invalid Default Account Expiration specified");
            }
            if (!re.IsMatch(ViewModel.ADDefaultsForm.PasswordExpiration.ToString()))
            {
                ModelState.AddModelError("PasswordExpiration", "Invalid Default Password Expiration specified");
                ServerLog.Debug("Adding Error to Model: Invalid Default Password Expiration specified");
            }

            //Validate the User Notification Defaults Configuration
            if (!re.IsMatch(ViewModel.UserNotificationsDefaultsForm.PasswordExpirationReminder.ToString()))
            {
                ModelState.AddModelError("PasswordExpirationReminder", "Invalid Password Expiration Reminder specified");
                ServerLog.Debug("Adding Error to Model: Invalid Password Expiration Reminder specified");
            }

            //Valid the Links Configuration
            if (ViewModel.LinksForm.PasswordResetURLInternal == null)
            {
                ModelState.AddModelError("PasswordResetURLInternal", "No Internal Password Reset URL specified");
                ServerLog.Debug("Adding Error to Model: No Internal Password Reset URL specified");
            }
            if (ViewModel.LinksForm.PasswordResetURLExternal == null)
            {
                ModelState.AddModelError("PasswordResetURLExternal", "No External Password Reset URL specified");
                ServerLog.Debug("Adding Error to Model: No External Password Reset URL specified");
            }
            if (ViewModel.LinksForm.PrivacyPolicyURL == null)
            {
                ModelState.AddModelError("PrivacyPolicyURL", "No Privacy Policy URL specified");
                ServerLog.Debug("Adding Error to Model: No Privacy Policy URL specified");
            }
            if (ViewModel.LinksForm.TermsOfServiceURL == null)
            {
                ModelState.AddModelError("TermsOfServiceURL", "No Terms Of Service URL specified");
                ServerLog.Debug("Adding Error to Model: No Terms Of Service URL specified");
            }

            if (ModelState.IsValid)
            {
                //Check for agent-config
                if (AgentConfig != null)
                {
                    if (AgentConfig.ContentLength > 0)
                    {
                        ServerLog.Debug("Found Agent-Config in POST");
                        try
                        {
                            string FilePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\PingFederate\\agent-config.txt");
                            AgentConfig.SaveAs(FilePath);
                            ServerLog.Debug("Agent-Config file saved");
                        }
                        catch (Exception ex)
                        {
                            ModelState.AddModelError("AgentConfig", "Error while uploading Agent-Config file");
                            ServerLog.Error("Exception caught while saving agent-config.txt", ex);
                        }
                    }
                }
            }

            if (ModelState.IsValid)
            {
                //Set PingFederate
                PingLinks.PFHost = ViewModel.PingConfigForm.PingHost;
                PingLinks.HTTPSPort = ViewModel.PingConfigForm.HTTPSPort;
                PingLinks.WriteConfig();

                //Set Directory Access
                ADLDAPConfig.ConnectionString = ViewModel.ADLDAPModuleForm.LDAPForm.ConnectionString;
                ADLDAPConfig.Path = ViewModel.ADLDAPModuleForm.LDAPForm.Path;
                ADLDSLDAPConfig.ConnectionString = ViewModel.ADLDSLDAPModuleForm.LDAPForm.ConnectionString;

                //Set Authentication Error Messages
                AS.NoResource = ViewModel.AuthStringsForm.NoResource;
                AS.NoUsername = ViewModel.AuthStringsForm.NoUsername;
                AS.NoPassword = ViewModel.AuthStringsForm.NoPassword;
                AS.AccountExpired = ViewModel.AuthStringsForm.AccountExpired;
                AS.PasswordExpired = ViewModel.AuthStringsForm.PasswordExpired;
                AS.PasswordExpiresToday = ViewModel.AuthStringsForm.PasswordExpiresToday;
                AS.PasswordExpiresSoon = ViewModel.AuthStringsForm.PasswordExpiresSoon;
                AS.AccountDisabled = ViewModel.AuthStringsForm.AccountDisabled;
                AS.AccountLocked = ViewModel.AuthStringsForm.AccountLocked;
                AS.InvalidUsername = ViewModel.AuthStringsForm.InvalidUsername;
                AS.InvalidPassword = ViewModel.AuthStringsForm.InvalidPassword;

                //Set LDAP Policy Defaults
                ADD.AccountExpiration = ViewModel.ADDefaultsForm.AccountExpiration;
                ADD.PasswordExpiration = ViewModel.ADDefaultsForm.PasswordExpiration;

                //Set User Notification Defaults
                UND.PasswordExpirationReminder = ViewModel.UserNotificationsDefaultsForm.PasswordExpirationReminder;

                //Set Links
                LoginLinks.PasswordResetInternal = ViewModel.LinksForm.PasswordResetURLInternal;
                LoginLinks.PasswordResetExternal = ViewModel.LinksForm.PasswordResetURLExternal;
                LoginLinks.PrivacyPolicy = ViewModel.LinksForm.PrivacyPolicyURL;
                LoginLinks.TermsOfService = ViewModel.LinksForm.TermsOfServiceURL;
            }

            //Repopulate the read-only fields
            
            WindowsIdentity newId = WindowsIdentity.GetCurrent();
            WindowsImpersonationContext impersonatedUser = newId.Impersonate();

            ViewModel.PingConfigForm.TokenType = PingLinks.TokenType;
            ViewModel.PingConfigForm.UseSSL = PingLinks.UseSSL;

            ViewModel.ADLDAPModuleForm.LDAPForm.ID = "ActiveDirectory";
            ViewModel.ADLDAPModuleForm.LDAPForm.HasDomainPolicy = ADLDAPConfig.HasDomainPolicy;
            ViewModel.ADLDAPModuleForm.LDAPForm.Impersonate = ADLDAPConfig.Impersonate;
            ViewModel.ADLDAPModuleForm.LDAPForm.ImpersonateUser = newId.Name;
            ViewModel.ADLDAPModuleForm.LDAPForm.UseSSL = ADLDAPConfig.UseSSL;
            ViewModel.ADLDAPModuleForm.LDAPForm.LoginAttribute = ADLDAPConfig.LoginAttribute;

            ViewModel.ADLDSLDAPModuleForm.LDAPForm.ID = "ADLDS";
            ViewModel.ADLDSLDAPModuleForm.LDAPForm.HasDomainPolicy = ADLDSLDAPConfig.HasDomainPolicy;
            ViewModel.ADLDSLDAPModuleForm.LDAPForm.Impersonate = ADLDSLDAPConfig.Impersonate;
            ViewModel.ADLDSLDAPModuleForm.LDAPForm.ImpersonateUser = newId.Name;
            ViewModel.ADLDSLDAPModuleForm.LDAPForm.UseSSL = ADLDSLDAPConfig.UseSSL;
            ViewModel.ADLDSLDAPModuleForm.LDAPForm.LoginAttribute = ADLDSLDAPConfig.LoginAttribute;
            
            return View(ViewModel);
        }
    }
}

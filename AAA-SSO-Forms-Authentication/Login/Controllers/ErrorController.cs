﻿/*******************************************************************************
* Name
*   Error Controller
*
* Identifier
*   Filename: ErrorController.cs
*   
* Purpose
*   This controller returns the Error View when any error is encountered within
*   the application.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using log4net;

#endregion

namespace LoginApp.Controllers
{
    /// <summary>
    /// This controller returns the Error View when any error is encountered within the application.
    /// </summary>
    public class ErrorController : BaseController
    {
        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        /// <summary>
        /// Controller for the HttpError View. This View is returned when the application
        /// encounters any error but a 404 status code.
        /// </summary>
        /// <returns></returns>
        public ActionResult HttpError()
        {
            Exception ex = null;

            //Pull the exception out the the session
            try
            {
                ex = (Exception)HttpContext.Application[Request.UserHostAddress.ToString()];
            }
            catch (Exception e) { }

            if (ex != null)
            {
                //Log the exception
                ServerLog.Error("Exception caught in application", ex);
            }

            return View("Error");
        }

        /// <summary>
        /// Controller for the Http404 View. This View is returned when the application returns
        /// a 404 status code.
        /// </summary>
        /// <returns></returns>
        public ActionResult Http404()
        {
            return View("Error");
        }

    }
}

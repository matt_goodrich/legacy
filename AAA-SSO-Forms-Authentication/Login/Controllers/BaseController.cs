﻿/*******************************************************************************
* Name
*   Base Controller
*
* Identifier
*   Filename: BaseController.cs
*   
* Purpose
*   This controller is created to pass configuration data to the masterpage
*   so that each controller does not have to explicityly pass this data.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Web;
    using System.Web.Mvc;
    using System.Xml;
    using LoginUtils;
    using Utilities;
    using log4net;

#endregion

namespace LoginApp.Controllers
{
    /// <summary>
    /// This controller is created to pass configuration data to the masterpage 
    /// so that each controller does not have to explicityly pass this data.
    /// </summary>
    public class BaseController : Controller
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        Links Links;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseController"/> class.
        /// </summary>
        public BaseController()
        {
            ServerLog.Info("Base Controller Constructor Called");

            Links = new Links();

            ViewData["PrivacyPolicy"] = Links.PrivacyPolicy;
            ViewData["TermsOfService"] = Links.TermsOfService;

            ServerLog.InfoFormat("Retrieved Privacy Policy and Terms Of Service URLs: \"{0}\", \"{1}\"", Links.PrivacyPolicy, Links.TermsOfService);
        }
    }
}

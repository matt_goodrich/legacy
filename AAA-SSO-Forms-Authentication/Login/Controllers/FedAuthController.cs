﻿/*******************************************************************************
* Name
*   FedAuth Controller
*
* Identifier
*   Filename: FedAuthController.cs
*   
* Purpose
*   This controller is used to send a request and recieve an OpenToken from 
*   the Service Provider capability of PingFederate. Once an OpenToken has
*   been recieved, a FormsAuthenticationTicket is created and an OpenToken
*   is returned to the IdP capability of PingFederate. Should a user have an
*   existing session, an OpenToken is returned without querying the SP 
*   capability of PingFederate.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using ADIntegration;
    using LoginApp.ActionFilters;
    using LoginApp.ViewModels;
    using LoginUtils;
    using PingIntegration;
    using log4net;

#endregion

namespace LoginApp.Controllers
{
    /// <summary>
    /// This controller is used to send a request and receive an OpenToken from 
    /// the Service Provider capability of PingFederate. Once an OpenToken has
    /// been received, a FormsAuthenticationTicket is created and an OpenToken
    /// is returned to the IdP capability of PingFederate. Should a user have an
    /// existing session, an OpenToken is returned without querying the SP 
    /// capability of PingFederate.
    /// </summary>
    [SEOCanonicalize]
    public class FedAuthController : BaseController
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        OpenTokenIntegration OTI;

        AuthStrings AS;

        public FedAuthController()
        {
            OTI = new OpenTokenIntegration();
            AS = new AuthStrings();
        }

        /// <summary>
        /// Controller for the Index View. This controller determines if a 
        /// Forms Auth session already exists. If no session exists an OpenToken
        /// is requested from the SP side of the SSO server. If a session already
        /// exists, the requested OpenToken from the IdP side of the SSO server is 
        /// returned.
        /// </summary>
        /// <param name="PartnerIdpId">The partner idp id.</param>
        /// <returns></returns>
        public ActionResult Index()
        {
            ServerLog.Info("FedAuth Controller Index Called");

            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                ServerLog.Debug("Existing session found");

                string RedirectURL = "";
                FormsAuthenticationTicket FormsAuthTicket = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                //Create an OpenToken and add the subject to it
                OTI.AddDataToOpenToken(Constants.Subject, FormsAuthTicket.Name);

                ServerLog.DebugFormat("Decrypting Forms Authentication Ticket. Adding subject to OpenToken with value: {0}", FormsAuthTicket.Name);

                RedirectURL = "";
                if (Request[Constants.ResumePath] != null)
                {
                    //Generate the SSO Server URL to send the OpenToken to
                    RedirectURL = PingIntegration.Links.HostPF + Request[Constants.ResumePath];
                    RedirectURL = OTI.GetOpenTokenRedirect(RedirectURL);

                    ServerLog.InfoFormat("Sending OpenToken to URL: {0}", RedirectURL);

                    //Send the OpenToken
                    Response.Redirect(RedirectURL, true);
                }
            }
            else
            {
                ServerLog.Debug("No existing session found");

                string RedirectURL = "";
                Regex portlessRegex = new Regex(@":\d+");
                string cleanUrl = portlessRegex.Replace(Request.Url.ToString(), "");
                Uri URI = new Uri(cleanUrl);
                string PartnerIdpId = Request.QueryString["PartnerIdpId"];
                string TargetResource = String.Format("{0}ResumeFedSSO/?{1}={2}", URI.GetLeftPart(UriPartial.Path), Constants.ResumePath, Request[Constants.ResumePath]);
                RedirectURL = OTI.GetRequestOpenTokenURL(PartnerIdpId, TargetResource);

                ServerLog.InfoFormat("Requesting OpenToken from URL: {0}", RedirectURL);
                
                Response.Redirect(RedirectURL, true);
            }
            
            return View();
        }

        /// <summary>
        /// Controller for the ResumeFedSSO View. This controller is what is called
        /// when the SSO server returns an OpenToken from the SP side. The OpenToken
        /// is processed and a new Forms Auth Session is created, and the originally
        /// requested OpenToken from the IdP side of the SSO server is returned.
        /// </summary>
        /// <returns></returns>
        public ActionResult ResumeFedSSO()
        {
            FedAuthResumeFedSSOViewModel ViewModel = new FedAuthResumeFedSSOViewModel();
            ServerLog.Info("FedAuth Controller Resume Called");

            string UPN = OTI.GetOpenTokenSubject(System.Web.HttpContext.Current.Request, System.Web.HttpContext.Current.Response);

            ViewModel.Form.UserID = UPN;

            bool IsValid = false;

            ADAuth ADLDS = new ADAuth(UPN, "ADLDS");

            if (ADLDS.FoundUser())
            {
                if (ValidateUser(ADLDS))
                {
                    IsValid = true;
                }
            }
            else
            {
                ADAuth AD = new ADAuth(UPN, "ActiveDirectory");
                if (AD.FoundUser())
                {
                    if(ValidateUser(AD))
                    {
                        IsValid = true;
                    }
                }
            }


            if (IsValid)
            {
                //Create a Forms Authentication Ticket and Corresponding Forms Authentication Cookie
                FormsAuthenticationTicket FormsAuthTicket = new FormsAuthenticationTicket(1, UPN, DateTime.Now, DateTime.Now.AddMinutes(FormsAuth.GetTimeout()), false, "");
                HttpCookie AuthCookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(FormsAuthTicket));
                AuthCookie.Path = FormsAuthentication.FormsCookiePath;
                Response.Cookies.Add(AuthCookie);

                Regex portlessRegex = new Regex(@":\d+");
                string cleanUrl = portlessRegex.Replace(Request.Url.ToString(), "");

                ServerLog.DebugFormat("Redirecting user \"{0}\" to {1}", UPN, cleanUrl.Replace("ResumeFedSSO", ""));

                Response.Redirect(cleanUrl.Replace("ResumeFedSSO", ""), true);
            }

            return View();
        }

        protected bool ValidateUser(ADAuth Directory)
        {
            bool ErrorThrown = false;
            //Check if the users account has expired
            if (DateTime.Compare(Directory.GetAccountExpiration(), DateTime.Now) <= 0)
            {
                ErrorThrown = true;
                ModelState.AddModelError("UserID", AS.AccountExpired);
                ServerLog.DebugFormat("Adding Error to Model: Account has expired. Returning Error to User: {0}", AS.AccountExpired);
            }
            //Check if the users account is disabled
            if (Directory.IsAccountDisabled())
            {
                ErrorThrown = true;
                ModelState.AddModelError("UserID", AS.AccountDisabled);
                ServerLog.DebugFormat("Adding Error to Model: Account is disabled. Returning Error to User: {0}", AS.AccountDisabled);
            }
            //Check of the users account is locked
            if (Directory.IsAccountLocked())
            {
                ErrorThrown = true;
                ModelState.AddModelError("UserID", AS.AccountLocked);
                ServerLog.DebugFormat("Adding Error to Model: Account is locked. Returning Error to User: {0}", AS.AccountLocked);
            }

            return !ErrorThrown;
        }
    }
}

﻿/*******************************************************************************
* Name
*   Home Controller
*
* Identifier
*   Filename: HomeController.cs
*   
* Purpose
*   This controller sends data to the Index View that is the Login Page and
*   allows for user authentication using the ADIntegration class library. Once
*   the user has been successfully authenticated, the OpenTokenIntegration class
*   library is used to return an OpenToken to the PingFederate instance.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;
    using System.Xml;
    using LoginApp.ActionFilters;
    using LoginApp.ViewModels;
    using ADIntegration;
    using Log4NetExtenstion;
    using LoginUtils;
    using PingIntegration;
    using Utilities;
    using log4net;

#endregion

namespace LoginApp.Controllers
{
    /// <summary>
    /// This controller sends data to the Index View that is the Login Page and
    /// allows for user authentication using the ADIntegration class library. Once
    /// the user has been successfully authenticated, the OpenTokenIntegration class
    /// library is used to return an OpenToken to the PingFederate instance.
    /// </summary>
    [SEOCanonicalize]
    public class HomeController : BaseController
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        //Create AuthStrings object
        AuthStrings AS;

        //Create LoginUtils Links object
        UserNotificationDefaults UND;

        //Create Ping Links object
        PingIntegration.Links links;

        //Set a default Password Expiration Reminder
        int PasswordExpirationReminder = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeController"/> class.
        /// </summary>
        public HomeController()
        {
            ServerLog.Info("Home Controller Constructor Called");
            AS = new AuthStrings();
            UND = new UserNotificationDefaults();
            links = new PingIntegration.Links();

            PasswordExpirationReminder = UND.PasswordExpirationReminder;
        }

        /// <summary>
        /// Controller for the Index View.
        /// </summary>
        /// <returns>The Index View</returns>
        public ActionResult Index()
        {
            ServerLog.Info("Home Controller Index Called");

            if (Request.QueryString["spentity"] != null)
                Log4NetHelper.WriteToSession("ConnectionID", Request.QueryString["spentity"]);
            
            //Check to see if the user has a valid session
            CheckForExistingSession();

            //Create our ViewModel and pass it to the view
            HomeIndexViewModel ViewModel = new HomeIndexViewModel();

            return View(ViewModel);
        }

        /// <summary>
        /// Controller that consumes the POST from the Index View.
        /// </summary>
        /// <param name="ViewModel">The populated model.</param>
        /// <returns>The Index View</returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index([Bind(Include = "Form")]HomeIndexViewModel ViewModel, string Command)
        {
            if(Command.Equals("Reset your Password"))
                return ChangePassword(ViewModel.Form.UserID);

            bool LegacyIDUsed = false;
            string UserID = (ViewModel.Form.UserID == null ? "" : ViewModel.Form.UserID);

            ServerLog.Info("Home Controller Index has been POSTed to");
            //Our regular expression to test the validity of the email address entered by the user
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            //Create our new regex object using the regular expression just defined for email addresses
            Regex re = new Regex(strRegex);

            //Check that the user entered a User ID
            if (ViewModel.Form.UserID == null)
            {
                ModelState.AddModelError("UserID", AS.NoUsername);
                ServerLog.DebugFormat("Adding Error to Model: No User ID entered. Returning Error to User: {0}", AS.NoUsername);
            }
            //Check that the user entered a valid E-Mail Address
            else if (!re.IsMatch(ViewModel.Form.UserID))
            {
                if (UserID.Contains("\\"))
                {
                    ServerLog.InfoFormat("Found domain qualification with username: {0}", UserID);
                    UserID = UserID.Substring(UserID.IndexOf("\\") + 1);
                }

                if (UserID.Length >= 7 && UserID.Length <= 8)
                    LegacyIDUsed = true;
                else
                {
                    ModelState.AddModelError("UserID", AS.InvalidUsername);
                    ServerLog.DebugFormat("Adding Error to Model: User ID is not an email address. Returning Error to User: {0}", AS.InvalidUsername);
                }
            }
            //Check that the user entered a password
            if (ViewModel.Form.Password == null)
            {
                ModelState.AddModelError("Password", AS.NoPassword);
                ServerLog.DebugFormat("Adding Error to Model: No Password entered. Returing Error to User: {0}", AS.NoPassword);
            }

            //If we are error free
            if (ModelState.IsValid)
            {
                //Look in the ADLDS First
                ServerLog.InfoFormat("Creating ADAuth ADLDS Instance for user: {0}", ViewModel.Form.UserID);
                ADAuth B2BADA = new ADAuth(ViewModel.Form.UserID, "ADLDS");

                //If we do not find the user in the ADLDS
                if (!ValidateUser(B2BADA, ViewModel))
                {
                    ServerLog.DebugFormat("User \"{0}\" not found in ADLDS", ViewModel.Form.UserID);
                    ServerLog.InfoFormat("Creating ADAuth Active Directory Instance for user: {0}", ViewModel.Form.UserID);
                    //Look in Active Directory
                    ADAuth ADADA = (LegacyIDUsed ? new ADAuth(UserID, "ActiveDirectory", "sAMAccountName") : new ADAuth(ViewModel.Form.UserID, "ActiveDirectory"));
                    //If we do not find the user in Active Directory
                    if (!ValidateUser(ADADA, ViewModel))
                    {
                        ServerLog.DebugFormat("User \"{0}\" not found in Active Directory", UserID);
                        ModelState.AddModelError("UserID", AS.InvalidUsername);
                        ServerLog.DebugFormat("Adding Error to Model: Username not found. Returning Error to User: {0}", AS.InvalidUsername);
                    }
                }
            }

            return View(ViewModel);
        }

        /// <summary>
        /// Controller that returns the Index View for the purposes of the change
        /// password functionality.
        /// </summary>
        /// <param name="UserID">The user ID.</param>
        /// <returns>The Index View</returns>
        public ActionResult ChangePassword(string UserID)
        {
            ServerLog.Info("Home Controller ChangePassword has been posted to");

            bool LegacyIDUsed = false;
            string userID = (UserID == null ? "" : UserID);

            //Our regular expression to test the validity of the email address entered by the user
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                      @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                      @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            //Create our new regex object using the regular expression just defined for email addresses
            Regex re = new Regex(strRegex);

            //Check that the user entered a User ID
            if (UserID == null)
            {
                ModelState.AddModelError("UserID", AS.NoUsername);
                ServerLog.DebugFormat("Adding Error to Model: No User ID entered. Returning Error to User: {0}", AS.NoUsername);
            }
            //Check that the user entered a valid E-Mail Address
            else if (!re.IsMatch(UserID))
            {
                if (UserID.Contains("\\"))
                {
                    ServerLog.InfoFormat("Found domain qualification with username: {0}", UserID);
                    userID = UserID.Substring(UserID.IndexOf("\\") + 1);
                }

                if (userID.Length >= 7 && userID.Length <= 8)
                    LegacyIDUsed = true;
                else
                {
                    ModelState.AddModelError("UserID", AS.InvalidUsername);
                    ServerLog.DebugFormat("Adding Error to Model: User ID is not an email address. Returning Error to User: {0}", AS.InvalidUsername);
                }
            }

            //If we are error free
            if (ModelState.IsValid)
            {
                string PasswordResetURL = "";
                
                XMLDocHelper LinksDocHelper;
                XmlDocument Linksdoc;
                //Get the path of the config file in the calling application.
                //It is assumed the config file is in the App_Data/Links directory and is named Links.xml
                LinksDocHelper = new XMLDocHelper(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\Links\\Links.xml"));
                Linksdoc = LinksDocHelper.GetXMLDocument();

                //Look in the ADLDS First
                ServerLog.InfoFormat("Creating ADAuth ADLDS Instance for user: {0}", UserID);
                ADAuth B2BADA = new ADAuth(UserID, "ADLDS");

                //If we do not find the user in the ADLDS
                if (!B2BADA.FoundUser())
                {
                    ServerLog.DebugFormat("User \"{0}\" not found in ADLDS", UserID);
                    ServerLog.InfoFormat("Creating ADAuth Active Directory Instance for user: {0}", UserID);
                    //Look in Active Directory
                    ADAuth ADADA = (LegacyIDUsed ? new ADAuth(userID, "ActiveDirectory", "sAMAccountName") : new ADAuth(UserID, "ActiveDirectory"));
                    //If we do not find the user in Active Directory
                    if (!ADADA.FoundUser())
                    {
                        ServerLog.DebugFormat("User \"{0}\" not found in Active Directory", UserID);
                        ModelState.AddModelError("UserID", AS.InvalidUsername);
                        ServerLog.DebugFormat("Adding Error to Model: Username not found. Returning Error to User: {0}", AS.InvalidUsername);
                    }
                    else
                    {
                        //Extract the Password Reset URL from the configuration file
                        PasswordResetURL = XMLHelper.GetString(Linksdoc, "PasswordReset-Internal");
                        ServerLog.InfoFormat("Redirecting user \"{0}\" to Internal Password Reset URL: {1}", UserID, PasswordResetURL);
                        Response.Redirect(PasswordResetURL, true);
                    }
                }
                else
                {
                    //Extract the Password Reset URL from the configuration file
                    PasswordResetURL = XMLHelper.GetString(Linksdoc, "PasswordReset-External");
                    ServerLog.InfoFormat("Redirecting user \"{0}\" to External Password Reset URL: {1}", UserID, PasswordResetURL);
                    Response.Redirect(PasswordResetURL, true);
                }
            }

            HomeIndexViewModel ViewModel = new HomeIndexViewModel();
            return View("Index", ViewModel);
        }

        /// <summary>
        /// Checks for existing session. If a session exists, an Open Token is returned to the SSO Server
        /// </summary>
        protected void CheckForExistingSession()
        {
            ServerLog.Info("Checking for an existing session");
            
            //Check for a Forms Authentication Cookie
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                ServerLog.Debug("Existing session found");
                string RedirectURL = "";
                string ForceAuthn = Request[Constants.ForceAuthnParam];
                //Check for Force Authentication Parameter
                if (ForceAuthn != null && ForceAuthn.Equals("true"))
                {
                    ServerLog.Debug("Force Authentication is true. Killing Session and logging out.");
                    //Kill the session and logout the user
                    Session.Clear();
                    FormsAuthentication.SignOut();

                    //Redirect to login page
                    if (Request[Constants.ResumePath] != null)
                    {
                        RedirectURL += String.Format("?ReturnUrl={0}/?cmd=sso%26{1}={2}", Request.ApplicationPath, Constants.ResumePath, Request[Constants.ResumePath]);
                        ServerLog.InfoFormat("Redirecting User for re-authentication to URL: {0}", RedirectURL);
                    }

                    Response.Redirect(RedirectURL, true);
                }

                //No force authentication parameter found. Decrypt Forms Authentication Ticket
                FormsAuthenticationTicket FormsAuthTicket = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                //Create an OpenToken and add the subject to it
                OpenTokenIntegration OTI = new OpenTokenIntegration();
                OTI.AddDataToOpenToken(Constants.Subject, FormsAuthTicket.Name);

                ServerLog.DebugFormat("Decrypting Forms Authentication Ticket. Adding subject to OpenToken with value: {0}", FormsAuthTicket.Name);

                RedirectURL = "";
                if (Request[Constants.ResumePath] != null)
                {
                    //Generate the SSO Server URL to send the OpenToken to
                    RedirectURL = PingIntegration.Links.HostPF + Request[Constants.ResumePath];
                    RedirectURL = OTI.GetOpenTokenRedirect(RedirectURL);

                    ServerLog.InfoFormat("Sending OpenToken to URL: {0}", RedirectURL);

                    //Send the OpenToken
                    Response.Redirect(RedirectURL, true);
                }
                else if (Request["ReturnUrl"] != null)
                {
                    ServerLog.InfoFormat("Redirecting user \"{0}\" to \"{1}\"", FormsAuthTicket.Name, Request["ReturnUrl"]);
                    Response.Redirect(Request["ReturnUrl"], true);
                }
            }
        }

        /// <summary>
        /// Validates the user against a directory.
        /// </summary>
        /// <param name="Directory">The directory to validate against.</param>
        /// <param name="ViewModel">The view model with the authentication information.</param>
        /// <returns>A boolean indicating if the user was found.</returns>
        protected bool ValidateUser(ADAuth Directory, HomeIndexViewModel ViewModel)
        {
            ServerLog.Info("Attempting to Validate the User");
            //If we found the user
            if (Directory.FoundUser())
            {
                ServerLog.DebugFormat("User \"{0}\" found in {1}", Directory.UserID, Directory.LDAPName);
                //This allows the user to see that their password is expiring soon before being sent to the application
                if (Session["Skip"] == null)
                {
                    //Get the days left before the users password expires
                    double DaysLeft = GetDaysLeft(Directory.GetPasswordExpiration());
                    if (DaysLeft < 0)
                        DaysLeft = 0;
                    //If we are within the password reset reminder threshold set for the application
                    if (DaysLeft < PasswordExpirationReminder)
                    {
                        ServerLog.DebugFormat("Days Left ({0}) less than Password Expiration Reminder ({1})", DaysLeft, PasswordExpirationReminder);
                        //The users password has expired
                        if (DaysLeft == 0)
                        {
                            ModelState.AddModelError("UserID", AS.PasswordExpired);
                            ServerLog.DebugFormat("Adding Error to Model: Password has expired. Returning Error to User: {0}", AS.PasswordExpired);
                        }
                        //The users password expires today
                        else if (DaysLeft > 0 && DaysLeft <= 1)
                        {
                            Session["Skip"] = true;
                            ModelState.AddModelError("UserID", AS.PasswordExpiresToday);
                            ServerLog.DebugFormat("Adding Error to Model: Password expires today. Returning Error to User: {0}", AS.PasswordExpiresToday);
                        }
                        //The users password expires soon
                        else
                        {
                            Session["Skip"] = true;
                            ModelState.AddModelError("UserID", String.Format(AS.PasswordExpiresSoon, Convert.ToInt32(DaysLeft)));
                            ServerLog.DebugFormat("Adding Error to Model: Password expires soon. Returning Error to User: {0}", String.Format(AS.PasswordExpiresSoon, Convert.ToInt32(DaysLeft)));
                        }
                    }
                }
                //Check if the users account has expired
                if (DateTime.Compare(Directory.GetAccountExpiration(), DateTime.Now) <= 0)
                {
                    ModelState.AddModelError("UserID", AS.AccountExpired);
                    ServerLog.DebugFormat("Adding Error to Model: Account has expired. Returning Error to User: {0}", AS.AccountExpired);
                }
                //Check if the users account is disabled
                if (Directory.IsAccountDisabled())
                {
                    ModelState.AddModelError("UserID", AS.AccountDisabled);
                    ServerLog.DebugFormat("Adding Error to Model: Account is disabled. Returning Error to User: {0}", AS.AccountDisabled);
                }
                //Check of the users account is locked
                if (Directory.IsAccountLocked())
                {
                    ModelState.AddModelError("UserID", AS.AccountLocked);
                    ServerLog.DebugFormat("Adding Error to Model: Account is locked. Returning Error to User: {0}", AS.AccountLocked);
                }

                if (ModelState.IsValid)
                {
                    //Attempt to authenticate the user
                    if (Directory.LogonUser(ViewModel.Form.Password))
                    {
                        //Get the users User Principal Name
                        string UPN = Directory.GetUserPrincipalName();
                        UPN = (UPN.Contains("@") ? UPN.Substring(0, UPN.IndexOf("@")) : UPN);

                        ServerLog.DebugFormat("Creating Forms Authentication Ticket and adding Cookie for user \"{0}\". Ticket expires at {1}", UPN, DateTime.Now.AddMinutes(FormsAuth.GetTimeout()));
                        
                        //Create a Forms Authentication Ticket and Corresponding Forms Authentication Cookie
                        FormsAuthenticationTicket FormsAuthTicket = new FormsAuthenticationTicket(1, UPN, DateTime.Now, DateTime.Now.AddMinutes(FormsAuth.GetTimeout()), false, "");
                        HttpCookie AuthCookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(FormsAuthTicket));
                        AuthCookie.Path = FormsAuthentication.FormsCookiePath;
                        Response.Cookies.Add(AuthCookie);

                        ServerLog.DebugFormat("Redirecting user \"{0}\" to {1}", ViewModel.Form.UserID, Request.Url.PathAndQuery);
                        Response.Redirect(Request.Url.PathAndQuery, true);

                        return true;
                    }
                    //Authentication failed for the user
                    else
                    {
                        ModelState.AddModelError("UserID", AS.InvalidPassword);
                        ServerLog.DebugFormat("Adding Error to Model: Authentication Failed. Returning Error to User: {0}", AS.InvalidPassword);
                    }
                }
                
                return true;
            }
            //The user was not found
            return false;
        }

        /// <summary>
        /// Gets the days left before the date passed in.
        /// </summary>
        /// <param name="Expiration">The expiration date as a DateTime.</param>
        /// <returns>The number of days before the Expiration date</returns>
        protected double GetDaysLeft(DateTime Expiration)
        {
            ServerLog.InfoFormat("GetDaysLeft called with value: Expiration = \"{0}\"", Expiration);
            
            if (Expiration == DateTime.MaxValue)
                return TimeSpan.MaxValue.TotalDays;
            if (Expiration == DateTime.MinValue)
                return TimeSpan.MinValue.TotalDays;
            if (Expiration.CompareTo(DateTime.Now) > 0)
                return Expiration.Subtract(DateTime.Now).TotalDays;
            return TimeSpan.MinValue.TotalDays;
        }
    }
}

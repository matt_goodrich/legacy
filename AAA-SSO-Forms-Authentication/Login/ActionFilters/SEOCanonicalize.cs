﻿/*******************************************************************************
* Name
*   SEO Canonicalization
*
* Identifier
*   Filename: SEOCanonicalize.cs
*   
* Purpose
*   This class creates a custom ActionFilter that ensures that a trailing 
*   forward slash is appended to URL's when the Controller is called without
*   any addition action Data or parameters.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

#endregion

namespace LoginApp.ActionFilters
{
    /// <summary>
    /// This class creates a custom ActionFilter that ensures that a trailing 
    /// forward slash is appended to URL's when the Controller is called without
    /// any addition action Data or parameters.
    /// </summary>
    public class SEOCanonicalize : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // grab the URL:
            HttpContextBase Current = filterContext.HttpContext;
            string path = Current.Request.Url.ToString() ?? "/";

            Uri URI = new Uri(path);

            // make sure that the path ends in a "/"
            //      (doesn't apply in some cases... but those methods/etc won't
            //          be explicitly decorated with this attribute)
            if (!URI.GetLeftPart(UriPartial.Path).EndsWith("/"))
            {
                string newLocation = path + "/";

                Current.Response.StatusCode = 301;
                Current.Response.TrySkipIisCustomErrors = true;
                Current.Response.Status = "301 Moved Permanently";
                Current.Response.AppendHeader("Location", newLocation);
                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
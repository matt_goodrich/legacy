﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<LoginApp.ViewModels.FedAuthResumeFedSSOViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Federated SSO Login
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="prefix_7 grid_12">
        <div class="content">
            <div class="banner">
                <h2>AAA Insurance Agent Portal</h2>
            </div>
            <% using (Html.BeginForm()) {%>
                <table>
                    <tr>
                        <th></th>
                        <td><%= Html.ValidationSummary() %></td>
                    </tr>
                    <tr>
                        <th><%= Html.LabelFor(m => m.Form.UserID) %></th>
                        <td><%= Html.TextBoxFor(m => m.Form.UserID, new { @class = "readonly", @readonly = "readonly" })%></td>
                    </tr>
                </table>
            <% } %>
        </div>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<LoginApp.ViewModels.ConfigurationIndexViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Application Configuration
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="server">
    <script src="<%=ResolveClientUrl("~/Scripts/Configuration/Configuration.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="grid_24">
        <div class="content">
            <div class="banner">
                <h2>Application Configuration</h2>
            </div>
            <p>You are configuration the application on <%= System.Environment.MachineName %>. Please ensure this configuration is updated on the other nodes in the cluster as well.</p>
            <% using (Html.BeginForm(null, null, FormMethod.Post, new { @id = "ConfigForm", @action = "./", @enctype = "multipart/form-data" }))
               { %>
                <table>
                    <col width="5%" />
                    <col width="95%" />
                    <tr>
                        <td></td>
                        <td>
                            <%= Html.ValidationSummary() %>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>PingFederate</h3></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.PingConfigForm.PingHost) %></th>
                                    <td><%= Html.TextBoxFor(model => model.PingConfigForm.PingHost, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.PingConfigForm.TokenType) %></th>
                                    <td><%= Html.TextBoxFor(model => model.PingConfigForm.TokenType, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.PingConfigForm.UseSSL) %></th>
                                    <td><%= Html.TextBoxFor(model => model.PingConfigForm.UseSSL, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.PingConfigForm.HTTPSPort) %></th>
                                    <td><%= Html.TextBoxFor(model => model.PingConfigForm.HTTPSPort, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.PingConfigForm.AgentConfig) %></th>
                                    <td><input type="file" id="AgentConfig" name="AgentConfig" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Directory Access</h3></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDAPModuleForm.LDAPForm.ID) %></th>
                                    <td><%= Html.TextBoxFor(model => model.ADLDAPModuleForm.LDAPForm.ID, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDAPModuleForm.LDAPForm.ConnectionString) %></th>
                                    <td><%= Html.TextBoxFor(model => model.ADLDAPModuleForm.LDAPForm.ConnectionString, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDAPModuleForm.LDAPForm.HasDomainPolicy) %></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDAPModuleForm.LDAPForm.HasDomainPolicy, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDAPModuleForm.LDAPForm.Path) %></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDAPModuleForm.LDAPForm.Path, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDAPModuleForm.LDAPForm.Impersonate) %></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDAPModuleForm.LDAPForm.Impersonate, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDAPModuleForm.LDAPForm.ImpersonateUser) %></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDAPModuleForm.LDAPForm.ImpersonateUser, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDAPModuleForm.LDAPForm.UseSSL) %></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDAPModuleForm.LDAPForm.UseSSL, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDAPModuleForm.LDAPForm.LoginAttribute) %></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDAPModuleForm.LDAPForm.LoginAttribute, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDSLDAPModuleForm.LDAPForm.ID) %></th>
                                    <td><%= Html.TextBoxFor(model => model.ADLDSLDAPModuleForm.LDAPForm.ID, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDSLDAPModuleForm.LDAPForm.ConnectionString)%></th>
                                    <td><%= Html.TextBoxFor(model => model.ADLDSLDAPModuleForm.LDAPForm.ConnectionString, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDSLDAPModuleForm.LDAPForm.HasDomainPolicy)%></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDSLDAPModuleForm.LDAPForm.HasDomainPolicy, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDSLDAPModuleForm.LDAPForm.Impersonate)%></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDSLDAPModuleForm.LDAPForm.Impersonate, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDSLDAPModuleForm.LDAPForm.ImpersonateUser)%></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDSLDAPModuleForm.LDAPForm.ImpersonateUser, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDSLDAPModuleForm.LDAPForm.UseSSL)%></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDSLDAPModuleForm.LDAPForm.UseSSL, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADLDSLDAPModuleForm.LDAPForm.LoginAttribute)%></th>
                                    <td><%= Html.TextBoxFor(model => Model.ADLDSLDAPModuleForm.LDAPForm.LoginAttribute, new { @class = "readonly", @readonly = "readonly" })%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Authentication Error Messages</h3></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.NoResource) %></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.NoResource, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.NoUsername)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.NoUsername, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.NoPassword)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.NoPassword, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.AccountExpired)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.AccountExpired, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.PasswordExpired)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.PasswordExpired, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.PasswordExpiresToday)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.PasswordExpiresToday, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.PasswordExpiresSoon)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.PasswordExpiresSoon, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.AccountDisabled)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.AccountDisabled, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.AccountLocked)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.AccountLocked, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.InvalidUsername)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.InvalidUsername, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.AuthStringsForm.InvalidPassword)%></th>
                                    <td><%= Html.TextBoxFor(model => model.AuthStringsForm.InvalidPassword, new { @class = "full" })%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>LDAP Policy Defaults</h3></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADDefaultsForm.AccountExpiration) %></th>
                                    <td><%= Html.TextBoxFor(model => model.ADDefaultsForm.AccountExpiration)%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.ADDefaultsForm.PasswordExpiration)%></th>
                                    <td><%= Html.TextBoxFor(model => model.ADDefaultsForm.PasswordExpiration)%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>User Notification Defaults</h3></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.UserNotificationsDefaultsForm.PasswordExpirationReminder) %></th>
                                    <td><%= Html.TextBoxFor(model => model.UserNotificationsDefaultsForm.PasswordExpirationReminder)%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><h3>Links</h3></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <table>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.LinksForm.PasswordResetURLInternal) %></th>
                                    <td><%= Html.TextBoxFor(model => model.LinksForm.PasswordResetURLInternal, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.LinksForm.PasswordResetURLExternal) %></th>
                                    <td><%= Html.TextBoxFor(model => model.LinksForm.PasswordResetURLExternal, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.LinksForm.PrivacyPolicyURL) %></th>
                                    <td><%= Html.TextBoxFor(model => model.LinksForm.PrivacyPolicyURL, new { @class = "full" })%></td>
                                </tr>
                                <tr>
                                    <th><%= Html.LabelFor(model => model.LinksForm.TermsOfServiceURL) %></th>
                                    <td><%= Html.TextBoxFor(model => model.LinksForm.TermsOfServiceURL, new { @class = "full" })%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a id="SaveButton" class="button primary disabled">Save</a>
                            <noscript>
                                <input type="submit" value="Save" />
                            </noscript>
                        </td>
                    </tr>
                </table>
            <% } %>
        </div>
    </div>
</asp:Content>



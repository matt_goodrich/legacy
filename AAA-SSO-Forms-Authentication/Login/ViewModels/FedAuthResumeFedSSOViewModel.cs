﻿/*******************************************************************************
* Name
*   Fed Auth Resume Fed SSO View Model
*
* Identifier
*   Filename: FedAuthResumeFedSSOViewModel.cs
*   
* Purpose
*   This set of classes provides the ability to create strongly typed views
*   that allow object oriented views to be created and binded.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web;

#endregion

namespace LoginApp.ViewModels
{
    /// <summary>
    /// The type of object that will be returned to create the strongly typed view
    /// </summary>
    public class FedAuthResumeFedSSOViewModel
    {
        public LoginForm Form { get; set; }
    }
}
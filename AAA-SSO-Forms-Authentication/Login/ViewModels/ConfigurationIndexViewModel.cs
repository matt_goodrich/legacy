﻿/*******************************************************************************
* Name
*   Configuration Index ViewModel
*
* Identifier
*   Filename: ConfigurationIndexViewModel.cs
*   
* Purpose
*   This set of classes provides the ability to create strongly typed views
*   that allow object oriented views to be created and binded.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web;

#endregion

namespace LoginApp.ViewModels
{
    /// <summary>
    /// A collection of objects to be returned as a single object as the type of the view
    /// </summary>
    public class ConfigurationIndexViewModel
    {
        public AuthStringsForm AuthStringsForm { get; set; }
        public ADDefaultsForm ADDefaultsForm { get; set; }
        public UserNotificationsDefaultsForm UserNotificationsDefaultsForm { get; set; }
        public LinksForm LinksForm { get; set; }
        public ADLDAPModuleForm ADLDAPModuleForm { get; set; }
        public ADLDSLDAPModuleForm ADLDSLDAPModuleForm { get; set; }
        public PingConfigForm PingConfigForm { get; set; }
    }

    /// <summary>
    /// Getters and Setters for the Ping Config
    /// </summary>
    public class PingConfigForm
    {
        /// <summary>
        /// Gets or sets the ping host.
        /// </summary>
        /// <value>
        /// The ping host.
        /// </value>
        [DisplayName("Host")]
        public string PingHost { get; set; }

        /// <summary>
        /// Gets or sets the type of the token.
        /// </summary>
        /// <value>
        /// The type of the token.
        /// </value>
        [ReadOnly(true)]
        [DisplayName("Token Type")]
        public string TokenType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [use SSL].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use SSL]; otherwise, <c>false</c>.
        /// </value>
        [ReadOnly(true)]
        [DisplayName("Use SSL")]
        public bool UseSSL { get; set; }

        /// <summary>
        /// Gets or sets the HTTPS port.
        /// </summary>
        /// <value>
        /// The HTTPS port.
        /// </value>
        [DisplayName("HTTPS Port")]
        public int HTTPSPort { get; set; }

        /// <summary>
        /// Gets or sets the agent config.
        /// </summary>
        /// <value>
        /// The agent config.
        /// </value>
        [DisplayName("Agent-Config")]
        public HttpPostedFile AgentConfig { get; set; }
    }

    /// <summary>
    /// Getter and setter for the AD LDAPForm
    /// </summary>
    public class ADLDAPModuleForm
    {
        /// <summary>
        /// Gets or sets the LDAP form for AD.
        /// </summary>
        /// <value>
        /// The LDAP form.
        /// </value>
        public LDAPModuleForm LDAPForm { get; set; }
    }

    /// <summary>
    /// Getter and setter for the ADLDS LDAPForm
    /// </summary>
    public class ADLDSLDAPModuleForm
    {
        /// <summary>
        /// Gets or sets the LDAP form for the ADLDS.
        /// </summary>
        /// <value>
        /// The LDAP form.
        /// </value>
        public LDAPModuleForm LDAPForm { get; set; }
    }

    /// <summary>
    /// Getters and setters for the LDAPModuleForm type
    /// </summary>
    public class LDAPModuleForm
    {
        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        /// <value>
        /// The ID.
        /// </value>
        [ReadOnly(true)]
        [DisplayName("LDAP Name")]
        public string ID { get; set; }

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        [DisplayName("Connection String")]
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has domain policy.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has domain policy; otherwise, <c>false</c>.
        /// </value>
        [DisplayName("Has Domain Policy")]
        public bool HasDomainPolicy { get; set; }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        /// <value>
        /// The path.
        /// </value>
        [DisplayName("LDAP Path")]
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="LDAPModuleForm"/> is impersonate.
        /// </summary>
        /// <value>
        ///   <c>true</c> if impersonate; otherwise, <c>false</c>.
        /// </value>
        [ReadOnly(true)]
        [DisplayName("Impersonate")]
        public bool Impersonate { get; set; }

        /// <summary>
        /// Gets or sets the impersonate user.
        /// </summary>
        /// <value>
        /// The impersonate user.
        /// </value>
        [ReadOnly(true)]
        [DisplayName("Impersonated User")]
        public string ImpersonateUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [use SSL].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use SSL]; otherwise, <c>false</c>.
        /// </value>
        [ReadOnly(true)]
        [DisplayName("Use SSL")]
        public bool UseSSL { get; set; }

        /// <summary>
        /// Gets or sets the login attribute.
        /// </summary>
        /// <value>
        /// The login attribute.
        /// </value>
        [ReadOnly(true)]
        [DisplayName("Login Attribute")]
        public string LoginAttribute { get; set; }
    }

    /// <summary>
    /// Getters and setters for authentication strings
    /// </summary>
    public class AuthStringsForm
    {
        /// <summary>
        /// Gets or sets the no resource string.
        /// </summary>
        /// <value>
        /// The no resource string.
        /// </value>
        [DisplayName("No Resource")]
        public string NoResource { get; set; }

        /// <summary>
        /// Gets or sets the no username string.
        /// </summary>
        /// <value>
        /// The no username string.
        /// </value>
        [DisplayName("No Username")]
        public string NoUsername { get; set; }

        /// <summary>
        /// Gets or sets the no password string.
        /// </summary>
        /// <value>
        /// The no password string.
        /// </value>
        [DisplayName("No Password")]
        public string NoPassword { get; set; }

        /// <summary>
        /// Gets or sets the account expired string.
        /// </summary>
        /// <value>
        /// The account expired string.
        /// </value>
        [DisplayName("Account Expired")]
        public string AccountExpired { get; set; }

        /// <summary>
        /// Gets or sets the password expired string.
        /// </summary>
        /// <value>
        /// The password expired string.
        /// </value>
        [DisplayName("Password Expired")]
        public string PasswordExpired { get; set; }

        /// <summary>
        /// Gets or sets the password expires today string.
        /// </summary>
        /// <value>
        /// The password expires today string.
        /// </value>
        [DisplayName("Password Expires Today")]
        public string PasswordExpiresToday { get; set; }

        /// <summary>
        /// Gets or sets the password expires soon string.
        /// </summary>
        /// <value>
        /// The password expires soon string.
        /// </value>
        [DisplayName("Password Expires Soon")]
        public string PasswordExpiresSoon { get; set; }

        /// <summary>
        /// Gets or sets the account disabled string.
        /// </summary>
        /// <value>
        /// The account disabled string.
        /// </value>
        [DisplayName("Account Disabled")]
        public string AccountDisabled { get; set; }

        /// <summary>
        /// Gets or sets the account locked string.
        /// </summary>
        /// <value>
        /// The account locked string.
        /// </value>
        [DisplayName("Account Locked")]
        public string AccountLocked { get; set; }

        /// <summary>
        /// Gets or sets the invalid username string.
        /// </summary>
        /// <value>
        /// The invalid username string.
        /// </value>
        [DisplayName("Invalid Username")]
        public string InvalidUsername { get; set; }

        /// <summary>
        /// Gets or sets the invalid password string.
        /// </summary>
        /// <value>
        /// The invalid password string.
        /// </value>
        [DisplayName("Invalid Password")]
        public string InvalidPassword { get; set; }
    }

    /// <summary>
    /// Getter and Setter for Account and Password Expiration defaults
    /// </summary>
    public class ADDefaultsForm
    {
        /// <summary>
        /// Gets or sets the default account expiration.
        /// </summary>
        /// <value>
        /// The default account expiration.
        /// </value>
        [DisplayName("Account Expiration (Days)")]
        public int AccountExpiration { get; set; }

        /// <summary>
        /// Gets or sets the default password expiration.
        /// </summary>
        /// <value>
        /// The default password expiration.
        /// </value>
        [DisplayName("Password Expiration (Days)")]
        public int PasswordExpiration { get; set; }
    }

    /// <summary>
    /// Getter and Setter for Password Expiration Reminder
    /// </summary>
    public class UserNotificationsDefaultsForm
    {
        /// <summary>
        /// Gets or sets the password expiration reminder.
        /// </summary>
        /// <value>
        /// The password expiration reminder.
        /// </value>
        [DisplayName("Password Expiration Reminder (Days)")]
        public int PasswordExpirationReminder { get; set; }
    }

    /// <summary>
    /// Getters and setters for Password Reset and Footer Links
    /// </summary>
    public class LinksForm
    {
        /// <summary>
        /// Gets or sets the privacy policy URL.
        /// </summary>
        /// <value>
        /// The privacy policy URL.
        /// </value>
        [DisplayName("Privacy Policy URL")]
        public string PrivacyPolicyURL { get; set; }

        /// <summary>
        /// Gets or sets the terms of service URL.
        /// </summary>
        /// <value>
        /// The terms of service URL.
        /// </value>
        [DisplayName("Terms of Service URL")]
        public string TermsOfServiceURL { get; set; }

        /// <summary>
        /// Gets or sets the external password reset URL.
        /// </summary>
        /// <value>
        /// The external password reset URL.
        /// </value>
        [DisplayName("Password Reset URL (External)")]
        public string PasswordResetURLExternal { get; set; }

        /// <summary>
        /// Gets or sets the internal password reset URL.
        /// </summary>
        /// <value>
        /// The internal password reset URL.
        /// </value>
        [DisplayName("Password Reset URL (Internal)")]
        public string PasswordResetURLInternal { get; set; }
    }
}
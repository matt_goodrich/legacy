﻿/*******************************************************************************
* Name
*   Home Index ViewModel
*
* Identifier
*   Filename: HomeIndexViewModel.cs
*   
* Purpose
*   This set of classes provides the ability to create strongly typed views
*   that allow object oriented views to be created and binded.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Web;

#endregion

namespace LoginApp.ViewModels
{
    /// <summary>
    /// The type of object that will be returned to create the strongly typed view
    /// </summary>
    public class HomeIndexViewModel
    {
        public LoginForm Form { get; set; }
    }

    /// <summary>
    /// An object type that stores the username and password getters and setters
    /// </summary>
    public class LoginForm
    {
        /// <summary>
        /// Gets or sets the user ID.
        /// </summary>
        /// <value>
        /// The user ID.
        /// </value>
        [DisplayName("User ID")]
        public string UserID { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        [DisplayName("Password")]
        public string Password { get; set; }
    }
}
﻿$(document).ready(function () {
    $("input").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            SubmitLogin();
        }
    });

    $("#LoginButton").click(function () {
        SubmitLogin();
    });

    function SubmitLogin() {
        var input = $("<input>").attr("type", "hidden").attr("name", "Command").val("Login");
        $("#LoginForm").append($(input));
        $("#LoginForm").submit();
    }

    $("#ChangePassButton").click(function () {
        var input = $("<input>").attr("type", "hidden").attr("name", "Command").val("Reset your Password");
        $("#LoginForm").append($(input));
        $("#LoginForm").submit();
    });

    $(".defaultText").focus(function (srcc) {
        if ($(this).val() == $(this)[0].title) {
            $(this).removeClass("defaultTextActive");
            $(this).val("");
        }
    });

    $(".defaultText").blur(function () {
        if ($(this).val() == "") {
            $(this).addClass("defaultTextActive");
            $(this).val($(this)[0].title);
        }
    });

    $(".defaultText").blur();

    $("#LoginForm").submit(function () {
        $(".defaultText").each(function () {
            if ($(this).val() == $(this)[0].title) {
                $(this).val("");
            }
        });
    });
});
﻿/*******************************************************************************
* Name
*   Log4Net Extender
*
* Identifier
*   Filename: Log4NetExtender.cs
*   
* Purpose
*   This class provides a custom layout for the Log4Net Audit Log
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Web;
    using log4net.Layout.Pattern;

#endregion

namespace Log4NetExtenstion
{
    /// <summary>
    /// Writes the users IP address to the Audit Log
    /// </summary>
    public class IPPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current != null)
            {
                writer.Write(HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]);
            }
        }
    }

    /// <summary>
    /// Writes the current user from the session to the Audit Log
    /// </summary>
    public class SubjectPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current.Session["Subject"] != null)
            {
                writer.Write(HttpContext.Current.Session["Subject"]);
            }
        }
    }

    /// <summary>
    /// Writes the URL to the Audit Log
    /// </summary>
    public class UrlPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current != null)
                writer.Write(HttpContext.Current.Request.Url.AbsoluteUri);
        }
    }

    /// <summary>
    /// Writes the Connection ID from the session to the Audit Log
    /// </summary>
    public class ConnectionIDPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current.Session["ConnectionID"] != null)
            {
                writer.Write(HttpContext.Current.Session["ConnectionID"]);
            }
        }
    }

    /// <summary>
    /// Writes the current Machine Name to the Audit Log
    /// </summary>
    public class MachinePatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            writer.Write(Environment.MachineName);
        }
    }

    /// <summary>
    /// Writes the status from the session to the Audit Log. Success or Failure
    /// </summary>
    public class StatusPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current.Session["Status"] != null)
            {
                writer.Write(HttpContext.Current.Session["Status"]);
            }
        }
    }
}

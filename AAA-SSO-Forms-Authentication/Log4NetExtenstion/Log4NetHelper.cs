﻿/*******************************************************************************
* Name
*   Log4Net Helper
*
* Identifier
*   Filename: Log4NetHelper.cs
*   
* Purpose
*   This class contains methods to assist in writing to the session. The values
*   in the session will be extracted by the Log4NetExtender class.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;

#endregion

namespace Log4NetExtenstion
{
    /// <summary>
    /// This class contains methods to assist in writing to the session. The values 
    /// in the session will be extracted by the Log4NetExtender class.
    /// </summary>
    public class Log4NetHelper
    {
        /// <summary>
        /// Writes to session.
        /// </summary>
        /// <param name="SessionID">The session ID.</param>
        /// <param name="SessionValue">The session value.</param>
        public static void WriteToSession(string SessionID, string SessionValue)
        {
            HttpContext.Current.Session[SessionID] = SessionValue;
        }

        /// <summary>
        /// Clears the session data.
        /// </summary>
        /// <param name="SessionID">The session ID.</param>
        public static void ClearSessionData(string SessionID)
        {
            HttpContext.Current.Session[SessionID] = null;
        }
    }
}

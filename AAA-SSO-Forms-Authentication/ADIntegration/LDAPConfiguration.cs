﻿/*******************************************************************************
* Name
*   LDAP Configuration
*
* Identifier
*   Filename: LDAPConfiguration.cs
*   
* Purpose
*   The purpose of this class is to act as a data structure for an
*   LDAP connections properties
*
******************************************************************************
*/

#region usings

    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using AESEncryption;
    using Utilities;

#endregion

namespace ADIntegration
{
    /// <summary>
    /// Data structure class to hold and LDAP connections properties
    /// </summary>
    public class LDAPConfiguration
    {
        //Get the path of the config file in the calling application.
        //It is assumed the config file is in the App_Data/Modules directory and is named LDAP.xml
        protected string ConfigFile = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\Modules\\LDAP.xml");

        //Create a new XmlDocument
        protected XmlDocument doc = new XmlDocument();

        private string ID;

        /// <summary>
        /// Initializes a new instance of the <see cref="LDAPConfiguration"/> class.
        /// </summary>
        /// <param name="ID">The ID of the LDAP Connection.</param>
        public LDAPConfiguration(string ID)
        {
            this.ID = ID;
            doc.PreserveWhitespace = true;
            doc.Load(ConfigFile);
        }

        #region Getters/Setters

        /// <summary>
        /// Gets or sets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        public string ConnectionString
        {
            get { return XMLHelper.GetStringByXPath(doc, String.Format("//Item[@ID='{0}']/ConnectionString", ID)); }
            set { XMLHelper.SetStringByXPath(doc, ConfigFile, String.Format("//Item[@ID='{0}']/ConnectionString", ID), value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has a domain policy.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has domain policy; otherwise, <c>false</c>.
        /// </value>
        public bool HasDomainPolicy
        {
            get { return Convert.ToBoolean(XMLHelper.GetStringByXPath(doc, String.Format("//Item[@ID='{0}']/HasDomainPolicy", ID))); }
            set { XMLHelper.SetStringByXPath(doc, ConfigFile, String.Format("//Item[@ID='{0}']/HasDomainPolicy", ID), value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the path of the domain.
        /// </summary>
        /// <value>
        /// The path of the domain.
        /// </value>
        public string Path
        {
            get { return XMLHelper.GetStringByXPath(doc, String.Format("//Item[@ID='{0}']/Path", ID)); }
            set { XMLHelper.SetStringByXPath(doc, ConfigFile, String.Format("//Item[@ID='{0}']/Path", ID), value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this connection will use impersonation.
        /// </summary>
        /// <value>
        ///   <c>true</c> if impersonate; otherwise, <c>false</c>.
        /// </value>
        public bool Impersonate
        {
            get { return Convert.ToBoolean(XMLHelper.GetStringByXPath(doc, String.Format("//Item[@ID='{0}']/Impersonate", ID))); }
            set { XMLHelper.SetStringByXPath(doc, ConfigFile, String.Format("//Item[@ID='{0}']/Impersonate", ID), value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username
        {
            get { return XMLHelper.GetStringByXPath(doc, String.Format("//Item[@ID='{0}']/Username", ID)); }
            set { XMLHelper.SetStringByXPath(doc, ConfigFile, String.Format("//Item[@ID='{0}']/Username", ID), value); }
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password
        {
            get { return Encryption.Decrypt(XMLHelper.GetStringByXPath(doc, String.Format("//Item[@ID='{0}']/Password", ID))); }
            set { XMLHelper.SetStringByXPath(doc, ConfigFile, String.Format("//Item[@ID='{0}']/Password", ID), Encryption.Encrypt(value)); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [use SSL].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use SSL]; otherwise, <c>false</c>.
        /// </value>
        public bool UseSSL
        {
            get { return Convert.ToBoolean(XMLHelper.GetStringByXPath(doc, String.Format("//Item[@ID='{0}']/UseSSL", ID))); }
            set { XMLHelper.SetStringByXPath(doc, ConfigFile, String.Format("//Item[@ID='{0}']/UseSSL", ID), value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the attribute used to logon.
        /// </summary>
        /// <value>
        /// The attribute used to logon.
        /// </value>
        public string LoginAttribute
        {
            get { return XMLHelper.GetStringByXPath(doc, String.Format("//Item[@ID='{0}']/LoginAttribute", ID)); }
            set { XMLHelper.SetStringByXPath(doc, ConfigFile, String.Format("//Item[@ID='{0}']/LoginAttribute", ID), value); }
        }

        #endregion
    }
}

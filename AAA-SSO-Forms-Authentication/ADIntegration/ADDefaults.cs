﻿/*******************************************************************************
* Name
*   Active Directory Defaults
*
* Identifier
*   Filename: ADDefaults.cs
*   
* Purpose
*   Class to control default values used within the Active Directory Integration Library
*
******************************************************************************
*/
#region usings

    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Utilities;

#endregion

namespace ADIntegration
{
    /// <summary>
    /// Class to control default values used within the Active Directory Integration Library
    /// </summary>
    public class ADDefaults
    {
        //Get the path of the config file in the calling application.
        //It is assumed the config file is in the App_Data/SystemDefaults directory and is named ADDefaults.xml
        protected string ConfigFile = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\SystemDefaults\\ADDefaults.xml");

        //Create a new XmlDocument
        protected XmlDocument doc = new XmlDocument();

        /// <summary>
        /// Initializes a new instance of the <see cref="ADDefaults"/> class.
        /// </summary>
        public ADDefaults()
        {
            doc.PreserveWhitespace = true;
            doc.Load(ConfigFile);
        }

        #region Getters/Setters

        /// <summary>
        /// Gets or sets the account expiration default value in days.
        /// </summary>
        /// <value>
        /// The default account expiration value in days.
        /// </value>
        public int AccountExpiration
        {
            get { return Convert.ToInt32(XMLHelper.GetString(doc, "AccountExpiration")); }
            set { XMLHelper.SetString(doc, ConfigFile, "AccountExpiration", value.ToString()); }
        }

        /// <summary>
        /// Gets or sets the password expiration default value in days.
        /// </summary>
        /// <value>
        /// The default password expiration value in days.
        /// </value>
        public int PasswordExpiration
        {
            get { return Convert.ToInt32(XMLHelper.GetString(doc, "PasswordExpiration")); }
            set { XMLHelper.SetString(doc, ConfigFile, "PasswordExpiration", value.ToString()); }
        }

        #endregion
    }
}

﻿/*******************************************************************************
* Name
*   Domain Policy
*
* Identifier
*   Filename: DomainPolicy.cs
*   
* Purpose
*   This class contains the methods to grab the domain security policy as it 
*   relates to users authenticating.
*
******************************************************************************
*/

#region usings

    using System;
    using System.DirectoryServices;
    using System.Text;
    using log4net;

#endregion

namespace ADIntegration
{
    [Flags]
    public enum PasswordPolicy
    {
        DOMAIN_PASSWORD_COMPLEX = 1,
        DOMAIN_PASSWORD_NO_ANON_CHANGE = 2,
        DOMAIN_PASSWORD_NO_CLEAR_CHANGE = 4,
        DOMAIN_LOCKOUT_ADMINS = 8,
        DOMAIN_PASSWORD_STORE_CLEARTEXT = 16,
        DOMAIN_REFUSE_PASSWORD_CHANGE = 32
    }

    /// <summary>
    /// This class contains the methods to grab the domain security policy as it relates to users authenticating.
    /// </summary>
    public class DomainPolicy
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        ResultPropertyCollection attribs;

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainPolicy"/> class.
        /// This is used to query Active Directory for the domain security policy. 
        /// It grabs a collection of attributes that determine password policies.
        /// </summary>
        /// <param name="domainRoot">The root at which to search for the policies</param>
        public DomainPolicy(DirectoryEntry domainRoot)
        {
            ServerLog.Info("DomainPolicy Constructor called");
            string[] policyAttributes = new string[] { "maxPwdAge", "minPwdAge", "minPwdLength", "lockoutDuration", "lockOutObservationWindow", "lockoutThreshold", "pwdProperties", "pwdHistoryLength", "objectClass", "distinguishedName" };

            DirectorySearcher ds = new DirectorySearcher(domainRoot, "(objectClass=domainDNS)", policyAttributes, SearchScope.Base);
            SearchResult result;
            try
            {
                ServerLog.Debug("Attempting to retrive domain security policy");
                result = ds.FindOne();
                this.attribs = result.Properties;
            }
            catch (Exception ex) { ServerLog.Error("Exception thrown while retrieving domain security policy", ex); }
        }

        /// <summary>
        /// This "getter" determines the Maximum password
        /// age as per the password policy
        /// </summary>
        /// <returns>A Timespan representing the maximum password age</returns>
        public TimeSpan MaxPasswordAge
        {
            get
            {
                ServerLog.Info("Get MaxPasswordAge called");
                string val = "maxPwdAge";
                if (this.attribs != null)
                {
                    if (this.attribs.Contains(val))
                    {
                        long ticks = GetAbsValue(
                          this.attribs[val][0]
                          );

                        if (ticks > 0)
                        {
                            ServerLog.DebugFormat("MaxPasswordAge found. Returning \"{0}\" days.", TimeSpan.FromTicks(ticks).TotalDays);
                            return TimeSpan.FromTicks(ticks);
                        }
                    }
                }

                return TimeSpan.MaxValue;
            }
        }

        /// <summary>
        /// Gets the password properties.
        /// </summary>
        public PasswordPolicy PasswordProperties
        {
            get
            {
                ServerLog.Info("Get PasswordProperties called");
                string val = "pwdProperties";
                //this should fail if not found
                ServerLog.DebugFormat("PasswordProperties found. Returning \"{0}\"", (PasswordPolicy)this.attribs[val][0]);
                return (PasswordPolicy)this.attribs[val][0];
            }
        }

        /// <summary>
        /// This is a helper function used to invert numbers.
        /// Intervals are stored as negative numbers.
        /// </summary>
        /// <param name="longInt">The value that needs to be inverted.</param>
        /// <returns>The inverted value as a long</returns>
        private long GetAbsValue(object longInt)
        {
            ServerLog.Info("GetAbsValue called");
            ServerLog.DebugFormat("Returning AbsValue of \"{0}\"", Math.Abs((long)longInt));
            return Math.Abs((long)longInt);
        }

    }
}

﻿/*******************************************************************************
* Name
*   Active Directory Authentication
*
* Identifier
*   Filename: ADAuth.cs
*   
* Purpose
*   This class contains the methods to provide forms based authentication
*   against Active Directory
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.DirectoryServices;
    using System.DirectoryServices.AccountManagement;
    using System.Security.Principal;
    using System.Text;
    using log4net;
    using Log4NetExtenstion;

#endregion

namespace ADIntegration
{
    /// <summary>
    /// This class contains the methods to provide forms based authentication against Active Directory
    /// </summary>
    public class ADAuth
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");
        private static readonly ILog AuditLog = LogManager.GetLogger("AuditLog");

        const int UF_DONT_EXPIRE_PASSWD = 0x10000;

        protected DirectoryEntry user;
        protected ADDefaults Defaults = new ADDefaults();
        protected LDAPConfiguration LDAPConfig;
        protected DomainPolicy policy;

        public string LDAPName;
        public string UserID;

        /// <summary>
        /// Initializes a new instance of the <see cref="ADAuth"/> class.
        /// </summary>
        /// <param name="userID">The user ID.</param>
        /// <param name="LDAPID">The LDAPID.</param>
        public ADAuth(string userID, string LDAPID) : this(userID, LDAPID, "")
        {
            
        }

        public ADAuth(string userID, string LDAPID, string UserAttribute)
        {
            ServerLog.InfoFormat("ADAuth Constructor called with parameters: userID = \"{0}\", LDAPID = \"{1}\", UserAttribute = \"{2}\"", userID, LDAPID, UserAttribute);
            UserID = userID;
            LDAPName = LDAPID;

            LDAPConfig = new LDAPConfiguration(LDAPName);

            //If the LDAP is setup for impersonation
            if (LDAPConfig.Impersonate)
            {
                WindowsIdentity newId = WindowsIdentity.GetCurrent();
                WindowsImpersonationContext impersonatedUser = newId.Impersonate();

                ServerLog.InfoFormat("{0} configured for impersonation, running as \"{1}\"", LDAPName, newId.Name);

                //If the LDAP has a Domain Policy
                if (LDAPConfig.HasDomainPolicy)
                {
                    ServerLog.InfoFormat("{0} has a domain policy", LDAPName);
                    policy = new DomainPolicy(new DirectoryEntry(String.Format("{0}/{1}", LDAPConfig.ConnectionString, LDAPConfig.Path)));
                }
            }
            else //No impersonation for this connection
            {
                //If the LDAP has a Domain Policy
                if (LDAPConfig.HasDomainPolicy)
                {
                    ServerLog.InfoFormat("{0} has a domain policy", LDAPName);
                    policy = new DomainPolicy(new DirectoryEntry(String.Format("{0}/{1}", LDAPConfig.ConnectionString, LDAPConfig.Path), LDAPConfig.Username, LDAPConfig.Password, AuthenticationTypes.FastBind | (LDAPConfig.UseSSL ? AuthenticationTypes.SecureSocketsLayer : AuthenticationTypes.Secure)));
                }
            }

            user = FindUser(userID, new List<string>(), (String.IsNullOrEmpty(UserAttribute) ? LDAPConfig.LoginAttribute : UserAttribute));
        }

        #region FindUser

        /// <summary>
        /// This method finds a user in Active Directory. 
        /// The search is done based on the user ID supplied in the authentication 
        /// attempt. We search the directory based on the specified
        /// attribute. If a user is found we return a DirecotryEntry object with the 
        /// specified properties, if no user is found a null object is returned.
        /// </summary>
        /// <param name="userID">The user ID to search for</param>
        /// <param name="PropertiesToLoad">The AD attributes that need to be returned</param>
        /// <returns>A Directory that is either null or contains a user</returns>
        public DirectoryEntry FindUser(string userID, List<string> PropertiesToLoad)
        {
            ServerLog.InfoFormat("FindUser called with parameters: userID = \"{0}\", PropertiesToLoad = \"{1}\"", userID, String.Join(", ", PropertiesToLoad.ToArray()));

            return FindUser(userID, PropertiesToLoad, LDAPConfig.LoginAttribute);
        }

        /// <summary>
        /// This method finds a user in Active Directory. 
        /// The search is done based on the user ID supplied in the authentication 
        /// attempt. We search the directory based on the specified
        /// attribute. If a user is found we return a DirecotryEntry object with the 
        /// specified properties, if no user is found a null object is returned.
        /// </summary>
        /// <param name="userID">The user ID to search for</param>
        /// <param name="PropertiesToLoad">The AD attributes that need to be returned</param>
        /// <param name="attribute">The AD attribute that will be used to find the userID</param>
        /// <returns>A Directory that is either null or contains a user</returns>
        public DirectoryEntry FindUser(string userID, List<string> PropertiesToLoad, string attribute)
        {
            ServerLog.InfoFormat("FindUser called with parameters: userID = \"{0}\", PropertiesToLoad = \"{1}\", attribute = \"{2}\"", userID, String.Join(", ", PropertiesToLoad.ToArray()), attribute);
            DirectoryEntry v_searchRoot;

            //If LDAP is configured for Impersonation
            if (LDAPConfig.Impersonate)
            {
                WindowsIdentity newId = WindowsIdentity.GetCurrent();
                WindowsImpersonationContext impersonatedUser = newId.Impersonate();
                ServerLog.InfoFormat("{0} configured for impersonation, running as \"{1}\"", LDAPName, newId.Name);
                v_searchRoot = new DirectoryEntry(LDAPConfig.ConnectionString);
            }
            else //No impersonation for this connection
                v_searchRoot = new DirectoryEntry(LDAPConfig.ConnectionString, LDAPConfig.Username, LDAPConfig.Password, AuthenticationTypes.FastBind | (LDAPConfig.UseSSL ? AuthenticationTypes.SecureSocketsLayer : AuthenticationTypes.Secure));

            DirectorySearcher v_findUser = new DirectorySearcher(v_searchRoot, String.Format("({0}={1})", attribute, userID));

            //Add additional properties we will need to validate a user can authenticate
            v_findUser.PropertiesToLoad.Add("userPrincipalName");
            v_findUser.PropertiesToLoad.Add("pwdLastSet");
            v_findUser.PropertiesToLoad.Add("userAccountControl");
            v_findUser.PropertiesToLoad.Add("disginguishedName");
            v_findUser.PropertiesToLoad.Add("accountExpires");
            v_findUser.PropertiesToLoad.Add("lockoutTime");

            //Add any additional attributes specified in the PropertiesToLoad data structure to the list of properties to be returned
            foreach (string str in PropertiesToLoad)
                v_findUser.PropertiesToLoad.Add(str);

            SearchResult v_result = null;

            //Find the individual user
            try
            {
                ServerLog.DebugFormat("Attempting to find user \"{0}\"", userID);
                v_result = v_findUser.FindOne();
            }
            catch (Exception ex) { ServerLog.Error("Exception thrown while finding user", ex); }

            v_searchRoot.Close();

            return (v_result != null ? v_result.GetDirectoryEntry() : new DirectoryEntry());
        }

        #endregion

        #region Getters

        /// <summary>
        /// Gets the name of the user principal.
        /// </summary>
        /// <returns></returns>
        public string GetUserPrincipalName()
        {
            try
            {
                ServerLog.DebugFormat("GetUserPrincipalName called. Returning \"{0}\"", user.Properties["userPrincipalName"][0].ToString());
                return user.Properties["userPrincipalName"][0].ToString();
            }
            catch (Exception ex)
            {
                ServerLog.DebugFormat("Exception while getting UserPrincipalName", ex);
                return "";
            }
        }

        /// <summary>
        /// Gets the groups for the current user.
        /// </summary>
        /// <returns>A list of strings that contain the group names</returns>
        public List<string> GetGroups()
        {
            List<string> result = new List<string>();
            PrincipalContext Domain = new PrincipalContext(ContextType.Domain);
            
            UserPrincipal User = UserPrincipal.FindByIdentity(Domain, IdentityType.UserPrincipalName, GetUserPrincipalName());
            
            if (user != null)
            {
                PrincipalSearchResult<Principal> Groups = User.GetAuthorizationGroups();

                foreach (Principal p in Groups)
                {
                    if (p is GroupPrincipal)
                        result.Add(p.Name);
                }
            }
            return result;
        }

        #endregion

        #region Logon

        /// <summary>
        /// This function does the authentication for the specified user against Active Directory.
        /// </summary>
        /// <param name="Password">Password as entered into the login form</param>
        /// <returns>Boolean value of whether or not the user was authenticated</returns>
        public bool LogonUser(string Password)
        {
            ServerLog.Info("LogonUser called");
            //Create a new DirectoryEntry object and connect as the user attempting to authenticate - 
            //this is connecting to the correct LDAP using a ternary function based on the external variable
            DirectoryEntry de = new DirectoryEntry(LDAPConfig.ConnectionString, GetUserPrincipalName(), Password, AuthenticationTypes.FastBind | (LDAPConfig.UseSSL ? AuthenticationTypes.SecureSocketsLayer : AuthenticationTypes.Secure));
            Log4NetHelper.WriteToSession("Subject", UserID);
            try
            {
                if (de.Properties.Count > 0)
                {
                    //This will force a bind/authentication
                    Object obj = de.NativeObject;
                    //If we do not throw an exception, the user has authenticated and we can return true
                    de.Close();
                    ServerLog.DebugFormat("\"{0}\" authenticated. UserPrincipalName = \"{1}\"", UserID, de.Username);
                    Log4NetHelper.WriteToSession("Status", "Success");
                    AuditLog.Debug("");
                }
                return true;
            }
            catch (Exception ex)
            {
                ServerLog.Error("Exception thrown while authenticating user", ex);
                Log4NetHelper.WriteToSession("Status", "Failure");
                AuditLog.Debug("");
                //An exception was thrown meaning the user could not be authenticated, we can return false
                de.Close();
                return false;
            }
        }

        #endregion

        #region Expirations

        /// <summary>
        /// This is a helper method that will return a DateTime of when the password is set to
        /// expire based on the domain security policy. It will throw an error if the user does
        /// not have a password.
        /// </summary>
        /// <param name="user">A DirectoryEntry of the user we are checking the password for</param>
        /// <returns>A DateTime of when the password expires</returns>
        public DateTime GetPasswordExpiration()
        {
            ServerLog.Info("GetPasswordExpiration called");
            long ticks = GetInt64("pwdLastSet");

            //User must change password at next login
            if (ticks == 0 || ticks == -1)
            {
                return DateTime.MinValue;
            }

            //Get when the user last set their password
            DateTime pwdLastSet = DateTime.FromFileTime(ticks);

            //Use our policy class to determine when it will expire
            try
            {
                ServerLog.DebugFormat("Returning Password Expiration for user \"{0}\" of \"{1}\"", UserID, pwdLastSet.Add(policy.MaxPasswordAge));
                return pwdLastSet.Add(policy.MaxPasswordAge);
            }
            catch (Exception ex)
            {
                ServerLog.Error(String.Format("Exception thrown while determining password expiration. Returning \"{0}\"", pwdLastSet.AddDays(Defaults.PasswordExpiration)), ex);
                return pwdLastSet.AddDays(Defaults.PasswordExpiration);
            }
        }

        /// <summary>
        /// This is a helper method that will return a DateTime of when the account is set to expire
        /// </summary>
        /// <param name="user">A directory entry of the user to get the account expiration for</param>
        /// <returns>A DateTime of when the account is set to expire</returns>
        public DateTime GetAccountExpiration()
        {
            ServerLog.Info("GetAccountExpiration called");
            try
            {
                //Get the accountExpires property
                Int64 ldapdatetime = GetInt64("accountExpires");
                if (!ldapdatetime.Equals(Int64.MaxValue) && ldapdatetime != 0)
                {
                    ServerLog.DebugFormat("Returning Account Expiration for user \"{0}\" of \"{1}\"", UserID, DateTime.FromFileTime(ldapdatetime));
                    return DateTime.FromFileTime(ldapdatetime);
                }
            }
            catch (Exception ex) 
            {
                ServerLog.Error(String.Format("Exception thrown while determining password expiration. Returning \"{0}\"", DateTime.Now.AddDays(Defaults.AccountExpiration)), ex);
            }
            //Return a default
            return DateTime.Now.AddDays(Defaults.AccountExpiration);
        }

        /// <summary>
        /// Determines whether [is account disabled].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is account disabled]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsAccountDisabled()
        {
            ServerLog.Info("IsAccountDisabled called");
            //If this is Active Directory
            if (LDAPConfig.HasDomainPolicy)
            {
                int uac = Convert.ToInt32(user.Properties["userAccountControl"][0]);
                const int ADS_UF_ACCOUNTDISABLE = 0x00000002;
                ServerLog.DebugFormat("Using userAccountControl to determine if account is disabled for user \"{0}\", result \"{1}\"", UserID, (uac & ADS_UF_ACCOUNTDISABLE) == ADS_UF_ACCOUNTDISABLE);
                return (uac & ADS_UF_ACCOUNTDISABLE) == ADS_UF_ACCOUNTDISABLE;
            }
            else
            {
                try
                {
                    ServerLog.DebugFormat("Using msDS-UserAccountDisabled to determine if account is disabled for user \"{0}\", result \"{1}\"", UserID, Convert.ToBoolean(user.Properties["msDS-UserAccountDisabled"].Value));
                    return Convert.ToBoolean(user.Properties["msDS-UserAccountDisabled"].Value);
                }
                catch (Exception ex) { ServerLog.Error("Exception thrown while determining if the account is disabled. Returning \"false\"", ex); }
            }
            return false;
        }

        /// <summary>
        /// Determines whether [is account locked].
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is account locked]; otherwise, <c>false</c>.
        /// </returns>
        public bool IsAccountLocked()
        {
            ServerLog.Info("IsAccountLocked called");
            if (LDAPConfig.HasDomainPolicy)
            {
                int uac = Convert.ToInt32(user.Properties["userAccountControl"][0]);
                const int ADS_UF_LOCKOUT = 0x00000010;
                ServerLog.DebugFormat("Using userAccountControl to determine if account is locked for user \"{0}\", result \"{1}\"", UserID, (uac & ADS_UF_LOCKOUT) == ADS_UF_LOCKOUT);
                return (uac & ADS_UF_LOCKOUT) == ADS_UF_LOCKOUT;
            }
            else
            {
                long lockouttime = GetInt64("lockoutTime");
                if (lockouttime == 0)
                {
                    ServerLog.DebugFormat("Using lockoutTime to determine if account is locked for user \"{0}\", result \"false\"", UserID);
                    return false;
                }
                else
                {
                    ServerLog.DebugFormat("Using lockoutTime to determine if account is locked for user \"{0}\", result \"true\"", UserID);
                    return true;
                }
            }
        }

        #endregion

        #region FoundUser

        /// <summary>
        /// Returns a boolean based upon whether the user was found or not
        /// </summary>
        /// <returns></returns>
        public bool FoundUser()
        {
            ServerLog.Info("FoundUser called");
            if (user != null)
            {
                if (GetUserPrincipalName().Trim().Length > 0)
                {
                    ServerLog.DebugFormat("User \"{0}\" found. Returning \"true\".", UserID);
                    return true;
                }
                return false;
            }
            ServerLog.DebugFormat("User \"{0}\" not found. Returning \"false\".", UserID);
            return false;
        }

        #endregion

        #region HelperMethods

        /// <summary>
        /// This is a private helper method that will return a Unicode byte array of the password
        /// </summary>
        /// <param name="entry">The DirectoryEntry that has a connection to the PC LDS.</param>
        /// <param name="attr">The attribute in AD that we would like to get the Int64 from</param>
        /// <returns>An Int64 of the requested value</returns>
        private Int64 GetInt64(string attr)
        {
            ServerLog.Info("GetInt64 called");
            //we will use the marshaling behavior of
            //the searcher
            DirectorySearcher ds = new DirectorySearcher(user, String.Format("({0}=*)", attr), new string[] { attr }, System.DirectoryServices.SearchScope.Base);

            SearchResult sr = null;

            try
            {
                 sr = ds.FindOne();
            }
            catch (Exception ex) { ServerLog.Error(String.Format("Exception while getting attribute value \"{0}\" for user \"{1}\"", attr, UserID), ex); }

            if (sr != null)
            {
                if (sr.Properties.Contains(attr))
                {
                    return (Int64)sr.Properties[attr][0];
                }
            }
            return -1;
        }

        #endregion
    }
}

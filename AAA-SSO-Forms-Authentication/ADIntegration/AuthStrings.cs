﻿/*******************************************************************************
* Name
*   Authentication Strings
*
* Identifier
*   Filename: AuthStrings.cs
*   
* Purpose
*   Class to control strings used within the Active Directory Integration Library
*
******************************************************************************
*/
#region usings

    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using Utilities;

#endregion

namespace ADIntegration
{
    /// <summary>
    /// Class to control strings used within the Active Directory Integration Library
    /// </summary>
    public class AuthStrings
    {
        //Get the path of the config file in the calling application.
        //It is assumed the config file is in the App_Data/SystemText directory and is named AuthStrings.xml
        protected string ConfigFile = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\SystemText\\AuthStrings.xml");
        
        //Create a new XmlDocument
        protected XmlDocument doc = new XmlDocument();

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthStrings"/> class.
        /// </summary>
        public AuthStrings()
        {
            doc.PreserveWhitespace = true;
            doc.Load(ConfigFile);
        }

        #region Getters/Setters

        /// <summary>
        /// Gets or sets the no resource string.
        /// </summary>
        /// <value>
        /// The no resource string.
        /// </value>
        public string NoResource
        {
            get { return XMLHelper.GetString(doc, "NoResource"); }
            set { XMLHelper.SetString(doc, ConfigFile, "NoResource", value); }
        }

        /// <summary>
        /// Gets or sets the no username string.
        /// </summary>
        /// <value>
        /// The no username string.
        /// </value>
        public string NoUsername
        {
            get { return XMLHelper.GetString(doc, "NoUsername"); }
            set { XMLHelper.SetString(doc, ConfigFile, "NoUsername", value); }
        }

        /// <summary>
        /// Gets or sets the no password string.
        /// </summary>
        /// <value>
        /// The no password string.
        /// </value>
        public string NoPassword
        {
            get { return XMLHelper.GetString(doc, "NoPassword"); }
            set { XMLHelper.SetString(doc, ConfigFile, "NoPassword", value); }
        }

        /// <summary>
        /// Gets or sets the account expired string.
        /// </summary>
        /// <value>
        /// The account expired string.
        /// </value>
        public string AccountExpired
        {
            get { return XMLHelper.GetString(doc, "AccountExpired"); }
            set { XMLHelper.SetString(doc, ConfigFile, "AccountExpired", value); }
        }

        /// <summary>
        /// Gets or sets the password expired string.
        /// </summary>
        /// <value>
        /// The password expired string.
        /// </value>
        public string PasswordExpired
        {
            get { return XMLHelper.GetString(doc, "PasswordExpired"); }
            set { XMLHelper.SetString(doc, ConfigFile, "PasswordExpired", value); }
        }

        /// <summary>
        /// Gets or sets the password expires today string.
        /// </summary>
        /// <value>
        /// The password expires today string.
        /// </value>
        public string PasswordExpiresToday
        {
            get { return XMLHelper.GetString(doc, "PasswordExpiresToday"); }
            set { XMLHelper.SetString(doc, ConfigFile, "PasswordExpiresToday", value); }
        }

        /// <summary>
        /// Gets or sets the password expires soon string.
        /// </summary>
        /// <value>
        /// The password expires soon string.
        /// </value>
        public string PasswordExpiresSoon
        {
            get { return XMLHelper.GetString(doc, "PasswordExpiresSoon"); }
            set { XMLHelper.SetString(doc, ConfigFile, "PasswordExpiresSoon", value); }
        }

        /// <summary>
        /// Gets or sets the account disabled string.
        /// </summary>
        /// <value>
        /// The account disabled string.
        /// </value>
        public string AccountDisabled
        {
            get { return XMLHelper.GetString(doc, "AccountDisabled"); }
            set { XMLHelper.SetString(doc, ConfigFile, "AccountDisabled", value); }
        }

        /// <summary>
        /// Gets or sets the account locked string.
        /// </summary>
        /// <value>
        /// The account locked string.
        /// </value>
        public string AccountLocked
        {
            get { return XMLHelper.GetString(doc, "AccountLocked"); }
            set { XMLHelper.SetString(doc, ConfigFile, "AccountLocked", value); }
        }

        /// <summary>
        /// Gets or sets the invalid username string.
        /// </summary>
        /// <value>
        /// The invalid username string.
        /// </value>
        public string InvalidUsername
        {
            get { return XMLHelper.GetString(doc, "InvalidUsername"); }
            set { XMLHelper.SetString(doc, ConfigFile, "InvalidUsername", value); }
        }

        /// <summary>
        /// Gets or sets the invalid password string.
        /// </summary>
        /// <value>
        /// The invalid password string.
        /// </value>
        public string InvalidPassword
        {
            get { return XMLHelper.GetString(doc, "InvalidPassword"); }
            set { XMLHelper.SetString(doc, ConfigFile, "InvalidPassword", value); }
        }

        #endregion
    }
}

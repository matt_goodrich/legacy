﻿/*******************************************************************************
* Name
*   Configuration Helper
*
* Identifier
*   Filename: ConfigurationHelper.cs
*   
* Purpose
*   The purpose of this class is to provide an interface to the configuration items
*   in the pingfederate-idp-config.props file
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using log4net;

#endregion

namespace PingIntegration
{
    /// <summary>
    /// The purpose of this class is to provide an interface to the configuration items 
    /// in the pingfederate-idp-config.props file
    /// </summary>
    class ConfigurationHelper
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        public IDictionary ConfigTable;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationHelper"/> class.
        /// </summary>
        /// <param name="ConfigPath">The config path.</param>
        public ConfigurationHelper(string ConfigPath)
        {
            ServerLog.InfoFormat("ConfigurationHelper Constructor called with arguments: ConfigPath = \"{0}\"", ConfigPath);
            
            ConfigTable = GetConfigProps(ConfigPath);

            if (ConfigTable == null)
                throw new OpenTokenException("Invalid config-file");
        }

        /// <summary>
        /// Gets the config props.
        /// </summary>
        /// <param name="ConfigPath">The config path.</param>
        /// <returns></returns>
        private IDictionary GetConfigProps(string ConfigPath)
        {
            ServerLog.InfoFormat("GetConfigProps called with arguments: ConfigPath = \"{0}\"", ConfigPath);
            return LoadConfig((TextReader)new StreamReader(ConfigPath));
        }

        /// <summary>
        /// Loads the config.
        /// </summary>
        /// <param name="properties">The properties.</param>
        /// <returns></returns>
        private IDictionary LoadConfig(TextReader properties)
        {
            ServerLog.Info("LoadConfig called");
            try
            {
                Hashtable hashtable = new Hashtable();
                int counter = 0;
                string str1;
                while ((str1 = properties.ReadLine()) != null)
                {
                    ++counter;
                    string line = str1.TrimStart(new char[0]);
                    if (line.Length != 0 && !line.StartsWith("#") && !line.StartsWith("!"))
                    {
                        int equalSignIndex = this.GetEqualSignIndex(line, counter);
                        char[] key1 = line.ToCharArray(0, equalSignIndex);
                        char[] key2 = line.Substring(equalSignIndex + 1).TrimStart(new char[0]).ToCharArray();
                        string str2 = this.Replace(key1);
                        string str3 = this.Replace(key2);
                        hashtable.Add((object)str2, (object)str3);
                    }
                }
                return (IDictionary)hashtable;
            }
            finally
            {
                if (properties != null)
                    properties.Close();
            }
        }

        /// <summary>
        /// Replaces the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private string Replace(char[] key)
        {
            ServerLog.InfoFormat("Replace called with value: key = \"{0}\"", key);
            char[] chArray = new char[key.Length];
            int length = 0;
            bool flag = false;
            for (int index = 0; index < key.Length; ++index)
            {
                char ch = key[index];
                if ((int)ch == 92 && !flag)
                {
                    flag = true;
                }
                else
                {
                    chArray[length++] = ch;
                    flag = false;
                }
            }
            return new string(chArray, 0, length);
        }

        /// <summary>
        /// Gets the index of the equal sign.
        /// </summary>
        /// <param name="line">The line.</param>
        /// <param name="counter">The counter.</param>
        /// <returns></returns>
        private int GetEqualSignIndex(string line, int counter)
        {
            ServerLog.InfoFormat("GetEqualsSignIndex called with values: line = \"{0}\", counter = \"{1}\"", line, counter);
            int num;
            do
            {
                num = line.IndexOf('=');
                switch (num)
                {
                    case -1:
                        string message = "Wrong configuration file format in line " + (object)counter;
                        throw new OpenTokenException(message);
                    case 0:
                        return 0;
                    default:
                        continue;
                }
            }
            while ((int)line[num - 1] == 92);
            return num;
        }
    }
}

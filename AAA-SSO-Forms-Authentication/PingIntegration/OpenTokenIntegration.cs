﻿/*******************************************************************************
* Name
*   Open Token Integration
*
* Identifier
*   Filename: OpenTokenIntegration.cs
*   
* Purpose
*   The purpose of this class is to provide an easy to use interface for OpenTokens
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Web;
    using opentoken;
    using opentoken.util;
    using log4net;

#endregion

namespace PingIntegration
{
    /// <summary>
    /// The purpose of this class is to provide an easy to use interface for OpenTokens
    /// </summary>
    public class OpenTokenIntegration
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        //Get the path of the config file in the calling application.
        //It is assumed the config file is in the App_Data/PingFederate directory and is named pingfederate-idp-config.props
        private string ConfigFile = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\PingFederate\\" + Constants.AgentConfigFile);

        //Create a new instance of the MultiStringDictionary used to hold the users information for the Open token
        MultiStringDictionary UserInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenTokenIntegration"/> class.
        /// </summary>
        public OpenTokenIntegration()
        {
            ServerLog.Info("OpenTokenIntegration Constructor Called");
            UserInfo = new MultiStringDictionary();
        }

        /// <summary>
        /// Gets the open token redirect URL.
        /// </summary>
        /// <param name="BaseURL">The base URL.</param>
        /// <returns></returns>
        public string GetOpenTokenRedirect(string BaseURL)
        {
            ServerLog.InfoFormat("GetOpenTokenRedirect called with BaseURL: \"{0}\"", BaseURL);
            HttpResponse Response = new HttpResponse(null);
            Agent agent = new Agent(ConfigFile);
            UrlHelper URL = new UrlHelper(BaseURL);
            agent.WriteToken(UserInfo, Response, URL, false);
            ServerLog.DebugFormat("Returning OpenToken Redirect URL: \"{0}\"", URL.ToString());
            return URL.ToString();
        }

        /// <summary>
        /// Adds the data to open token.
        /// </summary>
        /// <param name="Key">The key.</param>
        /// <param name="Value">The value.</param>
        public void AddDataToOpenToken(string Key, string Value)
        {
            ServerLog.InfoFormat("AddDataToOpenToken called with values: Key = \"{0}\", Value = \"{1}\"", Key, Value);
            UserInfo.Add(Key, Value);
        }

        /// <summary>
        /// Gets the request open token URL.
        /// </summary>
        /// <param name="PartnerIdpId">The partner idp id.</param>
        /// <param name="TargetResource">The target resource.</param>
        /// <returns></returns>
        public string GetRequestOpenTokenURL(string PartnerIdpId, string TargetResource)
        {
            ServerLog.InfoFormat("GetRequestOpenTokenURL called with values: PartnerIdpId = \"{0}\", TargetResource = \"{1}\"", PartnerIdpId, TargetResource);
            Links Links = new Links();
            return String.Format("{0}{1}{2}?{3}={4}&{5}={6}", Links.HostPF, Constants.SPDir, Constants.StartSSO, Constants.PartnerIdPId, PartnerIdpId, Constants.TargetResource, TargetResource);
        }

        /// <summary>
        /// Gets the open token subject.
        /// </summary>
        /// <param name="Request">The request.</param>
        /// <param name="Response">The response.</param>
        /// <returns></returns>
        public string GetOpenTokenSubject(HttpRequest Request, HttpResponse Response)
        {
            ServerLog.Info("GetOpenTokenSubject called with Request and Response");

            Dictionary<string, string> TokenValues = new Dictionary<string,string>();
            Agent agent = new Agent(ConfigFile);
            try
            {
                ServerLog.Debug("Attempting to read OpenToken");
                TokenValues = agent.ReadToken(Request);
                return TokenValues[Constants.Subject];
            }
            catch (TokenException ex)
            {
                agent.DeleteToken(Response);
                ServerLog.Error("Exception caught while reading OpenToken", ex);
                return "";
            }
            catch (TokenExpiredException ex)
            {
                agent.DeleteToken(Response);
                ServerLog.Error("Exception caught while reading OpenToken: Token is Expired", ex);
                return "";
            }
        }
    }
}

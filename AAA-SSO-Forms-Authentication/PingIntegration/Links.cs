﻿/*******************************************************************************
* Name
*   Links
*
* Identifier
*   Filename: Links.cs
*   
* Purpose
*   Provides the URLs of the PingFederate server based upon the supplied configuration
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using log4net;

#endregion

namespace PingIntegration
{
    /// <summary>
    /// Provides the URLs of the PingFederate server based upon the supplied configuration
    /// </summary>
    public class Links
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        //Get the path of the config file in the calling application.
        //It is assumed the config file is in the App_Data/PingFederate directory and is named pingfederate-idp-config.props
        string ConfigFile = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "App_Data\\PingFederate\\pingfederate-idp-config.props");

        ConfigurationHelper Config;

        public static string HostPF;

        protected string _PFHost;
        protected string _TokenType;
        protected bool _UseSSL;
        protected int _HTTPPort;
        protected int _HTTPSPort;

        /// <summary>
        /// Initializes a new instance of the <see cref="Links"/> class.
        /// </summary>
        public Links()
        {
            ServerLog.Info("Links Constructor called");
            
            Config = new ConfigurationHelper(ConfigFile);

            HostPF = !((string)Config.ConfigTable[(object)"useSSL"]).Equals("True") ? "http://" + ((string)Config.ConfigTable[(object)"hostPF"]).Trim() + ":" + ((string)Config.ConfigTable[(object)"httpPort"]).Trim() : "https://" + ((string)Config.ConfigTable[(object)"hostPF"]).Trim() + ":" + ((string)Config.ConfigTable[(object)"httpsPort"]).Trim();

            _PFHost = Config.ConfigTable[(object)"hostPF"].ToString();
            _TokenType = Config.ConfigTable[(object)"tokenType"].ToString();
            _UseSSL = Convert.ToBoolean(Config.ConfigTable[(object)"useSSL"]);
            _HTTPPort = Convert.ToInt32(Config.ConfigTable[(object)"httpPort"]);
            _HTTPSPort = Convert.ToInt32(Config.ConfigTable[(object)"httpsPort"]);

            ServerLog.DebugFormat("PingFederate Host set to: \"{0}\"", HostPF);
        }

        #region Getters/Setters

        /// <summary>
        /// Gets or sets the PF host.
        /// </summary>
        /// <value>
        /// The PF host.
        /// </value>
        public string PFHost
        {
            get { return _PFHost; }
            set { _PFHost = value; }
        }

        /// <summary>
        /// Gets or sets the type of the token.
        /// </summary>
        /// <value>
        /// The type of the token.
        /// </value>
        public string TokenType
        {
            get { return _TokenType; }
            set { _TokenType = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [use SSL].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [use SSL]; otherwise, <c>false</c>.
        /// </value>
        public bool UseSSL
        {
            get { return _UseSSL; }
            set { _UseSSL = value; }
        }

        /// <summary>
        /// Gets or sets the HTTP port.
        /// </summary>
        /// <value>
        /// The HTTP port.
        /// </value>
        public int HTTPPort
        {
            get { return _HTTPPort; }
            set { _HTTPPort = value; }
        }

        /// <summary>
        /// Gets or sets the HTTPS port.
        /// </summary>
        /// <value>
        /// The HTTPS port.
        /// </value>
        public int HTTPSPort
        {
            get { return _HTTPSPort; }
            set { _HTTPSPort = value; }
        }

        #endregion

        #region WriteConfig

        /// <summary>
        /// Writes the configuration to the configuration file.
        /// </summary>
        /// <returns></returns>
        public bool WriteConfig()
        {
            StreamWriter propsWriter = null;

            try
            {
                propsWriter = new StreamWriter(ConfigFile);

                propsWriter.WriteLine(String.Format("hostPF={0}", _PFHost));
                propsWriter.WriteLine(String.Format("tokenType={0}", _TokenType));
                propsWriter.WriteLine(String.Format("useSSL={0}", _UseSSL));
                propsWriter.WriteLine(String.Format("httpPort={0}", _HTTPPort));
                propsWriter.WriteLine(String.Format("httpsPort={0}", _HTTPSPort));

                propsWriter.Flush();
                propsWriter.Close();

                ServerLog.DebugFormat("Writing PingFederate Config: hostPF = \"{0}\", tokenType = \"{1}\", useSSL = \"{2}\", httpPort = \"{3}\", httpsPort = \"{4}\"", PFHost, TokenType, UseSSL, HTTPPort, HTTPSPort);

                return true;
            }
            catch (Exception ex)
            {
                propsWriter.Flush();
                propsWriter.Close();

                ServerLog.Error("Exception caught while updating PingFederate Config", ex);

                return false;
            }
        }

        #endregion
    }
}

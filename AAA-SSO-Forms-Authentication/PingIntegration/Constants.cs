﻿/*******************************************************************************
* Name
*   Constants
*
* Identifier
*   Filename: Constants.cs
*   
* Purpose
*   The purpose of this class is to provide values that are constant across the application
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.Text;

#endregion

namespace PingIntegration
{
    /// <summary>
    /// The purpose of this class is to provide values that are constant across the application
    /// </summary>
    public class Constants
    {
        public const string Subject = "subject";
        public const string ResumePath = "resume";
        public const string AgentConfigFile = "agent-config.txt";
        public const string ForceAuthnParam = "ForceAuthn";
        public const string AuthnContext = "authncontext";
        public const string SPDir= "/sp";
        public const string StartSSO = "/startSSO.ping";
        public const string PartnerIdPId = "PartnerIdpId";
        public const string TargetResource = "TargetResource";
    }
}

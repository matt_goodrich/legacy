﻿/*******************************************************************************
* Name
*   Open Token Exception
*
* Identifier
*   Filename: OpenTokenException.cs
*   
* Purpose
*   Custom Exception Class for Exceptions thrown in the Open Token class library
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.Text;
    using log4net;

#endregion

namespace PingIntegration
{
    /// <summary>
    /// Custom Exception Class for Exceptions thrown in the Open Token class library
    /// </summary>
    [Serializable]
    public class OpenTokenException : Exception
    {
        //Setup our Log4Net Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenTokenException"/> class.
        /// </summary>
        public OpenTokenException()
        {
            ServerLog.Info("OpenTokenException Constructor called");
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenTokenException"/> class.
        /// </summary>
        /// <param name="Message">The message.</param>
        public OpenTokenException(string Message) : base (Message)
        {
            ServerLog.ErrorFormat("OpenTokenException thrown. Error: \"{0}\"", Message);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenTokenException"/> class.
        /// </summary>
        /// <param name="Message">The message.</param>
        /// <param name="InnerException">The inner exception.</param>
        public OpenTokenException(string Message, Exception InnerException) : base(Message, InnerException)
        {
            ServerLog.Error(String.Format("OpenTokenException thrown. Error: \"{0}\"", Message), InnerException);
        }
    }
}

﻿/*******************************************************************************
* Name
*   XML Helper
*
* Identifier
*   Filename: XmlHelper.cs
*   
* Purpose
*   Helper class to read strings from a configuration file
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml;

#endregion

namespace Utilities
{
    /// <summary>
    /// Helper class to read strings from a configuration file
    /// </summary>
    public class XMLHelper
    {
        #region XMLReadWrite

        /// <summary>
        /// Gets the specified string from the configuration file.
        /// </summary>
        /// <param name="doc">The XML Document to search.</param>
        /// <param name="ID">The ID of the string to return.</param>
        /// <returns>
        /// The string representing the passed in ID
        /// </returns>
        public static string GetString(XmlDocument doc, string ID)
        {
            return GetStringByXPath(doc, String.Format("//Item[@ID='{0}']", ID));
        }

        /// <summary>
        /// Gets the specified string from the configuration file by X path.
        /// </summary>
        /// <param name="doc">The XML Document to search.</param>
        /// <param name="XPath">The XPath of the node.</param>
        /// <returns></returns>
        public static string GetStringByXPath(XmlDocument doc, string XPath)
        {
            XmlNode node = doc.SelectSingleNode(String.Format("{0}", XPath));
            if (node != null)
                return node.InnerText;
            else
                return "";
        }

        /// <summary>
        /// Sets the specified string in the configuration file.
        /// </summary>
        /// <param name="doc">The XML Document to search.</param>
        /// <param name="FilePath">The file path to save the XML Document to.</param>
        /// <param name="ID">The ID of the string to update.</param>
        /// <param name="Value">The string to save.</param>
        public static void SetString(XmlDocument doc, string FilePath, string ID, string Value)
        {
            SetStringByXPath(doc, FilePath, String.Format("//Item[@ID='{0}']", ID), Value);
        }

        /// <summary>
        /// Sets the string by X path.
        /// </summary>
        /// <param name="doc">The XML Document to search.</param>
        /// <param name="FilePath">The file path to save the XML Document to.</param>
        /// <param name="XPath">The XPath of the node.</param>
        /// <param name="Value">The string to save.</param>
        public static void SetStringByXPath(XmlDocument doc, string FilePath, string XPath, string Value)
        {
            XmlNode node = doc.SelectSingleNode(String.Format("{0}", XPath));
            if (node != null)
            {
                node.InnerText = Value;
                doc.Save(FilePath);
            }
        }

        #endregion
    }
}

﻿/*******************************************************************************
* Name
*   XML Document Helper
*
* Identifier
*   Filename: XmlDocHelper.cs
*   
* Purpose
*   Helper class to read the passed in configuration file XML document and 
*   return an XML document object.
*
******************************************************************************
*/

#region usings

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml;

#endregion

namespace Utilities
{
    /// <summary>
    /// Helper class to read the passed in configuration file XML document and 
    /// return an XML document object.
    /// </summary>
    public class XMLDocHelper
    {
        //Get the path of the config file in the calling application.
        //It is assumed the config file is in the App_Data/Links directory and is named Links.xml
        protected string ConfigFile;

        //Create a new XmlDocument
        protected XmlDocument doc;

        /// <summary>
        /// Initializes a new instance of the <see cref="XMLDocHelper"/> class.
        /// </summary>
        /// <param name="ConfigPath">The config path.</param>
        public XMLDocHelper(string ConfigPath)
        {
            ConfigFile = ConfigPath;
            doc = new XmlDocument();

            doc.PreserveWhitespace = true;
            doc.Load(ConfigFile);
        }

        /// <summary>
        /// Gets the XML document.
        /// </summary>
        /// <returns></returns>
        public XmlDocument GetXMLDocument()
        {
            return doc;
        }
    }
}

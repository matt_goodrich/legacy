﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Text;

namespace CompareServiceAccounts
{
    class Program
    {
        static void Main(string[] args)
        {
             

            DirectoryEntry root = new DirectoryEntry("LDAP://p01iaw001.ent.rt.csaa.com", "gergood@ent.rt.csaa.com", "WTF123@@");
            DirectorySearcher searcher = new DirectorySearcher(root, "(CN=Counter Terminal*)");
            searcher.PropertiesToLoad.Add("memberOf");

            SearchResultCollection result = null;

            try
            {
                Console.WriteLine("Searching");
                result = searcher.FindAll();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            foreach (SearchResult rs in result)
            {
                using (StreamWriter file = new StreamWriter(@"C:\ServiceAccounts.csv", true))
                {
                    file.WriteLine(rs.Path + ": " + rs.Properties["memberOf"].Count);
                    for (int i = 0; i < rs.Properties["memberOf"].Count; i++)
                    {
                        file.WriteLine("\t::" + rs.Properties["memberOf"][i]);
                    }
                }
                
                //ResultPropertyCollection resultPropColl = rs.Properties;
                //foreach (Object memberColl in resultPropColl["member"])
                //{
                //    Console.WriteLine("Connecting");
                //    DirectoryEntry gpMemberEntry = new DirectoryEntry("LDAP://" + memberColl);
                //    PropertyCollection userProps = gpMemberEntry.Properties;

                //    string attributes = "";

                //    IDictionaryEnumerator ide = userProps.GetEnumerator();
                //    ide.Reset();
                //    while (ide.MoveNext())
                //    {
                //        PropertyValueCollection pvc = ide.Entry.Value as PropertyValueCollection;
                //        attributes += pvc.Value + ",";
                //    }
                //    using (StreamWriter file = new StreamWriter(@"C:\ServiceAccounts.csv", true))
                //    {
                //        file.WriteLine(attributes);
                //    }
                //    Console.WriteLine("AccountName=" + gpMemberEntry.Properties["samaccountname"].Value.ToString());
                //}
            }
        }
    }
}

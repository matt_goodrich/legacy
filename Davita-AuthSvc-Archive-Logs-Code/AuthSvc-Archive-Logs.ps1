####################################################################################################
#                                                                                                  #
# Original Date: 8/7/2012                                                                          #
# Version 1.0                                                                                      #
# Name: SSO-Archive-Logs.ps1                                                                       #
# Path: C:\scripts\AuthSvc-Archive-Logs.ps1                                                            #
# Author: Seros                                                                                    #
# Purpose: archive Authentication Service log files older than 1 day and move archive to 		   #
#			D: volume on server                                                                    #
# Notes: This archives both the audit and server log files; it does not touch Windows logs;        #
#        This scripts runs as a scheduled task on each of the dev and production servers           #
#                                                                                                  #
####################################################################################################

#Function to create zip archive, add log files, and remove originals
function Add-Zip
{
	if(-not (test-path($logfilestozip)))
	{
		set-content $logfilestozip ("PK" + [char]5 + [char]6 + ("$([char]0)" * 18))
		(dir $logfilestozip).IsReadOnly = $false	
	}
	
	$shellApplication = new-object -com shell.application
	$zipPackage = $shellApplication.NameSpace($logfilestozip)
	
	foreach($file in $checkforlogs) 
	{ 
            $zipPackage.MoveHere($file.FullName)
            Start-sleep -milliseconds 20000
	    remove-item $file
	}
}

#Function to check there are log files to zip, then create zip file name, call add-zip, and finally move zip file to destination volume
function Checkandzip
{
	if ($checkforlogs -ne $NULL) 
	{
		$logfilestozip = $dirlogpath + "_" + $type + "_" + $date + ".zip"
		Add-Zip($logfilestozip)
		Move-Item $logfilestozip $archivefilelocation
	}
}

#Global variables
$archivefilelocation = "C:\SSO\ArchiveLogs"
$yesterday = [datetime]::now.adddays(-1)
$date = get-date -format yyyyMMddHHmm
$archivetime = [datetime]::now.adddays(-90).date


#############Authentication Service Log Files#############
$Wrapperdirlogpath = "C:\inetpub\wwwroot\SSO\App_Data\Logs"
$Authdirlogpath = "C:\inetpub\wwwroot\Login\App_Data\Logs"

#Grab wrapper server logs older than 24 hours
$logpath = $Wrapperdirlogpath + "\server.log*"
$checkforlogs = get-childitem $logpath|where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $yesterday)}|sort lastWriteTime -desc
$type = "wrapper_server"
Checkandzip($checkforlogs,$type)

#Grab auth app audit logs older than 24 hours
$logpath = $Authdirlogpath + "\audit.log*"
$checkforlogs = get-childitem $logpath|where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $yesterday)}|sort lastWriteTime -desc
$type="authapp_audit"
Checkandzip($checkforlogs,$type)

#Grab auth app server logs older than 24 hours
$logpath = $Authdirlogpath + "\server.log.*"
$checkforlogs = get-childitem $logpath|where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $yesterday)}|sort lastWriteTime -desc
$type="authapp_server"
Checkandzip($checkforlogs,$type)


#############Archives Files#############
#Delete archived containers older than 90 days
$checkarchives = get-childitem $archivefilelocation |where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $archivetime)}|sort lastWriteTime -desc
if ($checkarchives -ne $NULL) 
{
	foreach($archivefile in $checkarchives) 
	{ 
	    remove-item $archivefile.fullname
	}
}


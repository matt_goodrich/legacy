####################################################################################################
#                                                                                                  #
# Original Date: 8/7/2012                                                                          #
# Version 1.0                                                                                      #
# Name: SSO-Archive-Logs.ps1                                                                       #
# Path: C:\scripts\SSO-Archive-Logs.ps1                                                            #
# Author: Seros                                                                                    #
# Purpose: archive PingFederate log files older than 1 day and move archive to D: volume on server #
# Notes: This archives both the engine and admin log files; it does not touch Windows logs;        #
#        This scripts runs as a scheduled task on each of the dev and production servers           #
#                                                                                                  #
####################################################################################################

#Function to create zip archive, add log files, and remove originals
function Add-Zip
{
	if(-not (test-path($logfilestozip)))
	{
		set-content $logfilestozip ("PK" + [char]5 + [char]6 + ("$([char]0)" * 18))
		(dir $logfilestozip).IsReadOnly = $false	
	}
	
	$shellApplication = new-object -com shell.application
	$zipPackage = $shellApplication.NameSpace($logfilestozip)
	
	foreach($file in $checkforlogs) 
	{ 
            $zipPackage.MoveHere($file.FullName)
            Start-sleep -milliseconds 20000
	    remove-item $file
	}
}

#Function to check there are log files to zip, then create zip file name, call add-zip, and finally move zip file to destination volume
function Checkandzip
{
if ($checkforlogs -ne $NULL) 
{
	$logfilestozip = $dirlogpath + "_" + $type + "_" + $date + ".zip"
	Add-Zip($logfilestozip)
	Move-Item $logfilestozip $archivefilelocation
}
}

#Global variables
$archivefilelocation = "D:\SSO\ArchiveLogs"
$yesterday = [datetime]::now.adddays(-1)
$date = get-date -format yyyyMMddHHmm
$archivetime = [datetime]::now.adddays(-90).date


#############Engine Log Files#############
$dirlogpath = "D:\PingFederate-engine-6-6\pingfederate\log"
#Grab engine request logs older than 24 hours
$logpath = $dirlogpath + "\*request.log"
$checkforlogs = get-childitem $logpath|where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $yesterday)}|sort lastWriteTime -desc
$type = "engine_request"
Checkandzip($checkforlogs,$type)

#Grab engine admin logs older than 24 hours
$logpath = $dirlogpath + "\admin.log*"
$checkforlogs = get-childitem $logpath|where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $yesterday)}|sort lastWriteTime -desc
$type="engine_admin"
Checkandzip($checkforlogs,$type)

#Grab engine audit logs older than 24 hours
$logpath = $dirlogpath + "\audit.log*"
$checkforlogs = get-childitem $logpath|where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $yesterday)}|sort lastWriteTime -desc
$type="engine_audit"
Checkandzip($checkforlogs,$type)

#Grab engine provisioner logs older than 24 hours
$logpath = $dirlogpath + "\provisioner.log.*"
$checkforlogs = get-childitem $logpath|where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $yesterday)}|sort lastWriteTime -desc
$type="engine_provisioner"
Checkandzip($checkforlogs,$type)

#Grab engine server logs older than 24 hours
$logpath = $dirlogpath + "\server.log.*"
$checkforlogs = get-childitem $logpath|where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $yesterday)}|sort lastWriteTime -desc
$type="engine_server"
Checkandzip($checkforlogs,$type)

#Grab engine transaction logs older than 24 hours
$logpath = $dirlogpath + "\transaction.log.*"
$checkforlogs = get-childitem $logpath|where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $yesterday)}|sort lastWriteTime -desc
$type="engine_transaction"
Checkandzip($checkforlogs,$type)

#############Archives Files#############
#Delete archived containers older than 90 days
$checkarchives = get-childitem $archivefilelocation |where {!$_.PSIsCntainer -AND ($_.lastWriteTime -le $archivetime)}|sort lastWriteTime -desc
if ($checkarchives -ne $NULL) 
{
foreach($archivefile in $checkarchives) 
	{ 
	    remove-item $archivefile.fullname
	}
}


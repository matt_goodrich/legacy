﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ModalLogin.aspx.cs" Inherits="ModalLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="./_support/css/Styles.css" /> 
</head>
<body class="login">
	<div id="login">
		<h1><a href="http://Seros.com" title="Powered by PingFederate and Seros">Seros</a></h1>

		<form name="loginform" id="loginform" action="#" method="post">
			<p>
				<label>Username<br />
				<input type="text" name="log" id="user_login" class="input" value="" size="20" tabindex="10" /></label>
			</p>
			<p>
				<label>Password<br />
				<input type="password" name="pwd" id="user_pass" class="input" value="" size="20" tabindex="20" /></label>
			</p>
			<p class="forgetmenot"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" tabindex="90" /> Remember Me</label></p>
			<p class="submit">
				<input type="submit" name="la-submit" id="la-submit" class="button-primary" value="Log In" tabindex="100" />
			</p>
		</form>

		<p id="nav">
			<a href="#" title="Change Password">Change Password</a>
		</p>
	</div>

	<script type="text/javascript">
	    function attempt_focus() {
	        setTimeout(function () {
	            try {
	                d = document.getElementById('user_login');
	                d.focus();
	                d.select();
	            } catch (e) { }
	        }, 200);
	    }

	    attempt_focus();
	</script>
</body>
</html>

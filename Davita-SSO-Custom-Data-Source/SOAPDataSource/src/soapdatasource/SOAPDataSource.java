//Imports
import com.pingidentity.sources.ConfigurableDriver;
import com.pingidentity.sources.CustomDataSourceDriver;
import com.pingidentity.sources.CustomDataSourceDriverDescriptor;
import com.pingidentity.sources.gui.FilterFieldsGuiDescriptor;
import java.io.*;
import java.util.*;
import java.net.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sourceid.saml20.adapter.conf.*;
import org.sourceid.saml20.adapter.gui.*;
import org.sourceid.saml20.adapter.gui.validation.impl.RequiredFieldValidator;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import java.util.regex.*;

/**
 *
 * @author Matt
 */
public class SOAPDataSource implements ConfigurableDriver, CustomDataSourceDriver{

    //Setup logging within this class
    private Log log = LogFactory.getLog(this.getClass());
    
    //Final static variables that reference the names of UI elements in the Data 
    //Stores screen
    private static final String tableName = "Teammate Web Service Endpoint";
    private static final String columnOneName = "Endpoint Environment";
    private static final String columnTwoName = "Username";
    private static final String columnThreeName = "Password";
    private static final String columnFourName = "Hostname";
    
    private static int SOAPEnv = 0;
    private static String Username = "";
    private static String Password = "";
    private static String HostName = "";

     /**
    * An adapter requires configuration fields in the PingFederate administrative 
    * console. The UI descriptor for custom data-source implementations works in 
    * much the same way as the same method does for adapters. An 
    * AdapterConfigurationGuiDescriptor is populated with objects that will 
    * appear as controls in the PingFederate administrative console when a 
    * custom data source is deployed. The AdapterConfigurationGuiDescriptor is 
    * passed into the constructor of the SourceDescriptor that is later returned 
    * from the getSourceDescriptor() method. This method is required by Ping.
    *
    * @return      CustomDataSourceDriverDescriptor
    */
    @Override
    public CustomDataSourceDriverDescriptor getSourceDescriptor()
    {	
        log.debug("SOAPDataSource:getSourceDescriptor()");
        
        //Description text shown at the top of the Data-Stores configuration screen
        String description = "Please provide configuration details for the SOAP Data Source.";
        
        //Build a new adapter
        AdapterConfigurationGuiDescriptor adapterConfGuiDesc = new AdapterConfigurationGuiDescriptor(description);
        
        //Build the table
        TableDescriptor table = new TableDescriptor(tableName, "");
       
        List<AbstractSelectionFieldDescriptor.OptionValue> selectOptions;
        selectOptions = new ArrayList<AbstractSelectionFieldDescriptor.OptionValue>();
        selectOptions.add(SelectFieldDescriptor.SELECT_ONE);
        selectOptions.add(new AbstractSelectionFieldDescriptor.OptionValue("Dev", "0"));
        selectOptions.add(new AbstractSelectionFieldDescriptor.OptionValue("Stage", "1"));
        selectOptions.add(new AbstractSelectionFieldDescriptor.OptionValue("Prod", "2"));
        SelectFieldDescriptor selectField = new SelectFieldDescriptor(columnOneName, "", selectOptions);
        selectField.addValidator(new RequiredFieldValidator());
        table.addRowField(selectField);
        
        //Build Column 2 (Username) and mark it as required
        TextFieldDescriptor column2 = new TextFieldDescriptor(columnTwoName, "");
        column2.addValidator(new RequiredFieldValidator());
        table.addRowField(column2);
        
        //Build Column 3 (Password) and mark it as required
        TextFieldDescriptor column3 = new TextFieldDescriptor(columnThreeName, "", true);
        column3.addValidator(new RequiredFieldValidator());
        table.addRowField(column3);
        
        //Build Column 4 (Hostname) and mark it as required
        TextFieldDescriptor column4 = new TextFieldDescriptor(columnFourName, "");
        column4.addValidator(new RequiredFieldValidator());
        table.addRowField(column4);
        
        //Add the table to the GUI
        adapterConfGuiDesc.addTable(table);
        
        //Add Filter Capability so we can search in an SP connection
        FilterFieldsGuiDescriptor filter = new FilterFieldsGuiDescriptor();
        TextAreaFieldDescriptor textarea = new TextAreaFieldDescriptor("Filter", "", 5, 25);
        filter.addField(textarea);

        
        //Return the GUI
        return new CustomDataSourceDriverDescriptor(this,"Teammate Data Source",adapterConfGuiDesc, filter);
    }
    
    /**
    * A custom data source receives the configuration set in the PingFederate 
    * administrative console in the same way that an IdP adapter does. This 
    * method is required by Ping.
    * 
    * @param configuration The Configuration object that contains the information setup in the Data-Stores screen
    */
    @Override
    public void configure(Configuration configuration)
    {
        log.debug("SOAPDataSource:configure(Configuration configuration)");
        
        //Get the configuration table
        Table tbl = configuration.getTable(tableName);
        
        //Iterate each row in the table
        for(Row r : tbl.getRows())
        {
            SOAPEnv = Integer.parseInt(r.getFieldValue(columnOneName));
            Username = r.getFieldValue(columnTwoName);
            Password = r.getFieldValue(columnThreeName);
            HostName = r.getFieldValue(columnFourName);
        }
        
        log.debug("SOAPDataSource:getSourceDescriptor(): Configuration Saved");
    }
    
     /**
    * When associating a custom data source with an IdP or SP connection, 
    * PingFederate tests connectivity to the data source by calling the 
    * testConnection() method. Your implementation of this method should perform 
    * the necessary steps to demonstrate a successful connection and return true. 
    * Return false if your implementation cannot communicate with the data store. 
    * A false result prevents an administrator from continuing with the 
    * data-source configuration. This method is required by Ping.
    * 
    * @return boolean
    */
    @Override
    public boolean testConnection() 
    {
        //Log where we are
        log.debug("SOAPDataSource:testConnection()");
        try
        {
            URL u = new URL("http://sea-rescdv01.davita.com/person/teammate.cfc");
            
            log.debug("SOAPDataSource:testConnection(): SOAPEnv: " + SOAPEnv);
            //Determine which SOAP service to use
            switch (SOAPEnv)
            {
                case 0:
                    u = new URL("http://sea-rescdv01.davita.com/person/teammate.cfc");
                    log.debug("SOAPDataSource:testConnection(): Setting endpoint: http://sea-rescdv01.davita.com/person/teammate.cfc");
                    break;
                case 1:
                    u = new URL("http://sea-rescstg01.davita.com/person/teammate.cfc");
                    log.debug("SOAPDataSource:testConnection(): Setting endpoint: http://sea-rescstg01.davita.com/person/teammate.cfc");
                    break;
                case 2:
                    u = new URL("http://resource.davita.com/person/teammate.cfc");
                    log.debug("SOAPDataSource:testConnection(): Setting endpoint: http://resource.davita.com/person/teammate.cfc");
                    break;
            }
        
            //Open the connection
            URLConnection uc = u.openConnection();
            log.debug("SOAPDataSource:testConnection(): Testing of connection was successful");
            //If this succeeds, return true
            return true;
        }
        catch (Exception e)
        {
            //Something happened, log it and return false
            log.debug("SOAPDataSource:testConnection(): Exception thrown while testing connection");
            log.error(e);
            return false;
        }
    }
    
     /**
    * PingFederate calls the getAvailableFields() method to determine the
    * available fields that could be returned from a query of this data source. 
    * These fields are displayed to the PingFederate administrator during the 
    * configuration of data-store lookup. The administrator can then select the 
    * attributes from the data source and map them to the adapter or attribute 
    * contract. PingFederate requires at least one field returned from this 
    * method. This method is required by Ping.
    * 
    * @return List<String>
    */
    @Override
    public List<String> getAvailableFields()
    {	
        //Just return our static list based on the WSDL definition
        List<String> fields = new ArrayList<String>();
        fields.add("FULLNAME");
        fields.add("PRIV_ID_LIST");
        fields.add("MERGEORG");
        fields.add("WORKFORCEID");
        fields.add("USERNO");
        fields.add("USERNAME");
        fields.add("EMAIL");
        fields.add("CONTEXT");
        fields.add("TITLE");
        
        return fields;
    }
    
    /**
    * When processing a connection using a custom data source, PingFederate 
    * calls the retrieveValues() method to perform the actual query for user 
    * attributes. This method receives a list of attribute names that should be 
    * populated with data. The method may also receive a filterConfiguration 
    * object containing criteria to use for selecting a specific record based on 
    * data during runtime. This method returns a map of name-value pairs. This 
    * map contains the collection of attribute names passed into the method and 
    * their corresponding values retrieved from the query. This method is 
    * required by Ping.
    * 
    * @param attributeNamesToFill   A collection of attribute names that require a value
    * @param filterConfiguration    The search criteria for the user to get the attribute values for
    * @return Map<String, Object>
    */
    @Override
    public java.util.Map<String,Object> retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration)
    {
        log.debug("SOAPDataSource:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration)");
        
        //Map to store data to be returned
        Map<String, Object> map = new HashMap<String, Object>();
        
        //Extract our filter and remove everything but the subject name
        String filter = filterConfiguration.getFieldValue("Filter");
        
        //Log the filter data
        log.debug("SOAPDataSource:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): Filter = " + filter);
        
        //Setup some StringBuffers to use with the regex matches below
        StringBuffer userName = new StringBuffer();
        StringBuffer AppName = new StringBuffer();
        
        //Get the Username
        Pattern Regex = Pattern.compile("\\(sAMAccountName=(.*?)\\)(.+)", Pattern.CANON_EQ);
        Matcher RegexMatcher = Regex.matcher(filter);
        while(RegexMatcher.find())
        {
            try
            {
                RegexMatcher.appendReplacement(userName, "$1");
                log.debug("SOAPDataSource:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): Username = " + userName);
            }
            catch (Exception e) {
                log.debug("SOAPDataSource:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): Error Matching Username");                
                log.error(e);
            } 
        }
        
        //Get the App Name
        Regex = Pattern.compile("(.+)\\(appName=(.+)\\)", Pattern.CANON_EQ);
        RegexMatcher = Regex.matcher(filter);
        
        while(RegexMatcher.find())
        {
            try
            {
                RegexMatcher.appendReplacement(AppName, "$2");
                log.debug("SOAPDataSource:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): AppName = " + AppName);                
            }
            catch (Exception e) {
                log.debug("SOAPDataSource:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): Error Matching AppName");                
                log.error(e);
            } 
        }
        
        //Call getAppId()
        double AppNumber = 0;
        
        try
        {
            AppNumber = GetAppID(AppName.toString());
        }
        catch (Exception e)
        {
            log.debug("SOAPDataSource:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): Error Calling GetAppID");                
            log.error(e);
        }
        
        //Save the results from calling GetUserPrivs
        Map<String, String> userPrivs = new HashMap<String, String>();
        try
        {
            userPrivs = GetUserPrivs(AppNumber, userName.toString());
        }
        catch (Exception e)
        {
            log.debug("SOAPDataSource:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): Error Calling GetUserPrivs");                
            log.error(e);
        }
        
        for(String str: attributeNamesToFill)
        {
            for(Map.Entry<String, String> e : userPrivs.entrySet())
            {
                if (str.equals(e.getKey()))
                {
                    //Add the value to the map and map it to the name of the value we need to return
                    map.put(str, e.getValue());
                    log.debug("SOAPDataSource:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): Found attribute \"" + str + "\" for user \"" + userName + "\" with value \"" + e.getValue() + "\".");
                }
            }
        }
        
        return map;
    }
    
    /**
    * Gets the App ID by calling the SOAP web service "getAppID".
    * 
    * @param AppNumber  The app number.
    * @param Username   The username.
    * @return Map<String, Object>
    */
    private double GetAppID(String Nickname) throws MalformedURLException, IOException
    {
        //Object to hold all values returned from the call
        double AppID = 0;
        
        //Setup our endpoing URL to be populated with the switch statement below
        URL u = new URL("http://devresource.davita.com/component/app.cfc");
        
        //Determine which SOAP service to use
        switch (SOAPEnv)
        {
            case 0:
                u = new URL("http://devresource.davita.com/component/app.cfc");
                log.debug("SOAPDataSource:GetAppID(String Nickname): Setting endpoint = http://devresource.davita.com/component/app.cfc");
                break;
            case 1:
                u = new URL("http://sea-rescstg01.davita.com/component/app.cfc");
                log.debug("SOAPDataSource:GetAppID(String Nickname): Setting endpoint = http://sea-rescstg01.davita.com/component/app.cfc");
                break;
            case 2:
                u = new URL("http://resource.davita.com/component/app.cfc");
                log.debug("SOAPDataSource:GetAppID(String Nickname): Setting endpoint = http://resource.davita.com/component/app.cfc");
                break;
        }
        
        String userPassword = Username + ":" + Password;
        String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
        
        //Setup the connection
        URLConnection uc = u.openConnection();
        HttpURLConnection connection = (HttpURLConnection) uc;
        
        connection.setRequestProperty("Authorization", "Basic " + encoding);
        
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("SOAPAction", "getAppID");
        
        //Write the SOAP request to the outputstream to be sent in the request
        OutputStream out = connection.getOutputStream();
        Writer wout = new OutputStreamWriter(out);
        wout.write(getSoapAppIDRequestXMLString(Nickname));
        wout.flush();
        wout.close();
        
        log.debug("SOAPDataSource:GetAppID(String Nickname): SOAP Request = " + getSoapAppIDRequestXMLString(Nickname));
        
        //Get the response as a string
        String xmlString = convertStreamToString(connection.getInputStream());
        
        log.debug("SOAPDataSource:GetAppID(String Nickname): SOAP Response = " + xmlString);
        
        //Build an XML doc so we can parse the return
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        
        try
        {
            builder = factory.newDocumentBuilder();
            //Pull the XML into the doc
            Document doc = builder.parse( new InputSource(new StringReader( xmlString )));
            doc.getDocumentElement().normalize();
            
            //Get the items from the response
            NodeList nList = doc.getElementsByTagName("getAppIDReturn");
            
            //Get the key/value pairs and save them to be returned
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                
                AppID = Double.valueOf(nNode.getChildNodes().item(0).getTextContent());
                
                log.debug("SOAPDataSource:GetAppID(String Nickname): AppID = " + AppID);
            }
        }
        catch (Exception e) 
        {  
            //Something went wrong, log it
            log.debug("SOAPDataSource:GetAppID(String Nickname): Exception thrown while parsing XML response");
            log.error(e);
        }
        
        //Return the Dictionary of values
        return AppID;
    }
    
    /**
    * Gets the user privs by calling the teammate SOAP web service "getUserPrivs".
    * 
    * @param AppNumber  The app number.
    * @param Username   The username.
    * @return Map<String, Object>
    */
    private Map<String, String> GetUserPrivs(double AppNumber, String Username) throws MalformedURLException, IOException
    {
        //Object to hold all values returned from the call
        Map<String, String> returnVals = new HashMap<String, String>();
        
        //Setup our endpoing URL to be populated with the switch statement below
        URL u = new URL("http://sea-rescdv01.davita.com/person/teammate.cfc");
        
        //Determine which SOAP service to use
        switch (SOAPEnv)
        {
            case 0:
                u = new URL("http://sea-rescdv01.davita.com/person/teammate.cfc");
                log.debug("SOAPDataSource:GetUserPrivs(int AppNumber, String Username): Setting endpoint = http://sea-rescdv01.davita.com/person/teammate.cfc");
                break;
            case 1:
                u = new URL("http://sea-rescstg01.davita.com/person/teammate.cfc");
                log.debug("SOAPDataSource:GetUserPrivs(int AppNumber, String Username): Setting endpoint = http://sea-rescstg01.davita.com/person/teammate.cfc");
                break;
            case 2:
                u = new URL("http://resource.davita.com/person/teammate.cfc");
                log.debug("SOAPDataSource:GetUserPrivs(int AppNumber, String Username): Setting endpoint = http://resource.davita.com/person/teammate.cfc");
                break;
        }
        
        
        //Setup the connection
        URLConnection uc = u.openConnection();
        HttpURLConnection connection = (HttpURLConnection) uc;
        
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("SOAPAction", "getUserPrivs");
        
        //Write the SOAP request to the outputstream to be sent in the request
        OutputStream out = connection.getOutputStream();
        Writer wout = new OutputStreamWriter(out);
        wout.write(getSoapRequestXMLString(AppNumber, HostName, Username, AppNumber));
        wout.flush();
        wout.close();
        
        log.debug("SOAPDataSource:GetUserPrivs(int AppNumber, String Username): SOAP Request = " + getSoapRequestXMLString(AppNumber, HostName, Username, AppNumber));
        
        //Get the response as a string
        String xmlString = convertStreamToString(connection.getInputStream());
        
        log.debug("SOAPDataSource:GetUserPrivs(int AppNumber, String Username): SOAP Response = " + xmlString);
        
        //Build an XML doc so we can parse the return
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        
        try
        {
            builder = factory.newDocumentBuilder();
            //Pull the XML into the doc
            Document doc = builder.parse( new InputSource(new StringReader( xmlString )));
            doc.getDocumentElement().normalize();
            
            //Get the items from the response
            NodeList nList = doc.getElementsByTagName("item");
            
            //Get the key/value pairs and save them to be returned
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                
                returnVals.put(nNode.getChildNodes().item(1).getTextContent(), nNode.getChildNodes().item(3).getTextContent());
                log.debug("SOAPDataSource:GetUserPrivs(int AppNumber, String Username): Adding Return Values: Key:" + nNode.getChildNodes().item(1).getTextContent() + " Value: " + nNode.getChildNodes().item(3).getTextContent());
            }
        }
        catch (Exception e) 
        {  
            //Something went wrong, log it
            log.debug("SOAPDataSource:GetUserPrivs(int AppNumber, String Username): Exception thrown while parsing XML response");
            log.error(e);
        }
        
        //Return the Dictionary of values
        return returnVals;
    }
    
    /**
    * Gets the SOAP request to send to the teammate SOAP web service "getUserPrivs".
    * 
    * @param appNumber  The nickname of the app to get the ID for
    * @return String
    */
    private static String getSoapAppIDRequestXMLString(String Nickname)
    {
        //Setup our SOAP request
        String xmldata = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<soap:Envelope " +
                "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:com=\"http://component\" " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
                "<soap:Body soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                "<com:getAppID>" +
			"<Nickname xsi:type=\"xsd:string\">" + Nickname + "</Nickname>" +
		"</com:getAppID>" +
                "</soap:Body>" +
                "</soap:Envelope>";
        
        //return the request as a String
        return xmldata;
    }
    
    /**
    * Gets the SOAP request to send to the teammate SOAP web service "getUserPrivs".
    * 
    * @param appNumber  The app number making the call
    * @param HostName  The HostName of the calling application.
    * @param Username   The username.
    * @param privAppNumber  The app number to get privs for
    * @return String
    */
    private static String getSoapRequestXMLString(double appNumber, String HostName, String Username, double privAppNumber)
    {
        //Setup our SOAP request
        String xmldata = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<soap:Envelope " +
                "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:tns=\"http://person\" " +
                "xmlns:types=\"http://person/encodedTypes\" " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
                "<soap:Body soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                "<tns:getUserPrivs>" +
			"<app_number xsi:type=\"xsd:double\">" + appNumber + "</app_number>" +
			"<host_name xsi:type=\"xsd:string\">" + HostName + "</host_name>" +
			"<username xsi:type=\"xsd:string\">" + Username + "</username>" +
			"<privAppNo xsi:type=\"xsd:double\">" + privAppNumber + "</privAppNo>" +
		"</tns:getUserPrivs>" +
                "</soap:Body>" +
                "</soap:Envelope>";
        
        //return the request as a String
        return xmldata;
    }
    
    /**
    * Gets the SOAP response as an InputStrea from the teammate SOAP web service "getUserPrivs" and converts it to a String
    * 
    * @param is  The InputStream with the SOAP response from the teammate SOAP web service "getUserPrivs"
    * @return Map<String, Object>
    */
    private String convertStreamToString(java.io.InputStream is) 
    {
        try 
        {
            return new java.util.Scanner(is).useDelimiter("\\A").next();
        } 
        catch (java.util.NoSuchElementException e) 
        {
            return "";
        }
    }
}

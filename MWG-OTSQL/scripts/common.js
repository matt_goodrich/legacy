/*******************************************************************************
 * Copyright (C) 2007 Ping Identity Corporation All rights reserved.
 * 
 * This software is licensed under the Open Software License v2.1 (OSL v2.1).
 * 
 * A copy of this license has been provided with the distribution of this
 * software. Additionally, a copy of this license is available at:
 * http://opensource.org/licenses/osl-2.1.php
 *  
 ******************************************************************************/

//Common javascript routines

function openHelp(url)
{   
  
	helpWindow = window.open(url, 'WWHFrame', 'top=20,left=20,height=500,width=700,resizable,toolbar,scrollbars,status=no');
	if (helpWindow.opener == null) {
		helpWindow.opener = window;
	}
	helpWindow.focus();
}


function copyToClipboard(text) {
//	document.getElementById('testText').style.visibility = 'hidden'
        if (window.clipboardData) window.clipboardData.setData("Text", text);
        else if (window.netscape) {
	  	try { 
			// ask user for permission
			netscape.security.PrivilegeManager.enablePrivilege('UniversalXPConnect');
		} catch (e) {
			// user refused permission
			//alert("UniversalXPConnect priviliges denied. If using Mozilla Firefox, access 'about:config' through the address bar and set 'signed.applets.codebase_principal_support' to 'true'."); 
		}
                var clip = Components.classes['@mozilla.org/widget/clipboard;1'].createInstance(Components.interfaces.nsIClipboard);
             	if (clip) {
	                var trans = Components.classes['@mozilla.org/widget/transferable;1'].createInstance(Components.interfaces.nsITransferable);
                	if (!trans) return;
                	trans.addDataFlavor('text/unicode');
                	var str = new Object();
                	var len = new Object();
                	var str = Components.classes["@mozilla.org/supports-string;1"].createInstance(Components.interfaces.nsISupportsString);
                	str.data=text;
                	trans.setTransferData("text/unicode",str,text.length*2);
                	var clipid=Components.interfaces.nsIClipboard;
                	if (!clipid) return;
                	clip.setData(trans,null,clipid.kGlobalClipboard);
                }
        }
        return;
}


function createSSOUrl(url) {
	if(document.getElementById('TargetResource') != null) {
		if(document.getElementById('TargetResource').value != "") {
			url += "&TargetResource=";
			url += document.getElementById('TargetResource').value;
		}
	}
	if(document.getElementById('AllowCreate') != null) {
		if(document.getElementById('AllowCreate').checked) {
			url += "&AllowCreate=true";
		}						
	}									
	if(document.getElementById('Binding') != null) {
		if(document.getElementById('Binding').value != "") {
			url += "&Binding=";
			url += document.getElementById('Binding').value;
		}			
	}
	if(document.getElementById('RequestedBinding') != null) {
		if(document.getElementById('RequestedBinding').value != "") {
			url += "&RequestedBinding=";
			url += document.getElementById('RequestedBinding').value;
		}			
	}			
	if(document.getElementById('RequestedACSIdx') != null) {
		if(document.getElementById('RequestedACSIdx').value != "") {
			url += "&RequestedACSIdx=";
			url += document.getElementById('RequestedACSIdx').value;
		}			
	}									
	if(document.getElementById('RequestedFormat') != null) {
		if(document.getElementById('RequestedFormat').value != "") {
			url += "&RequestedFormat=";
			url += document.getElementById('RequestedFormat').value;
		}						
	}
	if(document.getElementById('IsPassive') != null) {
		if(document.getElementById('IsPassive').checked) {
			url += "&IsPassive=true";
		}						
	}			
	if(document.getElementById('ForceAuthn') != null) {
		if(document.getElementById('ForceAuthn').checked) {
			url += "&ForceAuthn=true";
		}						
	}						
	if(document.getElementById('RequestedSPNameQualifier') != null) {
		if(document.getElementById('RequestedSPNameQualifier').value != "") {
			url += "&RequestedSPNameQualifier=";
			url += document.getElementById('RequestedSPNameQualifier').value;
		}						
	}
	if(document.getElementById('AdapterInstanceId') != null) {
		if(document.getElementById('AdapterInstanceId').value != "") {
			url += "&SpSessionAuthnAdapterId=";
			url += document.getElementById('AdapterInstanceId').value;
		}
	}			
	return url;
}	
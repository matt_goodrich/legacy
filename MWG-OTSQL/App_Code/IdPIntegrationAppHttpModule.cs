/*******************************************************************************
* Copyright (C) 2008 Ping Identity Corporation All rights reserved.
*
* This software is licensed under the Open Software License v2.1 (OSL v2.1).
*
* A copy of this license has been provided with the distribution of this
* software. Additionally, a copy of this license is available at:
* http://opensource.org/licenses/osl-2.1.php
*
******************************************************************************
*/
using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Collections;
using System.Collections.Generic;
using Com.PingIdentity.Adapters.Pftoken.Core;
using Com.PingIdentity.Adapters.Pftoken.AuthnContext;
using Com.PingIdentity.PingFederate.SampleApp.SampleAppUtility;
using opentoken;

namespace com.pingidentity.adapters.sampleapp.idp
{

    /// <summary>
    /// Summary description for IdPIntegrationAppHttpModule
    /// </summary>
    public class IdPIntegrationAppHttpModule : IHttpModule
    {
        public const String USER_ID = "subject";
        public const String SUBJECT = "subject";
        public const String RESUME_PATH = "resumePath";
        public const String PFAGENT_PROPERTIES = "pfagent.properties";
        private Links samlLinks;
        private String configPath = "";

        public IdPIntegrationAppHttpModule()
        {
        }

        public String ModuleName
        {
            get { return "IdPIntegrationAppHttpModule"; }
        }

        // In the Init function, register for HttpApplication 
        // events by adding your handlers.
        public void Init(HttpApplication application)
        {
            HttpContext context = application.Context;
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
      
                response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");
          
            //get properties from Idp config file
            String idpConfig = ConfigurationManager.AppSettings["config-file"];
            configPath = context.Server.MapPath("config/");
            if ((configPath.Contains("/config")) ||
                (configPath.Contains("/scripts")) ||
                (configPath.Contains("/images")))
            {
                configPath = context.Server.MapPath("");
            }
            samlLinks = new Links(context, configPath + idpConfig);
            application.BeginRequest +=
                (new EventHandler(this.Application_BeginRequest));
            application.EndRequest +=
                (new EventHandler(this.Application_EndRequest));
        }

        private void Application_BeginRequest(Object source,
             EventArgs e)
        {
            if (samlLinks.configTable["tokenType"].Equals("PFTOKEN"))
            {

                // Create HttpApplication and HttpContext objects to access
                // request and response properties.
                HttpApplication application = (HttpApplication)source;
                HttpContext context = application.Context;
                HttpRequest request = context.Request;
                HttpResponse response = context.Response;
                //context.Response.Write("<h1><font color=red>IdPIntegrationAppHttpModule: Beginning of Request</font></h1><hr>");

                String cmd = request["cmd"];
                switch (cmd)
                {
                    case "sso":
                        HandleIdPSSORequest(context, request, response);
                        //Response.Redirect("SSOIdpHandler.aspx?" + Request.QueryString);
                        break;

                    case "slo":
                        HandleIdPSLORequest(context, request, response);
                        //Response.Redirect("SLOIdpHandler.aspx?" + Request.QueryString);
                        break;

                    case "logout":
                        //Response.Redirect("MainHandler.aspx?" + Request.QueryString);
                        break;

                    default:
                        //Response.Redirect("MainHandler.aspx?" + Request.QueryString);
                        break;

                }

                // Handle resumePath
                if ((request.Cookies[FormsAuthentication.FormsCookieName] != null) &&
                        (request[RESUME_PATH] != null))
                {

                }
                /*
                        if (request.Cookies[FormsAuthentication.FormsCookieName] == null)
                        {

                            FormsAuthenticationTicket tkt;
                            string cookiestr;
                            HttpCookie ck;
                            tkt = new FormsAuthenticationTicket(1, "sarah", DateTime.Now,
                              DateTime.Now.AddMinutes(30), false, "your custom data");
                            cookiestr = FormsAuthentication.Encrypt(tkt);
                            ck = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);
                            //        if (chkPersistCookie.Checked)
                            //          ck.Expires = tkt.Expiration;
                            ck.Path = FormsAuthentication.FormsCookiePath;
                            response.Cookies.Add(ck);

                            string strRedirect;
                            strRedirect = request["ReturnUrl"];
                            if (strRedirect == null)
                                strRedirect = "default.aspx";
                            response.Redirect(strRedirect, true);
     
                        }
                        else
                        {
                            string cookiestr;
                            cookiestr = request.Cookies[FormsAuthentication.FormsCookieName].Value;
                            FormsAuthenticationTicket tkt = FormsAuthentication.Decrypt(cookiestr);
                            Dictionary<string, string> attributes = new Dictionary<string, string>();
                            attributes.Add(USER_ID, tkt.Name);
                            string strRedirect;
                            strRedirect = request["ReturnUrl"];
                            if (strRedirect == null)
                                strRedirect = "http://localhost:4693/IntegrationApp/default.aspx";
                            strRedirect = setPFToken(context, strRedirect, attributes);
                            response.Redirect(strRedirect, true);
                            //HttpCookie ck = new HttpCookie("LoginUser", tkt.UserData);
                            //response.Cookies.Add(ck);
                        }
                 */
            }
        }

        private void Application_EndRequest(Object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;
            //context.Response.Write("<hr><h1><font color=red>IntegrationAppHttpModule: End of Request</font></h1>");
        }

        public void Dispose()
        {
        }

        private void HandleIdPSSORequest(HttpContext context, HttpRequest request, HttpResponse response)
        {
            if (request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                string cookiestr;
                cookiestr = request.Cookies[FormsAuthentication.FormsCookieName].Value;
                FormsAuthenticationTicket tkt = FormsAuthentication.Decrypt(cookiestr);
                Dictionary<string, string> attributes = new Dictionary<string, string>();
                //if (context.Session != null)
                //{
                //    // Get attributes from session
                //    MultiStringDictionary v_otattributes = (MultiStringDictionary)context.Session[Constants.USER_INFO];

                //    foreach (string v_key in v_otattributes.Keys)
                //    {
                //        attributes.Add(v_key, v_otattributes[v_key][0]);
                //    }
                    
                //}
                //else
                //{
                    attributes.Add(USER_ID, tkt.Name + "(No Session)");
                //}

                
                string strRedirect;
                if (request[RESUME_PATH] != null)
                {
                    strRedirect = Links.hostPF + request[RESUME_PATH];
                    strRedirect = setPFToken(context, strRedirect, attributes);
                    response.Redirect(strRedirect, true);

                }
            }
            else
            {
                string strRedirect;
                strRedirect = "default.aspx";
                if (request[RESUME_PATH] != null)
                {
                    strRedirect += "?ReturnUrl=" + Links.hostPF + request[RESUME_PATH];
                }
                response.Redirect(strRedirect, true);
            }
        }

        private void HandleIdPSLORequest(HttpContext context, HttpRequest request, HttpResponse response)
        {
            if (request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                string cookiestr;
                cookiestr = request.Cookies[FormsAuthentication.FormsCookieName].Value;
                FormsAuthenticationTicket tkt = FormsAuthentication.Decrypt(cookiestr);
                Dictionary<string, string> attributes = new Dictionary<string, string>();
                attributes.Add(USER_ID, tkt.Name);
                FormsAuthentication.SignOut();
                string strRedirect;
                if (request[RESUME_PATH] != null)
                {
                    strRedirect = Links.hostPF + request[RESUME_PATH];
                    strRedirect = setLogoutPFToken(context, strRedirect, attributes, true);
                    response.Redirect(strRedirect, true);
                }
            }
        }

        /// <summary>
        /// setPFToken method uses the Java Integration Kit API to create an 
        /// instance of Idp Agent. The user attributes are added to this agent and 
        /// then the PFToken is created as a cookie or query parameter
        /// </summary>
        /// <param name="context">current HttpContext</param>
        /// <param name="url">url to append pftoken</param>
        /// <param name="userInfo">Hashtable of user attributes</param>
        /// <returns>URL with pftoken</returns>
        private String setPFToken(HttpContext context, String url, Dictionary<string, string> userInfo)
        {
            String returnUrl = url;

            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            HttpSessionState session = context.Session;

            //read properties from Idp pfagent-properties file
            //String idpProps = ConfigurationSettings.AppSettings["pfagent-properties"];
            String propsPath = configPath + PFAGENT_PROPERTIES;

            // Create PFToken Idp Agent
            PftokenAgent idpAgent = new PftokenAgent(new StreamReader(propsPath));

            // Add user attributes to the agent		
            idpAgent.AddAttributes(IdPIntegrationAppHttpModule.ConvertToHashtable(userInfo));

            // Create PFToken
            returnUrl = idpAgent.ConstructPFToken(request, response, url);
            return returnUrl;
        }

        /// <summary>
        /// setLogoutPFToken method uses the Java Integration Kit API to create an 
        /// instance of Idp Agent. The user attributes are added to this agent and 
        /// then the logout PFTOKEN is created
        /// </summary>
        /// <param name="context">current HttpContext</param>
        /// <param name="context">url for redirect after performing logout</param>
        /// <param name="userInfo">Hashtable of user attributes</param>
        /// <param name="logoutSuccessful">flag indicating successful logout</param>
        /// <returns>URL with pftoken logout parameter</returns>
        private String setLogoutPFToken(HttpContext context, String url, Dictionary<string, string> userInfo,
                bool logoutSuccessful)
        {

            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            HttpSessionState session = context.Session;

            //read properties from Sp pfagent-properties file
            //String idpProps = ConfigurationSettings.AppSettings["pfagent-properties"];
            String propsPath = configPath + PFAGENT_PROPERTIES;

            // Create PFToken Idp Agent
            PftokenAgent idpAgent = new PftokenAgent(new StreamReader(propsPath));

            // Add user attributes to the agent
            idpAgent.AddAttributes(IdPIntegrationAppHttpModule.ConvertToHashtable(userInfo));

            // Create logout PFToken
            return idpAgent.ConstructLogoutPFToken(request, response, url, logoutSuccessful);
        }

        //Utility methods
        public static Dictionary<string, string> ConvertToDictionary(Hashtable attributes)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            foreach (String key in attributes.Keys)
            {
                String infoName = key;
                String infoValue = (String)attributes[key];
                props.Add(infoName, infoValue);

                // Handle Subject
                if (String.Compare(infoName, USER_ID, true) == 0)
                {
                    if (!(props.ContainsKey(SUBJECT)))
                    {
                        props.Add(SUBJECT, infoValue);
                    }
                }
            }
            return props;
        }

        public static Hashtable ConvertToHashtable(Dictionary<string, string> attributes)
        {
            Hashtable props = new Hashtable();

            foreach (KeyValuePair<string, string> kvp in attributes)
            {
                String infoName = kvp.Key;
                String infoValue = kvp.Value;

                props.Add(infoName, infoValue);

                // Handle Subject
                if (String.Compare(infoName, SUBJECT, true) == 0)
                {
                    if (!(props.ContainsKey(SUBJECT)))
                    {
                        props.Add(USER_ID, infoValue);
                    }
                }

            }
            return props;
        }


    }
}
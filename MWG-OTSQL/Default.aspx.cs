/*******************************************************************************
* Copyright (C) 2008 Ping Identity Corporation All rights reserved.
*
* This software is licensed under the Open Software License v2.1 (OSL v2.1).
*
* A copy of this license has been provided with the distribution of this
* software. Additionally, a copy of this license is available at:
* http://opensource.org/licenses/osl-2.1.php
*
******************************************************************************
*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Com.PingIdentity.PingFederate.SampleApp.SampleAppUtility;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.DirectoryServices;
using opentoken;
using System.Text.RegularExpressions;
using opentoken.util;


public partial class _Default : System.Web.UI.Page
{
    /**
     * Added by HBW to support LDAP based authentication
     */

    [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
        int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public extern static bool CloseHandle(IntPtr handle);

    [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public extern static bool DuplicateToken(IntPtr ExistingTokenHandle,
        int SECURITY_IMPERSONATION_LEVEL, ref IntPtr DuplicateTokenHandle);
    /*
     * End
     */
    protected String idpStartSLO = "";
    protected bool showAdvanced = false;
    protected bool sessionExists = false;
    public Links samlLinks;
    protected String usersFilePath = null;

    protected void Page_Load(object sender, EventArgs e)
    {

        Form1.Action = "default.aspx?cmd=Login" + ((Request["ReturnUrl"] != null) ? "&ReturnUrl=" + HttpUtility.UrlEncode(Request["ReturnUrl"]) : "");

        if (Request.Url.Query.Contains(ConfigurationManager.AppSettings["RedirectRequestUri"]))
        {
            if (Request.Headers["Referer"] == null)
            {
                Response.Redirect(ConfigurationManager.AppSettings["RedirectTargetUrl"]);
                return;
            }
            else if (!Request.Headers["Referer"].Contains(ConfigurationManager.AppSettings["RedirectReferrer"]))
            {
                Response.Redirect(ConfigurationManager.AppSettings["RedirectTargetUrl"]);
                return;
            }

        }

        //prevent the output of aspx page from being cached by the browser
        Response.AddHeader("Cache-Control", "no-cache");
        Response.AddHeader("Pragma", "no-cache");

        Response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

        HttpContext.Current.Response.AddHeader("p3p", "CP=\"IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT\"");

        //get properties from Idp config file
        String idpConfig = ConfigurationManager.AppSettings["config-file"];
        String configPath = Context.Server.MapPath("config/" + idpConfig);

        //read properties from Idp users XML file
        String idpUsers = ConfigurationManager.AppSettings["users-props-file"];
        usersFilePath = Context.Server.MapPath("config/" + idpUsers);
        samlLinks = new Links(Context, configPath);

        idpStartSLO = samlLinks.getIdpSLOLink();

        if (Request["showAdvanced"] != null)
        {
            if (Request["showAdvanced"].Equals("true"))
            {
                showAdvanced = true;
            }
            else
            {
                showAdvanced = false;
            }
        }

        if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
        {
            string cookiestr;
            cookiestr = Request.Cookies[FormsAuthentication.FormsCookieName].Value;
            FormsAuthenticationTicket tkt = FormsAuthentication.Decrypt(cookiestr);
            // Check to see if user has the required role
            Session.Add("FORMAUTHTICKET", tkt);
            Session.Timeout = 840;
            sessionExists = true;

        }

        if (!IsPostBack)
        {


            //PopulateUserList();

        }
        else
        {
            XmlUtility xmlUtility = new XmlUtility();

            //MultiStringDictionary userInfo = xmlUtility.AuthenticateUser(userName.Value, password.Value, usersFilePath);
            MultiStringDictionary userInfo = null;

            //Validate Email Address Format
            string inputEmail = emailAddress.Value;
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
            {
                string authResult = AuthenticateUser(emailAddress.Value, password.Value, out userInfo);

                if (userInfo != null)
                {

                    //Dictionary<string, List<string>> v_userInfoDict = new Dictionary<string, List<string>>();
                    string v_userInfoString = "";
                    foreach (string key in userInfo.Keys)
                    {
                        foreach (string value in userInfo[key])
                        {
                            v_userInfoString += key + "~:~" + value + "|||";
                        }
                    }

                    //Serialize Userinfo
                    //HttpCookie v_ui = new HttpCookie("userInfo");
                    //if (!publicComputer.Checked) v_ui.Expires = DateTime.Now.AddMinutes(60);
                    //Stream v_ms = new MemoryStream();
                    //try
                    //{
                    //    IFormatter v_bf = new BinaryFormatter();
                    //    v_bf.Serialize(v_ms, v_userInfoDict);
                    //    v_ms.Seek(0, SeekOrigin.Begin);
                    //    byte[] v_buffer = new byte[v_ms.Length];
                    //    v_ms.Read(v_buffer, 0, (int)v_ms.Length);
                    //    v_ui.Value = Convert.ToBase64String(v_buffer);
                    //    HttpContext.Current.Response.Cookies.Add(v_ui);
                    //}
                    //finally
                    //{
                    //    v_ms.Close();
                        
                    //}
                    
                    //Session.Add(Constants.USER_INFO, userInfo);
                    //Session.Timeout = 840;

                    FormsAuthenticationTicket tkt;
                    string cookiestr;
                    HttpCookie ck;
                    if (publicComputer.Checked)
                    {
                        tkt = new FormsAuthenticationTicket(1, emailAddress.Value, DateTime.Now,
                            DateTime.Now.AddMinutes(480), false, v_userInfoString);
                        cookiestr = FormsAuthentication.Encrypt(tkt);
                        ck = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);
                        ck.Path = FormsAuthentication.FormsCookiePath;
                    }
                    else
                    {
                        tkt = new FormsAuthenticationTicket(1, emailAddress.Value, DateTime.Now,
                            DateTime.Now.AddMinutes(480), true, v_userInfoString);
                        cookiestr = FormsAuthentication.Encrypt(tkt);
                        ck = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);
                        ck.Expires = DateTime.Now.AddMinutes(60);
                        ck.Path = FormsAuthentication.FormsCookiePath;


                    }
                    Response.Cookies.Add(ck);

                    string strRedirect;
                    strRedirect = Request.QueryString["ReturnUrl"];
                    if (strRedirect == null)
                        strRedirect = Request.ApplicationPath + "/default.aspx";
                    Response.Redirect(strRedirect, true);
                }
                else if (authResult == "NotInGroup") Response.Redirect("default.aspx?cmd=login&error=" + HttpUtility.UrlEncode("Your account has not been granted access to this application.<br />If you feel this is in error, please contact the Helpdesk at " + ConfigurationManager.AppSettings["HelpEmail"] + "<br />using Reference Code 09.") + "&ReturnUrl=" + HttpUtility.UrlEncode(Request["ReturnUrl"]), true);
                else if (authResult == "UnknownUser") Response.Redirect("default.aspx?cmd=login&error=" + HttpUtility.UrlEncode("We were unable to find this email address in our records.  Please try again.<br />If the issue persists, please email the Helpdesk at " + ConfigurationManager.AppSettings["HelpEmail"] + "<br />using Reference Code 04.") + "&ReturnUrl=" + HttpUtility.UrlEncode(Request["ReturnUrl"]), true);
                else if (authResult == "BadPassword") Response.Redirect("default.aspx?cmd=login&error=" + HttpUtility.UrlEncode("This password doesn't match our records. Please try again, but be aware that 10 failed attempts will lock out your account temporarily.  If the issue persists, please email the Helpdesk at " + ConfigurationManager.AppSettings["HelpEmail"] + " using Reference Code 05.") + "&ReturnUrl=" + HttpUtility.UrlEncode(Request["ReturnUrl"]), true);
                else if (authResult == "AccountLockedOut") Response.Redirect("default.aspx?cmd=login&error=" + HttpUtility.UrlEncode("Your account has been locked out temporarily.  Please try again in 30 minutes.") + "&ReturnUrl=" + HttpUtility.UrlEncode(Request["ReturnUrl"]), true);
                else if (authResult == "AccountDisabled") Response.Redirect("default.aspx?cmd=login&error=" + HttpUtility.UrlEncode("This account has been disabled.  Please contact your administrator.") + "&ReturnUrl=" + HttpUtility.UrlEncode(Request["ReturnUrl"]), true);
                else if (authResult == "PasswordMustChange") Response.Redirect(ConfigurationManager.AppSettings["PasswordChangeSite"] + "?email=" + HttpUtility.UrlEncode(emailAddress.Value), true);
                else if (authResult == "PasswordExpired") Response.Redirect(ConfigurationManager.AppSettings["PasswordChangeSite"] + "?email=" + HttpUtility.UrlEncode(emailAddress.Value), true);
                Response.Redirect("default.aspx?cmd=login&error=" + HttpUtility.UrlEncode("We cannot authenticate your account.  Please contact the Helpdesk at " + ConfigurationManager.AppSettings["HelpEmail"] + " using Reference Code 08.") + "&ReturnUrl=" + HttpUtility.UrlEncode(Request["ReturnUrl"]), true);

            }
            else Response.Redirect("default.aspx?cmd=login&error=" + HttpUtility.UrlEncode("Your email seems to be incorrectly formatted.  Please try again...") + "&ReturnUrl=" + HttpUtility.UrlEncode(Request["ReturnUrl"]), true);
        }


    }

    //private void PopulateUserList()
    //{
    //    XmlUtility xmlUtility = new XmlUtility();
    //    ArrayList usersList = xmlUtility.GetPropsFromXmlFile(usersFilePath);
    //    if (usersList == null || usersList.Count == 0)
    //    {
    //        String errorMessage = "Error in " + usersList;
    //        Context.Response.Redirect(Constants.WEB_PREFIX + Constants.ERROR_PAGE + errorMessage);
    //    }
    //    else
    //    {
    //        for (int i = 0; i < usersList.Count; i++)
    //        {
    //            Hashtable user = (Hashtable)usersList[i];
    //            String subject = (String)user[Constants.USER_ID];
    //            userName.Items.Add(subject);
    //        }
    //    }
    //}

    public String PrintUserAttributes()
    {
        return samlLinks.WriteHtmlUserAttributes(Request, (MultiStringDictionary)Session[Constants.USER_INFO]);
    }

    public string AuthenticateUser(string email, string password, out MultiStringDictionary userInfo)
    {
        string returnValue = "AuthenticationSuccessful";
        string domain = "";
        userInfo = null;

        DirectoryEntry v_searchRoot = new DirectoryEntry(ConfigurationManager.AppSettings["LDAPConnectionString_CORP"], ConfigurationManager.AppSettings["CorpLDAPUser"], ConfigurationManager.AppSettings["CorpLDAPPassword"]);
        DirectorySearcher v_findUser = new DirectorySearcher(v_searchRoot, "(|(mail=" + email + ")(proxyAddresses=smtp:" + email + "))");
        v_findUser.PropertiesToLoad.Add("mail");
        v_findUser.PropertiesToLoad.Add("displayName");
        v_findUser.PropertiesToLoad.Add("samAccountName");
        v_findUser.PropertiesToLoad.Add("extensionAttribute8");
        v_findUser.PropertiesToLoad.Add("sn");
        v_findUser.PropertiesToLoad.Add("givenName");
        v_findUser.PropertiesToLoad.Add("streetAddress");
        v_findUser.PropertiesToLoad.Add("st");
        v_findUser.PropertiesToLoad.Add("l");
        v_findUser.PropertiesToLoad.Add("c");
        v_findUser.PropertiesToLoad.Add("co");
        v_findUser.PropertiesToLoad.Add("objectGuid");
        v_findUser.PropertiesToLoad.Add("postalCode");
        v_findUser.PropertiesToLoad.Add("memberOf");
        v_findUser.PropertiesToLoad.Add("telephoneNumber");
        v_findUser.PropertiesToLoad.Add("distinguishedName");
        v_findUser.PropertiesToLoad.Add("company");

        SearchResult v_result = v_findUser.FindOne();
        if (v_result == null)
        {
            domain = "ipgext";
            v_searchRoot.Path = ConfigurationManager.AppSettings["LDAPConnectionString_ExternalUsers"];
            v_searchRoot.Username = ConfigurationManager.AppSettings["ExtLDAPUser"];
            v_searchRoot.Password = ConfigurationManager.AppSettings["ExtLDAPPassword"];
            v_searchRoot.RefreshCache();
            v_result = v_findUser.FindOne();
            if (v_result == null) return "UnknownUser";

        }
        else
        {
            if (v_result.Path.ToLower().Contains("dc=na,")) domain = "ipgna";
            if (v_result.Path.ToLower().Contains("dc=emea,")) domain = "ipgemea";
            if (v_result.Path.ToLower().Contains("dc=ap,")) domain = "ipgap";
            if (v_result.Path.ToLower().Contains("dc=la,")) domain = "ipgla";
        }


        IntPtr tokenHandle = new IntPtr(0);
        IntPtr dupeTokenHandle = new IntPtr(0);
        try
        {
            List<string> v_groupList = new List<string>();

            bool v_isInGroup = false;
            if (v_result.Properties["memberOf"].Count > 0)
            {
                if (ConfigurationManager.AppSettings["RequiredSecurityGroups"] != null)
                {
                    foreach (string v_group in ConfigurationManager.AppSettings["RequiredSecurityGroups"].Split(';'))
                    {
                        if (v_result.Properties["memberOf"].Contains(v_group.Trim()))
                        {
                            v_isInGroup = true;
                            if (!v_groupList.Contains(GetGroupName(v_group.Trim()))) v_groupList.Add(GetGroupName(v_group.Trim()));
                        }

                    }
                }
                else v_isInGroup = true;

                if (ConfigurationManager.AppSettings["OptionalSecurityGroups"] != null)
                {
                    foreach (string v_group in ConfigurationManager.AppSettings["OptionalSecurityGroups"].Split(';'))
                    {
                        if (v_result.Properties["memberOf"].Contains(v_group.Trim()))
                        {
                            if (!v_groupList.Contains(GetGroupName(v_group.Trim()))) v_groupList.Add(GetGroupName(v_group.Trim()));
                        }
                    }
                }
            }
            else if(ConfigurationManager.AppSettings["RequiredSecurityGroups"] != null) return "NotInGroup";
            if (!v_isInGroup) return "NotInGroup";

            const int LOGON32_PROVIDER_DEFAULT = 0;
            const int LOGON32_LOGON_INTERACTIVE = 2;

            tokenHandle = IntPtr.Zero;
            bool authSuccess = LogonUser((string)v_result.Properties["samAccountName"][0], domain, password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, ref tokenHandle);

            if (authSuccess == false)
            {
                int ret = Marshal.GetLastWin32Error();
                if (ret == 1907)
                {
                    returnValue = "PasswordMustChange";

                }
                else if (ret == 1330)
                {
                    returnValue = "PasswordExpired";

                }
                else if (ret == 1909) returnValue = "AccountLockedOut";
                else if (ret == 1331) returnValue = "AccountDisabled";
                else if (ret == 1326) returnValue = "BadPassword";
                else returnValue = "Unknown(" + ret.ToString() + ")";
                return returnValue;
                //throw new System.ComponentModel.Win32Exception(ret);
            }

            userInfo = new MultiStringDictionary();
            userInfo.Add("eduPersonEntitlement", v_groupList);
            userInfo.Add("authncontext", "PASSWORD");
           
            //userInfo.Add("displayName", (string)v_result.Properties["displayName"][0]);
            //userInfo.Add("DN", (string)v_result.Properties["distinguishedName"][0]);
            //userInfo.Add("samAccountName", (string)v_result.Properties["samAccountName"][0]);
            
            userInfo.Add("subject", email);

            /*
            string v_extensionAttribute8 = "N/A";
            if (v_result.Properties["extensionAttribute8"].Count > 0) v_extensionAttribute8 = (string)v_result.Properties["extensionAttribute8"][0];
            userInfo.Add("extensionAttribute8", v_extensionAttribute8.Replace(Environment.NewLine, " "));

            string v_c = "";
            if (v_result.Properties["c"].Count > 0) v_c = (string)v_result.Properties["c"][0];
            userInfo.Add("c", v_c.Replace(Environment.NewLine, " "));
*/
            string v_sn = "";
            if (v_result.Properties["sn"].Count > 0) v_sn = (string)v_result.Properties["sn"][0];
            userInfo.Add("sn", v_sn.Replace(Environment.NewLine, " "));

            string v_givenName = "";
            if (v_result.Properties["givenName"].Count > 0) v_givenName = (string)v_result.Properties["givenName"][0];
            userInfo.Add("givenName", v_givenName.Replace(Environment.NewLine, " "));
/*
            string v_streetAddress = "";
            if (v_result.Properties["streetAddress"].Count > 0) v_streetAddress = (string)v_result.Properties["streetAddress"][0];
            if (v_streetAddress.Contains(Environment.NewLine)) v_streetAddress = v_streetAddress.Substring(0, v_streetAddress.IndexOf(Environment.NewLine));
            userInfo.Add("streetAddress", v_streetAddress);

            string v_l = "";
            if (v_result.Properties["l"].Count > 0) v_l = (string)v_result.Properties["l"][0];
            userInfo.Add("l", v_l.Replace(Environment.NewLine, " "));

            string v_st = "";
            if (v_result.Properties["st"].Count > 0) v_st = (string)v_result.Properties["st"][0];
            userInfo.Add("st", v_st.Replace(Environment.NewLine, " "));

            string v_postalCode = "";
            if (v_result.Properties["postalCode"].Count > 0) v_postalCode = (string)v_result.Properties["postalCode"][0];
            userInfo.Add("postalCode", v_postalCode.Replace(Environment.NewLine, " "));

            string v_telephoneNumber = "";
            if (v_result.Properties["telephoneNumber"].Count > 0) v_telephoneNumber = (string)v_result.Properties["telephoneNumber"][0];
            userInfo.Add("telephoneNumber", v_telephoneNumber.Replace(Environment.NewLine, " "));
*/
            string v_mail = "";
            if (v_result.Properties["mail"].Count > 0) v_mail = (string)v_result.Properties["mail"][0];
            userInfo.Add("mail", v_mail.Replace(Environment.NewLine, " "));
 /*           
            string v_company = "";
            if (v_result.Properties["company"].Count > 0) v_company = (string)v_result.Properties["company"][0];
            userInfo.Add("company", v_mail.Replace(Environment.NewLine, " "));

            Guid v_guid = new Guid((byte[])v_result.Properties["objectGuid"][0]);
            userInfo.Add("objectGuid", v_guid.ToString().ToUpper());
*/
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (tokenHandle != IntPtr.Zero) CloseHandle(tokenHandle);
            if (dupeTokenHandle != IntPtr.Zero) CloseHandle(dupeTokenHandle);
            v_findUser.Dispose();
            v_searchRoot.Dispose();

        }

        return returnValue;
    }

    private string GetGroupName(string groupDN)
    {
        string v_return = "urn:";
        if(groupDN.Contains("DC=ext,DC=ipgnetwork,DC=com")) v_return += "ext.ipgnetwork.com:";
        if(groupDN.Contains("DC=na,DC=corp,DC=ipgnetwork,DC=com")) v_return += "na.corp.ipgnetwork.com:";
        if (groupDN.Contains("DC=ap,DC=corp,DC=ipgnetwork,DC=com")) v_return += "ap.corp.ipgnetwork.com:";
        if (groupDN.Contains("DC=la,DC=corp,DC=ipgnetwork,DC=com")) v_return += "la.corp.ipgnetwork.com:";
        if (groupDN.Contains("DC=emea,DC=corp,DC=ipgnetwork,DC=com")) v_return += "emea.corp.ipgnetwork.com:";

        v_return = v_return + ((groupDN.Replace("\\,", " ").Split(','))[0]).Replace("CN=", "").Replace(" ", "-");

        return v_return;
    }

}

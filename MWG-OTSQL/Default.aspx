﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default"    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
    <title>Login to wgPass</title>


<link href="css/wgPass_Login_styles.css" rel="stylesheet" type="text/css" media="screen" />
<style type="text/css"> 
/* place css fixes for all versions of IE in this conditional comment */
.thrColEls #sidebar1, .thrColEls #sidebar2 { padding-top: 30px; }
.thrColEls #mainContent { zoom: 1; padding-top: 15px; }
/* the above proprietary zoom property gives IE the hasLayout it needs to avoid several bugs */
</style>


    <script type="text/javascript" src="scripts/common.js"></script>

    <script language="javascript">
		function initiateSSO() {
			url = createSSOUrl(document.getElementById('PartnerSpId').value);
			window.location = url;
		}
		function createSSOLink() {
			url = createSSOUrl(document.getElementById('PartnerSpId').value);
			document.getElementById('ssolink').value = url;
			document.getElementById('ssolink').select();
			copyToClipboard(document.getElementById('ssolink').value);
		}		
		function initiateSLO() {
			url = "<%= idpStartSLO %>";
			window.location = url;
		}		
		function createSLOLink() {
			url = "<%= idpStartSLO %>";
			document.getElementById('slolink').value = url;
			document.getElementById('slolink').select();
			copyToClipboard(document.getElementById('slolink').value);
		}
		
		function submitForm()
		{
		    if(document.getElementById('emailAddress').value.length == 0)
		    {
		        document.getElementById('errorMessage').innerHTML = 'Email cannot be blank.  Please type a valid email address.';
		        return;
		    }
		    if(document.getElementById('password').value.length == 0)
		    {
		        document.getElementById('errorMessage').innerHTML = 'Password cannot be blank.  Please type your email password.';
		        return;
		    }
		    //document.getElementById('loginButton').style.display = 'none';
		    //document.getElementById('workingButton').style.display = 'block';
		    document.Form1.submit();
		}				
		
		function entsub(event)
        {
            event = event || window.event;
            if(event.which == 13) 
            {
                submitForm();
                return false;
            }
            else if (event.keyCode == 13)
            {
                submitForm();
                return false;
            }
            else return true;
        }
    </script>

</head>
<body class="thrColEls" onload="javascript:document.getElementById('emailAddress').focus();">
    <div id="container">
        <form id="Form1" method="post" runat="server" action="Default.aspx?cmd=login">

<table cellspacing="0" cellpadding="0" border="0">

  <tr>
    <td colspan="4" style="padding-top:50px; padding-left:50px" align="left">
      <img src="images/mw_sitelogo.jpg"  alt="McCann Worldgroup"/>
    </td>
  </tr>
  <tr>
    <td>
      <img src="images/blank.png" width="200" />
    </td>
    <td>
      <img src="images/blank.png" />
    </td>
    <td style="padding-top:100px;">
      <img src="images/LogoWgPass.png" width="255" height="94" alt="wg PASS" />
    </td>
    <td>
      <img src="images/blank.png" />
    </td>
  </tr>
  <tr>
    <td>
      <img src="images/blank.png" />
    </td>
    <td width="191" valign="top">
      <div id="sidebar1">
        <h3>Sign On</h3>
        <p>Signing On here automatically signs you on to other <strong>wgPass</strong> sites for the next 8 hours.</p>
      </div>
    </td>
    <td width="256" valign="top">
      <div class="login">
      <div class="loginText">Email</div>
      <div><input runat="server" name="emailAddress" id="emailAddress" type="text" class="input" onkeypress="entsub(event);" /></div>
      </div>

      <div class="login">
      <div class="loginText" style="margin-top:20px">Password</div>
      <div><input runat="server" type="password" class="input" id="password" name="password" onkeypress="entsub(event);" /></div>
      </div>

      <div class="login">
      <div class="checkbox"><input type="checkbox" runat="server" id="publicComputer" name="publicComputer" style="background-color:#fff; border:none" checked="checked" />This is a public or shared computer</div>
      </div>

      <div class="login">
      <div class="loginText"></div>
      <div align="right"><input name="Login" type="button" class="loginButton" onclick="javascript:submitForm();" /></div>
      </div>

      <div class="login" style="margin-top:10px">
      <div class="contact" align="right"><a href="mailto:Helpdesk@your-accounts.com?subject=Portal Login support issue">Need Help?</a></div>
      </div>
    </td>
    <td width="191" valign="top">
    <div id="sidebar2">
      <table border="0">
      <tr>
        <td valign="top" style="padding-right:5px; padding-top:5px">
        <%	String errorMessage = Request["error"];

                  if (errorMessage != null)
                  {

              %>
              <img src="images/ErrorBullet.png" width="5" height="18" alt="Error" / >
              <%}
                else
                {%>
              &nbsp;
              <%} %>
        
        
        </td>
          <td valign="top">
          <%	

                  if (errorMessage != null)
                  {

              %>
              <%=errorMessage%>
              <%}
                else
                {%>
              &nbsp;
              <%} %>
          </td>
     </tr>
     </table> 
     </div>
    </td>
  </tr>
</table>


</form>   
	<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />
<!-- end #container --></div>
       
</body>
</html>

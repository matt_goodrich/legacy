﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="config.aspx.cs" Inherits="config_config" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
	<head>
	<META http-equiv=Content-Type content="text/html; charset=utf-8">
	<LINK media="screen" href="../images/main.css" type=text/css rel=stylesheet>
	<META content="MSHTML 6.00.2800.1400" name=GENERATOR>	
	<title>IdP Integration Configuration</title>
	<script type="text/javascript" src="../scripts/common.js"></script>
</head>



<BODY>
<div id="container">
	<div id="left_header">
		<img src="../images/idpbadge.png" align="middle"/>
	</div>
	<div id="right_header">
		<div id="text" style="clear: both;margin-top: 30px;">Identity Provider</div>
	</div>
	<div id="menu">
		<a href="../default.aspx">Identity Provider Home</a> 
	</div>

	<div id="content">
   		<form name="config" method="POST">        
		<table class="cell" style="margin-left: auto; margin-right: auto;">
			<tr>
				<td colspan=2 style="text-align: center;">
					<h1>Configuration Options</h1>
					<hr class="cell"/>
				</td>
			</tr>
  				   <tr>
  					   <td align="left">PF Host Name: </td>
  					   <td align="left"><input type="text" name="<%= SAMPLE_PF_HOST %>" id="<%= SAMPLE_PF_HOST %>" value="<%= hostPF %>"></td>
  				   </tr>
  				   <tr>
  					   <td align="left">Adapter Type: </td>
  					   <td align="left">  					   	
  					   	<select name="<%= SAMPLE_TOKEN_TYPE %>" id="<%= SAMPLE_TOKEN_TYPE %>">
  					   	<% if( tokenType.Equals("OpenToken") ) { %>
  							<option value ="OpenToken" selected>OpenToken</option>
  							<option value ="PFTOKEN">Standard (PFTOKEN)</option>
							<option value ="Other">Other</option>						  							  							
  						<% } else if( tokenType.Equals("PFTOKEN") ) { %>
  							<option value ="OpenToken">OpenToken</option>
  							<option value ="PFTOKEN" selected>Standard (PFTOKEN)</option>						
							<option value ="Other">Other</option>						  							
						<% } else { %>
  							<option value ="OpenToken">OpenToken</option>
  							<option value ="PFTOKEN">Standard (PFTOKEN)</option>						
							<option value ="Other" selected>Other</option>						  													
  						<% } %>
  						</select>
  					   </td>
  				   </tr>				   

				    <tr>
					<td></td>
					<td>
					<% if (showAdvanced) { %>
						<a href="?showAdvanced=false">Hide advanced options</a>
					<% } else {%>
						<a href="?showAdvanced=true">Show advanced options</a>
					<% } %>
					</td>
				    </tr>				     								   

            			<% 	if (showAdvanced) { %>  				   
  				   <tr>
  					   <td align="left">Use SSL: </td>
  					   <td align="left">  					   	
                        <input type="checkbox" name="<%= SAMPLE_USE_SSL %>" id="<%= SAMPLE_USE_SSL %>" value="true"  					   
  					   <% if (useSSL.Equals("true")) 
  					   { %>
  					   	    checked
  					   	<% } %>
  					   	    >
  					   </td>
  				   </tr>				   				   				     				               			
  				   <tr>
  					   <td align="left">PF HTTP Port: </td>
  					   <td align="left">  					   	
  					   	<input type="text" name="<%= SAMPLE_PF_HTTP_PORT %>" id="<%= SAMPLE_PF_HTTP_PORT %>" value="<%= httpPort %>">
  					   </td>
  				   </tr>				   				   				     				   
  				   <tr>
  					   <td align="left">PF HTTPS Port: </td>
  					   <td align="left">  					   	
  					   	<input type="text" name="<%= SAMPLE_PF_HTTPS_PORT %>" id="<%= SAMPLE_PF_HTTPS_PORT %>" value="<%= httpsPort %>">
  					   </td>
  				   </tr>				   				   				     				     				   
  				   <tr>
  					   <td align="left">PF Web Service Username: </td>
  					   <td align="left">  					   	
  					   	<input type="text" name="<%= SAMPLE_PF_WS_UNAME %>" id="<%= SAMPLE_PF_WS_UNAME %>" value="<%= wsUname %>">
  					   </td>
  				   </tr>				   				   				     				     				     				   
  				   <tr>
  					   <td align="left">PF Web Service Password: </td>
  					   <td align="left">  					   	
  					   	<input type="password" name="<%= SAMPLE_PF_WS_PWD %>" id="<%= SAMPLE_PF_WS_PWD %>" value="<%= wsPwd %>">
  					   </td>
  				   </tr>				   				   				     				   
		   				   				   				   				   				   
  				   
				<%	} %>
  				   <tr>
					<td></td>
  					<td>
  						<input runat=server type="submit" value="Save" id="Submit1" onserverclick="Submit1_ServerClick">
  					   </td>
				   </tr>
       </TABLE>
  				</form>        


				<FORM id="FORM1" ENCTYPE="multipart/form-data" runat="server">            	
					<table class="cell" style="margin-left: auto; margin-right: auto;">				   
				<tr>
  					   <td align="left">Adapter Properties: </td>
  					   <td align="left">
  						<INPUT TYPE="file" ID="propFile" NAME="propFile" runat="server">
  						<INPUT runat="server" id=propFileUpload TYPE="submit" VALUE="Upload" onserverclick="propFileUpload_ServerClick">
					</td>
				</tr>   				  				          
        				</TABLE>
				</form>          
</div></div>
  </body>
</html>

/*******************************************************************************
* Copyright (C) 2008 Ping Identity Corporation All rights reserved.
*
* This software is licensed under the Open Software License v2.1 (OSL v2.1).
*
* A copy of this license has been provided with the distribution of this
* software. Additionally, a copy of this license is available at:
* http://opensource.org/licenses/osl-2.1.php
*
******************************************************************************
*/
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Com.PingIdentity.Adapters.Pftoken.Util;
using Com.PingIdentity.PingFederate.SampleApp.SampleAppUtility;

public partial class config_config : System.Web.UI.Page
{
    public const String SAMPLE_PF_HOST = "hostPF";	
	public const String SAMPLE_TOKEN_TYPE = "tokenType";
    public const String SAMPLE_USE_SSL = "useSSL";
	public const String SAMPLE_ACCOUNT_LINKING = "accountLinking";		
	public const String SAMPLE_RAW_DATA = "rawData";			
	public const String SAMPLE_ATTRS_NAMES_LIST = "attributeNamesList";	
	public const String SAMPLE_PF_HTTP_PORT = "httpPort";	
	public const String SAMPLE_PF_HTTPS_PORT = "httpsPort";	
	public const String SAMPLE_PF_WS_UNAME = "wsUname";
    public const String SAMPLE_PF_WS_PWD = "wsPwd";

    protected String hostPF = "";
    protected String tokenType = "";
    protected String useSSL = "";
    protected String httpPort = "";
    protected String httpsPort = "";
    protected String wsUname = "";
    protected String wsPwd = "";	

    protected bool showAdvanced = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["showAdvanced"] != null)
        {
            if (Request["showAdvanced"].Equals("true"))
            {
                showAdvanced = true;
            }
            else
            {
                showAdvanced = false;
            }
        }

        String idpConfig = ConfigurationManager.AppSettings["config-file"];
        String configPath = Context.Server.MapPath(idpConfig);

        // Read properties
        PropertyReader configPropertyReader = new PropertyReader();

        StreamReader configStreamReader = new StreamReader(configPath);
        IDictionary configProperties = configPropertyReader.Load(configStreamReader);
        configStreamReader.Close();

        if ((Request.RequestType.Equals("POST")) &&
            (propFile.PostedFile == null))
        {
            Dictionary<String, String> properties = new Dictionary<String, String>();
            properties.Add(SAMPLE_PF_HOST, Request[SAMPLE_PF_HOST]);
            properties.Add(SAMPLE_TOKEN_TYPE, Request[SAMPLE_TOKEN_TYPE]);
	    if (Request[SAMPLE_PF_HTTP_PORT] != null)
	    {
		if (Request[SAMPLE_USE_SSL] != null)
		{
			if (Request[SAMPLE_USE_SSL].Equals("true"))
			{
				properties.Add(SAMPLE_USE_SSL, "true");
			}
			else
			{
                    		properties.Add(SAMPLE_USE_SSL, "false");
			}
		}
		else
		{
			properties.Add(SAMPLE_USE_SSL, "false");
		}
	    }
	    else
	    {
		properties.Add(SAMPLE_USE_SSL, (String)configProperties[SAMPLE_USE_SSL]);
	    }
            if (Request[SAMPLE_PF_HTTP_PORT] != null)
            {
                properties.Add(SAMPLE_PF_HTTP_PORT, Request[SAMPLE_PF_HTTP_PORT]);
            }
            else
            {
                properties.Add(SAMPLE_PF_HTTP_PORT, (String)configProperties[SAMPLE_PF_HTTP_PORT]);
            }
            if (Request[SAMPLE_PF_HTTPS_PORT] != null)
            {
                properties.Add(SAMPLE_PF_HTTPS_PORT, Request[SAMPLE_PF_HTTPS_PORT]);
            }
            else
            {
                properties.Add(SAMPLE_PF_HTTPS_PORT, (String)configProperties[SAMPLE_PF_HTTPS_PORT]);
            }
            if (Request[SAMPLE_PF_WS_UNAME] != null)
            {
                properties.Add(SAMPLE_PF_WS_UNAME, Request[SAMPLE_PF_WS_UNAME]);
            }
            else
            {
                properties.Add(SAMPLE_PF_WS_UNAME, (String)configProperties[SAMPLE_PF_WS_UNAME]);
            }
            if (Request[SAMPLE_PF_WS_PWD] != null)
            {
                properties.Add(SAMPLE_PF_WS_PWD, Request[SAMPLE_PF_WS_PWD]);
            }
            else
            {
                properties.Add(SAMPLE_PF_WS_PWD, (String)configProperties[SAMPLE_PF_WS_PWD]);
            }

            StreamWriter propsWriter = new StreamWriter(configPath);

            foreach (KeyValuePair<string, string> kvp in properties)
            {
                String infoName = kvp.Key;
                String infoValue = kvp.Value;

                propsWriter.WriteLine(infoName + "=" + infoValue);

            }

            propsWriter.Flush();
            propsWriter.Close();

            hostPF = (String)properties[SAMPLE_PF_HOST];
            tokenType = (String)properties[SAMPLE_TOKEN_TYPE];
            useSSL = (String)properties[SAMPLE_USE_SSL];
            httpPort = (String)properties[SAMPLE_PF_HTTP_PORT];
            httpsPort = (String)properties[SAMPLE_PF_HTTPS_PORT];
            wsUname = (String)properties[SAMPLE_PF_WS_UNAME];
            wsPwd = (String)properties[SAMPLE_PF_WS_PWD];	

        }
        else
        {
            hostPF = (String)configProperties[SAMPLE_PF_HOST];
            tokenType = (String)configProperties[SAMPLE_TOKEN_TYPE];
            useSSL = (String)configProperties[SAMPLE_USE_SSL];
            httpPort = (String)configProperties[SAMPLE_PF_HTTP_PORT];
            httpsPort = (String)configProperties[SAMPLE_PF_HTTPS_PORT];
            wsUname = (String)configProperties[SAMPLE_PF_WS_UNAME];
            wsPwd = (String)configProperties[SAMPLE_PF_WS_PWD];	
        }

    }
    protected void Submit1_ServerClick(object sender, EventArgs e)
    {
        Dictionary<String, String> properties = new Dictionary<String, String>();
        properties.Add(SAMPLE_PF_HOST,hostPF);
        String idpConfig = ConfigurationManager.AppSettings["config-file"];
        String configPath = Context.Server.MapPath(idpConfig);
        Links.SaveConfig(properties, configPath);
    }
    protected void propFileUpload_ServerClick(object sender, EventArgs e)
    {
        if ((propFile.PostedFile != null) && (propFile.PostedFile.ContentLength > 0))
        {
            string SaveLocation = "";
            if (tokenType.Equals("PFTOKEN"))
            {
                SaveLocation = Context.Server.MapPath("pfagent.properties");
            }
            else
            {
                SaveLocation = Context.Server.MapPath("agent-config.txt");
            }
            try
            {
                propFile.PostedFile.SaveAs(SaveLocation);
                Response.Write("The file has been uploaded.");
            }
            catch (Exception ex)
            {
                Response.Write("Error: " + ex.Message);
                //Note: Exception.Message returns a detailed message that describes the current exception. 
                //For security reasons, we do not recommend that you return Exception.Message to end users in 
                //production environments. It would be better to put a generic error message. 
            }
        }
        else
        {
            Response.Write("Please select a file to upload.");
        }
    }
}

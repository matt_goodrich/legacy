/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testteammatewsdl;


import java.net.*;
import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

/**
 *
 * @author Matt
 */
public class TestTeammateWSDL {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException, IOException{
        // TODO code application logic here
               
        String soapMessage = getSoapAppIDRequestXMLString("VillageWeb"); //getSoapRequestXMLString(0, "SEA-DVAAUTHDV01", "SHAHOLMES", 0);
        
        URL u = new URL("http://sea-rescstg01.davita.com/component/app.cfc");
        URLConnection uc = u.openConnection();
        HttpURLConnection connection = (HttpURLConnection) uc;
        
        String userPassword = "foo:bar";
        String encoding = new sun.misc.BASE64Encoder().encode(userPassword.getBytes());
        
        connection.setRequestProperty("Authorization", "Basic " + encoding);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("SOAPAction", "getAppID");
        
        OutputStream out = connection.getOutputStream();
        Writer wout = new OutputStreamWriter(out);
        
        wout.write(soapMessage);
        wout.flush();
        wout.close();
        
        String xmlString = convertStreamToString(connection.getInputStream());
        
        System.out.println(xmlString);
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        
        try
        {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse( new InputSource(new StringReader( xmlString )));
            doc.getDocumentElement().normalize();
            
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("getAppIDReturn");
            System.out.println("-----------------------");
            System.out.println("NodeList Length: " + nList.getLength());
            
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                
                System.out.println(nNode.getChildNodes().item(0).getTextContent());
                
                //System.out.print(nNode.getChildNodes().item(3).getNodeName() + ": ");
                //System.out.println(nNode.getChildNodes().item(3).getTextContent());
            }
        }
        catch (Exception e) { e.printStackTrace(); }
        

        //InputStream in = ;
        //int c;
        //while ((c = in.read()) != -1) System.out.write(c);
        //in.close();        
    }
    
    private static String getSoapAppIDRequestXMLString(String Nickname)
    {
        //Setup our SOAP request
        String xmldata = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<soap:Envelope " +
                "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:com=\"http://component\" " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
                "<soap:Body soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                "<com:getAppID>" +
			"<Nickname xsi:type=\"xsd:string\">" + Nickname + "</Nickname>" +
		"</com:getAppID>" +
                "</soap:Body>" +
                "</soap:Envelope>";
        
        //return the request as a String
        return xmldata;
    }
    
    public static String getSoapRequestXMLString(double appNumber, String HostName, String Username, double privAppNumber)
    {
        String xmldata = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<soap:Envelope " +
                "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
                "xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" " +
                "xmlns:tns=\"http://person\" " +
                "xmlns:types=\"http://person/encodedTypes\" " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">" +
                "<soap:Body soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
                "<tns:getUserPrivs>" +
			"<app_number xsi:type=\"xsd:double\">" + appNumber + "</app_number>" +
			"<host_name xsi:type=\"xsd:string\">" + HostName + "</host_name>" +
			"<username xsi:type=\"xsd:string\">" + Username + "</username>" +
			"<privAppNo xsi:type=\"xsd:double\">" + privAppNumber + "</privAppNo>" +
		"</tns:getUserPrivs>" +
                "</soap:Body>" +
                "</soap:Envelope>";
        
        return xmldata;
    }
    
    static String convertStreamToString(java.io.InputStream is) {
    try {
        return new java.util.Scanner(is).useDelimiter("\\A").next();
    } catch (java.util.NoSuchElementException e) {
        return "";
    }
}
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Error
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="prefix_7 grid_12">
        <div class="content">
            <div class="banner">
                <h2>AAA Insurance Agent Portal</h2>
            </div>
            <h3>Login Error</h3>
            <p>An error has occurred. Please contact the ITSC at 877-554-2911 to resolve this issue.</p> 
        </div>
    </div>

</asp:Content>



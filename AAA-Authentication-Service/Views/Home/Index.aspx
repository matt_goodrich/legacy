﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/MasterPage.Master" Inherits="System.Web.Mvc.ViewPage<LoginApp.ViewModels.HomeIndexViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Login
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Head" runat="server">
    <script src="<%=ResolveClientUrl("~/Scripts/Home/Home.js")%>" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div class="prefix_7 grid_12">
        <div class="content">
            <div class="banner">
                <h2>AAA Insurance Agent Portal</h2>
            </div>
            <% using (Html.BeginForm(null, null, FormMethod.Post, new { @id = "LoginForm", @action = String.Format("./{0}", Request.Url.Query) })) { %>
                <table>
                    <tr>
                        <th></th>
                        <td><%= Html.ValidationSummary() %></td>
                    </tr>
                    <tr>
                        <th><%= Html.LabelFor(m => m.Form.UserID) %></th>
                        <td><%= Html.TextBoxFor(m => m.Form.UserID, new { @autocomplete = "off", @title = "Your Email Address or AAA IE User ID", @class = "defaultText"}) %></td>
                    </tr>
                    <tr>
                        <th><%= Html.LabelFor(m => m.Form.Password) %></th>
                        <td><%= Html.PasswordFor(m => m.Form.Password, new { @autocomplete = "off" }) %></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a id="LoginButton" class="button primary disabled">Login</a>
                            <noscript>
                                <input type="submit" value="Login" id="NoScriptLoginButton" name="Command" />
                            </noscript>    
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <a id="ChangePassButton" class="disabled" href="#">Reset your Password</a>
                            <noscript>
                                <input type="submit" value="Reset your Password" id="NoScriptChangePassButton" name="Command" />
                            </noscript>  
                        </td>
                    </tr>
                </table>
            <% } %>
        </div>
    </div>
</asp:Content>



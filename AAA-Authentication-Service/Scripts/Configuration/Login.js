﻿$(document).ready(function () {
    $("input").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            SubmitLogin();
        }
    });

    $("#LoginButton").click(function () {
        SubmitLogin();
    });

    function SubmitLogin() {
        $("#LoginForm").submit();
    }


    $(".defaultText").focus(function (srcc) {
        if ($(this).val() == $(this)[0].title) {
            $(this).removeClass("defaultTextActive");
            $(this).val("");
        }
    });

    $(".defaultText").blur(function () {
        if ($(this).val() == "") {
            $(this).addClass("defaultTextActive");
            $(this).val($(this)[0].title);
        }
    });

    $(".defaultText").blur();

    $("#LoginForm").submit(function () {
        $(".defaultText").each(function () {
            if ($(this).val() == $(this)[0].title) {
                $(this).val("");
            }
        });
    });
});
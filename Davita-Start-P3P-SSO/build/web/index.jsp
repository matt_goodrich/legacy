<%-- 
    Document   : index
    Created on : Mar 9, 2012, 11:50:26 AM
    Author     : Matt
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%

    String host = request.getServerName();
    String SPID = "";
    String ACSIdx = "";
    
    String BuiltURL = "";
    
    if(request.getParameter("PartnerSpId") != null) {
        SPID = request.getParameter("PartnerSpId");
        BuiltURL += "PartnerSpId=" + SPID;
    }
    if(request.getParameter("ACSIdx") != null) {
        ACSIdx = request.getParameter("ACSIdx");
        BuiltURL += "&ACSIdx=" + ACSIdx;
    }
        
    response.addHeader("p3p", "CP=\"ALL ADM PSA COM STA OUR OTRo STP\"");
    response.sendRedirect("https://" + host + ":9031/idp/startSSO.ping?" + BuiltURL);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Start P3P SSO</title>
    </head>
    <body>
    </body>
</html>

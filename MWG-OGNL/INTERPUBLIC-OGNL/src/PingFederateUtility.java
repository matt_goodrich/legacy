/*
--[ PROLOGUE
--@ ABSTRACT
--@   Name
--|     Ping Federate OGNL Utilities
--|
--@   Identifier
--|     Filename:  PingFederateUtility.java
--|
--@   Contract Identifier
--|     MWG - McCann Worldwide Group
--|
--@   Purpose
--|     This class contains utilities that support the Ping Identity
--|     PingFederate SSO Server, specifically the utilizes are called
--|     via OGNL expressions from the SSO Server.
--|
--@ DESCRIPTION
--@   Error Codes Returned/Exceptions Raised
--|     This object does not raise exceptions.
--|
--@ NOTES
--@   Design Constraints
--|     (1) The method getBrandingCode connects to the SQL Server 2005
--|         database hosting the user's attributes.  The connection
--|         uses a hardcoded service account and password.  Any change
--|         to the password will require this library to be updated
--|         and redployed to PingFederate.  Additionally, the class 
--|         has hardcoded the IP Address of the SQL Server.
--|
--@   Special Processing
--|     None
--|
--@   Side Effects
--|     None
--|
--@   Performance Issues
--|     None
--|
--@   Limitations
--|     None
--|
--@   Target Dependencies
--|     None
--|
--@   Compiler Dependencies
--|     None
--|
--@ LAST REVISION
--|     Date       DR Number  OPR       Modification
--|     =========  =========  ========  =================================
--|     01-APR-11  N/A        Woodward  Initial Release
--|     03-MAY-11  N/A        Uythoven  Implemented assured connection close
--|                                     in the getBrandingCode method.
--|
--------------------------------------------------------------------------
*/

package com.interpublic.pingfederate;


// Import Java libraries
import java.util.*;
import java.sql.*;

// Import PingFederate library for SAML v2.0 token (In pf-protocolengine.jar)
import org.sourceid.saml20.adapter.attribute.*;


// Import the MS SQL Server JDBC Driver (In sqljdbc.jar)
import com.microsoft.sqlserver.jdbc.*;




/**
* The PingFederateUtility class contains utilities that support the Ping Identity
* PingFederate SSO Server, specifically the utilizes are called via OGNL expressions
* from the SSO Server.  This class is deployed to the PingFederate server library
* folder located at <code><Ping-Federate-Install>/pingfederate/server/default/lib</code>.
* <p>
* In order to deploy the OGNL library to PingFederate, the server will need to be
* restarted for the new library to be loaded by the PingFederate class loader.
*
* @author  Halcyon Woodward
* @version 1.0, 01 April 2011
*/
public class PingFederateUtility {

	/**
	* This method takes a long string from SQL database which is a delimited
	* set of SAML attributes and converts into a linked list of attributes
	* so that SAML token can be built by the caller.
	*
	* @param  <code>input</code> String of attributes
	* @param  <code>delimiter</code> Delimiter separating attributes
	* @return <code>List<String></code> - Linked list of individual attribute strings
	*/
	public AttributeValue explode(String input, String delimiter){
		
		String[] v_valueArray = input.split(delimiter);
		List<String> v_values = new LinkedList<String>();
		
		for(int i=0; i<v_valueArray.length; i++)
		{
			v_values.add(v_valueArray[i]);
		}
				
		AttributeValue v_return = new AttributeValue(v_values);
		
		return v_return;
		
	}
	

	/**
	* This method will determine the branding code for a specific user based
	* upon the user's attributes.  The algorithm to determine the branding code
	* first inspects the department, then the brand, and finally user email 
	* address.  The MS SQL Server 2005 database contains tables for
	* <code>BrandingDepartments</code> and <code>BrandingDomains</code> which 
	* are integrated to query for branding code based upon the user's input
	* parameters.  The logic in this method will query the SQL tables using
	* the user's attributes searching for the branding code.
	*
	* @param  <code>department</code> User's Department
	* @param  <code>brand</code> User's Brand
	* @param  <code>email</code> User's Email address
	* @return <code>String</code> - Branding Code (Default is MWG)
	*/
	public AttributeValue getBrandingCode(String department, String brand, String email) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		Driver v_driver;
		Connection v_con = null;
		Statement v_st = null;
		ResultSet v_rs = null;
		String v_return = "MWG";
		
		try {
			//Prepare connection
			v_driver = (Driver)Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
			v_con = DriverManager.getConnection("jdbc:sqlserver://10.196.176.26:1433;database=MWG_PingFederateAttributes", "PingFederateService", "M00seL00se");
			v_st = v_con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			
			//Try to find Department rule
			v_rs = v_st.executeQuery("SELECT Branding FROM BrandingDepartments WHERE Department = '" + department.replaceAll("'", "''") + "'");
			if(v_rs.first()){
				v_return = v_rs.getString(1);
			}
			else{
				//Try to find OU Rule
				v_rs.close();
				v_rs =  v_st.executeQuery("SELECT Branding FROM BrandingOUs WHERE OU = '" + brand.replaceAll("'", "''") + "'");
				if(v_rs.first()){
					v_return = v_rs.getString(1);
				}
				else{
					//Try to find Domain Rule
					v_rs.close();
					String domain = email.split("@")[1];
					v_rs =  v_st.executeQuery("SELECT Branding FROM BrandingDomains WHERE Domain = '" + domain.replaceAll("'", "''") + "'");
					if(v_rs.first()){
						v_return = v_rs.getString(1);
					}
				}
			}
			
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw e;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw e;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw e;
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		} finally {
			// Always clean up connections
			v_rs.close();
			v_st.close();
			v_con.close();
		}
		
		
		return new AttributeValue(v_return);
		
	}

}

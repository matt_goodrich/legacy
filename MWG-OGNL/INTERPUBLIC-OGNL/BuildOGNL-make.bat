@echo off
REM #--[ PROLOGUE -----------------------------------------------------------------
REM #--@ ABSTRACT
REM #--|
REM #--@   Name
REM #--|     Build script for OGNL Java Library
REM #--|
REM #--@   Identifier
REM #--|     Filename:   BuildOGNL.bat
REM #--|
REM #--@   Contract Identifier
REM #--|     MWG - McCann Worldwide Group
REM #--|
REM #--@   Purpose
REM #--|	 This file manages the build environment and compile of the
REM #--|         Interprublic OGNL java code,
REM #--|
REM #--@ DESCRIPTION
REM #--@   Exit Status Explicity Set
REM #--|
REM #--@ NOTES
REM #--@   Design Constraints
REM #--|     (1) The environment variables need to be updated for default pathes
REM #--|         on the build computer.  Need to update the following variables
REM #--|         in this file.
REM #--|         OGNL_HOME     - Set to the directory containing INTERPUBLIC-OGNL
REM #--|         JAVA_HOME     - Set to root directory containing Java JDK
REM #--|         PING_HOME     - set to the root directory of the Ping Federate Server
REM #--|         MSJDBC_HOME   - Set to the root directory of the MS JDBC Driver
REM #--|
REM #--@   Special Processing
REM #--|     None
REM #--|
REM #--@   Target Dependencies
REM #--|     None
REM #--|
REM #--@ LAST REVISION
REM #--|   Date       DR Number    OPR        Modification
REM #--|   =========  ===========  ========   ==================================
REM #--|   03-MAY-11  N/A          Uythoven   Initial Release
REM #--]
REM #------------------------------------------------------------------------------


REM
REM Set environment variables for compile
REM
    echo .
    echo Setting environment for compile
    echo .
    set OGNL_HOME=C:\WORK\DEV\Tools\INTERPUBLIC-OGNL
    set JAVA_HOME=C:\WORK\DEV\Tools\JDK-1.6
    set PING_HOME=C:\WORK\DEV\Tools\PF-6.3
    set MSJDBC_HOME=C:\WORK\DEV\Tools\MSJDBC
    echo .
    echo Environment Dump
    echo .
    set


REM
REM Verify NMAKE Utility is present
REM
    echo .
    echo Searching for NMAKE
    echo .
    set STATUS=BAD
    if exist "%OGNL_HOME%\bin\NMAKE.EXE" set STATUS=OK
    if "%DRIVE%ERROR"=="OKERROR" goto :error_nmake
    set NMAKE_EXE=%OGNL_HOME%\bin\NMAKE.EXE
    echo .
    echo NMAKE found at %NMAKE_EXE%
    echo .


REM
REM Call the MakeSegment targets
REM
    echo .
    echo Starting Compile
    echo .
    echo on
    %NMAKE_EXE% MAKE=%NMAKE_EXE% -f Makefile.OGNL all
    goto :complete


REM
REM Error with Location of NMAKE
REM
:error_nmake
    echo .
    echo ERROR - Unable to locate NMAKE.EXE
    echo .
    goto :complete


REM
REM Exit point in batch file
REM
:complete
    pause

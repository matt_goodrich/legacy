@echo off
REM #--[ PROLOGUE -----------------------------------------------------------------
REM #--@ ABSTRACT
REM #--|
REM #--@   Name
REM #--|     Build script for OGNL Java Library
REM #--|
REM #--@   Identifier
REM #--|     Filename:   BuildOGNL.bat
REM #--|
REM #--@   Contract Identifier
REM #--|     MWG - McCann Worldwide Group
REM #--|
REM #--@   Purpose
REM #--|	 This file manages the build environment and compile of the
REM #--|         Interprublic OGNL java code,
REM #--|
REM #--@ DESCRIPTION
REM #--@   Exit Status Explicity Set
REM #--|
REM #--@ NOTES
REM #--@   Design Constraints
REM #--|     (1) The environment variables need to be updated for default pathes
REM #--|         on the build computer.  Need to update the following variables
REM #--|         in this file.
REM #--|         ANT_HOME      - Set to root directory containing ANT
REM #--|         JAVA_HOME     - Set to root directory containing Java JDK
REM #--|     (2) The environment variables need to be updated for pathing in the
REM #--|         build.properties file before initiating compile.
REM #--|         OGNL_HOME     - Set to the directory containing INTERPUBLIC-OGNL
REM #--|         PING_HOME     - set to the root directory of the Ping Federate Server
REM #--|         MSJDBC_HOME   - Set to the root directory of the MS JDBC Driver
REM #--|
REM #--@   Special Processing
REM #--|     None
REM #--|
REM #--@   Target Dependencies
REM #--|     None
REM #--|
REM #--@ LAST REVISION
REM #--|   Date       DR Number    OPR        Modification
REM #--|   =========  ===========  ========   ==================================
REM #--|   03-MAY-11  N/A          Uythoven   Initial Release
REM #--]
REM #------------------------------------------------------------------------------


REM
REM Set environment variables for compile
REM
    echo .
    echo Setting environment for compile
    echo .
    set ANT_HOME=C:\WORK\DEV\Tools\ANT-1.8.2
    set JAVA_HOME=C:\WORK\DEV\Tools\JDK-1.6


REM
REM Verify ANT utility is present
REM
    echo .
    echo Searching for ANT at %ANT_HOME%
    echo .
    set STATUS=BAD
    if exist "%ANT_HOME%" set STATUS=OK
    if "%STATUS%ERROR"=="BADERROR" goto :error_ant
    set PATH=%PATH%;%ANT_HOME%\bin
    echo .
    echo ANT found at %ANT_HOME%
    echo .


REM
REM Verify JDK is present
REM
    echo .
    echo Searching for JAVA at %JAVA_HOME%
    echo .
    set STATUS=BAD
    if exist "%JAVA_HOME%" set STATUS=OK
    if "%STATUS%ERROR"=="BADERROR" goto :error_java
    set PATH=%PATH%;%JAVA_HOME%
    echo .
    echo JDK found at %JAVA_HOME%
    echo .


REM
REM Dump environment variables for troubleshooting
REM
    echo .
    echo Environment Dump
    echo .
    set


REM
REM Call ant targets to compile
REM
    echo .
    echo Starting Compile
    echo .
    echo on
    call ant -verbose -f build.xml
    goto :complete


REM
REM Error with Location of ANT
REM
:error_ant
    echo .
    echo ERROR - Unable to locate ANT utilities
    echo .
    goto :complete


REM
REM Error with Location of JDK
REM
:error_java
    echo .
    echo ERROR - Unable to locate JDK
    echo .
    goto :complete


REM
REM Exit point in batch file
REM
:complete
    pause

<%-- 
/*******************************************************************************
* Name
*   StartWorkdaySSO
*
* Identifier
*   Filename: index.jsp
*   
* Purpose
*   This class acts as a proxy between the PingFederate SSO server and the 
*   Workday Service Provider. When Workday encounters an error it will populate
*   the "status" query string parameter with a value of "AuthnFailed", which
*   generally indicates that the user is not provisioned on the Workday application
*   or the user is not a member of the correct group within the Insurance Exchange.
*   This proxy will detect the error and redirect the user to an error page.
*
******************************************************************************
*/
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%
    
    try
    {
        //Get the hostname of the current request (This will be the SSO server)
        String host = request.getServerName();
        //Get the Service Provider ID so we can do a proper redirect to the SSO server to start SSO
        String SPID = request.getParameter("PartnerSpId");
        String ValidatedSPID = "";
        if(SPID.equals("http://www.workday.com"))
        {
            ValidatedSPID = SPID;
        }

        //Get the IdP Adapter ID so we can authenticate using the correct mechanism
        String IDPID = request.getParameter("IdpAdapterId");
        String ValidatedIdPAdapterID = "";
        if(IDPID.equals("IdpAdapterIWAENT"))
        {
            ValidatedIdPAdapterID = IDPID;
        }

        //Check for the status query string parameter
        if(request.getParameter("status") != null) {
            //Check for a status equal to "AuthnFailed"
            if(request.getParameter("status").equals("AuthnFailed")) {
                //If found, send to error page
                response.sendRedirect("./Error.html");
            }
        }
        else {
            //No status was found, redirect for SSO using the saved values
            response.sendRedirect("https://" + host + "/idp/startSSO.ping?PartnerSpId=" + ValidatedSPID + "&IdpAdapterId=" + ValidatedIdPAdapterID);
        }
    }
    catch (Exception ex)
    {
        response.sendRedirect("./Error.html");
    }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Start Workday SSO</title>
    </head>
    <body>
        
    </body>
</html>

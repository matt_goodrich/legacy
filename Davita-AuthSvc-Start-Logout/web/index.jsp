<%-- 
    Document   : index
    Created on : Feb 23, 2012, 1:36:01 PM
    Author     : Matt
--%>

<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="javax.xml.parsers.DocumentBuilder,org.w3c.dom.*"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<html>
<head>
    <title>Successful Logout - Ping Identity</title>
    <script language="Javascript">       
        function deletecook(URL) {
            document.cookie = "PF=;path=/";
            try {
            document.execCommand("ClearAuthenticationCache", false);
            } catch (err) {}
            document.location = URL;
    }

  </script>
</head>
<body>
    <%
    Cookie cookie = new Cookie ("PF","");
    cookie.setMaxAge(0);
    response.addCookie(cookie);
    
    //Get the hostname of the current request (This will be the SSO server)
    String host = request.getServerName();
    //Get the port of the current request
    int port = request.getServerPort();
    //Get the Service Provider ID so we can do a proper redirect
    String SPID = request.getParameter("SPID");
        
    //Setup the Document to be read
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder db = dbf.newDocumentBuilder();
    Document doc = db.parse("StartLogout.xml");
    
    NodeList nl= doc.getElementsByTagName("AuthService");
    //Get the URL from the XML doc
    String AuthServiceURL =  nl.item(0).getFirstChild().getNodeValue();
    %>
    <script>
        deletecook('<% out.print(AuthServiceURL); %><% out.print(SPID); %>');
    </script>
</body>
</html>
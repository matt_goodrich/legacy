﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SSO
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute("StartSSOForceAuthn", "StartSSO/{Issuer}/{RelayState}/{ForceAuthn}", new { controller = "Home", action = "StartSSO", Issuer = UrlParameter.Optional, RelayState = UrlParameter.Optional, ForceAuthn = UrlParameter.Optional });
            //routes.MapRoute("StartSSO", "StartSSO/{Issuer}/{RelayState}", new { controller = "Home", action = "StartSSO", Issuer = UrlParameter.Optional, RelayState = UrlParameter.Optional });
            //routes.MapRoute("ACS", "ACS", new { controller = "Home", action = "ACS", id = UrlParameter.Optional});

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterRoutes(RouteTable.Routes);

            string l4net = Server.MapPath("~/Configuration/log4net.config");
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo(l4net));
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            Application[HttpContext.Current.Request.UserHostAddress.ToString()] = ex;
        }

        protected void Application_PreSendRequestHeaders(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Start Single Sign-On - Davita SSO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <%--<h2>Start Single Sign-On</h2>--%>

    <form action="<%=ViewData["action"]%>" method="post" id="Form1">
        <%= Html.Hidden("SAMLRequest", ViewData["request"])%> 
        <%= Html.Hidden("RelayState", ViewData["relaystate"])%> 
    </form>
    <script type="text/javascript">
        document.getElementById('Form1').submit();
    </script>

</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Single Sign On Wrapper - Davita SSO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend>Start Single Sign-On</legend>
        <% using (Html.BeginForm("Index", "StartSSO", FormMethod.Get))
           { %>
            <p><label>Issuer: </label> <%= Html.TextBox("Issuer")%></p>
            <p><label>RelayState: </label><%= Html.TextBox("RelayState")%></p>
            <p><input type="submit" /></p>
        <%} %>
    </fieldset>
    <fieldset>
        <legend>Start Single Application Logout</legend>
        <% using (Html.BeginForm("Index", "StartLogout", FormMethod.Get))
           { %>
            <p><label>Issuer: </label> <%= Html.TextBox("Issuer")%></p>
            <p><input type="submit" /></p>
        <%} %>
    </fieldset>
</asp:Content>

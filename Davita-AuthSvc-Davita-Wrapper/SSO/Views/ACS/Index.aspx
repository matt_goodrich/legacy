﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	SAML Assertion Consumer Service - Davita SSO
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>SAML Assertion Consumer Service</h2>
    <fieldset>
        <legend>Information Recieved From SSO Server</legend>
        <p><strong>Relay State: </strong><br /><%= ViewData["RelayState"] %></p>
        <p><strong>SAML Response:</strong><br /><%= ViewData["SAMLResponse"] %></p>
        <p><strong>SAML Token: </strong><br /><%= ViewData["SAMLDecoded"] %></p>
    </fieldset>
    <fieldset>
        <legend>SAML Validation</legend>
        <fieldset>
            <legend class="<%= (ViewData["Signature"].ToString().Equals(ViewData["ExpectedSignature"]) ? "green" : "red") %>">Signature</legend>
            <p><strong>Asserted Value: </strong><%= ViewData["Signature"] %></p>
            <p><strong>Expected Value: </strong><%= ViewData["ExpectedSignature"]%></p>
        </fieldset>
        <fieldset>
            <legend class="<%= (ViewData["InResponseTo"].ToString().Equals(ViewData["ExpectedInResponseTo"]) ? "green" : "red") %>">InResponseTo</legend>
            <p><strong>Asserted Value: </strong><%= ViewData["InResponseTo"] %></p>
            <p><strong>Expected Value: </strong><%= ViewData["ExpectedInResponseTo"] %></p>
        </fieldset>
        <fieldset>
            <legend class="<%= (ViewData["StatusCode"].ToString().Equals(ViewData["ExpectedStatusCode"]) ? "green" : "red") %>">Status</legend>
            <p><strong>Asserted Value: </strong><%= ViewData["StatusCode"]%></p>
            <p><strong>Expected Value: </strong><%= ViewData["ExpectedStatusCode"]%></p>
        </fieldset>
        <fieldset>
            <legend class="<%= (ViewData["Issuer"].ToString().Equals(ViewData["ExpectedIssuer"]) ? "green" : "red") %>">Issuer</legend>
            <p><strong>Asserted Value: </strong><%= ViewData["Issuer"] %></p>
            <p><strong>Expected Value: </strong><%= ViewData["ExpectedIssuer"]%></p>
        </fieldset>
        <fieldset>
            <legend class="<%= (ViewData["Audience"].ToString().Equals(ViewData["ExpectedAudience"]) ? "green" : "red") %>">Audience</legend>
            <p><strong>Asserted Value: </strong><%= ViewData["Audience"] %></p>
            <p><strong>Expected Value: </strong><%= ViewData["ExpectedAudience"]%></p>
        </fieldset>
        <fieldset>
            <legend class="<%= (Convert.ToDateTime(ViewData["NotOnOrAfter"].ToString()) > Convert.ToDateTime(ViewData["ExpectedNotOnOrAfter"].ToString()) ? "green" : "red") %>">NotOnOrAfter</legend>
            <p><strong>Asserted Value: </strong><%= ViewData["NotOnOrAfter"] %></p>
            <p><strong>Expected Value: </strong><%= ViewData["ExpectedNotOnOrAfter"]%></p>
        </fieldset>
        <fieldset>
            <legend class="<%= (Convert.ToDateTime(ViewData["NotBefore"].ToString()) < Convert.ToDateTime(ViewData["ExpectedNotBefore"].ToString()) ? "green" : "red") %>">NotBefore</legend>
            <p><strong>Asserted Value: </strong><%= ViewData["NotBefore"] %></p>
            <p><strong>Expected Value: </strong><%= ViewData["ExpectedNotBefore"]%></p>
        </fieldset>
    </fieldset>  
</asp:Content>
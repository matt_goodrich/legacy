﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Web;

using log4net;

namespace SSO.Common
{
    public class JackmanCookie
    {
        RailoCFMXCompat cfmx = new RailoCFMXCompat();

        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        //Setup constant properties used to create cookie
        public static string CookieName = "JACKMAN_";
        public static readonly string Domain = "davita.com";
        public static readonly bool HttpOnly = false;
        public static readonly bool Secure = false;
        public static readonly string Path = "/";

        /// <summary>
        /// Gets or sets the style.
        /// </summary>
        /// <value>
        /// The style.
        /// </value>
        public string Style { get; set; }

        /// <summary>
        /// Gets or sets the layout.
        /// </summary>
        /// <value>
        /// The layout.
        /// </value>
        public string Layout { get; set; }

        /// <summary>
        /// Gets or sets the last app ID.
        /// </summary>
        /// <value>
        /// The last app ID.
        /// </value>
        public int LastAppID { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="JackmanCookie"/> class
        /// using the default values.
        /// </summary>
        /// <param name="UserNumber">The user number.</param>
        /// <param name="LastApplicationID">The last application ID.</param>
        public JackmanCookie(int UserNumber, int LastApplicationID = 0)
        {
            CookieName = (CookieName.Length > 8 ? CookieName : CookieName + UserNumber);
            Style = "ver3";
            Layout = "ver3";
            LastAppID = LastApplicationID;

            ServerLog.DebugFormat("Creating {0} Cookie with for user {1}", CookieName, UserNumber);
        }

        /// <summary>
        /// Retrieves the pipe delimited encrypted cookie value.
        /// </summary>
        /// <returns>Encrypted Cookie Value</returns>
        public string GetCookieValue()
        {
            string Value = "";
            Value += Style + "|";
            Value += Layout + "|";
            Value += LastAppID;

            ServerLog.DebugFormat("Jackman Cookie Values before encryption: {0}", Value);

            return HttpUtility.UrlEncode(Convert.ToBase64String(cfmx.transformString(AppConfig.DavitaCookieEncryptionKey, Encoding.UTF8.GetBytes(Value))));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SSO.Common
{
    /// <summary>
    /// This class is used as an interface to retrieve values
    /// from the applications' web.config AppSettings section.
    /// The values returned from this class can and will change
    /// between environments.
    /// </summary>
    public class AppConfig
    {
        /// <summary>
        /// Retrieves the SSO Server URL used to send the 
        /// SAML Authentication Request.
        /// </summary>
        /// <example>
        /// https://localhost
        /// </example>
        /// <value>
        /// The SSO Server URL.
        /// </value>
        public static string SSOServerURL
        {
            get { return ConfigurationManager.AppSettings["SSOServer-URL"]; }
        }

        /// <summary>
        /// Retrieves the SSO Server Port used for communication.
        /// </summary>
        /// <example>
        /// 9031
        /// </example>
        /// <value>
        /// The port number of the SSO Server
        /// </value>
        public static string SSOServerPort
        {
            get { return ConfigurationManager.AppSettings["SSOServer-Port"]; }
        }

        /// <summary>
        /// Retrieves the SSO Server Endpoint used to
        /// consume the SAML Authentication Request
        /// </summary>
        /// <example>
        /// /idp/SSO.saml2
        /// </example>
        /// <value>
        /// The path of the SSO Server SAML Consumer
        /// </value>
        public static string SSOServerEndpoint
        {
            get { return ConfigurationManager.AppSettings["SSOServer-Endpoint"]; }
        }

        /// <summary>
        /// Retrieves the "Issuer" that the SSO server will
        /// refer to itself as when sending a SAML
        /// Assertion.
        /// </summary>
        /// <example>
        /// https://ssoserver.domain.com OR DEV:SSO
        /// </example>
        /// <value>
        /// The path of the SSO Server SAML Consumer
        /// </value>
        public static string SSOServerIssuerName
        {
            get { return ConfigurationManager.AppSettings["SSOServer-IssuerName"]; }
        }

        /// <summary>
        /// Retrieves the SSO server logout endpoint used to end
        /// the current session at the SSO server.
        /// </summary>
        /// <example>
        /// /StartLogout/?SPID=
        /// </example>
        /// <value>
        /// The path of the SSO Server Logout Mechanism
        /// </value>
        /// <remarks>
        /// For the Davita SSO Implementation, the LogoutEndpoint
        /// is the "Cookie Killer" application that allows the SSO
        /// session to end without forcing logout of other applications.
        /// </remarks>
        public static string SSOServerLogoutEndpoint
        {
            get { return ConfigurationManager.AppSettings["SSOServer-LogoutEndpoint"]; }
        }

        /// <summary>
        /// Retrieves the davita cookie encryption key used to
        /// Base64 XOR encode the values of the "WINGO" and "JACKMAN_XXX"
        /// cookies.
        /// </summary>
        /// <value>
        /// A string of characters representing the public or private key
        /// </value>
        public static string DavitaCookieEncryptionKey
        {
            get { return ConfigurationManager.AppSettings["DavitaCookieEncryptionKey"]; }
        }

        /// <summary>
        /// Retrieves the identifier of the SOAP endpoint to call for user privs
        /// </summary>
        /// <value>
        /// A string containing "Dev", "Stage" or "Prod"
        /// </value>
        public static string SOAPEndpoint
        {
            get { return ConfigurationManager.AppSettings["SOAPEndpoint"]; }
        }
    }
}
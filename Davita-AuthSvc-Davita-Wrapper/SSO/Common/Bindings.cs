﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSO.Common.SAML20
{
    /// <summary>
    /// This document specifies SAML protocol bindings for the use of 
    /// SAML assertions and request-response messages in communications 
    /// protocols and frameworks.
    /// </summary>
    public class Bindings
    {
        /// <summary>
        /// SOAP is a lightweight protocol intended for exchanging structured 
        /// information in a decentralized, distributed environment [SOAP11]. 
        /// It uses XML technologies to define an extensible messaging framework 
        /// providing a message construct that can be exchanged over a variety of 
        /// underlying protocols. The framework has been designed to be independent 
        /// of any particular programming model and other implementation specific 
        /// semantics. Two major design goals for SOAP are simplicity and extensibility. 
        /// SOAP attempts to meet these goals by omitting, from the messaging framework, 
        /// features that are often found in distributed systems. Such features include 
        /// but are not limited to "reliability", "security", "correlation", "routing", 
        /// and "Message Exchange Patterns" (MEPs).
        /// 
        /// A SOAP message is fundamentally a one-way transmission between SOAP nodes 
        /// from a SOAP sender to a SOAP receiver, possibly routed through one or more 
        /// SOAP intermediaries. SOAP messages are expected to be combined by applications 
        /// to implement more complex interaction patterns ranging from request/response 
        /// to multiple, back-and-forth "conversational" exchanges [SOAP-PRIMER].
        /// 
        /// SOAP defines an XML message envelope that includes header and body sections, 
        /// allowing data and control information to be transmitted. SOAP also defines 
        /// processing rules associated with this envelope and an HTTP binding for SOAP
        /// message transmission.
        /// 
        ///The SAML SOAP binding defines how to use SOAP to send and receive SAML requests 
        ///and responses.
        ///
        ///Like SAML, SOAP can be used over multiple underlying transports. This binding has 
        ///protocol-independent aspects, but also calls out the use of SOAP over HTTP as 
        ///REQUIRED (mandatory to implement).
        /// </summary>
        public static string SOAP
        {
            get { return "urn:oasis:names:tc:SAML:2.0:bindings:SOAP"; }
        }

        /// <summary>
        /// This binding leverages the Reverse HTTP Binding for SOAP specification [PAOS]. 
        /// Implementers MUST comply with the general processing rules specified in [PAOS]
        /// in addition to those specified in this document. In case of conflict, 
        /// [PAOS] is normative.
        /// </summary>
        public static string PAOS
        {
            get { return "urn:oasis:names:tc:SAML:2.0:bindings:PAOS"; }
        }

        /// <summary>
        /// The HTTP Redirect binding is intended for cases in which the SAML requester 
        /// and responder need to communicate using an HTTP user agent 
        /// (as defined in HTTP 1.1 [RFC2616]) as an intermediary. This may be necessary, 
        /// for example, if the communicating parties do not share a direct path of 
        /// communication. It may also be needed if the responder requires an interaction 
        /// with the user agent in order to fulfill the request, such as when the user 
        /// agent must authenticate to it.
        /// 
        /// Note that some HTTP user agents may have the capacity to play a more active 
        /// role in the protocol exchange and may support other bindings that use HTTP, 
        /// such as the SOAP and Reverse SOAP bindings. This binding assumes nothing 
        /// apart from the capabilities of a common web browser.
        /// </summary>
        public static string HttpRedirect
        {
            get { return "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"; }
        }

        /// <summary>
        /// The HTTP POST binding defines a mechanism by which SAML protocol messages 
        /// may be transmitted within the base64-encoded content of an HTML form control. 
        /// This binding MAY be composed with the HTTP Redirect binding and the HTTP 
        /// Artifact binding to transmit request and response messages in a single 
        /// protocol exchange using two different bindings.
        /// </summary>
        public static string HttpPost
        {
            get { return "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"; }
        }

        /// <summary>
        /// In the HTTP Artifact binding, the SAML request, the SAML response, or both 
        /// are transmitted by reference using a small stand-in called an artifact. A 
        /// separate, synchronous binding, such as the SAML SOAP binding, is used to 
        /// exchange the artifact for the actual protocol message using the artifact 
        /// resolution protocol defined in the SAML assertions and protocols specification 
        /// [SAMLCore]. This binding MAY be composed with the HTTP Redirect binding and the 
        /// HTTP POST binding to transmit request and response messages in a single protocol 
        /// exchange using two different bindings.
        /// </summary>
        public static string HttpArtifact
        {
            get { return "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact"; }
        }

        /// <summary>
        /// URIs are a protocol-independent means of referring to a resource. This binding 
        /// is not a general SAML request/response binding, but rather supports the encapsulation 
        /// of a <samlp:AssertionIDRequest> message with a single <saml:AssertionIDRef> into the 
        /// resolution of a URI. The result of a successful request is a SAML <saml:Assertion> 
        /// element (but not a complete SAML response). Like SOAP, URI resolution can occur over 
        /// multiple underlying transports. This binding has transportindependent aspects, but 
        /// also calls out the use of HTTP with SSL 3.0 [SSL3] or TLS 1.0 [RFC2246] as REQUIRED 
        /// (mandatory to implement).
        /// </summary>
        public static string URI
        {
            get { return "urn:oasis:names:tc:SAML:2.0:bindings:URI"; }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;
using System.Xml.Serialization;
using System.Web;

using log4net;

namespace SSO.Common.SAML20
{
    /// <summary>
    /// This class is used to DeSerialize the SAML Response
    /// into an object. The object is then parsed for the needed
    /// information for Validation of the token as well as attribute 
    /// values.
    /// </summary>
    public class SAMLResponse
    {
        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        /// <summary>
        /// DeSerialize the SAML response into an object.
        /// </summary>
        /// <param name="Response">The decoded SAML Response</param>
        /// <returns>A Dictionary containing the Keys and Values needed</returns>
        public static Dictionary<string, object> DeSerializeSAMLResponse(string Response)
        {
            //Setup the Dictionary we will return
            Dictionary<string, object> SAMLValues = new Dictionary<string, object>();
            //Set the response type to null
            ResponseType response = null;

            //Setup our XML Serializer for the current object type
            XmlSerializer serializer = new XmlSerializer(typeof(ResponseType));

            //Read the XML from the string and DeSerialize it into an object
            response = (ResponseType)serializer.Deserialize(XmlReader.Create(new StringReader(Response)));

            //Grab the InResponseTo and add it to the dictionary
            SAMLValues.Add("InResponseTo", response.InResponseTo);
            //Grab the Issuer and add it to the dictionary
            SAMLValues.Add("Issuer", response.Issuer.Value);
            //Grab the StatusCode and add it to the dictionary
            SAMLValues.Add("StatusCode", response.Status.StatusCode.Value);

            ServerLog.DebugFormat("Response Deserialized: InResponseTo = {0}, Issuer = {1}, StatusCode = {2}", response.InResponseTo, response.Issuer, response.Status.StatusCode.Value);

            //If the Status Code indicates a success from the SSO server
            if (response.Status.StatusCode.Value.Equals(StatusCodes.Success))
            {
                ServerLog.Debug("Items:");

                //Iterate over the response items array
                foreach (object o in response.Items)
                {
                    //Get the type of the current item
                    Type itemType = o.GetType();
                    ServerLog.DebugFormat("Item Type: {0}", itemType);

                    //If the current item type is AssertionType
                    if (itemType == typeof(AssertionType))
                    {
                        //Cast into an object
                        AssertionType AT = (AssertionType)o;

                        //Setup the Audience string
                        string Audience = "";
                        //Iterate over the Conditions
                        foreach (object p in AT.Conditions.Items)
                        {
                            //Get the Current Condition Type
                            Type conditionType = p.GetType();
                            //If the current condition type is AudienceRestrictionType
                            if (conditionType == typeof(AudienceRestrictionType))
                            {
                                //Cast into an object
                                AudienceRestrictionType ART = (AudienceRestrictionType)p;
                                //Iterate over the Audience Items
                                foreach (string str in ART.Audience)
                                {
                                    //Build a comma delimited string
                                    Audience += str + ",";
                                }
                                //Remove trailing comma
                                if (Audience.Length > 1)
                                    Audience = Audience.Substring(0, Audience.Length - 1);
                            }
                        }
                        //Iterate over the Subject Items object array
                        foreach (object q in AT.Subject.Items)
                        {
                            //Get the type of the current Subject Item
                            Type SubjectType = q.GetType();
                            ServerLog.DebugFormat("Subject Items: {0}", SubjectType);

                            //If the current Subject Item Type is NameIDType
                            if (SubjectType == typeof(NameIDType))
                            {
                                //Cast into an oject
                                NameIDType NIDT = (NameIDType)q;
                                //Add the subject value to the dictionary to return
                                SAMLValues.Add("SAMLSubject", NIDT.Value);
                                ServerLog.DebugFormat("SAMLSubject = {0}", NIDT.Value);
                            }
                        }
                        //Iterate over the Assertion Items
                        foreach (object r in AT.Items)
                        {
                            //Get the type of the current Assertion Item
                            Type AssertionItemType = r.GetType();
                            ServerLog.DebugFormat("Assertion Item Type: {0}", AssertionItemType);

                            //If the current Assertion Item has a type of AttributeStatementType
                            if (AssertionItemType == typeof(AttributeStatementType))
                            {
                                //Cast into an object
                                AttributeStatementType AST = (AttributeStatementType)r;
                                //Iterate over the Attribte Statement Items
                                foreach (object s in AST.Items)
                                {
                                    //Get the type of the current Attribute Statement Item
                                    Type ASIT = s.GetType();
                                    ServerLog.DebugFormat("Attribte Statement Item Type: {0}", ASIT);

                                    //If the current attribte statement item has a type of AttributeType
                                    if (ASIT == typeof(AttributeType))
                                    {
                                        //Cast into an object
                                        AttributeType AttrType = (AttributeType)s;
                                        //Add the value to the dictionary to be returned
                                        SAMLValues.Add(AttrType.Name, AttrType.AttributeValue[0]);
                                        //Add Prefix to be able to parse out attributes later
                                        SAMLValues.Add("ATTR::" + AttrType.Name, AttrType.AttributeValue[0]);
                                        ServerLog.DebugFormat("Attribute Name: {0}, Attribute Value: {1}", AttrType.Name, AttrType.AttributeValue[0]);
                                    }
                                }
                            }
                        }
                        //Add the NotOnOrAfter Condition to the dictionary to be returned
                        SAMLValues.Add("NotOnOrAfter", AT.Conditions.NotOnOrAfter);
                        //Add the NotBefore Condition to the dictionary to be returned
                        SAMLValues.Add("NotBefore", AT.Conditions.NotBefore);
                        //Add the Audience string generated above to the dictionary to be returned
                        SAMLValues.Add("Audience", Audience);

                        //Create an XML document for the purposes of signature validation with the appropriate settings
                        XmlDocument xml = new XmlDocument();
                        xml.PreserveWhitespace = true;
                        //Load the response into the XML document
                        xml.Load(new StringReader(Response));
                        //Validate the signature
                        bool SignatureValid = IsValidSignature(xml);

                        //Add a boolean to the dictionary to be returned indicating whether the signature was valid
                        SAMLValues.Add("Signature", SignatureValid);

                        ServerLog.DebugFormat("Assertion: Issuer = {0}, NotOnOrAfter = {1}, NotBefore = {2}, Audience = {3}, Signature (Valid) = {4}", AT.Issuer.Value, AT.Conditions.NotOnOrAfter, AT.Conditions.NotBefore, Audience, SignatureValid);
                    }
                    else
                    {
                        //Set Some Default Values in the case we do not get an Assertion within the Response
                        
                        //Add values to the dictionary to be returned that will force the Validation to fail
                        SAMLValues.Add("NotOnOrAfter", DateTime.MinValue);
                        SAMLValues.Add("NotBefore", DateTime.MaxValue);
                        SAMLValues.Add("Audience", response.Status.StatusCode.Value);
                        SAMLValues.Add("Signature", false);
                    }
                }
            }
            else //Non-successful case
            {
                //Add values to the dictionary to be returned that will force the Validation to fail
                SAMLValues.Add("NotOnOrAfter", DateTime.MinValue);
                SAMLValues.Add("NotBefore", DateTime.MaxValue);
                SAMLValues.Add("Audience", response.Status.StatusCode.Value);

                //Even though the attempt was unsuccessful, validate the signature, so the result can be passed back to the caller
                XmlDocument xml = new XmlDocument();
                xml.PreserveWhitespace = true;
                xml.Load(new StringReader(Response));
                bool SignatureValid = IsValidSignature(xml);
                SAMLValues.Add("Signature", SignatureValid);
            }

            //Return the dictionary
            return SAMLValues;
        }

        /// <summary>
        /// Determines whether [is valid signature] for [the specified doc].
        /// </summary>
        /// <param name="Doc">The xml document to validate the signature for</param>
        /// <returns>
        ///   <c>true</c> if [is valid signature] for [the specified doc]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidSignature(XmlDocument Doc)
        {
            ServerLog.Debug("Validating Signature");
            
            try
            {
                //Create a local variable to hold the XML document passed in with approprate settings
                XmlDocument xmlDoc = Doc;
                xmlDoc.PreserveWhitespace = true;

                //Add the appropriate namespaces
                XmlNamespaceManager _documentNamespaceManager;
                _documentNamespaceManager = new XmlNamespaceManager(xmlDoc.NameTable);
                _documentNamespaceManager.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
                _documentNamespaceManager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");
                _documentNamespaceManager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");

                //Create a SignedXml object
                SignedXml signedXml = new SignedXml(xmlDoc);
                //Locate the Signature block node list
                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("ds:Signature");
                //Grab the desired signature node
                XmlNode xmlNode = xmlDoc.DocumentElement.SelectSingleNode("/samlp:Response/ds:Signature", _documentNamespaceManager);

                //Load the appropriate section into our SignedXml object
                signedXml.LoadXml((XmlElement)nodeList[0]);

                //Create an X509Certificate2 object and set it to null
                X509Certificate2 certificate = null;
                //Iterate the KeyInfo
                foreach (KeyInfoClause clause in signedXml.KeyInfo)
                {
                    //Determine if X509 Data was found
                    if (clause is KeyInfoX509Data)
                    {
                        //If a certificate was found
                        if (((KeyInfoX509Data)clause).Certificates.Count > 0)
                        {
                            //Set the certificate
                            certificate = (X509Certificate2)((KeyInfoX509Data)clause).Certificates[0];
                        }
                    }
                }

                //If no certificate was found, log it
                if (certificate == null)
                    ServerLog.Debug("No Certificate Found");

                ServerLog.Debug("Testing with Certificate in the XML");
                
                //Use the .NET XML Signature Validator to determine if the assertion was signed correctly
                if (signedXml.CheckSignature(certificate, true))
                {
                    ServerLog.Debug("Signature Validated");
                    return true;
                }
                else
                {
                    ServerLog.Debug("Signature Validation Failed");
                    return false;
                }
            }
            catch (Exception ex)
            {
                //Something went wrong, signature not valid
                ServerLog.ErrorFormat("Error caught while Validating Signature: {0}", ex);
                return false;
            }

        }
    }
}
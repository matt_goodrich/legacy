﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using log4net;

namespace SSO.Common
{
    public class DavitaCookies
    {
        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        //Setup an enumeration of CookieTypes so that we can create the correct cookie
        public enum CookieTypes { DavitaSSO, Wingo, Jackman };

        //Store the Cookies within the DavitaCookies object
        public List<HttpCookie> Cookies = new List<HttpCookie>();

        /// <summary>
        /// Retrieves an HttpCookie of the specified CookieType
        /// </summary>
        /// <param name="CookieType">Type of the cookie.</param>
        /// <param name="CookieValues">The cookie values to be added to the cookie.</param>
        /// <returns></returns>
        public HttpCookie GetCookie(CookieTypes CookieType, Dictionary<string, object> CookieValues)
        {
            ServerLog.DebugFormat("Creating Cookie: {0}", CookieType.ToString());
            switch (CookieType)
            {
                case CookieTypes.DavitaSSO:
                    DavitaSSOCookie DavitaSSO = new DavitaSSOCookie();
                    try
                    {
                        DavitaSSO.Audience = CookieValues["Audience"].ToString();
                    }
                    catch (Exception ex)
                    {
                        //Log the exception
                        ServerLog.ErrorFormat("Exception caught while creating DavitaSSO cookie: {0}", ex);
                    }
                    try
                    {
                        foreach (KeyValuePair<string, object> kvp in CookieValues)
                        {
                            if (kvp.Key.Contains("ATTR::"))
                                DavitaSSO.AddAttribute(kvp.Key.Replace("ATTR::", ""), kvp.Value.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        //Log the exception
                        ServerLog.ErrorFormat("Exception caught while creating DavitaSSO cookie: {0}", ex);
                    }

                    //Create the cookie to add to the browser
                    HttpCookie _DCookie = new HttpCookie(DavitaSSOCookie.CookieName);
                    _DCookie.HttpOnly = DavitaSSOCookie.HttpOnly;
                    _DCookie.Secure = DavitaSSOCookie.Secure;
                    _DCookie.Path = DavitaSSOCookie.Path;
                    _DCookie.Domain = ".davita.com";
                    _DCookie.Value = HttpUtility.UrlEncode(String.Format("Audience={0}&Attributes={1}", DavitaSSO.Audience, DavitaSSO.Attributes));
                    //_DCookie.Values.Add("Audience", DavitaSSO.Audience);
                    //_DCookie.Values.Add("Attributes", DavitaSSO.Attributes);

                    //Add the Cookie to our collection
                    Cookies.Add(_DCookie);

                    ServerLog.DebugFormat("Creating {0} Cookie with Values: HttpOnly = {1}, Secure = {2}, Path = {3}, Audience = {4}, Attributes = {5}", DavitaSSOCookie.CookieName, DavitaSSOCookie.HttpOnly, DavitaSSOCookie.Secure, DavitaSSOCookie.Path, DavitaSSO.Audience, DavitaSSO.Attributes);

                    //return the Cookie to be added to the browser
                    return _DCookie;
                    break;
                case CookieTypes.Wingo:
                    //Case WINGO Cookie
                    //Create a new WingoCookie object
                    WingoCookie WCookie = new WingoCookie();
                    
                    //Attempt to add the cookie values passed in
                    try
                    {
                        WCookie.UserLoggedIn = Convert.ToBoolean(CookieValues["UserLoggedIn"]);
                        WCookie.EmailAddress = CookieValues["EmailAddress"].ToString();
                        WCookie.Username = CookieValues["Username"].ToString();
                        WCookie.FirstLastName = CookieValues["FirstLastName"].ToString();
                        WCookie.Org = CookieValues["Org"].ToString();
                        
                        int _UserNumber;
                        bool converted = Int32.TryParse(CookieValues["UserNumber"].ToString(), out _UserNumber);
                        if (converted)
                            WCookie.UserNumber = _UserNumber;
                        else
                            WCookie.UserNumber = 0;

                        WCookie.WorkforceID = CookieValues["WorkforceID"].ToString();
                        WCookie.PriviledgeIDList = CookieValues["PriviledgeIDList"].ToString();
                        WCookie.ADContext = CookieValues["ADContext"].ToString();

                        int TitleID;
                        bool TitleConverted = Int32.TryParse(CookieValues["TitleID"].ToString(), out TitleID);
                        if (TitleConverted)
                            WCookie.TitleID = TitleID;
                        else
                            WCookie.TitleID = 0;
                    }
                    catch (Exception ex) //A value likely did not exist, NullReferenceException
                    {
                        //Log the exception
                        ServerLog.ErrorFormat("Exception caught while creating Wingo cookie: {0}", ex);
                        //Create cookie with the default values
                        WCookie = new WingoCookie();
                    }
                    
                    //Create the Cookie to add to the browser
                    HttpCookie _WCookie = new HttpCookie(WingoCookie.CookieName);
                    _WCookie.HttpOnly = WingoCookie.HttpOnly;
                    _WCookie.Secure = WingoCookie.Secure;
                    _WCookie.Path = WingoCookie.Path;
                    _WCookie.Domain = ".davita.com";
                    _WCookie.Value = WCookie.GetCookieValue();
                    
                    //Add the Cookie to our collection
                    Cookies.Add(_WCookie);

                    ServerLog.DebugFormat("Creating {0} Cookie with Values: HttpOnly = {1}, Secure = {2}, Path = {3}, Value = {4}", WingoCookie.CookieName, WingoCookie.HttpOnly, WingoCookie.Secure, WingoCookie.Path, WCookie.GetCookieValue());

                    //return the Cookie to be added to the browser
                    return _WCookie;
                    break;
                case CookieTypes.Jackman:
                    //Case JACKMAN Cookie
                    //Attempt to get the User Number from the values passed in
                    int UserNumber = 0;
                    try
                    {
                        UserNumber = Convert.ToInt32(CookieValues["UserNumber"]);
                    }
                    catch (Exception ex)
                    {
                        //Log the exception
                        ServerLog.ErrorFormat("Exception caught while locating UserNumber: {0}", ex);
                    }

                    int LastAppID = 0;
                    try 
                    {
                        LastAppID = Convert.ToInt32(CookieValues["LastAppID"]);
                    }
                    catch (Exception ex)
                    {
                        //Log the exception
                        ServerLog.ErrorFormat("Exception caught while locating LastAppID: {0}", ex);
                    }

                    //Create a new JackmanCookie object
                    JackmanCookie JCookie = new JackmanCookie(UserNumber, LastAppID);
                    HttpCookie _JCookie = new HttpCookie(JackmanCookie.CookieName);
                    _JCookie.HttpOnly = JackmanCookie.HttpOnly;
                    _JCookie.Secure = JackmanCookie.Secure;
                    _JCookie.Path = JackmanCookie.Path;
                    _JCookie.Domain = ".davita.com";
                    _JCookie.Value = JCookie.GetCookieValue();

                    //Add the Cookie to our collection
                    Cookies.Add(_JCookie);

                    ServerLog.DebugFormat("Creating {0} Cookie with Values: HttpOnly = {1}, Secure = {2}, Path = {3}, Value = {4}", JackmanCookie.CookieName, JackmanCookie.HttpOnly, JackmanCookie.Secure, JackmanCookie.Path, JCookie.GetCookieValue());

                    //return the Cookie to be added to the browser
                    return _JCookie;
                    break;
            }
            return new HttpCookie("NULL");
        }
    }
}
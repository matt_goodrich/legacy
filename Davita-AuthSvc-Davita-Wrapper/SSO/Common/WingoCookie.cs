﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

using log4net;

namespace SSO.Common
{
    public class WingoCookie
    {
        RailoCFMXCompat cfmx = new RailoCFMXCompat();

        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        //Setup constant properties used to create cookie
        public static readonly string CookieName = "WINGO";
        public static readonly bool HttpOnly = false;
        public static readonly bool Secure = false;
        public static readonly string Path = "/";

        /// <summary>
        /// Gets or sets a value indicating whether [user logged in].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [user logged in]; otherwise, <c>false</c>.
        /// </value>
        public bool UserLoggedIn { get; set; }
        
        /// <summary>
        /// Gets or sets the email address.
        /// </summary>
        /// <value>
        /// The email address.
        /// </value>
        public string EmailAddress { get; set; }
        
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the first and last name.
        /// </summary>
        /// <value>
        /// The first and last name.
        /// </value>
        public string FirstLastName { get; set; }

        /// <summary>
        /// Gets or sets the org.
        /// </summary>
        /// <value>
        /// The org.
        /// </value>
        public string Org { get; set; }

        /// <summary>
        /// Gets or sets the user number.
        /// </summary>
        /// <value>
        /// The user number.
        /// </value>
        public int UserNumber { get; set; }

        /// <summary>
        /// Gets or sets the workforce ID.
        /// </summary>
        /// <value>
        /// The workforce ID.
        /// </value>
        public string WorkforceID { get; set; }

        /// <summary>
        /// Gets or sets the priviledge ID list.
        /// </summary>
        /// <value>
        /// The priviledge ID list.
        /// </value>
        public string PriviledgeIDList { get; set; }

        /// <summary>
        /// Gets or sets the AD context.
        /// </summary>
        /// <value>
        /// The AD context.
        /// </value>
        public string ADContext { get; set; }

        /// <summary>
        /// Gets or sets the title ID.
        /// </summary>
        /// <value>
        /// The title ID.
        /// </value>
        public int TitleID { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WingoCookie"/> class
        /// using the default values.
        /// </summary>
        public WingoCookie()
        {
            UserLoggedIn = false;
            EmailAddress = "noemail@davita.com";
            Username = "nobody";
            FirstLastName = "unknown";
            Org = "unk";
            UserNumber = 0;
            WorkforceID = "0";
            PriviledgeIDList = "UNK";
            ADContext = "OU=UNK";
            TitleID = 0;

            ServerLog.Debug("Creating Wingo Cookie with Default Values");
        }

        /// <summary>
        /// Retrieves the pipe delimited encrypted cookie value.
        /// </summary>
        /// <returns>Encrypted Cookie Value</returns>
        public string GetCookieValue()
        {
            string Value = "";
            Value += UserLoggedIn.ToString() + "|";
            Value += EmailAddress + "|";
            Value += Username + "|";
            Value += FirstLastName + "|";
            Value += Org + "|";
            Value += UserNumber + "|";
            Value += WorkforceID + "|";
            Value += PriviledgeIDList + "|";
            Value += ADContext + "|";
            Value += TitleID;

            ServerLog.DebugFormat("Wingo Cookie Values before encryption: {0}", Value);

            return HttpUtility.UrlEncode(Convert.ToBase64String(cfmx.transformString(AppConfig.DavitaCookieEncryptionKey, Encoding.UTF8.GetBytes(Value))));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

using log4net;

namespace SSO.Common
{
    public class DavitaSSOCookie
    {
        RailoCFMXCompat cfmx = new RailoCFMXCompat();

        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        //Setup constant properties used to create cookie
        public static readonly string CookieName = "DavitaSSO";
        public static readonly string Domain = "davita.com";
        public static readonly bool HttpOnly = false;
        public static readonly bool Secure = false;
        public static readonly string Path = "/";

        //Setup an object to hold attribute values
        private Dictionary<string, string> AttributeSet = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        /// <value>
        /// The audience.
        /// </value>
        public string Audience { get; set; }

        /// <summary>
        /// Gets the attributes in a delimited format (i.e. [[Key::Value][Key::Value]] ).
        /// </summary>
        public string Attributes 
        {
            get
            {
                string AttrStr = "[";
                foreach (KeyValuePair<string, string> kvp in AttributeSet)
                {
                    AttrStr += "[";
                    AttrStr += kvp.Key + "::" + kvp.Value;
                    AttrStr += "]";
                }
                AttrStr += "]";

                ServerLog.DebugFormat("DavitaSSO Cookie Attributes before encryption: {0}", AttrStr);

                return HttpUtility.UrlEncode(Convert.ToBase64String(cfmx.transformString(AppConfig.DavitaCookieEncryptionKey, Encoding.UTF8.GetBytes(AttrStr))));
            }
        }

        /// <summary>
        /// Adds an attribute to the Attribute Set
        /// </summary>
        /// <param name="AttributeName">Name of the attribute.</param>
        /// <param name="AttributeValue">The attribute value.</param>
        public void AddAttribute(string AttributeName, string AttributeValue)
        {
            AttributeSet.Add(AttributeName, AttributeValue);
            ServerLog.DebugFormat("Adding attribute {0} with value {1} to DavitaSSO Cookie", AttributeName, AttributeValue);
        }
    }
}
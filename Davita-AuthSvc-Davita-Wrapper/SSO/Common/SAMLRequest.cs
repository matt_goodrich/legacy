﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

using log4net;

namespace SSO.Common.SAML20
{
    /// <summary>
    /// This class is used to create a SAML 2.0 Authentication
    /// Request to send to the SSO Server for the Purposes of 
    /// requesting a SAML Assertion for the current user session.
    /// </summary>
    public class SAMLRequest
    {
        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        /// <summary>
        /// Returns the SAML 2.0 Authentication Request
        /// </summary>
        /// <param name="Issuer">The Issuer - Identifies the App on the SSO Server</param>
        /// <param name="ProviderName">Name of the provider. - The URL the request will be sent from</param>
        /// <param name="AssertionConsumerServiceURL">The assertion consumer service URL that will recieve the SAML Assertion.</param>
        /// <param name="ID">The ID of the request</param>
        /// <param name="ForceAuthn">If set to <c>true</c> [force authn].</param>
        /// <param name="isPassive">If set to <c>true</c> [is passive].</param>
        /// <returns></returns>
        public static string GetSamlRequest(string Issuer, string ProviderName, string AssertionConsumerServiceURL, string ID, bool ForceAuthn = false, bool isPassive = false)
        {
            ServerLog.DebugFormat("Creating SAML Request: Issuer = {0}, ProviderName = {1}, ACSURL = {2}, ForceAuthn = {3}, IsPassive = {4}", Issuer, ProviderName, AssertionConsumerServiceURL, ForceAuthn, isPassive);
            
            //Setup the Issuer, this will be the Application Identifier passed in
            NameIDType _Issuer = new NameIDType();
            _Issuer.Value = Issuer;

            //Set the default Name ID Policy
            NameIDPolicyType NameIDPolicy = new NameIDPolicyType();
            NameIDPolicy.AllowCreate = true;
            NameIDPolicy.AllowCreateSpecified = true;
            
            //Create the Authentication Request
            AuthnRequestType request = new AuthnRequestType();
            //Set the ACS URL
            request.AssertionConsumerServiceURL = AssertionConsumerServiceURL;
            //Set the Force Authn Attribute Value
            request.ForceAuthnSpecified = true;
            request.ForceAuthn = ForceAuthn;
            //Add the ID of the request that was passed in
            request.ID = ID;
            //Set the IsPassive Attribute Value
            request.IsPassiveSpecified = true;
            request.IsPassive = isPassive;
            //Set the Issue Instant - the Date and Time the request was generated
            request.IssueInstant = DateTime.UtcNow;
            //Set the Protocol
            request.ProtocolBinding = Bindings.HttpPost;
            //Set the ProviderName attribute value
            request.ProviderName = ProviderName;
            //Specify SAML 2.0
            request.Version = "2.0";
            //Add the Issuer to the request
            request.Issuer = _Issuer;
            //Add the Name ID Policy to the request
            request.NameIDPolicy = NameIDPolicy;

            //Add the appropriate SAML 2.0 namespaces that will be added to the Authentication Request
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("saml2", "urn:oasis:names:tc:SAML:2.0:assertion");
            ns.Add("saml2p", "urn:oasis:names:tc:SAML:2.0:protocol");

            //Setup our XML Serializer to transform the object into XML
            XmlSerializer requestSerializer = new XmlSerializer(request.GetType());

            //Setup our streams to write the xml and the appropriate settings
            StringWriter stringWriter = new StringWriter();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;

            //Create the XML writer to create the xml using the stream and settings above
            XmlWriter requestWriter = XmlTextWriter.Create(stringWriter, settings);

            //String to hold the Authn Request XML
            string samlString = string.Empty;

            //Serialize into XML
            requestSerializer.Serialize(requestWriter, request, ns);
            requestWriter.Close();

            //Write the result into the string
            samlString = stringWriter.ToString();

            //Create the XML Document and load the SAML Authn Request into it
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(samlString);

            ServerLog.DebugFormat("SAML Authentication Request before encoding = {0}", doc.OuterXml.ToString());

            //Get the appropriate XML
            string requestStr = doc.OuterXml;

            //Base 64 encode the request
            byte[] base64EncodedBytes = Encoding.UTF8.GetBytes((requestStr));

            //Convert to a base 64 string
            string returnValue = System.Convert.ToBase64String(base64EncodedBytes);

            //Return the Authentication Request
            return returnValue;
        }
    }
}
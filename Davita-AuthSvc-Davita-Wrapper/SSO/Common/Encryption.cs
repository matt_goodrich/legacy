﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace SSO.Common
{
    /// <summary>
    /// This class is used to implement the Base 64
    /// XOR encryption used by the Legacy Davita Cookies
    /// to encode the Value.
    /// </summary>
    public class Encryption
    {
        //Get default values for the encryption key and the initial vector
        protected static readonly string EncryptionKey = AppConfig.DavitaCookieEncryptionKey;
        protected const string _IV = "0000000000000000";


        /// <summary>
        /// Encrypts the text using the Encryption Key found in the web.config file
        /// and the optional Initial Vector
        /// </summary>
        /// <param name="Data">The data to encrypt</param>
        /// <param name="IV">The Initial Vector used to encrypted the data</param>
        /// <returns>The encrypted string</returns>
        public static String EncryptText(String Data, String IV = _IV)
        {
            //Call method below using Encryption Key found in web.config
            return EncryptText(Data, EncryptionKey, IV);
        }

        /// <summary>
        /// Encrypts the text using the supplied Key and the optional Initial Vector
        /// </summary>
        /// <param name="Data">The data to encrypt</param>
        /// <param name="Key">The key used to encrypt the data</param>
        /// <param name="IV">The Initial Vector used to encrypt the data</param>
        /// <returns></returns>
        public static String EncryptText(String Data, String Key, String IV = _IV)
        {
            // Extract the bytes of each of the values
            byte[] input = Encoding.UTF8.GetBytes(Data);
            byte[] key = Convert.FromBase64String(Key);
            byte[] iv = Convert.FromBase64String(IV);


            // Create a new instance of the algorithm with the desired settings
            RijndaelManaged algorithm = new RijndaelManaged();
            algorithm.Mode = CipherMode.ECB;
            algorithm.Padding = PaddingMode.PKCS7;
            algorithm.BlockSize = 128;
            algorithm.KeySize = 128;
            algorithm.Key = key;
            //algorithm.IV = iv;

            // Create a new encryptor and encrypt the given value
            ICryptoTransform cipher = algorithm.CreateEncryptor();
            byte[] output = cipher.TransformFinalBlock(input, 0, input.Length);

            // Finally, return the encrypted value in base64 format
            String encrypted = Convert.ToBase64String(output);

            return encrypted;
        }

        /// <summary>
        /// Decrypts the text using the Encryption Key found in the web.config file
        /// and the optional Initial Vector
        /// </summary>
        /// <param name="Data">The data to Decrypt</param>
        /// <param name="IV">The initial vector used to decrypt the data</param>
        /// <returns>The decrypted string</returns>
        public static String DecryptText(String Data, String IV = _IV)
        {
            //Call Method below using Encryption Key found in web.config
            return DecryptText(Data, EncryptionKey, IV);
        }

        /// <summary>
        /// Decrypts the text using the supplied Key and the optional Initial Vector.
        /// </summary>
        /// <param name="Data">The data to Decrypt</param>
        /// <param name="Key">The key to use to decrypt the data</param>
        /// <param name="IV">The initial vector to use to decrypt the data</param>
        /// <returns>The decrypted string</returns>
        public static String DecryptText(String Data, String Key, String IV = _IV)
        {
            // Extract the bytes of each of the values
            byte[] input = Convert.FromBase64String(Data);
            byte[] key = Convert.FromBase64String(Key);
            byte[] iv = Convert.FromBase64String(IV);

            // Create a new instance of the algorithm with the desired settings
            RijndaelManaged algorithm = new RijndaelManaged();
            algorithm.Mode = CipherMode.ECB;
            algorithm.Padding = PaddingMode.PKCS7;
            algorithm.BlockSize = 128;
            algorithm.KeySize = 128;
            algorithm.Key = key;

            //FromBase64String
            // Create a new encryptor and encrypt the given value
            ICryptoTransform cipher = algorithm.CreateDecryptor();
            byte[] output = cipher.TransformFinalBlock(input, 0, input.Length);

            // Finally, convert the decrypted value to UTF8 format
            String decrypted = Encoding.UTF8.GetString(output);

            //return the decrypted string
            return decrypted;
        }
    }
}
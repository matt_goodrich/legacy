﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

using log4net;

namespace SSO.Controllers
{
    /// <summary>
    /// Home Controller that will load the 
    /// default view for the web application.
    /// </summary>
    public class HomeController : Controller
    {
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        /// <summary>
        /// This will load the Default Page of 
        /// the web application. This view
        /// does not requre view data or any
        /// other information.
        /// </summary>
        public ActionResult Index()
        {
            return View();
        }

    }
}

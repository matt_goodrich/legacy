﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;

using SSO.Common;
using SSO.Common.SAML20;

using log4net;

namespace SSO.Controllers
{
    /// <summary>
    /// ACS Controller class is used to consume and validate
    /// the SAML Assertion. Once Validated, the Wingo and 
    /// Jackman cookies will be created along with the New
    /// Davita SSO cookie.
    /// </summary>
    public class ACSController : Controller
    {
        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        /// <summary>
        /// Controller action used to recieve and validate the SAML assertion.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //Setup our Cookie Store
            DavitaCookies DC = new DavitaCookies();

            //Check to see if we recieved a SAML Response from the SSO Server
            if (Request["SAMLResponse"] != null)
            {
                ServerLog.DebugFormat("Assertion Recieved from SSO Server: {0} with RelayState {1}", Request["SAMLResponse"], Request["RelayState"]);

                //Save the data from the request into the ViewData collection
                ViewData["RelayState"] = Request["RelayState"];
                ViewData["SAMLResponse"] = Request["SAMLResponse"];
                //Base 64 decode, UTF8 encode and HTML encode the SAML Response so it can be displayed on the screen
                ViewData["SAMLDecoded"] = Server.HtmlEncode(Encoding.UTF8.GetString(Convert.FromBase64String(Request["SAMLResponse"])));

                //Retrieve a dictionary of values from the SAML Response after the deserialization occurs
                Dictionary<string, object> SAMLValues = SAMLResponse.DeSerializeSAMLResponse(Encoding.UTF8.GetString(Convert.FromBase64String(Request["SAMLResponse"])));

                //Start the validation of the SAML response
                //Variable to determine if validation was successfull, false by default
                bool PassedValidation = false;

                //Get the "SAMLCookie" generated during the generation of the SAML Authentication Request
                HttpCookie SAMLCookie = Request.Cookies[SAMLValues["InResponseTo"].ToString()];
                
                //Validate the Signature
                if (Convert.ToBoolean(SAMLValues["Signature"]))
                    PassedValidation = true;
                ViewData["Signature"] = SAMLValues["Signature"].ToString();
                ViewData["ExpectedSignature"] = "True";

                //Validate InResponseTo
                if (SAMLCookie != null)
                {
                    if (SAMLCookie["ID"].Equals(SAMLValues["InResponseTo"].ToString()))
                    {
                        if (!PassedValidation)
                            PassedValidation = false;
                        else
                            PassedValidation = true;
                        ServerLog.Debug("InResponseTo Passed Validation");
                    }
                    else
                    {
                        PassedValidation = false;
                        ServerLog.Debug("InResponseTo Failed Validation");
                    }
                }
                ViewData["ExpectedInResponseTo"] = (SAMLCookie["ID"] != null ? SAMLCookie["ID"] : "NULL");
                ViewData["InResponseTo"] = SAMLValues["InResponseTo"].ToString();

                //Validate Status
                if (StatusCodes.Success.Equals(SAMLValues["StatusCode"].ToString()))
                {
                    if (!PassedValidation)
                        PassedValidation = false;
                    else
                        PassedValidation = true;
                    ServerLog.Debug("StatusCode Passed Validation");
                }
                else
                {
                    PassedValidation = false;
                    ServerLog.Debug("StatusCode Failed Validation");
                }
                ViewData["StatusCode"] = SAMLValues["StatusCode"].ToString();
                ViewData["ExpectedStatusCode"] = StatusCodes.Success;

                //Validate Issuer
                if (AppConfig.SSOServerIssuerName.Equals(SAMLValues["Issuer"].ToString()))
                {
                    if (!PassedValidation)
                        PassedValidation = false;
                    else
                        PassedValidation = true;
                    ServerLog.Debug("Issuer Passed Validation");
                }
                else
                {
                    PassedValidation = false;
                    ServerLog.Debug("Issuer Failed Validation");
                }
                ViewData["Issuer"] = SAMLValues["Issuer"].ToString();
                ViewData["ExpectedIssuer"] = AppConfig.SSOServerIssuerName;

                //Validate Audience
                if (SAMLCookie != null)
                {
                    if (SAMLCookie["Issuer"].Equals(SAMLValues["Audience"].ToString()))
                    {
                        if (!PassedValidation)
                            PassedValidation = false;
                        else
                            PassedValidation = true;
                        ServerLog.Debug("Audience Passed Validation");
                    }
                    else
                    {
                        PassedValidation = false;
                        ServerLog.Debug("Audience Failed Validation");
                    }
                }
                ViewData["Audience"] = SAMLValues["Audience"].ToString();
                ViewData["ExpectedAudience"] = (SAMLCookie["Issuer"] != null ? SAMLCookie["Issuer"] : "NULL");

                //Validate NotOnOrAfter
                if (SAMLCookie != null)
                {
                    if (Convert.ToDateTime(SAMLCookie["IssueInstant"]) < Convert.ToDateTime(SAMLValues["NotOnOrAfter"].ToString()))
                    {
                        if (!PassedValidation)
                            PassedValidation = false;
                        else
                            PassedValidation = true;
                        ServerLog.Debug("NotOnOrAfter Passed Validation");
                    }
                    else
                    {
                        PassedValidation = false;
                        ServerLog.Debug("NotOnOrAfter Failed Validation");
                    }
                }
                ViewData["NotOnOrAfter"] = Convert.ToDateTime(SAMLValues["NotOnOrAfter"]).ToString();
                ViewData["ExpectedNotOnOrAfter"] = (SAMLCookie["IssueInstant"] != null ? SAMLCookie["IssueInstant"] : "NULL");

                //Validate NotBefore
                if (SAMLCookie != null)
                {
                    if (Convert.ToDateTime(SAMLCookie["IssueInstant"]) > Convert.ToDateTime(SAMLValues["NotBefore"].ToString()))
                    {
                        if (!PassedValidation)
                            PassedValidation = false;
                        else
                            PassedValidation = true;
                        ServerLog.Debug("NotBefore Passed Validation");
                    }
                    else
                    {
                        PassedValidation = false;
                        ServerLog.Debug("NotBefore Failed Validation");
                    }
                }
                ViewData["NotBefore"] = Convert.ToDateTime(SAMLValues["NotBefore"]).ToString();
                ViewData["ExpectedNotBefore"] = (SAMLCookie["IssueInstant"] != null ? SAMLCookie["IssueInstant"] : "NULL");

                

                //Setup WINGO Cookie
                Dictionary<string, object> WingoValues = new Dictionary<string, object>();

                if (PassedValidation)
                {
                    ServerLog.Debug("SAML Assertion Passed Validation");

                    //Create NEW DavitaCookie dictionary to store cookie values
                    Dictionary<string, object> DavitaSSOValues = new Dictionary<string,object>();
                    //Add the audience to the dictionary of values that will be needed for the cookie
                    DavitaSSOValues.Add("Audience", SAMLValues["Audience"].ToString());
                    //Iterate over the list of values retrieved from the SAML Assertion looking for only attributes
                    foreach (KeyValuePair<string, object> kvp in SAMLValues)
                    {
                        if (kvp.Key.Contains("ATTR::"))
                            DavitaSSOValues.Add(kvp.Key, kvp.Value);
                    }

                    //Retrieve the Davita Cookie from the Cookie Store
                    HttpCookie DavitaCookie = DC.GetCookie(DavitaCookies.CookieTypes.DavitaSSO, DavitaSSOValues);
                    //Add the Cookie to the response
                    Response.Cookies.Add(DavitaCookie);

                    //Create Legacy Davita Cookies
                    //Wingo Cookie
                    //Attempt to grab the values from the saml assertion and load them into the dictionary created to populate the cookie value
                    WingoValues.Add("UserLoggedIn", "true");
                    try
                    {
                        WingoValues.Add("EmailAddress", SAMLValues["EmailAddress"].ToString());
                    }
                    catch (Exception ex)
                    {
                        WingoValues.Add("EmailAddress", "noemail@davita.com");
                        ServerLog.Error("Error Creating WINGO Cookie", ex);
                    }
                    try
                    {
                        WingoValues.Add("Username", SAMLValues["Username"].ToString());
                    }
                    catch (Exception ex)
                    {
                         WingoValues.Add("Username", "nobody");
                         ServerLog.Error("Error Creating WINGO Cookie", ex);
                    }
                    try
                    {
                        WingoValues.Add("FirstLastName", SAMLValues["FirstLastName"].ToString());
                    }
                    catch (Exception ex)
                    {
                        WingoValues.Add("FirstLastName", "unknown");
                        ServerLog.Error("Error Creating WINGO Cookie", ex);
                    }
                    try
                    {
                        WingoValues.Add("Org", SAMLValues["Org"].ToString());
                    }
                    catch (Exception ex)
                    {
                         WingoValues.Add("Org", "unk");
                         ServerLog.Error("Error Creating WINGO Cookie", ex);
                    }
                    try
                    {
                        WingoValues.Add("UserNumber", SAMLValues["UserNumber"].ToString());
                    }
                    catch (Exception ex)
                    {
                        WingoValues.Add("UserNumber", 0);
                        ServerLog.Error("Error Creating WINGO Cookie", ex);
                    }
                    try
                    {
                        WingoValues.Add("WorkforceID", SAMLValues["WorkforceID"].ToString());
                    }
                    catch (Exception ex)
                    {
                        WingoValues.Add("WorkforceID", "0");
                        ServerLog.Error("Error Creating WINGO Cookie", ex);
                    }
                    try
                    {
                        WingoValues.Add("PriviledgeIDList", (!String.IsNullOrEmpty(SAMLValues["PriviledgeIDList"].ToString()) ? SAMLValues["PriviledgeIDList"].ToString() : "UNK"));
                    }
                    catch (Exception ex)
                    {
                        WingoValues.Add("PriviledgeIDList", "UNK");
                        ServerLog.Error("Error Creating WINGO Cookie", ex);
                    }
                    try
                    {
                        WingoValues.Add("ADContext", Server.UrlEncode(SAMLValues["ADContext"].ToString()));
                    }
                    catch (Exception ex)
                    {
                         WingoValues.Add("ADContext", Server.UrlEncode("OU=UNK"));
                         ServerLog.Error("Error Creating WINGO Cookie", ex);
                    }
                    try
                    {
                        WingoValues.Add("TitleID", SAMLValues["TitleID"].ToString());
                    }
                    catch (Exception ex)
                    {
                        WingoValues.Add("TitleID", 0);
                        ServerLog.Error("Error Creating WINGO Cookie", ex);
                    }
                    //Retrieve the cookie from the cookie store
                    HttpCookie Wingo = DC.GetCookie(DavitaCookies.CookieTypes.Wingo, WingoValues);
                    //Add the cookie to the response
                    Response.Cookies.Add(Wingo);
                    try
                    {
                        //Jackman Cookie dictionary to store the cookie values
                        Dictionary<string, object> JackmanValues = new Dictionary<string, object>();
                        //Add the UserNumber to the dictionary
                        try
                        {
                            JackmanValues.Add("UserNumber", SAMLValues["UserNumber"].ToString());
                        }
                        catch (Exception ex)
                        {
                            JackmanValues.Add("UserNumber", "0");
                        }
                        //Add the LastAppID to the dictionary
                        JackmanValues.Add("LastAppID", SAMLCookie["LastAppID"].ToString());
                        //Get the cookie from the cookie store
                        HttpCookie Jackman = DC.GetCookie(DavitaCookies.CookieTypes.Jackman, JackmanValues);
                        //Add the cookie to the response
                        Response.Cookies.Add(Jackman);
                    }
                    catch (Exception ex)
                    {
                        ServerLog.Error("Error Creating JACKMAN Cookie", ex);
                    }

                    //Kill the SAML Request Cookie
                    SAMLCookie.Expires = DateTime.Now.AddDays(-2);
                    SAMLCookie.Values.Clear();
                    Response.Cookies.Add(SAMLCookie);

                    //Redirect to RelayState
                    Response.Redirect(Request["RelayState"]);
                }
                else
                {
                    ServerLog.Debug("SAML Assertion Failed Validation");

                    //Create Default WINGO cookie
                    HttpCookie Wingo = DC.GetCookie(DavitaCookies.CookieTypes.Wingo, new Dictionary<string, object>());
                    Response.Cookies.Add(Wingo);

                    ServerLog.Debug("Creating Default Wingo Cookie");

                    //User was not authenticated successfully so no Jackman Cookie will be created.
                    //Additionally, no DavitaSSO cookie will be created.

                    Response.Redirect("/Error/HttpError", true);
                }
            }
            else //No SAML Response was recieved
            {
                ServerLog.Debug("NO SAML Response Recieved");
                //Create Default WINGO cookie
                HttpCookie Wingo = DC.GetCookie(DavitaCookies.CookieTypes.Wingo, new Dictionary<string, object>());
                Response.Cookies.Add(Wingo);

                Response.Redirect("/Error/HttpError", true);
            }

            return View(ViewData);
        }

    }
}

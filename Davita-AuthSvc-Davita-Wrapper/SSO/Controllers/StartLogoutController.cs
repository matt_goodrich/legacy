﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;

using SSO.Common;

using log4net;

namespace SSO.Controllers
{
    /// <summary>
    /// StartLogout Controller class is used to Kill Cookies or
    /// set Cookies to a default value in order to kill the session
    /// for the current application without affecting other applications
    /// </summary>
    public class StartLogoutController : Controller
    {
        RailoCFMXCompat cfmx = new RailoCFMXCompat();

        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");
        
        /// <summary>
        /// View used when an issuer ID is passed to the controller.
        /// This will kill the "DavitaSSO" cookie as well as set default
        /// values for the "WINGO" cookie and the removal of the "JACKMAN_XXX"
        /// cookie.
        /// </summary>
        /// <param name="Issuer">The Issuer/Identifier of the application</param>
        /// <returns>StartLogout View</returns>
        public ActionResult Index(string Issuer)
        {
            DavitaCookies DC = new DavitaCookies();
            //Kill the NEW DavitaCookie
            //Get the Cookie from the browser
            HttpCookie DavitaCookie = Request.Cookies[DavitaSSOCookie.CookieName];
            //Make sure the cookie actually existed
            if (DavitaCookie != null)
            {
                //If the Cookie existed, make it a persistent cookie
                DavitaCookie.Expires = DateTime.Now.AddDays(-2);
                DavitaCookie.Domain = DavitaSSOCookie.Domain;
                DavitaCookie.HttpOnly = DavitaSSOCookie.HttpOnly;
                DavitaCookie.Path = DavitaSSOCookie.Path;
                //Add the expired cookie back to the browser, effectively deleting it
                Response.Cookies.Add(DavitaCookie);

                ServerLog.DebugFormat("Expired {0} Cookie added to browser", DavitaSSOCookie.CookieName);
            }
            else
            {
                ServerLog.DebugFormat("{0} Cookie not found", DavitaSSOCookie.CookieName);
            }

            //Kill the Legacy Davita Cookies
            //In order to kill the Jackman Cookie, we must know the UserNumber, so we shall pull it from the current cookie
            HttpCookie _WingoCookie = Request.Cookies[WingoCookie.CookieName];
            if (_WingoCookie != null)
            {
                //Decrypt the Value of the Wingo Cookie to grab the UserNumber
                string _WingoValue = Encoding.UTF8.GetString(cfmx.transformString(AppConfig.DavitaCookieEncryptionKey, Convert.FromBase64String(Server.UrlDecode(_WingoCookie.Value))));

                ServerLog.DebugFormat("Wingo Cookie Decrypted Value: {0}", _WingoValue);

                //Split the decrypted value by the pipe character
                string[] Values = _WingoValue.Split('|');

                //Ensure all the values are there
                if (Values.Length > 5)
                {
                    //Get the Jackman Cookie based upon the UserNumber
                    HttpCookie _JackmanCookie = Request.Cookies[JackmanCookie.CookieName];
                    //If we found the Jackman cookie
                    if (_JackmanCookie != null)
                    {
                        //Make the cookie a persistent cookie
                        _JackmanCookie.Expires = DateTime.Now.AddDays(-2);
                        _JackmanCookie.Domain = JackmanCookie.Domain;
                        _JackmanCookie.HttpOnly = JackmanCookie.HttpOnly;
                        _JackmanCookie.Path = JackmanCookie.Path;
                        //Add the expired cookie back to the browser, effectively deleting it
                        Response.Cookies.Add(_JackmanCookie);

                        ServerLog.DebugFormat("Expired {0} Cookie added to browser", JackmanCookie.CookieName);
                    }
                    else
                    {
                        ServerLog.DebugFormat("{0} Cookie not found", JackmanCookie.CookieName);
                    }
                }
            }
            else
            {
                ServerLog.DebugFormat("{0} Cookie not found", WingoCookie.CookieName);
            }

            //Wingo Cookie - The Wingo Cookie should not be deleted, it should be loaded with the default values
            _WingoCookie = DC.GetCookie(DavitaCookies.CookieTypes.Wingo, new Dictionary<string, object>());
            Response.Cookies.Add(_WingoCookie);
            ServerLog.DebugFormat("Adding {0} Cookie to browser with default values", WingoCookie.CookieName);

            //Redirect to SSO Server Cookie Killer
            Response.Redirect(AppConfig.SSOServerURL + ":" + AppConfig.SSOServerPort + AppConfig.SSOServerLogoutEndpoint + Issuer);

            return View();
        }

    }
}

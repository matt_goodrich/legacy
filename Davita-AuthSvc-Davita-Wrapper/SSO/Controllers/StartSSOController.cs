﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SSO.Common;
using SSO.Common.SAML20;

using log4net;

namespace SSO.Controllers
{
    public class StartSSOController : Controller
    {
        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");
        
        /// <summary>
        /// Controller action used to start the SSO transaction by sending
        /// a SAML 2.0 Authentication Request to the SSO IdP Server
        /// </summary>
        /// <param name="Issuer">The Issuer - Identifies the App on the SSO Server</param>
        /// <param name="RelayState">RelayState - The URL to return to after a successful SSO Login</param>
        /// <param name="ForceAuthn">"True" or "False" as to whether or not to force Authentication</param>
        /// <param name="LastAppID">The Legacy ID of the application</param>
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Index(string Issuer, string RelayState, string ForceAuthn = "false", string LastAppID = "0")
        {
            ServerLog.DebugFormat("Starting SSO: Issuer = {0}, RelayState = {1}, ForceAuthn = {2}, LastAppID = {3}", Issuer, RelayState, ForceAuthn, LastAppID);

            //Generate an ID for this transaction
            string ID = "_" + Guid.NewGuid().ToString();
            ServerLog.DebugFormat("Generating Transaction ID {0}", ID);

            //Create a cookie to be used for validation later
            HttpCookie SAMLCookie = new HttpCookie(ID);
            SAMLCookie.Values.Add("ID", ID);
            SAMLCookie.Values.Add("Issuer", Issuer);
            SAMLCookie.Values.Add("IssueInstant", DateTime.UtcNow.ToString());
            SAMLCookie.Values.Add("LastAppID", LastAppID);
            //SAMLCookie.Expires = DateTime.UtcNow.AddMinutes(10);
            SAMLCookie.Path = "/";
            //SAMLCookie.Domain = (Request.Url.Host.Contains(".") ? Request.Url.Host.Substring(Request.Url.Host.IndexOf(".")) : "localhost");
            
            //Add the cookie to the response
            Response.Cookies.Add(SAMLCookie);

            ServerLog.DebugFormat("Creating SAML Request Cookie with values: ID = {0}, Issuer = {1}, IssueInstant = {2}, Expires = {3}, Domain = {4}", ID, Issuer, DateTime.UtcNow.ToString(), DateTime.Now.AddMinutes(10), (Request.Url.Host.Contains(".") ? Request.Url.Host.Substring(Request.Url.Host.IndexOf(".")) : Request.Url.Host));

            //Generate the SAML Request and add it to the ViewData collection
            string request = SAMLRequest.GetSamlRequest(Issuer, Url.Action("Index", "StartSSO", new { Issuer = Issuer, RelayState = RelayState, ForceAuthn = ForceAuthn }, "https"), Url.Action("Index", "ACS", null, "https"), ID);//.Replace("+", "%2B");
            ViewData["request"] = request;

            //Generate the action of the form to post the Authentication Request and add it to the ViewData collection
            string action = AppConfig.SSOServerURL + ":" + AppConfig.SSOServerPort + AppConfig.SSOServerEndpoint;
            ViewData["action"] = action;

            //Add the RelayState to the ViewData collection
            ViewData["relaystate"] = RelayState;

            ServerLog.DebugFormat("POSTing to SSO Server at {0} with Request {1} and RelayState {2}", action, request, RelayState);

            //Return the view. The view will then automatically POST the Request to the SSO server
            return View(ViewData);
        }

    }
}

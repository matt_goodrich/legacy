﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using log4net;

namespace SSO.Controllers
{
    public class ErrorController : Controller
    {
        //Setup our Server Log
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        public ActionResult HttpError()
        {
            Exception ex = null;

            try
            {
                ex = (Exception)HttpContext.Application[Request.UserHostAddress.ToString()];
            }
            catch
            {
            }

            if (ex != null)
            {
                ServerLog.Error("Exception caught in application", ex);
            }

            ViewData["Title"] = "Page Expired";
            ViewData["Description"] = "To protect privacy and enhance security, the page you are trying to access is no longer available.";

            return View("Error");
        }

        public ActionResult Http404()
        {
            ViewData["Title"] = "The page you requested was not found";

            return View("Error");
        }
    }
}

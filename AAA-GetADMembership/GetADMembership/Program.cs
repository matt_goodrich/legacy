﻿using System;
using System.Collections.Generic;
using System.Text;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;


namespace GetADMembership
{
    class Program
    {
        public static string groupName = string.Empty;
        public static string domainName = string.Empty;

        static void Main(string[] args)
        {
            groupName = args[0]; 
            domainName = args[1]; 

            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, domainName); 
            GroupPrincipal grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, groupName); 

            GroupPrincipal R30 = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "GrpMemberPoint_R30");
            GroupPrincipal R31 = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "GrpMemberPoint_R31");
            GroupPrincipal R32 = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "GrpMemberPoint_R32");
            GroupPrincipal R33 = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "GrpMemberPoint_R33");
            GroupPrincipal R34 = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "GrpMemberPoint_R34");
            GroupPrincipal R35 = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "GrpMemberPoint_R35");
            GroupPrincipal R36 = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "GrpMemberPoint_R36");
            GroupPrincipal R38 = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "GrpMemberPoint_R38");
            GroupPrincipal R41 = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, "GrpMemberPoint_R41");

            if (grp != null) 
            {
                using (StreamWriter file = new StreamWriter(@"C:\Users.csv"))
                {
                    foreach (Principal p in grp.GetMembers(true)) 
                    { 
                        file.WriteLine(String.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}",p.Name, GetProperty(p, "employeeNumber"), p.SamAccountName, p.IsMemberOf(R30), p.IsMemberOf(R31), p.IsMemberOf(R32), p.IsMemberOf(R33), p.IsMemberOf(R34), p.IsMemberOf(R35), p.IsMemberOf(R36), p.IsMemberOf(R38), p.IsMemberOf(R41))); 
                    }
                }

                grp.Dispose(); 
                ctx.Dispose(); 

            } 
            else 
            { 
                Console.WriteLine("\nWe did not find that group in that domain, perhaps the group resides in a different domain?"); 
            }  
        }

        public static String GetProperty(Principal principal, String property)
        {
            DirectoryEntry directoryEntry = principal.GetUnderlyingObject() as DirectoryEntry;
            if (directoryEntry.Properties.Contains(property))
                return directoryEntry.Properties[property].Value.ToString();
            else
                return String.Empty;
        }

    }
}

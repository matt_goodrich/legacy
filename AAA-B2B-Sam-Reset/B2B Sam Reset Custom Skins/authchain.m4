define(`CUSTOM_AUTHCHAIN')

!!!AUTH_VIEW_authemailpin_no_email
<!--@container()-->
<!--@prompt( class="bold" text="%VIEW_SUBTITLE%" )--><!--@endprompt()-->
<!--@table( class="border" )-->
  <!--@tablerow()-->
    <!--@prompt( class="error" text="You haven't registered an e-mail address for use with E-Mail PIN authentication.  Please try another method or contact the help desk for further assistance." )--><!--@endprompt()-->
  <!--@endtablerow()-->
<!--@endtable()-->
<!--@endcontainer()-->

!!!AUTHEMAILPIN_TITLE
Verifying PIN 

!!!AUTHEMAILPIN_SUBTITLE
You will soon receive an e-mail contains a PIN.  Please enter that
PIN here once you receive it. Note that the PIN is only valid for
a limited period of time.

!!!AUTHEMAILPIN_FIELD_DESC
PIN: 

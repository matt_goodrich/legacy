dnl This is an example customization file
dnl
dnl You can use this file to construct the style.css file.
dnl Simply copy this file to
dnl    <install dir>\design\custom\style.m4
dnl Modify the style-sheet definitions, and run make.bat to regenerate the skins.

dnl Do not modify this line!
define(`CUSTOM_STYLE')

dnl Copy the marked section from <install dir>\design\src\css\style.m4 here
dnl and modify as needed.
dnl CUSTOMIZATIONS: Start copy


/*
 * Default styles for common HTML elements
 */

a, abbr, acronym, applet, b, big, blockquote, body, caption, center, code,
del, dfn, div, em, fieldset, font, form, h1, h2, h3, h4, h5, h6, html, hr,
i, iframe, img, label, li, object, ol, p, pre, small, span, strike, strong,
sub, sup, table, tbody, td, tfoot, th, thead, tr, u, ul
  {
  border: 0;
  font-size: 100%;
  margin: 0;
  outline: 0;
  padding: 0;
  vertical-align: baseline;
  }

:focus
  {
  outline: 0;
  }

/* Tables still need cellspacing="0" */
table
  {
  border-spacing: 0;
  empty-cells: show;
  }

input,
select,
textarea
  {
  font-size: 1em;
  }

html
  {
  padding: 0;
  padding-bottom: 20px;
  _DIR
  }

body
  {
  background-color: _WHITE;
  color: _BLACK;
  font-family: _FONT_FAMILY;
  font-size: _FONT_BASE;
  line-height: 1.25em;
  min-width: 800px;
  text-align: _DIR_LEFT;
  }

a,
a:active,
a:link,
a:visited
  {
  color: _LINK;
  font-family: _FONT_FAMILY;
  outline: 0;
  text-decoration: none;
  }

a:focus,
input.image:focus
  {
  outline: 1px dotted;
  }

a:hover
  {
  color: _LINKHOVER;
  text-decoration: underline;
  }

h1
  {
  color: _MEDIUMBLUE;
  font-size: _FONT_LARGEST;
  font-weight: normal;
  line-height: 1em;
  margin: 0 0 12px;
  padding: 10px 0 0;
  }

h2
  {
  font-size: _FONT_LARGER;
  margin: 12px 0 6px;
  }

input,
select,
textarea
  {
  font-family: _FONT_FAMILY;
  margin: 0 2px;
  padding: 2px;
  vertical-align: middle;
  }

input.text:focus
  {
  outline: 0;
  }

select
  {
  padding: 1px;
  }

img,
input.image
  {
  padding: 0;
  vertical-align: middle;
  }

input.text,
select,
textarea
  {
  background-color: _WHITE;
  border: 1px solid _LIGHTBLUE;
  }

input.checkbox,
input.radio
  {
  cursor: pointer;
  margin-_DIR_RIGHT: 4px;
  vertical-align: text-bottom;
  }

label[for]
  {
  cursor: pointer;
  }

input.text
  {
  background: _WHITE url(_CSSPICSDIR/field.png) top _DIR_LEFT no-repeat;
  width: 20em;
  }

select,
textarea
  {
  background: _WHITE url(_CSSPICSDIR/field_big.png) top _DIR_LEFT no-repeat;
  }

textarea
  {
  width: 90%;
  }

sup
  {
  line-height: 0.5em;
  vertical-align: top;
  }

input.text.focus,
input.text:focus
  {
  background-image: url(_CSSPICSDIR/field_focus.png);
  }

select.focus,
select:focus,
textarea.focus,
textarea:focus
  {
  background-image: url(_CSSPICSDIR/field_big_focus.png);
  }

input.focus.checkbox,
input.focus.radio
  {
  background-image: none;
  }

input.image
  {
  margin: 1px 0;
  vertical-align: top;
  }

a.button,
.miniblue,
input.minigreen,
input.minired,
input.submit
  {
  background: _BTN_BG;
  border: none;
  border-bottom: 1px solid _BUTTONBORDER;
  border-_DIR_RIGHT: 1px solid _BUTTONBORDER;
  color: _WHITE;
  cursor: pointer;
  font-size: _FONT_SMALLER;
  height: 1.9em;
  margin: 2px;
  padding: 2px 1.5em 2px;
  }

a.button,
a.button:link,
a.button:visited
a.miniblue,
a.miniblue:link,
a.miniblue:visited
  {
  border-bottom: 1px solid _BUTTONBORDER;
  border-_DIR_RIGHT: 1px solid _BUTTONBORDER;
  color: _WHITE;
  height: auto;
  overflow: auto;
  text-decoration: none;
  }

a.button,
a.button:link,
a.button:visited
  {
  padding-bottom: 3px;
  padding-top: 3px;
  }

a.button:active,
a.button:hover,
input.submit:hover
  {
  background: _BTN_HOVER_BG;
  color: _WHITE;
  text-decoration: none;
  }

input.submit.narrow
  {
  padding-_DIR_LEFT: 0.4em;
  padding-_DIR_RIGHT: 0.4em;
  }

.miniblue,
input.minigreen,
input.minired
  {
  font-weight: normal;
  height: 1.5em;
  line-height: 1.2em;
  padding: 1px 3px 1px;
  }

.miniblue,
input.miniblue
  {
  background: _BTN_MINIBLUE_BG;
  border-bottom: 1px solid _BUTTONBORDER;
  }

.miniblue:hover
  {
  background: _BTN_MINIBLUE_HOVER_BG;
  }

input.minigreen
  {
  background: _BTN_MINIGREEN_BG;
  border-bottom: 1px solid _BUTTONBORDER;
  }

input.minigreen:hover
  {
  background: _BTN_MINIGREEN_HOVER_BG;
  }

input.minired
  {
  background: _BTN_MINIRED_BG;
  border-bottom: 1px solid _BUTTONBORDER;
  }

input.minired:hover
  {
  background: _BTN_MINIRED_HOVER_BG;
  }

p
  {
  margin: 6px 0 4px;
  }

td
  {
  text-align: _DIR_LEFT;
  vertical-align: top;
  }

th
  {
  background: _TABLE_OUTER_HEADER_BG;
  color: _WHITE;
  text-align: _DIR_LEFT;
  vertical-align: middle;
  }

ol,
ul
  {
  padding: 0;
  padding-_DIR_LEFT: 20px;
  }

hr
  {
  width: 100%;
  height: 1px;
  color: _LIGHTERGREY;
  background-color: _LIGHTERGREY;
  }



/*
 * Global Alignment Styles
 */

/* Clear forces new elements under floating elements (not floating beside) */
.clearFloat
  {
  clear: both;
  }

/* Gives an element no height hiding its contents */
.noHeight
  {
  height: 0px;
  overflow: hidden;
  }

/* Vertical align contents to middle of cell */
td.middle
  {
  vertical-align: middle;
  }

/* Vertical align contents to top of cell */
td.top
  {
  vertical-align: top;
  }

/* Center align contents */
.center
  {
  text-align: center;
  }
div.center table
  {
  margin-_DIR_LEFT: auto;
  margin-_DIR_RIGHT: auto;
  }

/* Left align contents */
.left
  {
  text-align: _DIR_LEFT;
  }
div.left table
  {
  margin-_DIR_LEFT: 0;
  margin-_DIR_RIGHT: auto;
  }
.floatLeft
  {
  float: _DIR_LEFT;
  }

/* Right align contents */
.right
  {
  text-align: _DIR_RIGHT;
  }
div.right table
  {
  margin-_DIR_LEFT: auto;
  margin-_DIR_RIGHT: 0;
  }

/* Top align contents */
table.top tr td
  {
  vertical-align: top;
  }

div.indent
  {
  float: _DIR_LEFT;
  height: 1px;
  width: 21px;
  }



/*
 * Global Text Styles
 */

.italic
  {
  font-style: italic;
  }

.bold,
table tr.bold td
  {
  font-weight: bold;
  }

span.deleted,
div.deleted,
p.deleted,
table tr.deleted td span
  {
  text-decoration: line-through;
  }

.large,
table tr.large td
  {
  font-size: _FONT_LARGER;
  }

.small
  {
  font-size: _FONT_SMALLER !important;
  line-height: 1.2em;
  }

.below
  {
  display: block;
  }

ul.subList li
  {
  font-style: italic;
  line-height: 1.25em;
  }

ul.subList
  {
  list-style: none;
  padding-_DIR_LEFT: 16px;
  margin: 0;
  }

.reference
  {
  font-weight: bold;
  font-style: italic;
  white-space: nowrap;
  }

.selectedObj
  {
  color: _BLUE;
  font-size: 0.7em;
  font-weight: normal;
  line-height: 1em;
  margin: 0 0 12px;
  padding: 10px 0 0;
  }

/* Dotted horizontal divider */
div.hr
  {
  background: url(_CSSPICSDIR/header_dotline.gif) repeat-x;
  height: 1px;
  font-size: 1pt;
  }

/* Enable wrapping on element */
.wrap
  {
  white-space: normal;
  }

/* Change colour of text in readonly fields */
input.readonly
  {
  color: _BLACK;
  }

/* Change border colour of disabled fields (CSS3, will not work on all browsers) */
input[readonly],
input[disabled],
input.disabled,
textarea[readonly],
textarea[disabled],
textarea.disabled
  {
  border-color: _LIGHTGREY;
  }
select[disabled],
select.disabled
  {
  border-color: _LIGHTGREY;
  }

/* Remove border from element */
.noBorder
  {
  border: 0;
  }

/* Remove padding from element */
.noPad
  {
  margin: 0;
  padding: 0;
  }

/* Disable wrapping on element */
.noWrap
  {
  white-space: nowrap;
  }

/* Hide element */
.collapseSectionHide,
.formToggleHide,
.hidden,
.hide
  {
  display: none;
  }

div.notice
  {
  color: _DARKGREY;
  }

td.sysVarName
  {
  width: 23%;
  }

table.list > tbody > tr > td > input.sysVarLongString
  {
  width: 90%;
  }



/*
 * Page Layout Styles
 */

#containerWrap
  {
  margin: 0 20px 20px;
  }

/* Hack to keep top margin consistent on all browsers (only necessary
 * because of how IE handles hidden form fields) */
#topMargin
  {
  color: _WHITE;
  font: 20px/20px _FONT_FAMILY;
  line-height: 20px;
  }

#skipNav,
#skipNav a,
#skipNav a:visited
  {
  color: _WHITE;
  cursor: default;
  display: inline;
  font-size: 0.7em;
  line-height: normal;
  }

#skipNav a:focus
  {
  outline: 0;
  }

#header,
#navigation
  {
  border: 1px solid _DARKGREY;
  border-bottom: none;
  }

html#subPage body #header
  {
  border-bottom: 1px solid _DARKGREY;
  height: auto;
  }
/*CHANGE*/
#header {
  height: 135px;
}
/*END CHANGE*/
#companyLogo
  {
  float: _DIR_RIGHT;
  padding: 13px;
  padding-_DIR_LEFT: 0;
  padding-_DIR_RIGHT: 15px;
  }

#colWrap
  {
  background: _CONTENT_BG;
  border: 1px solid _DARKGREY;
  border-top: none;
  table-layout: fixed; /* Required to force content to wrap to window width (not exceed it) */
  width: 100%;
  }

#menuCol
  {
  background: _LIGHTESTBLUE;
  border-_DIR_RIGHT: 1px solid _DARKGREY;
  width: 169px;
  margin: 0;
  padding: 0;
  }

body.noSide #menuCol
  {
  display: none;
  }

#menuContent
  {
  background: url(_CSSPICSDIR/fade.gif) top left repeat-x;
  }

#mainCol
  {
  background: url(_CSSPICSDIR/fade.gif) top left repeat-x;
  margin: 0;
  padding: 0;
  }

#content
  {
  padding: 0 30px 90px;
  /* Cannot be relative - breaks RTL support, as a result all JS positioning must be relative to page itself */
  }

#userInfo
  {
  color: _MEDIUMBLUE;
  font-size: _FONT_SMALLER;
  height: 1em;
  margin-_DIR_RIGHT: -15px;
  padding: 2px 0 6px;
  text-align: _DIR_RIGHT;
  }



/*
 * Top Buttons Styles
 */

#topButtons
  {
  margin: 3px 0 0;
  }

#topButtons ul,
#topButtons ul li
  {
  list-style: none;
  margin: 0;
  padding: 0;
  }

#topButtons ul li
  {
  float: _DIR_LEFT;
  }

#topButtons ul li input
  {
  cursor: pointer;
  font-weight: bold;
  font-size: _FONT_SMALLER;
  padding: 0;
  padding-_DIR_LEFT: 23px;
  padding-_DIR_RIGHT: 4px;
  }

#topButtons ul li input.inactiveIcon,
#topButtons ul li input.submitIcon
  {
  border: none;
  background: transparent;
  background-position: _DIR_LEFT center;
  margin: 0;
  margin-_DIR_LEFT: 6px;
  }

#topButtons ul li input.inactiveIcon
  {
  color: _DARKGREY;
  }

#topButtons ul li input.submit
  {
  border: 0;
  color: _BLACK;
  }

#topButtons ul li input.submit:hover
  {
  color: _DARKESTGREY;
  }

#topButtons ul li.inactive input.submit
  {
  border: 0;
  color: _GREY;
  cursor: default;
  }

#topButtons ul li input.back
  {
  background: url(_CSSPICSDIR/icon_back_on.gif) _DIR_LEFT center no-repeat;
  }

#topButtons ul li.inactive input.back
  {
  background: url(_CSSPICSDIR/icon_back_off.gif) _DIR_LEFT center no-repeat;
  }

#topButtons ul li input.close
  {
  background: url(_CSSPICSDIR/icon_close_on.gif) _DIR_LEFT center no-repeat;
  }

#topButtons ul li.inactive input.close
  {
  background: url(_CSSPICSDIR/icon_close_off.gif) _DIR_LEFT center no-repeat;
  }

#topButtons ul li input.home
  {
  background: url(_CSSPICSDIR/icon_home_on.gif) _DIR_LEFT center no-repeat;
  }

#topButtons ul li.inactive input.home
  {
  background: url(_CSSPICSDIR/icon_home_off.gif) _DIR_LEFT center no-repeat;
  }

#topButtons ul li input.refresh
  {
  background: url(_CSSPICSDIR/icon_refresh_on.gif) _DIR_LEFT center no-repeat;
  margin-_DIR_RIGHT: 3px;
  }

#topButtons ul li.inactive input.refresh
  {
  background: url(_CSSPICSDIR/icon_refresh_off.gif) _DIR_LEFT center no-repeat;
  }

#topButtons ul li input.logout
  {
  background: url(_CSSPICSDIR/icon_logout_on.gif) _DIR_LEFT center no-repeat;
  }

#topButtons ul li.inactive input.logout
  {
  background: url(_CSSPICSDIR/icon_logout_off.gif) _DIR_LEFT center no-repeat;
  }



/*
 * Top Navigation Styles (Nav Level 1)
 */

#topMenuBar
  {
  background: _MENU_TOP_BG;
  border-bottom: 1px solid _TOPMENUBORDER;
  border-_DIR_LEFT: 1px solid _DARKERGREY;
  border-_DIR_RIGHT: 1px solid _DARKERGREY;
  white-space: nowrap;
  min-height: 1.9em;
  }

body.noSide #topMenuBar
  {
  min-height: 1.9em;
  }

#topMenuBar ul
  {
  font-size: _FONT_SMALLER;
  font-weight: bold;
  list-style: none;
  margin: 0;
  padding: 0;
  }

#topMenuBar ul li
  {
  background: url(_CSSPICSDIR/nav_dotline.gif) top _DIR_RIGHT repeat-y;
  float: _DIR_LEFT;
  height: 2em;
  margin: 1px 0 0;
  padding: 0;
  white-space: nowrap;
  }

#topMenuBar ul li a
  {
  color: _WHITE;
  display: block;
  line-height: 2em;
  margin: 0;
  padding: 0 15px;
  }

#topMenuBar ul li a
  {
  text-decoration: none;
  }

#topMenuBar ul li.current
  {
  background: _MENU_TOP_HOVER_BG;
  }

#topMenuBar ul li.current a
  {
  border: none;
  color: _WHITE;
  }

#topMenuBar ul li a:hover
  {
  background: _MENU_TOP_HOVER_BG;
  }



/*
 * Side Menu Navigation Styles (Nav Level 2)
 */

#sideMenu
  {
  font-size: _FONT_SMALLER;
  padding: 8px 0 60px;
  }

#sideMenu ul
  {
  margin: 0;
  padding: 0;
  }

#sideMenu ul li
  {
  border-bottom: 1px solid _WHITE;
  list-style-type: none;
  margin: 0;
  padding: 0;
  }

#sideMenu ul li a
  {
  border-bottom: 1px solid _BLUEGREY;
  color: _MEDIUMBLUE;
  display: block;
  letter-spacing: 0.02em;
  margin: 0;
  padding: 4px;
  padding-_DIR_LEFT: 15px;
  padding-_DIR_RIGHT: 5px;
  }

#sideMenu ul li a:hover
  {
  background-color: _LIGHTERBLUE;
  color: _BLUE;
  text-decoration: none;
  }

#sideMenu ul li.current a
  {
  background-color: _GREYBLUE;
  color: _BLUE;
  font-weight: bold;
  }



/*
 * Inner Tab Navigation Styles (Nav Level 3)
 */

#sideMenu ul li.sub,
#sideMenu ul li.currentsub
  {
  background-color: _GREYBLUE;
  }

#sideMenu ul li.sub a,
#sideMenu ul li.currentsub a
  {
  background: #fbfcfd;
  border-bottom: 1px solid _BLUEGREY;
  padding: 2px;
  padding-_DIR_LEFT: 32px;
  padding-_DIR_RIGHT: 10px;
  }

#sideMenu ul li.sub a:hover,
#sideMenu ul li.currentsub a:hover
  {
  background: _LIGHTESTBLUE url(_CSSPICSDIR/arrow.gif) no-repeat _DIR_LEFT center;
  }

#sideMenu ul li.currentsub a,
#sideMenu ul li.currentsub a:hover
  {
  background: _LIGHTESTBLUE url(_CSSPICSDIR/arrow.gif) no-repeat _DIR_LEFT center;
  font-weight: bold;
  }



/*
 * Inner Tab Navigation Styles (Nav Level 4)
 */

#tabLinkBar
  {
  margin: 10px 0 5px;
  }

#tabLinkBar div.line
  {
  border-top: 1px solid _DARKESTGREY;
  margin-top: -1px;
  }

#tabLinkBar ul
  {
  list-style: none;
  margin: 0;
  padding: 0;
  }

#tabLinkBar ul li
  {
  background: _LIGHTERBLUE url(_CSSPICSDIR/spacer.gif);
  border: 1px solid _DARKESTGREY;
  border-radius: 4px 4px 0 0;
  -moz-border-radius: 4px 4px 0 0;
  -webkit-border-top-left-radius: 4px;
  -webkit-border-top-right-radius: 4px;
  float: _DIR_LEFT;
  margin: 0;
  margin-_DIR_RIGHT: 3px;
  padding: 0;
  white-space: nowrap;
  }

#tabLinkBar ul li a,
#tabLinkBar ul li input
  {
  background: transparent;
  border: none;
  color: _DARKESTBLUE;
  cursor: pointer;
  display: block;
  font-size: _FONT_SMALLER;
  height: 1.9em;
  line-height: 1.9em;
  margin: 0;
  padding: 0 0.4em;
  }

#tabLinkBar ul li input
  {
  overflow: visible;
  }

#tabLinkBar ul li.current
  {
  background-color: _WHITE;
  border-bottom-color: _WHITE;
  }

#tabLinkBar ul li.current a,
#tabLinkBar ul li.current input
  {
  font-weight: bold;
  }

#tabLinkBar ul li.hover,
#tabLinkBar ul li:hover
  {
  background-color: _WHITE;
  }

#tabLinkBar ul li a:hover,
#tabLinkBar ul li input:hover,
#tabLinkBar ul li.current a,
#tabLinkBar ul li.current input
  {
  color: _BLUE;
  text-decoration: none;
  }

#tabLinkBar ul li a.warn,
#tabLinkBar ul li a.warn:hover,
#tabLinkBar ul li.current a.warn,
#tabLinkBar ul li input.warn,
#tabLinkBar ul li input.warn:hover,
#tabLinkBar ul li.current input.warn
  {
  background: transparent url(_CSSPICSDIR/icon_alert_small.png) no-repeat _DIR_LEFT;
  padding-_DIR_LEFT: 17px;
  }

#tabLinkBar ul li a.warn,
#tabLinkBar ul li input.warn
  {
  margin-left: 4px;
  }



/*
 * Inner Sub-Menu (to the tab bar) Styles (Nav Level 5)
 */

#tabLinkSub
  {
  margin: 5px 0 0;
  padding-_DIR_LEFT: 5px;
  }

#tabLinkSub ul
  {
  border: 1px solid _DARKESTGREY;
  margin-bottom: 5px;
  margin-_DIR_LEFT: -5px;
  margin-_DIR_RIGHT: 0;
  margin-top: -6px;
  padding: 0 2px;
  }

#tabLinkSub ul li
  {
  display: inline;
  list-style-type: none;
  padding: 0;
  white-space: nowrap;
  }

#tabLinkSub ul li a,
#tabLinkSub ul li input
  {
  background: transparent;
  border: none;
  color: _DARKBLUE;
  height: 1.9em;
  line-height: 1.9em;
  padding: 0;
  padding-bottom: 2px;
  padding-_DIR_LEFT: 5px;
  }

#tabLinkSub ul li a
  {
  font-size: _FONT_SMALLER;
  margin: 2px 8px;
  margin-_DIR_LEFT: 0;
  }

#tabLinkSub ul li a:hover,
#tabLinkSub ul li input:hover
  {
  color: _BLACK;
  text-decoration: none;
  }

#tabLinkSub ul li.current a,
#tabLinkSub ul li.current a:hover,
#tabLinkSub ul li.current input,
#tabLinkSub ul li.current input:hover
  {
  color: _DARKBLUE;
  font-weight: bold;
  }

#tabLinkSub ul li input
  {
  cursor: pointer;
  margin: 0;
  }



/*
 * Main Menu Styles
 */

#mainMenu div.col
  {
  margin-bottom: 20px;
  margin-_DIR_LEFT: 0;
  margin-_DIR_RIGHT: 30px;
  margin-top: 10px;
  width: 29%;
  float: _DIR_LEFT;
  }

#mainMenu div.col ul
  {
  margin: 0;
  margin-_DIR_LEFT: 43px;
  margin-top: 8px;
  padding: 0;
  list-style: none;
  }

#mainMenu div.col ul li
  {
  margin: 0;
  padding: 4px 0;
  }

#mainMenu div.col div.header
  {
  background: #e9e9eb url(_CSSPICSDIR/header_gradient.gif) repeat-x;
  border-bottom: 1px solid _BORDERGREY;
  border-top: 1px solid _BORDERGREY;
  color: _BLACK;
  font-weight: bold;
  line-height: 19px;
  padding: 3px 5px;
  vertical-align: bottom;
  white-space: nowrap;
  }

#mainMenu div.col div.header img
  {
  margin: 0 3px;
  }



/*
 * Simple Grid Table Styles
 */

table.gridTable
  {
  border-_DIR_LEFT: 1px solid _DARKERGREY;
  border-top: 1px solid _DARKERGREY;
  padding: 0;
  margin: 0;
  width: 100%;
  }

table.gridTable tr td
  {
  border-bottom: 1px solid _DARKERGREY;
  border-_DIR_RIGHT: 1px solid _DARKERGREY;
  padding: 3px;
  text-align: _DIR_LEFT;
  vertical-align: top;
  }

table.gridTable tr td.header
  {
  background-color: _LIGHTESTGREY;
  width: 140px;
  }



/*
 * Outer Table Styles
 */

table.outer
  {
  background-color: _LIGHTESTBLUE;
  margin-top: 3px;
  width: 100%;
  }

table.outer > tbody > tr > th
  {
  background: _TABLE_OUTER_HEADER_BG;
  color: _BLUE;
  font-weight: bold;
  color: _WHITE;
  height: 25px;
  padding: 0 8px;
  }

table.outer > tbody > tr > td
  {
  padding: 5px 9px;
  }



/*
 * Border & List Table Styles
 */

table.border,
table.list
  {
  background-color: _ROWEVEN;
  border: 1px solid _DARKERGREY;
  cursor: default;
  margin: 4px 0 0;
  width: 100%;
  }

table.noBorder
  {
  border: none;
  margin: 0;
  padding: 0;
  }

table.border tr th,
table.list tr th
  {
  background: _TABLE_HEADER_BG;
  border-bottom: 1px solid _BORDERGREY;
  color: _MEDIUMBLUE;
  font-weight: bold;
  line-height: 1.5em;
  padding: 2px 6px;
  }

table.border table.list tr td,
table.list tr td
  {
  border-top: 1px solid _BORDERGREY;
  line-height: 1.6em;
  padding-bottom: 3px;
  padding-_DIR_LEFT: 6px;
  padding-_DIR_RIGHT: 4px;
  padding-top: 2px;
  }

table.border tr td,
table.list table.border tr td
  {
  padding: 4px;
  padding-_DIR_LEFT: 6px;
  padding-top: 2px;
  line-height: 1.5em;
  }

table.list tr th
  {
  border: 0;
  }

table.border tr td input.checkbox,
table.border tr td input.radio
  {
  margin-top: 4px;
  }

table.border > tbody > tr > td > input.text,
table.border > tbody > tr > td > table.noPad > tbody > tr > td > input.text
  {
  width: 75%;
  }

table.list > tbody > tr > td > input.text
  {
  width: 90%;
  }

table.list tr th.group
  {
  font-size: _FONT_SMALLER;
  font-style: italic;
  width: 40px;
  }

table.list tr td input.image
  {
  margin: 0;
  margin-_DIR_RIGHT: 3px;
  margin-top: 3px;
  vertical-align: top;
  }

/* Used to correct the position of oversized buttons */
table.list tr td input.large
  {
  margin: 1px 0;
  }

table.list tr td img
  {
  margin: 0;
  margin-_DIR_RIGHT: 3px;
  margin-top: 2px;
  vertical-align: top;
  }

table.list tr td input.checkbox,
table.list tr td input.radio
  {
  vertical-align: top;
  margin-top: 4px;
  }

table.border tr.foot td,
table.list tr.foot td
  {
  background: _TABLE_FOOTER_BG;
  border-top: 1px solid _BORDERGREY;
  height: 2em;
  padding: 1px 5px;
  text-align: center;
  white-space: nowrap;
  }

table.list > tbody > tr.foot > td > div.overlap
  {
  height: 2em;
  position: relative;
  }

table.list > tbody > tr.foot > td > div.overlap > div.links,
table.list > tbody > tr.foot > td > div.overlap > div.buttons
  {
  height: 2em;
  _DIR_LEFT: 0;
  position: absolute;
  top: 0;
  }

table.list > tbody > tr.foot > td > div.overlap > div.buttons
  {
  text-align: center;
  width: 100%;
  }

table.list > tbody > tr.foot > td > div.overlap > div.links
  {
  text-align: _DIR_LEFT;
  }

table.list > tbody > tr.foot > td > div.overlap > div.links > a
  {
  line-height: 1.9em;
  margin-_DIR_LEFT: 6px;
  }

table.list > tbody > tr.foot > td > div.overlap > div.links > a > img
  {
  margin: 0;
  vertical-align: text-bottom;
  }

table.list tr.noBorder td,
table.list tr.noBorder td table.list tr.noBorder td
  {
  border: 0;
  }

table.list > tbody > tr.even > td,
table.list > tbody > tr > td.even
  {
  background-color: _ROWEVEN;
  }

table.list > tbody > tr.odd > td,
table.list > tbody > tr > td.odd
  {
  background-color: _ROWODD;
  }

table.list tr.noBorder td table.list tr td
  {
  border-top: 1px solid _BORDERGREY;
  }

table.list > tbody > tr.small > td
  {
  padding-top: 1px;
  }

table.list > tbody > tr.small > td > input
  {
  margin-top: 2px;
  }

table.list tr.small td input.large
  {
  margin-top: 0;
  }

table.border > tbody > tr.subHead > th,
table.list > tbody > tr.subHead > th,
table.border > thead > tr.subHead > th,
table.list > thead > tr.subHead > th
  {
  border-bottom: 0;
  border-top: 1px solid _BORDERGREY;
  background: _LIGHTERBLUE;
  font-weight: bold;
  line-height: 1.5em;
  }

table.border > tbody > tr:first-child.subHead > th,
table.list > tbody > tr:first-child > td,
table.list > tbody > tr:first-child.subHead > th
  {
  border-top: none;
  }

table.join > tbody > tr:first-child.subHead > th
  {
  border-top: 1px solid _BORDERGREY;
  }

table.border > tbody > tr > td.prompt
  {
  line-height: 1.5em;
  text-align: _DIR_RIGHT;
  width: 50%;
  }

table.border.join
  {
  border-top: 0;
  margin-top: 0;
  }

table.border td.required,
table.list td.required
  {
  color: _RED;
  font-weight: bold;
  line-height: 1.5em;
  white-space: nowrap;
  width: 1%;
  }

table.list > tbody > tr > td.light
  {
  background-color: _LIGHTESTBLUE;
  }



/*
 * No Padding Table Styles
 */

table.noPad
  {
  width: 100%;
  }

table.noPad,
table.noPad > tbody > tr > td
  {
  background: transparent;
  border: none;
  margin: 0;
  padding: 0;
  }



/*
 * Reports Table Styles
 */

div.reportTablesWrap
  {
  margin: 4px 0;
  }

table.reportWidth
  {
  width: 100%;
  max-width: 1200px;
  }

table.reportWidth td.prompt
  {
  padding-_DIR_RIGHT: 8px;
  }

table.border > tbody > tr > td > input.reportRadioStringWidth
  {
  width: 20em;
  }



/*
 * Virtual Table Styles
 */

div.row
  {
  position: relative;
  }

div.row div.rightCol
  {
  line-height: normal;
  position: absolute;
  _DIR_RIGHT: 0;
  top: 0;
  }



/*
 * Main Menu Table Styles
 */

table.mainMenu
  {
  width: 100%;
  }

table.mainMenu tr td table
  {
  margin-bottom: 20px;
  }

table.mainMenu tr td table tr th
  {
  background: _TABLE_HEADER_BG;
  border-bottom: 1px solid _BORDERGREY;
  border-top: 1px solid _BORDERGREY;
  color: _BLACK;
  font-weight: bold;
  line-height: 1.75em;
  padding: 3px 5px;
  white-space: nowrap;
  }

table.mainMenu tr td table tr td
  {
  padding: 0;
  padding-_DIR_LEFT: 5px;
  padding-top: 8px;
  }



/*
 *  Table Separator Styles
 */
table tr td.sep,
table tr th.sep,
table.outer > tbody > tr > td.sep,
table.outer > tbody > tr > th.sep
  {
  border-_DIR_LEFT: 1px solid _WHITE;
  font-size: 1px;
  margin: 0;
  padding: 0;
  width: 1px;
  }



/*
 * Dashboard Styles
 */

table.dashboard
  {
  border: 1px solid _DARKERGREY;
  margin-top: 20px;
  }

table.dashboard tr td
  {
  padding: 5px 10px;
  }

table.dashboard div.header,
table.dashboard table.statsTable tr td
  {
  font-size: _FONT_SMALLER;
  line-height: 1.2em;
  }

table.dashboard select
  {
  font-size: 0.95em;
  padding: 0;
  }

table.dashboard table
  {
  background: none;
  border: 1px solid _BORDERGREY;
  margin-bottom: 5px;
  width: 100%;
  }

table.dashboard table tr td
  {
  padding: 4px;
  }

table.dashboard table tr
  {
  background-color: _ROWEVEN;
  }

table.dashboard table tr.odd
  {
  background-color: _ROWODD;
  }

table.dashboard table tr td.num
  {
  text-align: _DIR_RIGHT;
  white-space: nowrap;
  width: 5%;
  }

table.dashboard div.header
  {
  font-weight: bold;
  line-height: 2em;
  }

table.dashboard table tr.header
  {
  background-image: url(_CSSPICSDIR/header_dotline.gif);
  background-position: bottom _DIR_LEFT;
  background-repeat: repeat-x;
  font-weight: bold;
  }

table.dashboard table tr.total
  {
  background-image: url(_CSSPICSDIR/header_dotline.gif);
  background-repeat: repeat-x;
  font-weight: bold;
  }



/*
 * Special overrides for above styles
 */

input.integer,
table.border > tbody > tr > td > input.integer,
table.list > tbody > tr > td > input.integer
  {
  width: 6em;
  }



/*
 * Footer styles
 */


#footer
  {
  clear: both;
  color: _DARKERGREY;
  font-size: _FONT_SMALLER;
  padding: 6px 0;
  position: relative;
  height: 50px;
  }

#footerCompany,
#footerCopyright,
#footerVersion
  {
  position: absolute;
  width: 33%;
  }

#footerCompany
  {
  left: 33%;
  width: 34%;
  text-align: center;
  }

#footerCopyright
  {
  _DIR_RIGHT: 0;
  text-align: _DIR_RIGHT;
  }

#footerCopyright div
  {
  float: _DIR_RIGHT;
  }

#footer a,
#footer a:hover,
#footer a:visited
  {
  color: _DARKERGREY;
  }

#footer sup
  {
  font: normal 0.5em _FONT_FAMILY;
  }



/*
 * License Styles
 */

.license
  {
  font-weight: bold;
  margin-bottom: 25px;
  }

.installed
  {
  color: _GREEN;
  }

.licensed
  {
  color: _BLUE;
  }

.noLicense
  {
  color: _DARKGREY;
  }



/*
 * Notice Styles
 */

div.noticeBox
  {
  background: _LIGHTESTBLUE;
  border-bottom: 1px solid _DARKERGREY;
  border-top: 1px solid _DARKERGREY;
  margin: 5px 0;
  padding-bottom: 3px;
  padding-top: 3px;
  width: 100%;
  }

#content div.noticeBox
  {
  margin-bottom: 20px;
  }

div.noticeBox table tr td.icon
  {
  vertical-align: top;
  width: 30px;
  background-image: url(_CSSPICSDIR/notice_dotline.gif);
  background-repeat: repeat-y;
  background-position: _DIR_RIGHT;
  padding: 1px 5px;
  }

div.noticeBox table tr td.body
  {
  padding-_DIR_LEFT: 5px;
  color: _NOTICEGREY;
  width: 99%;
  }

div.noticeBox table tr td.body .license
  {
  margin: 0;
  }

div.noticeBox table tr td.body div.entry
  {
  margin: 6px 0;
  }

div.noticeBox table tr td.icon img
  {
  margin: 5px;
  }

div.error,
span.error
  {
  color: _RED;
  font-weight: bold;
  }

div.result,
span.result
  {
  color: _NOTICEGREY;
  font-weight: bold;
  }

div.result li,
div.span li,
p.result li.serviceOn
  {
  margin: 0;
  }

div.warning,
span.warning
  {
  color: _NOTICEGREY;
  font-weight: bold;
  }



/*
 * OrgChart Styles
 */
div.orgEntry
  {
  clear: both;
  margin-bottom: 4px;
  }
  
div.orgEntry > img
  {
  margin-_DIR_RIGHT: 3px;
  }



/*
 * Service Styles
 */

.serviceOn
  {
  color: _GREEN;
  }

.serviceOff
  {
  color: _RED;
  }

.serviceStarting
  {
  color: _ORANGE;
  }

.serviceStopping
  {
  color: _MAROON;
  }

.serviceNotInstalled
  {
  color: _BLUE;
  }



/*
 * Sorting Styles
 */

input.sortIcon
  {
  vertical-align: middle;
  }



/*
 * Status Styles
 */

.statusSuccess
  {
  color: _BLACK;
  }

.statusFailure
  {
  color: _RED;
  }

.statusInProgress
  {
  color: _DARKGREY;
  }

.statusWaiting
  {
  color: _DARKGREY;
  }



/*
 * DateTime Widget Styles
 */

.dateWidget
  {
  white-space: nowrap;
  }

.dateWidget div
  {
  float: _DIR_LEFT;
  }

.dateWidget .date input.text,
.dateWidget .time input.text
  {
  text-align: center;
  }

.dateWidget .date input.text
  {
  background: url(_CSSPICSDIR/field_date.png) top _DIR_LEFT no-repeat;
  padding-_DIR_LEFT: 20px;
  width: 6em;
  }

.dateWidget .date input.text:focus,
.dateWidget .date input.focus
  {
  background-image: url(_CSSPICSDIR/field_date_focus.png);
  }

.dateWidget .date input.error,
.dateWidget .date input.error:focus
  {
  background-image: url(_CSSPICSDIR/field_date_error.png);
  }

.dateWidget .time input.text
  {
  background: url(_CSSPICSDIR/field_time.png) top _DIR_LEFT no-repeat;
  padding-_DIR_LEFT: 20px;
  width: 5em;
  }

.dateWidget .time input.text:focus,
.dateWidget .time input.focus
  {
  background-image: url(_CSSPICSDIR/field_time_focus.png);
  }

.dateWidget .time input.error,
.dateWidget .time input.error:focus
  {
  background-image: url(_CSSPICSDIR/field_time_error.png);
  }



/*
 * Search & Paging Widget Styles
 */
div.searchPaging
  {
  margin-top: 2px;
  }

div.searchPaging + table.border,
div.searchPaging + table.list
  {
  margin-top: 2px;
  }

div.searchPaging table
  {
  empty-cells: hide;
  margin: 0;
  padding: 0;
  }

div.searchPaging td
  {
  height: 25px;
  margin: 0;
  padding: 0;
  vertical-align: middle;
  }

div.search
  {
  float: _DIR_LEFT;
  }

div.search td
  {
  padding-top: 2px;
  vertical-align: top;
  }

div.search select,
div.search input
  {
  margin-top: 0;
  }

div.search td.searchPrompt
  {
  padding-right: 5px;
  }

div.search td.searchHistory select.history
  {
  width: 90px;
  margin-_DIR_LEFT: 0;
  margin-_DIR_RIGHT: 5px;
  }

div.search input.searchBox
  {
  margin-_DIR_LEFT: 0;
  padding-_DIR_RIGHT: 20px;
  width: 10em;
  }

input.search
  {
  cursor: pointer;
  height: 14px;
  margin-bottom: 0;
  margin-_DIR_LEFT: -24px;
  margin-_DIR_RIGHT: 6px;
  margin-top: 2px;
  padding: 2px 2px 1px;
  width: 14px;
  z-index: 50;
  position: relative;
  }

div.search input.search
  {
  margin-top: 0;
  vertical-align: middle;
  }

div.search div.searchPrompt
  {
  font-weight: bold;
  padding-top: 3px;
  }

div.paging
  {
  float: _DIR_RIGHT;
  white-space: nowrap;
  }

div.paging input.text
  {
  text-align: center;
  width: 2em;
  }

div.paging input.miniblue
  {
  margin-_DIR_RIGHT: 10px;
  }

/* Adjust vertical position of input image and img tags as they do not naturally align */
div.paging img,
div.paging input.image
  {
  height: 12px;
  margin: 0;
  padding: 0;
  vertical-align: middle;
  width: 12px;
  }

div.paging .arrowFirst,
div.paging .arrowLast,
div.paging .arrowNext,
div.paging .arrowPrev
  {
  background: url(_CSSPICSDIR/arrow_first.gif) top _DIR_LEFT no-repeat;
  }

div.paging .arrowLast
  {
  background-image: url(_CSSPICSDIR/arrow_last.gif);
  }

div.paging .arrowNext
  {
  background-image: url(_CSSPICSDIR/arrow_next.gif);
  }

div.paging .arrowPrev
  {
  background-image: url(_CSSPICSDIR/arrow_prev.gif);
  }

div.paging img.arrowFirst,
div.paging img.arrowLast,
div.paging img.arrowNext,
div.paging img.arrowPrev
  {
  background-position: bottom _DIR_LEFT;
  }



/*
 * Highlight Word Custom Styles
 */

span.textMatchValue
  {
  color: #333;
  }

span.textMatchValue b
  {
  color: _BLACK;
  }



/*
 * Load without JS System Script
 */
div.jsWarning
  {
  background-color: _NOTE;
  border: 1px solid _BLACK;
  cursor: pointer;
  font-size: _FONT_SMALLER;
  _DIR_LEFT: 50%;
  margin-_DIR_LEFT: -60px;
  padding: 3px 8px;
  position: absolute;
  text-align: center;
  top: 100px;
  white-space: normal;
  width: 300px;
  z-index: 80;
  }



/*
 * AutoCompletePslang Script Styles
 * (note always LTR as content always English)
 */

textarea.pslComplete
  {
  direction: ltr;
  text-align: left;
  }

div.pslCompleteDisable
  {
  font-size: 0.8em;
  line-height: 1em;
  text-align: _DIR_RIGHT;
  }

div.pslCompleteDisable a
  {
  outline: 0;
  }

div.pslCompleteFunctions,
div.pslCompleteProducts,
div.pslCompleteSections,
div.pslCompleteVariables
  {
  display: none;
  }

div.pslAutoCompleter
  {
  background: _MENU_SIDE_BG;
  border: 1px solid _DARKERGREY;
  cursor: default;
  direction: ltr;
  margin: 0 10px 10px 0;
  min-width: 200px;
  overflow-y: auto;
  position: fixed;
  text-align: left;
  }

div.pslAutoCompleter ul
  {
  font-size: _FONT_SMALLER;
  list-style: none;
  margin: 0;
  padding: 0;
  }

div.pslAutoCompleter ul li
  {
  border-top: 1px solid _BLUEGREY;
  display: block;
  padding: 0 16px 0 6px;
  }

div.pslAutoCompleter ul li:first-child
  {
  border-top: none;
  }

div.pslAutoCompleter ul li.selected
  {
  background-color: _GREYBLUE;
  }

div.pslAutoCompleter ul li span.match
  {
  font-weight: bold;
  }

div.pslAutoCompleter ul li span.type
  {
  color: #666;
  }

div.pslAutoCompleter ul li span.variable
  {
  color: _BLACK;
  }

div.pslAutoCompleter ul li span.help
  {
  color: _DARKBLUE;
  cursor: pointer;
  font-weight: bold;
  padding: 0 2px;
  position: absolute;
  right: 0;
  }

div.pslAutoCompleter ul li span.help:hover
  {
  color: _BLACK;
  }

div.pslAutoCompleter ul li.divider
  {
  border-bottom: 1px solid _DARKERGREY;
  border-top: none;
  height: 0;
  overflow: hidden;
  }

div.pslAutoCompleter > ul > li.pslFullRef
  {
  padding: 10px;
  }

div.pslAutoCompleter > ul > li.pslFullRef > div
  {
  border: 1px solid #666;
  padding: 0 0 2px 0;
  }

div.pslAutoCompleter > ul > li.pslFullRef > div > div
  {
  padding: 0 5px;
  }

div.pslAutoCompleter > ul > li.pslFullRef > div > div.title > div.close
  {
  color: _WHITE;
  cursor: pointer;
  float: right;
  font-weight: normal;
  }

div.pslAutoCompleter > ul > li.pslFullRef > div > div.title > div.close:hover
  {
  color: _LIGHTERBLUE;
  }

div.pslAutoCompleter > ul > li.pslFullRef > div > div.title
  {
  background: url(_CSSPICSDIR/header_bg.gif) 0 -1px repeat-x;
  color: _WHITE;
  font-weight: bold;
  padding-top: 2px;
  }

div.pslAutoCompleter > ul > li.pslFullRef > div > div.section
  {
  font-weight: bold;
  }

div.pslAutoCompleter > ul > li.pslFullRef > div > div.argument,
div.pslAutoCompleter > ul > li.pslFullRef > div > div.return
  {
  margin-left: 10px;
  }



/*
 * AutoGrowField Script Styles
 */

textarea.original,
textarea.translated
  {
  height: 15px;
  overflow: hidden;
  }

div.autoGrowMetric
  {
  /* Left positioning breaks RTL languages on IE7 */
  position: absolute;
  top: -10000px;
  }



/*
 * CheckAll Script Styles
 */
th.checkAll
  {
  white-space: nowrap;
  }



/*
 * CollapseSection Script Styles
 */

img.collapseSectionIcon
  {
  cursor: pointer;
  display: block;
  float: _DIR_LEFT;
  margin: 0;
  margin-_DIR_RIGHT: 4px;
  margin-top: 3px;
  }



/*
 * IgnoreDoubleClick Script Styles
 */

#overlay
  {
  bottom: 0;
  cursor: wait;
  _DIR_LEFT: 0;
  position: fixed;
  _DIR_RIGHT: 0;
  top: 0;
  z-index: 1000;
  }



/*
 * DisplayNote Script Styles
 */

div.popupNote
  {
  display: none;
  }

.floatingNote
  {
  background-color: _NOTE;
  border: 1px solid _BLACK;
  color: _BLACK;
  font-size: _FONT_SMALLER;
  font-style: normal;
  font-weight: normal;
  line-height: 16px;
  opacity: 0.93;
  padding: 3px 8px;
  text-align: _DIR_LEFT;
  white-space: normal;
  z-index: 80;
  }

.floatingNote ul
  {
  padding-_DIR_LEFT: 20px;
  }

.floatingNote ul li
  {
  list-style-type: disc;
  }

.floatingNoteFrame {
  border: 0;
  position: absolute;
  z-index: 70;
  overflow: hidden;
  }



/*
 * PluginCtrl Widget Styles
 */

div.pluginCtrl
  {
  float: _DIR_LEFT;
  height: 22px;
  }

div.pluginCtrl div
  {
  float: _DIR_LEFT;
  margin-_DIR_RIGHT: 10px;
  }

div.pluginCtrl .miniblue
  {
  color: _WHITE;
  cursor: pointer;
  line-height: 1em;
  padding: 3px 5px 0;
  white-space: nowrap;
  }

div.pluginCtrl div.viewPassword
  {
  margin-top: 1px;
  }

div.pluginCtrl div.viewPassword div.pass
  {
  cursor: default;
  }

div.pluginCtrl div.viewPassword div.frame
  {
  border-bottom: 1px solid _WHITE;
  border-_DIR_LEFT: 1px solid #aca899;
  border-_DIR_RIGHT: 1px solid _WHITE;
  border-top: 1px solid #aca899;
  height: 11px;
  margin-top: 4px;
  position: relative;
  width: 120px;
  }

div.pluginCtrl div.viewPassword div.border
  {
  background-color: #ece9d8;
  border: 1px solid _BLACK;
  height: 9px;
  _DIR_LEFT: 0;
  position: absolute;
  top: -4px;
  width: 118px;
  }

div.pluginCtrl div.viewPassword div.inner
  {
  border: 1px solid _WHITE;
  font-size: 0.1em;
  line-height: 0.1em;
  height: 7px;
  _DIR_LEFT: 0;
  position: absolute;
  top: 0;
  width: 116px;
  }

div.pluginCtrl div.viewPassword div.bar
  {
  background-color: #316ac5;
  font-size: 1px;
  height: 7px;
  _DIR_LEFT: 0;
  line-height: 1px;
  position: absolute;
  top: 0;
  width: 116px;
  }

div.hidePass
  {
  margin-top: 2px;
  }

div.hidePass a
  {
  padding-top: 2px;
  }

div.hidePass a, div.hidePass a:visited, div.hidePass:active
  {
  color: _WHITE;
  cursor: default;
  text-decoration: none;
  }

div.hidePass a:hover
  {
  color: black;
  }

div.hidePass div.button,
div.hidePass a span.button
  {
  cursor: default;
  }

div.hidePass a span.button
  {
  margin-top: -2px;
  margin-_DIR_RIGHT: 5px;
  }

div.pluginCtrl div.notice
  {
  margin-top: 2px;
  }

table.list tr td.recentPluginList
  {
  padding: 0;
  }

td.recentPluginList table
  {
  background: transparent;
  border: 0;
  margin: 0 3px;
  }

td.recentPluginList table tr td
  {
  background: transparent;
  font-size: _FONT_SMALLER;
  padding: 2px;
  padding-top: 3px;
  }

td.recentPluginList > table > tbody > tr:first-child > td
  {
  border-top: none;
  }

td.recentPluginList table td.desc
  {
  padding-top: 4px;
  }



/*
 * PopupCalendar Script Styles
 */

.calendarFrame
  {
  border: 0;
  position: absolute;
  z-index: 70;
  }

div.popupCalendar
  {
  position: absolute;
  z-index: 80;
  }

div.popupCalendar table
  {
  border: 1px solid _DARKERGREY;
  border-collapse: separate;
  direction: ltr;
  }

div.popupCalendar table tr th,
div.popupCalendar table tr td
  {
  border-left: 1px solid _WHITE;
  border-top: 1px solid _WHITE;
  font-size: _FONT_SMALLER;
  font-weight: bold;
  line-height: normal;
  padding: 1px 2px;
  text-align: center;
  _DIR
  }

div.popupCalendar .lastCol
  {
  border-right: 1px solid _WHITE;
  }

div.popupCalendar .lastRow
  {
  border-bottom: 1px solid _WHITE;
  }

div.popupCalendar table tr th
  {
  background: url(_CSSPICSDIR/nav_bg.gif) 0 -2px repeat-x;
  border-bottom: 0;
  color: _WHITE;
  }

div.popupCalendar table tr td.day,
div.popupCalendar table tr th.changeMonth
  {
  cursor: pointer;
  }

div.popupCalendar table tr th.changeMonth.over
  {
  background: _LIGHTBLUE;
  }

div.popupCalendar table tr th.changeMonth.disabled
  {
  color: #7da1d0;
  cursor: default;
  }

div.popupCalendar table tr td.monthName,
div.popupCalendar table tr td.dayName
  {
  cursor: default;
  }

div.popupCalendar table tr td.monthName
  {
  font-weight: normal;
  }

div.popupCalendar table tr td.day
  {
  color: #314b72;
  background-color: _LIGHTESTBLUE;
  }

div.popupCalendar table tr td.day.weekend
  {
  background-color: _LIGHTERBLUE;
  }

div.popupCalendar table tr td.day.disabled
  {
  color: _DARKESTGREY;
  cursor: default;
  font-weight: normal;
  }

div.popupCalendar table tr td.day.faded,
div.popupCalendar table tr td.prevMonth,
div.popupCalendar table tr td.nextMonth,
div.popupCalendar table tr td.weekend.prevMonth,
div.popupCalendar table tr td.weekend.nextMonth
  {
  background-color: _WHITE;
  color: _LIGHTGREY;
  cursor: default;
  font-weight: normal;
  }

div.popupCalendar table tr td.day.selected
  {
  color: _WHITE;
  background-color: _SELECTGREEN;
  }

div.popupCalendar table tr td.day.over
  {
  color: _WHITE;
  background-color: _LIGHTBLUE;
  }

div.popupCalendar table tr td.day.today
  {
  border: 1px solid _BLUE;
  }



/*
 * PopupWindow Script Styles
 */

.popupWindow
  {
  background: _WHITE;
  border: 1px solid #333;
  position: fixed;
  _DIR_LEFT: 50px;
  top: 50px;
  z-index: 105;
  }

.popupWindowLoading
  {
  background: _WHITE;
  border: 1px solid #333;
  color: _MEDIUMBLUE;
  font-size: _FONT_LARGER;
  font-weight: bold;
  height: 50px;
  _DIR_LEFT: 50%;
  margin-_DIR_LEFT: -75px;
  margin-top: -25px;
  padding: 20px 0;
  position: fixed;
  text-align: center;
  top: 25%;
  width: 150px;
  z-index: 101;
  }

.popupWindowLoading img
  {
  margin-bottom: 10px;
  }

.popupWindowOverlay
  {
  background: _BLACK;
  opacity: 0.3;
  z-index: 100;
  }



/*
 * ResizeText Script Styles
 */

div.fontSizeControls
  {
  padding-_DIR_LEFT: 10px;
  padding-_DIR_RIGHT: 10px;
  }

div.fontSizeControls span
  {
  cursor: pointer;
  height: 1em;
  padding: 0 3px;
  }

div.fontSizeControls span.decrease
  {
  font-size: 0.7em;
  }

div.fontSizeControls span.reset
  {
  font-size: 0.85em;
  }

div.fontSizeControls span.disabled
  {
  color: _LIGHTERGREY;
  cursor: default;
  }



/*
 * RowSelection Script Styles
 */

table.list > tbody > tr.selectable > td
  {
  background-color: _ROWEVEN;
  cursor: pointer;
  }

table.list > tbody > tr.selectable:hover > td
  {
  background-color: _ROWHOVER;
  }

table.list > tbody > tr.selectable.click > td,
table.list > tbody > tr.selectable.select.click > td
  {
  background-color: _SELECTBLUE;
  }

table.list > tbody > tr.checkable > td,
table.list > tbody > tr.checkable > td > label
  {
  cursor: pointer;
  }

table.list > tbody > tr.selectable > td.selectDisabled,
table.list > tbody > tr.checkable > td.checkDisabled
  {
  cursor: default;
  }



/*
 * Selected Row Styles
 * Note: Must come after RowSelection Script Styles for proper inheritance
 *       and inheritance.
 */

table.border > tbody > tr.select > td,
table.list > tbody > tr.select > td,
table.border > tbody > tr.select > td.arrow,
table.list > tbody > tr.select > td.arrow
  {
  background-color: _SELECTGREEN;
  color: _WHITE;
  font-weight: bold;
  }

table.border > tbody > tr.select > td.arrow,
table.list > tbody > tr.select > td.arrow
  {
  background-image: url(_CSSPICSDIR/arrow_bg.png);
  background-position: _DIR_RIGHT center;
  background-repeat: no-repeat;
  }

table.list > tbody > tr.select.selectable:hover > td
  {
  background-color: _SELECTGREENHOVER;
  }



/*
 * ScrollableObject Script Styles
 */

div.border
  {
  border: 1px solid _DARKERGREY;
  margin: 4px 0;
  }

div.scrollable
  {
  border: 1px solid _DARKERGREY;
  margin: 4px 0;
  overflow-x: auto;
  overflow-y: hidden;
  padding: 0;
  }



/*
 * Translatable Script Styles
 */

.translatable
  {
  border: 1px solid transparent !important;
  cursor: pointer;
  }

.transHover
  {
  background-color: #ffc9c9 !important;
  border: 1px solid red !important;
  color: _BLACK !important;
  }

.transAltHover
  {
  background-color: #ffde9d !important;
  border: 1px solid orange !important;
  color: _BLACK !important;
  }


dnl CUSTOMIZATIONS: End copy
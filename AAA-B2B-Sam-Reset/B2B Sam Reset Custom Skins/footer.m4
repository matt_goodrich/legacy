dnl This is an example customization file
dnl You can use this file to modify the page footer of most pages.
dnl Simply copy this file to 
dnl    <install dir>\design\custom\footer.m4
dnl modify the HTML code, and run make.bat to regenerate the skin files.

dnl Do not modify this line!
define(`CUSTOM_FOOTER')

dnl Copy the marked section from <install dir>\design\src\common\footer.m4 here
dnl and modify as needed.

dnl CUSTOMIZATIONS: Start copy
                  <input type="hidden" name="SETTABLEFIELDSDB" value="%SETTABLEFIELDSDB%" />
                  <input type="hidden" name="SETTABLEFIELDSCOOKIE" value="%SETTABLEFIELDSCOOKIE%" />
                  ifelse(MENUBUTTON, yes, `
                    <div class="loginSwitch"><a class="SMALLTEXT" href="_HOME_URL?lang=_LANGUAGE">_PSF_LOGIN_TITLE</a></div>',
                  `')
              </div><!-- end #content -->
            </td><!-- end #mainCol -->
          </tr>
        </tbody>
      </table><!-- end #colWrap -->

      <div id="footer">
        <div id="footerCompany">
          &copy; 2011 AAA Northern California, Nevada &amp; Utah Insurance Exchange, All rights reserved.
          ifelse(PSAMODULE,`yes',`&nbsp;(%LOGGEDON_SERVER%)', `')
        </div>
      </div>

    </div><!-- end #container -->
  </div><!-- end #containerWrap -->
</form>
BODY_END
</html>
dnl CUSTOMIZATIONS: End copy
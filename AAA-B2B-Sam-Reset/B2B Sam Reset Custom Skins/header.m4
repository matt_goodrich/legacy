dnl This is an example customization file
dnl You can use this file to modify the page header of most pages.
dnl Simply copy this file to 
dnl    <install dir>\design\custom\header.m4
dnl modify the HTML code, and run make.bat to regenerate the skin files

dnl Do not modify this line!
define(`CUSTOM_HEADER')

dnl Copy marked section lines from <install dir>\design\src\common\header.m4 here
dnl and modify as needed.

dnl CUSTOMIZATIONS: Start copy
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
HEAD_BEGIN
<title>_SUITE: TEXT_TITLE</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta http-equiv="cache-control" content="private,no-cache,no-store,maxage=0,s-maxage=0,must-revalidate,proxy-revalidate,no-transform" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
%META_REDIRECT%
<script type="text/javascript">
// <![CDATA[
    // Array of fields to process and attempt to focus on page load
    document.focusFields = [
      ifdef(`ACTIVATE_FIELD', `"ACTIVATE_FIELD", ')
      ifdef(`ALTERNATE_ACTIVATE_FIELD', `"ALTERNATE_ACTIVATE_FIELD", ')
      null
    ];
// ]]>
</script>
<link rel="stylesheet" type="text/css" media="all" href="_CSSDIR/style.css" />
<!--[if lte IE 8]><link rel="stylesheet" type="text/css" media="all" href="_CSSDIR/style-ie8.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" media="all" href="_CSSDIR/style-ie7.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" media="all" href="_CSSDIR/style-ie6.css" /><![endif]-->
<script id="_TRANSID:%LANG%:%DISABLEJS%" src="_JSDIR/core.js" type="text/javascript"></script>
HEAD_END

BODY_BEGIN(%MAIN_CONTENT_WIDTH%)
  ifelse(PUSH_ERR,yes,<div id="loadingMessage">%ERROR%</div>,`')
<a name="top"></a>
<form name="theform" method="post" action="%PROGRAM%" autocomplete="off">
  <div id="containerWrap">
    <div id="container">
      <span id="topMargin">
        <input id="TRANSACTION" name="TRANSACTION" type="hidden" value="_TRANSID" />
        <input id="SESSDATA" name="SESSDATA" type="hidden" value="%SESSDATA%" />
        <input id="LANG" name="LANG" type="hidden" value="%LANG%" />
        <input disabled="disabled" id="currentDateTime" type="hidden" value="%CURRENTDATETIME%" />
        <input alt="" class="hideIfJs" id="DEFAULT_PAGE_ACTION" name="DEFAULT_UNUSED_BUTTON.x" src="_PICSDIR/spacer.gif" style="border: none; height: 1px; width: 1px;" tabindex="1000" title="" type="submit" />
        &nbsp;
        <span id="skipNav">
          <a href="#topMenu">skip to main navigation</a> |
          <a href="#sideMenu">skip to side navigation</a> |
          <a href="#content">skip to main content</a>
        </span><!-- end #skipNav -->
      </span><!-- end #topMargin -->
      <div id="header">
        <div id="companyLogo"><img src="_PICSDIR/SamReset.png" width="180" height="120" alt="SAM Reset" /></div>
        <div id="topButtons">
          <ul>
            ifelse(NO_BACK,`yes',<li class="inactive">DEADLINK(`_BUTTON_BACK_ALT',BACK,back)</li>,<li>STATICLINK(`_BUTTON_BACK_ALT',BACK,back)</li>)
            ifelse(HOMEBUTTON,`no',<li class="inactive">DEADLINK(`_BUTTON_HOME_ALT',HOME,home)</li>,<li>STATICLINK(`_BUTTON_HOME_ALT',HOME,home)</li>)
            ifelse(LOGOUTBUTTON,`no',<li class="inactive">DEADLINK(`_BUTTON_REFRESH_ALT',REFRESH,refresh)</li>,
                    ifelse(HOMEBUTTON,`no',
                          ifelse(_TRANSID,`F_OPTION',<li>STATICLINK(`_BUTTON_REFRESH_ALT',REFRESH,refresh)</li>,<li class="inactive">DEADLINK(`_BUTTON_REFRESH_ALT',REFRESH,refresh)</li>),
                          ifelse(NO_REFRESH,`yes',<li class="inactive">DEADLINK(`_BUTTON_REFRESH_ALT',REFRESH,refresh)</li>,<li>STATICLINK(`_BUTTON_REFRESH_ALT',REFRESH,refresh)</li>)))
            ifelse(LOGOUTBUTTON,`no',<li class="inactive">DEADLINK(`_BUTTON_LOGOUT_ALT',LOGOUT,logout)</li>,<li>STATICLINK(`_BUTTON_LOGOUT_ALT',LOGOUT,logout)</li>)
          </ul>
        </div><!-- end #topButtons -->
        <div class="clearFloat"></div>
      </div><!-- end #header -->
      <div id="topMenuBar">
        %TOPMENU%
        <div class="clearFloat"></div>
      </div><!-- end #topMenuBar -->

      <table cellspacing="0" id="colWrap">
        <tbody>
          <tr>
            <td id="menuCol">
              <div id="menuContent">
                %SIDEMENU%
              </div><!-- end #menuContent -->
            </td><!-- end #menuCol -->

            <td id="mainCol">
              <div id="content">
                <div id="userInfo">
                  ifelse(NO_USER,yes,`&nbsp;',`DISPLAY_USERID %LOGGEDIN_USERID% %DISPLAY_LOGGEDIN_USERNAME%')
                </div>
                ifelse(TABMENU,`yes',`
                <h1>
                  TEXT_TITLE
                  <span class="selectedObj"> %SELECTEDOBJECT% </span>
                </h1>
                ')
                %TABS%
                %TABSUBS%

                ifelse(TABMENU,`yes',`',`
                <h1>
                  TEXT_TITLE
                  <span class="selectedObj"> %SELECTEDOBJECT% </span>
                </h1>
                ')
dnl CUSTOMIZATIONS: End copy
# This file demonstrates the use of e-mail to deliver a temporary PIN to 
# end-user for logging on IDMSuite.
#
# To use this script: 
# 
# 1) Create an API user and configure the $API_USERID and $API_USERPWD 
#    accordingly.
#
# 2) Append the contents of samples\authemailpin.m4 to 
#    design\custom\authchain.m4 and rebuild IDMSuite user interface 
#    skins.
#
# 3) Create an user profile attribute for holding user e-mail and  
#    configure $EMAIL_PROFILE_ATTRIBUTE accordingly. 
#    
# 4) Use the PSF_FORCE_ENROLLMENT system variable to ensure user fill 
#    their e-mail information upon first time login.
#
# 5) Use this script as an external authentication plug-in and configure 
#    a custom authentication chain to allow user to use e-mail PIN for 
#    logging on IDMSuite.
#
include "idapi.psl"

###
# Configuration variables begin.
###

var $PIN_LENGTH=4;
var $PIN_MAXLIFE=600; # 10 minute of PIN life time.
var $PIN_FIELD_DESC="!!!AUTHEMAILPIN_FIELD_DESC";
var $PIN_FIELD="_PIN";
var $API_USERID="_API_USER";
var $API_USERPWD="C@llfor1t";
var $EMAIL_PROFILE_ATTRIBUTE="EMAIL";

# Be sure to fill the following variables with correct information.
var $IDMSUITE_SERVER = "smtp.csaa.com";
var $IDMSUITE_SENDER = "IDManager@goaaa.com"
var $IDMSUITE_SUBJECT = "Your temporary PIN"
var $IDMSUITE_INSTDIR = "C:\\Program Files (x86)\\Hitachi ID\\IDM Suite\\B2BDEV";

###
# Configurable variables end.
###

var $PSLSTORE="EMAIL_PIN"
var $SUCCESS=0;
var $FAILURE=-1;
var $TRUE=1;
var $FALSE=-1;
var $DIALOG_INVALID_ATTRS=-2;
var $DIALOG_PIN_SENT=-3;

function createPIN (const $userid, output $pin, output $errmsg) {

  $pin="";

  var $apihandle;
  var $ret;
  $ret = APIInit( "localhost", "3", $apihandle );
  if( $ret != 0 ) {
    log("Failed to initiate API");
    return $FAILURE; 
  }

  struct Login_scalarInput $inLogin;

  $inLogin.$userid = $API_USERID;
  $inLogin.$password = $API_USERPWD;
  $inLogin.$isadmin = 1;

  $ret = APIExecute( $apihandle, "Login", $inLogin, $errmsg );
  if( $ret != 0 ) { 
    log("API Login failure:  " + $errmsg + ".");
    return $ret; 
  }

  var $login_ok = 0;
  if( APIExecClose( $apihandle ) == 0 ) { $login_ok = 1; }

  var $rand;
  var $retval;
  var $neednewpin=0;

  if( $login_ok ) {

    struct PSLStoreGet_scalarInput $inPSLStoreGet;
    struct PSLStoreGet_vectorOutput $outPSLStoreGet;
    $inPSLStoreGet.$nameSpace = $PSLSTORE;
    $inPSLStoreGet.$inputArray[0].$key=$userid;
    $ret = APIExecute( $apihandle, "PSLStoreGet", $inPSLStoreGet,
                       $outPSLStoreGet, $errmsg );
    APIExecClose( $apihandle );
    if( $ret != 0 ) { $neednewpin=$TRUE; }
    else {
      var $fmt_err;
      var $pin_age;
      var $old_pin;
      $old_pin=substr($outPSLStoreGet.$value, 0, $PIN_LENGTH-1 );
      $pin_age=strtol(substr($outPSLStoreGet.$value, $PIN_LENGTH+1,
                              strlen($outPSLStoreGet.$value)-1 ), 
                      $fmt_err, 10);
      if( $fmt_err!=0 ) { return $FAILURE; } 
      $pin_age = ( time() - $pin_age );
      # log("PIN retrived: "+$old_pin + "["+$pin_age+"]");
      if( $pin_age > $PIN_MAXLIFE ) { $neednewpin=$TRUE; }
      else {$pin = $old_pin;}
    }

		#generate new PIN
    if($neednewpin==$TRUE) {
  	  $rand = rand() + "";
      while(strlen($rand) < $PIN_LENGTH) {
        $rand = $rand + rand() ;
      }
      $rand = substr($rand, 0, $PIN_LENGTH-1);

      struct PSLStoreSet_scalarInput $inPSLStoreSet;
      $inPSLStoreSet.$nameSpace = $PSLSTORE;
      $inPSLStoreSet.$inputArray[0].$key=$userid;
      $inPSLStoreSet.$inputArray[0].$value=$rand+":"+time();
      $ret = APIExecute( $apihandle, "PSLStoreSet", $inPSLStoreSet,
                         $errmsg );
      APIExecClose( $apihandle );
      if( $ret != 0 ) {
        return $FAILURE;
      }
      $pin=$rand;
      # log("New PIN: "+$pin);
    }
  }
  return $SUCCESS;
}	


function verifyPIN (const $userid, const $verify_pin, output $outerr) {

  var $apihandle;
  var $ret;
  $ret = APIInit( "localhost", "3", $apihandle );
  if( $ret != 0 ) {
    log("Failed to initiate API");
    return $FAILURE; 
  }

  struct Login_scalarInput $inLogin;

  $inLogin.$userid = $API_USERID;
  $inLogin.$password = $API_USERPWD;
  $inLogin.$isadmin = 1;

  var $errmsg;

  $ret = APIExecute( $apihandle, "Login", $inLogin, $errmsg );
  if( $ret != 0 ) { 
    log("API Login failure:  " + $errmsg + ".");
    return $ret; 
  }

  var $login_ok = 0;
  if( APIExecClose( $apihandle ) == 0 ) { $login_ok = 1; }

  var $retval;
  var $pin;
  var $pin_age;
  if( $login_ok ) {
		# retrive the PIN

    struct PSLStoreGet_scalarInput $inPSLStoreGet;
    struct PSLStoreGet_vectorOutput $outPSLStoreGet;
    $inPSLStoreGet.$nameSpace = $PSLSTORE;
    $inPSLStoreGet.$inputArray[0].$key=$userid;
    $ret = APIExecute( $apihandle, "PSLStoreGet", $inPSLStoreGet,
                       $outPSLStoreGet, $errmsg );
    APIExecClose( $apihandle );
    if( $ret != 0 ) { 
      $outerr="Failed to retrive PIN";
      return $FAILURE; 
    } else {
      var $fmt_err;
      $pin=substr($outPSLStoreGet.$value, 0, $PIN_LENGTH-1);      
      $pin_age=strtol(substr($outPSLStoreGet.$value, $PIN_LENGTH+1,
                              strlen($outPSLStoreGet.$value)-1 ), 
                      $fmt_err, 10);
      if( $fmt_err!=0 ) { 
        $outerr="Invalid PIN read";
        return $FAILURE;  
      } 
      $pin_age = ( time() - $pin_age );
      # log("PIN retrived: "+$pin+"["+$pin_age+"]");
    }

    # Uncomment following line to enforce a more strict PIN lifetime 
    # policy.
    # if( $pin_age > $PIN_MAXLIFE ) { 
    #   $outerr="PIN already expired";
    #   return $FAILURE; 
    # }

    if( $pin != $verify_pin ) { 
      $outerr = "Incorrect PIN.";
      return $FAILURE; 
    }

    # PIN has been used, remove it immediately.
    struct PSLStoreRemove_scalarInput $inPSLStoreRemove;
    $inPSLStoreRemove.$nameSpace = $PSLSTORE;
    $inPSLStoreRemove.$inputArray[0].$key=$outPSLStoreGet.$key;
    $inPSLStoreRemove.$inputArray[0].$value=$outPSLStoreGet.$value;
    $ret = APIExecute( $apihandle, "PSLStoreRemove", $inPSLStoreRemove,
                       $errmsg );
    if($ret!=0) {
      log($errmsg);
    }
    APIExecClose( $apihandle );
  }
  return $SUCCESS;
}	

function sendPIN(const $email_addr, const $pin, output $errmsg)
  {
  var $args[];
  var $tmp[];
  var $cmdretval;
  var $retval=0;

  $args[0] = "-server";
  $args[1] = $IDMSUITE_SERVER;
  $args[2] = "-to";
  $args[3] = $email_addr;
  $args[4] = "-from";
  $args[5] = $IDMSUITE_SENDER;
  $args[6] = "-subject";
  $args[7] = $IDMSUITE_SUBJECT;
  $args[8] = "-content";
  $args[9] = $pin;

  #log("PIN Sent: "+$pin);

  ##
  # Deliver temporary PIN to user's mailbox. 
  ##
  $retval = system($IDMSUITE_INSTDIR + "\\util\\psmail.exe",$args,"",$tmp,$cmdretval,5);

  if ($retval != 0) {
    $errmsg="Failed to send PIN to "+ $email_addr;
    if(strlen($tmp)>0) {$errmsg=$errmsg+" ["+$tmp+"]"; }
    log($errmsg);
    return $FAILURE;
  } else {
    log("Sent message to: " + $args[3]);
    return $SUCCESS;
  }
  return $SUCCESS;
}

function buildDialog(const $dialog_type) {

  var $kvgDialog = kvgCreate("dialog", "");
  if($dialog_type==$DIALOG_INVALID_ATTRS) {

    # User doesn't have a cellphone.
    kvgAddValue($kvgDialog, "id", "authemailpin_no_email");

  } elsif($dialog_type==$DIALOG_PIN_SENT) {
    kvgAddValue($kvgDialog, "id", "authemailpin" );
    kvgAddValue($kvgDialog, "title", "!!!AUTHEMAILPIN_TITLE");
    kvgAddValue($kvgDialog, "subtitle", "!!!AUTHEMAILPIN_SUBTITLE");

    var $kvgEntry = kvgCreate("entry", "");
    kvgAddValue($kvgEntry,"description", $PIN_FIELD_DESC);
    kvgAddValue($kvgEntry, "name", $PIN_FIELD); 
    kvgAddValue($kvgEntry, "type", "PASSWORD");

    kvgAddGroup($kvgDialog, $kvgEntry);
  }
  return $kvgDialog;
}

function main (const $argv) {

  log( "Authentication plugin for E-Mail PIN start." ); 

  var $readOk = 0;
  var $errmsg="";
  var $inkvg = kvgCreateFromHandle( $stdin, 10, $readOk );
  var $outkvg = kvgCreate( "", "" );

  if( $readOk != 0 ) {
    kvgAddValue($outkvg, "errmsg", "Failed to read input");
    kvgAddValue($outkvg, "retval", "1");
    kvgToHandle( $outkvg, $stdout, 0 );
    return $FAILURE;
  }

  # acquiring user inputs.
  var $params;
  $readOk = kvgGetGroup($inkvg, "parameters", "", $params);
  if( $readOk != 1 ) {
    kvgAddValue($outkvg, "errmsg", "Failed to acquire parameters from input");
    kvgAddValue($outkvg, "retval", "2");
    kvgToHandle( $outkvg, $stdout, 0 );
    return $FAILURE;
  } else {
    log ("parameters=" + kvgToString($params));
  }

  # acquiring user e-mail. 
  var $viewer;
  var $userid = "";
  var $email_addr = "";
  var $email_pin = "";

  $readOk = kvgGetGroup($inkvg, "viewer", "user", $viewer);
  if( $readOk != 1 ) {
    kvgAddValue($outkvg, "errmsg", "Failed to acquire user information.");
    kvgAddValue($outkvg, "retval", "2");
    kvgToHandle( $outkvg, $stdout, 0 );
    return $FAILURE;
  } else {
    log ("viewer=" + kvgToString($viewer));
    kvgGetValue($viewer, "id", $userid);
    var $kvgemail;
    var $kvgvalue;
    if(kvgGetGroup($viewer,"attribute",$EMAIL_PROFILE_ATTRIBUTE,$kvgemail)==1) {
      if(kvgGetGroup($kvgemail, "value", "", $kvgvalue)==1) {
        kvgGetValue($kvgvalue, "value", $email_addr);
      }
    }
  }

  $outkvg=kvgCreate("", "");
  if( $email_addr=="" ) {

    ###
    # user doesn't have a valid e-mail address.
    ###
    kvgAddGroup($outkvg, buildDialog($DIALOG_INVALID_ATTRS));
    kvgAddValue($outkvg, "retval", "0");
    kvgAddValue($outkvg, "status", "NEED_TOKENS");

  } elsif(kvgGetValue($params, $PIN_FIELD, $email_pin)!=1) {

    ###
    # User request a e-mail PIN for login.
    ### 
    if(createPIN($userid, $email_pin, $errmsg)==$SUCCESS) {
      if(sendPIN($email_addr, $email_pin, $errmsg)==$SUCCESS) {
        kvgAddGroup($outkvg, buildDialog($DIALOG_PIN_SENT));
        kvgAddValue($outkvg, "retval", "0");
        kvgAddValue($outkvg, "status", "NEED_TOKENS");

      } else { # failed to sent e-mail
        kvgAddValue($outkvg, "errmsg", $errmsg);
        kvgAddValue($outkvg, "retval", "1");
        kvgAddValue($outkvg, "status", "SYSTEM_ERROR");
      }
    } else { # failed to generate new PIN
      kvgAddValue($outkvg, "errmsg", $errmsg);
      kvgAddValue($outkvg, "retval", "1");
      kvgAddValue($outkvg, "status", "SYSTEM_ERROR");
    }

  } else {

    ###
    # Verify PIN received by user.
    ###
    if(strlen($email_pin)==0) { # User didn't enter anything.
      kvgAddGroup($outkvg, buildDialog($DIALOG_PIN_SENT));
      kvgAddValue($outkvg, "retval", "0");
      kvgAddValue($outkvg, "status", "NEED_TOKENS");
    } else {
      if(verifyPIN($userid, $email_pin, $errmsg)==$SUCCESS) {

        # Login success 
        kvgAddValue($outkvg, "retval", "0");
        kvgAddValue($outkvg, "status", "SUCCESS");
      
      } else {

        # Login failure
        kvgAddValue($outkvg, "retval", "0");
        kvgAddValue($outkvg, "errmsg", "!!!ERROR_INCORRECT_PASSWORD");
        kvgAddValue($outkvg, "status", "FAILED");

        # IMPORTANT:
        # be sure to clear the input field so that user may have a fresh 
        # start if the configuration allow them to retry. 
        var $parameters=kvgCreate("parameters","");
        kvgAddValue($parameters, $PIN_FIELD, "");
        kvgAddGroup($outkvg, $parameters);
      }
    }
  }

  kvgToHandle( $outkvg, $stdout, 0 );

  log( "Authentication plugin for E-Mail PIN complete." ); 
}

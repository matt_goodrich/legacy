﻿using System;
using System.DirectoryServices;

namespace ADManager
{
    internal class LDAP
    {
        public static string host;
        public static string port;
        public static string rootPath;

        public static bool Exists(string ldapPath)
        {
            bool found = false;
            if (DirectoryEntry.Exists("LDAP://" + ldapPath))
            {
                found = true;
            }
            return found;
        }
    }

    class ADManager
    {
        private UserManagement um;
        private OUManagement om;
        private GroupManagement gm;

        public ADManager(string host, string port, string rootPath)
        {
            LDAP.host = host;
            LDAP.port = port;
            LDAP.rootPath = rootPath;
            //Initialize();

            um = new UserManagement();
            om = new OUManagement();
            gm = new GroupManagement();
        }

        public void CreateUserAccount(string ldapPath, string userName, string userPassword)
        {
            um.CreateUserAccount(ldapPath, userName, userPassword);
        }

        public void SetUserPassword(string ldapPath, string userName, string password)
        {
            um.SetUserPassword(ldapPath, userName, password);
        }

        public void AddUserToGroup(string userDn, string groupDn)
        {
            um.AddUserToGroup(userDn, groupDn);
        }

        public void AddOU(string name, string description, string ldapPath)
        {
            om.AddOU(name, description, ldapPath);
        }

        public void CreateSecurityGroup(string ldapPath, string name)
        {
            gm.CreateSecurityGroup(ldapPath, name);
        }
    }

    class OUManagement
    {
        public void AddOU(string name, string description, string ldapPath)
        {
            DirectoryEntry objADAM;
            DirectoryEntry objOU;    // Organizational unit.
            string strDescription;   // Description of OU.
            string strOU;            // Organiztional unit.           

            // Specify Organizational Unit.
            strOU = "OU=" + name;
            strDescription = description;
            Console.WriteLine("Create:  {0}", strOU);

            ldapPath = (String.IsNullOrEmpty(ldapPath) ? "" : ldapPath + ",");

            // Create Organizational Unit.
            try
            {
                if (!DirectoryEntry.Exists(String.Concat("LDAP://", LDAP.host, ":", LDAP.port, "/OU=", name, ",", ldapPath, LDAP.rootPath)))
                {
                    objADAM = new DirectoryEntry(String.Concat("LDAP://", LDAP.host, ":", LDAP.port, "/", ldapPath, LDAP.rootPath));
                    objOU = objADAM.Children.Add(strOU, "OrganizationalUnit");
                    objOU.Properties["description"].Add(strDescription);
                    objOU.CommitChanges();

                    // Output Organizational Unit attributes.
                    Console.WriteLine("Success: Create succeeded.");
                    Console.WriteLine("Name:\t\t{0}", objOU.Name);
                    Console.WriteLine("Desc:\t\t{0}", objOU.Properties["description"].Value);
                    return;
                }

                // Output Organizational Unit attributes.
                Console.WriteLine("Success: OU Already Exists.");
                Console.WriteLine("Name:\t\t{0}", strOU);
                Console.WriteLine("Desc:\t\t{0}", strDescription);
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: Create failed.");
                Console.WriteLine("Message:\t\t{0}", e.Message);
                return;
            }
        }
    }

    class GroupManagement
    {
        public void CreateSecurityGroup(string ldapPath, string name)
        {
            if (!DirectoryEntry.Exists(String.Concat("LDAP://", LDAP.host, ":", LDAP.port, "/CN=", name, ",", ldapPath, ",", LDAP.rootPath)))
            {
                try
                {
                    DirectoryEntry entry = new DirectoryEntry(String.Concat("LDAP://", LDAP.host, ":", LDAP.port, "/", ldapPath, ",", LDAP.rootPath));
                    DirectoryEntry group = entry.Children.Add("CN=" + name, "group");
                    //group.Properties["sAmAccountName"].Value = name;
                    group.CommitChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error: Create Security Group failed.");
                    Console.WriteLine("Message:\t\t{0}", e.Message);
                }
            }
            else
            {
                Console.WriteLine("Success: Security Group already exists.");
            }
        }
    }

    class UserManagement
    {
        public void CreateUserAccount(string ldapPath, string userName, string userPassword)
        {
            string oGUID = string.Empty;
            try
            {
                DirectoryEntry dirEntry = new DirectoryEntry(String.Concat("LDAP://", LDAP.host, ":", LDAP.port, "/", ldapPath, ",", LDAP.rootPath));
                //Console.WriteLine("BIND:" + String.Concat("LDAP://", LDAP.host, ":", LDAP.port, "/", ldapPath, ",", LDAP.rootPath));
                DirectoryEntry newUser = dirEntry.Children.Add("CN=" + userName, "user");
                newUser.Properties["userPrincipalName"].Value = userName;
                newUser.CommitChanges();
                oGUID = newUser.Guid.ToString();

                Console.WriteLine("Success: Create User succeeded.");
                Console.WriteLine("Name:\t\t{0}", userName);
                Console.WriteLine("GUID:\t\t{0}", oGUID);

                SetUserPassword(ldapPath, userName, userPassword);
                newUser.Properties["msDS-UserAccountDisabled"].Value = false;
                newUser.CommitChanges();

                dirEntry.Close();
                newUser.Close();
            }
            catch (System.DirectoryServices.DirectoryServicesCOMException e)
            {
                Console.WriteLine("Error: Create User failed.");
                Console.WriteLine("Message:\t\t{0}", e.Message);
            }
        }

        public void SetUserPassword(string ldapPath, string userName, string password)
        {
            const long ADS_OPTION_PASSWORD_PORTNUMBER = 6;
            const long ADS_OPTION_PASSWORD_METHOD = 7;

            //const int ADS_PASSWORD_ENCODE_REQUIRE_SSL = 0;
            const int ADS_PASSWORD_ENCODE_CLEAR = 1;

            AuthenticationTypes AuthTypes;  // Authentication flags.
            int intPort;                    // Port for instance.
            DirectoryEntry objUser;         // User object.
            string strPath;                 // Binding path.

            strPath = String.Concat("LDAP://", LDAP.host, ":", LDAP.port, "/CN=", userName, ",", ldapPath, ",", LDAP.rootPath);
            //Console.WriteLine("Bind to: {0}", strPath);

            // Set authentication flags.
            // For non-secure connection, use LDAP port and
            //  ADS_USE_SIGNING |
            //  ADS_USE_SEALING |
            //  ADS_SECURE_AUTHENTICATION
            // For secure connection, use SSL port and
            //  ADS_USE_SSL | ADS_SECURE_AUTHENTICATION
            AuthTypes = AuthenticationTypes.Signing |
                AuthenticationTypes.Sealing |
                AuthenticationTypes.Secure;

            // Bind to user object using LDAP port.
            try
            {
                objUser = new DirectoryEntry(strPath, null, null, AuthTypes);
                objUser.RefreshCache();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: Bind failed.");
                Console.WriteLine("Message:\t\t{0}.", e.Message);
                return;
            }

            // Set port number, method, and password.
            intPort = Int32.Parse(LDAP.port);
            try
            {
                objUser.Invoke("SetOption", new object[] { ADS_OPTION_PASSWORD_PORTNUMBER, intPort });
                objUser.Invoke("SetOption", new object[] {ADS_OPTION_PASSWORD_METHOD, ADS_PASSWORD_ENCODE_CLEAR});
                objUser.Invoke("SetPassword", new object[] { password });
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: Set password failed.");
                Console.WriteLine("Message:\t\t{0}.", e.Message);
                return;
            }

            Console.WriteLine("Success: Password set.");
            return;
        }

        public void AddUserToGroup(string userDn, string groupDn)
        {
            try
            {
                DirectoryEntry dirEntry = new DirectoryEntry(String.Concat("LDAP://", LDAP.host, ":", LDAP.port, "/", groupDn , ",", LDAP.rootPath));
                dirEntry.Properties["member"].Add(String.Concat(userDn, ",", LDAP.rootPath));
                dirEntry.CommitChanges();
                dirEntry.Close();
            }
            catch (System.DirectoryServices.DirectoryServicesCOMException E)
            {
                Console.WriteLine("Error: Add User to Group failed.");
                Console.WriteLine("Message:\t\t{0}", E.Message);
                return;

            }
        }
    }
}

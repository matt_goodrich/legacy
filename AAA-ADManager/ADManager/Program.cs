﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADManager
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;

            ADManager ADM = new ADManager("localhost", "389", "o=Seros,c=US");

            ADM.AddOU("CSAA", "CSAA", "");
            ADM.AddOU("Security Groups", "Security Groups", "ou=CSAA");
            ADM.AddOU("Distribution Lists", "Distribution Lists", "ou=CSAA");

            ADM.AddOU("Active Directory Services", "Active Directory Services", "");
            ADM.AddOU("Application Security Groups", "Application Security Groups", "ou=Active Directory Services");

            ADM.AddOU("Partner Clubs", "Partner Clubs", "");
            ADM.AddOU("ACP", "ACP", "ou=Partner Clubs");
            ADM.AddOU("AZ", "Arizona",  "ou=ACP,ou=Partner Clubs");
            ADM.AddOU("CO", "Colorado", "ou=ACP,ou=Partner Clubs");
            ADM.AddOU("IN", "Indiana",  "ou=ACP,ou=Partner Clubs");
            ADM.AddOU("MAIG", "MAIG", "ou=Partner Clubs");
            ADM.AddOU("MD", "Maryland", "ou=MAIG,ou=Partner Clubs");
            ADM.AddOU("NJ", "New Jersey", "ou=MAIG,ou=Partner Clubs");
            ADM.AddOU("NY", "New York", "ou=MAIG,ou=Partner Clubs");

            //Create Security Groups
            ADM.CreateSecurityGroup("OU=Security Groups,OU=CSAA", "Service Accounts");
            
            //Setup our LDAP Service Account
            ADM.AddOU("Users", "Misc Users", "");
            ADM.CreateUserAccount("ou=Users", "LDAP_svc", "LD4PSvC");
            ADM.AddUserToGroup("cn=LDAP_svc,ou=Users", "cn=Service Accounts,OU=Security Groups,OU=CSAA");

            //Create Test Users
            ADM.CreateUserAccount("ou=AZ,ou=ACP,ou=Partner Clubs", "MattAZ", "tr!qwSSe4tgewch899");
            ADM.CreateUserAccount("ou=CO,ou=ACP,ou=Partner Clubs", "MattCO", "tr!qwSSe4tgewch899");
            ADM.CreateUserAccount("ou=IN,ou=ACP,ou=Partner Clubs", "MattIN", "tr!qwSSe4tgewch899");
            ADM.CreateUserAccount("ou=MD,ou=MAIG,ou=Partner Clubs", "MattMD", "tr!qwSSe4tgewch899");
            ADM.CreateUserAccount("ou=NJ,ou=MAIG,ou=Partner Clubs", "MattNJ", "tr!qwSSe4tgewch899");
            ADM.CreateUserAccount("ou=NY,ou=MAIG,ou=Partner Clubs", "MattNY", "tr!qwSSe4tgewch899");

            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            Environment.Exit(0);
        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            Console.WriteLine(e.ExceptionObject.ToString());
            Console.WriteLine("Press Enter to continue");
            Console.ReadLine();
            Environment.Exit(1);
        }

    }
}

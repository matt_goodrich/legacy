﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace DavitaEncryptionTest
{
    class Program
    {
        public static void Main()
        {
            try
            {
                // Just hard coded values for testing ...
                // MUST change them to match the values used in the CF code
                String thePlainData = "Nothing to see here folks";
                String theKey = "g4Bs2KYJ6+bF0PhK7QQyBw==";
                String theIV = "0000000000000000";
                String encryptedText = "PEh5SrbtXr+jkPRkkq0SYx46ELVYyHrQgZwngfnmrNbwIptBNlr08qe+etEzKqG7IGcD3i4TUBV2phvQ4d5b8Q==";
                String decryptedText = DecryptText(encryptedText, theKey, theIV);

                Console.WriteLine("Encrypted String: {0}", encryptedText);
                Console.WriteLine("Decrypted String: {0}", decryptedText);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }


        public static String EncryptText(String Data, String Key, String IV)
        {
            // Extract the bytes of each of the values
            byte[] input = Encoding.UTF8.GetBytes(Data);
            byte[] key = Convert.FromBase64String(Key);
            byte[] iv = Convert.FromBase64String(IV);


            // Create a new instance of the algorithm with the desired settings
            RijndaelManaged algorithm = new RijndaelManaged();
            algorithm.Mode = CipherMode.CBC;
            algorithm.Padding = PaddingMode.PKCS7;
            algorithm.BlockSize = 128;
            algorithm.KeySize = 128;
            algorithm.Key = key;
            algorithm.IV = iv;

            // Create a new encryptor and encrypt the given value
            ICryptoTransform cipher = algorithm.CreateEncryptor();
            byte[] output = cipher.TransformFinalBlock(input, 0, input.Length);

            // Finally, return the encrypted value in base64 format
            String encrypted = Convert.ToBase64String(output);

            return encrypted;
        }

        public static String DecryptText(String Data, String Key, String IV)
        {
            // Extract the bytes of each of the values
            byte[] input = Convert.FromBase64String(Data);
            byte[] key = Convert.FromBase64String(Key);
            byte[] iv = Convert.FromBase64String(IV);

            Console.WriteLine("here");

            // Create a new instance of the algorithm with the desired settings
            RijndaelManaged algorithm = new RijndaelManaged();
            algorithm.Mode = CipherMode.ECB;
            algorithm.Padding = PaddingMode.PKCS7;
            algorithm.BlockSize = 128;
            algorithm.KeySize = 128;
            algorithm.Key = key;
            //algorithm.IV = iv;

            Console.WriteLine("there");
            //FromBase64String
            // Create a new encryptor and encrypt the given value
            ICryptoTransform cipher = algorithm.CreateDecryptor();
            byte[] output = cipher.TransformFinalBlock(input, 0, input.Length);

            // Finally, convert the decrypted value to UTF8 format
            String decrypted = Encoding.UTF8.GetString(output);

            return decrypted;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

public partial class Error : System.Web.UI.Page
{
    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

    protected void Page_Load(object sender, EventArgs e)
    {
        Exception LastError = Server.GetLastError();
        if (LastError != null)
            ServerLog.Error("Error Screen Rendered: Last Exception:", LastError);
        else
            ServerLog.Debug("Error Screen Rendered");
    }
}
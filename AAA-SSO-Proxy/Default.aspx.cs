﻿/*******************************************************************************
* Name
*   Proxy Page Code Behind
*
* Identifier
*   Filename: Default.aspx.cs
*   
* Purpose
*   This class contains the methods to provide forms based authentication
*   or IWA based on a header value passed from the Threat Management Gateway
*
******************************************************************************
*/

//Microsoft Libraries
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.IO;
using log4net;

public partial class _Default : System.Web.UI.Page
{
    //Log
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

    /// <summary>
    /// This function is run on load of the page including post backs. 
    /// This method will check for an appropriate querystring parameter and
    /// check of the Reverse-Via field is populated. If it is populated
    /// the desired behavior is Forms Authentication as the user is coming
    /// from an external IP address, if not, redirect to the SSO URL passed
    /// in.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        //Check for the SSO URL as a querystring parameter, this will be in the format of https://qa-sso.tent.trt.csaa.pri/idp/startSSO.ping?<paramters>
        if (Request.QueryString["SSOURL"] != null)
	    {
            //Save the SSO URL
		    string SSOURL = Request.Url.ToString().Substring(Request.Url.ToString().IndexOf("?SSOURL=")+8);

            ServerLog.Debug("SSO URL Found: " + SSOURL);

            //Check for the Reverse-Via header
		    if (Request.Headers["Reverse-Via"] != null)
		    {
                ServerLog.Debug("Reverse-Via Populated with Value: " + Request.Headers["Reverse-Via"]);
                if (Request.Headers["Reverse-Via"].ToString() == "proxy")
                {
                    //It exists, split off all parameters and save them so we can manipulate and rebuild
                    Dictionary<string, string> URLparams = new Dictionary<string, string>();
                    string[] key = SSOURL.Substring(SSOURL.IndexOf('?')).Split('&');

                    //Assuming we parsed correctly
                    if (key.Length > 0)
                    {
                        //Iterate the array and add it to our Dictionary
                        for (int i = 0; i < key.Length; i++)
                        {
                            URLparams.Add(key[i].Substring(0, key[i].IndexOf('=')).Replace("?", ""), key[i].Substring(key[i].IndexOf('=') + 1));
                        }

                        //Build the new querystring
                        string newQueryString = "?";
                        foreach (KeyValuePair<string, string> kvp in URLparams)
                        {
                            //Change the IdPAdapterId to Forms since the header value exists otherwise add the normal key and value back to the URL
                            newQueryString += kvp.Key + "=" + (kvp.Key.Equals("IdpAdapterId") ? "IdPAdapterFORMB2B" : kvp.Value) + "&";
                        }

                        //Remove the trailing ampersand
                        newQueryString = newQueryString.Substring(0, newQueryString.Length - 1);

                        //Redirect to the newly built URL
                        ServerLog.Debug("Redirecting User To: " + SSOURL.Substring(0, SSOURL.IndexOf("?")) + newQueryString);
                        Response.Redirect(SSOURL.Substring(0, SSOURL.IndexOf("?")) + newQueryString);
                    }
                }
                else
                {
                    ServerLog.Debug("Reverse-Via Header Does Not Match \"proxy\"");
                    ServerLog.Debug("Redirecting To: " + SSOURL);
                    Response.Redirect(SSOURL);
                }
		    }
		    else
		    {
                ServerLog.Debug("No Reverse-Via Header Set");
                ServerLog.Debug("Redirecting To: " + SSOURL);
			    Response.Redirect(SSOURL);
		    }
	    }
	    else
	    {
            ServerLog.Debug("No Querystring Provided");
		    Response.Write("<br />ERROR! No Querystring provided.");
	    }
    }
}





﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

using log4net;

public partial class Loading : System.Web.UI.Page
{
    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

    private static string redirect = "";

    #region PreInit
    /// <summary>
    /// This function is the first method to be executed during the page load.
    /// In order to apply the correct theme before the page is rendered, we must
    /// apply the theme early in the page lifecycle. 
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Check if we have a Theme name saved in our session variables. Do further checking if so, apply the default if not
        if (Session["Theme"] != null)
        {
            ServerLog.Debug("Found theme in Session. Attempting to find theme: " + Session["Theme"]);
            //Check if the theme exists on the server. If so, apply the theme, if not apply the default.
            if (Directory.Exists(Server.MapPath("~/App_Themes/" + Session["Theme"])))
            {
                Page.Theme = Session["Theme"].ToString();
                ServerLog.Debug("Theme " + Page.Theme + " exists. Setting theme");
            }
            else
            {
                ServerLog.Debug("Theme " + Session["Theme"] + " not found in App_Themes. Setting default theme.");
                Page.Theme = "Default";
            }
        }
        else if (Request["PartnerSpId"] != null)
        {
            ServerLog.Debug("Found theme in URL. Attempting to find theme: " + Request["PartnerSpId"]);
            if (Directory.Exists(Server.MapPath("~/App_Themes/" + Request["PartnerSpId"].ToString().Replace(":", ""))))
            {
                Page.Theme = Request["PartnerSpId"].ToString().Replace(":", "");
                ServerLog.Debug("Theme " + Page.Theme + " exists. Setting theme");
            }
            else
            {
                ServerLog.Debug("Theme " + Request["PartnerSpId"] + " not found in App_Themes. Setting default theme.");
                Page.Theme = "Default";
            }
        }
        else
        {
            ServerLog.Debug("No theme information found. Setting default theme.");
            Page.Theme = "Default";
        }
    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadText();
        if (Request.QueryString["UserID"] != null)
        {
            UserInfo.Text = String.Format("{0}... Welcome!<br />", Request.QueryString["UserID"]);
        }
        if (Request.QueryString["Redirect"] != null)
        {
            UserInfo.Text += String.Format("Your browser should forward <a href=\"{0}\">here</a>", Request.QueryString["Redirect"]);
            redirect = Request.QueryString["Redirect"];
        }
    }
    #endregion

    #region Web Methods

    [WebMethod]
    public static object GetBannerInfo()
    {
        try
        {
            return new { 
                Timeout = TimeSpan.FromSeconds(Convert.ToDouble(ConfigurationManager.AppSettings["BannerTime"])).TotalMilliseconds,
                Redirect = redirect
            };
        }
        catch (Exception ex)
        {
            return new
            {
                Timeout = 5000,
                Redirect = redirect
            };
        }
    }

    #endregion

    #region Modify UI
    /// <summary>
    /// This function applies theme specific text to the page. 
    /// The text is stored in the Text.xml file associated with the current theme
    /// </summary>
    protected void LoadText()
    {
        ServerLog.Debug("Loading text for theme: " + Page.Theme);
        XmlTextReader TextReader = new XmlTextReader(Server.MapPath("~/App_Themes/" + Page.Theme + "/Text.xml"));
        XmlDocument TextDoc = new XmlDocument();
        TextDoc.Load(TextReader);
        TextReader.Close();
        XmlNode node = null;
        try
        {
            node = TextDoc.SelectSingleNode("//Literal[@ID='LoginTitle']");
            if (node != null)
                LoginTitle.Text = node.InnerText;
        }
        catch (Exception ex)
        {
            //ServerLog.Error("Exception while setting Login Title text", ex);
        }
        try
        {
            node = TextDoc.SelectSingleNode("//Literal[@ID='FooterInfo']");
            if (node != null)
                FooterInfo.Text = String.Format("<p>{0}</p>", node.InnerText);
        }
        catch (Exception ex)
        {
            ServerLog.Error("Exception while setting Footer text", ex);
        }
        try
        {
            node = TextDoc.SelectSingleNode("//Literal[@ID='Copy']");
            if (node != null)
                FooterInfo.Text += String.Format("<p>{0}</p>", node.InnerText);
        }
        catch (Exception ex)
        {
            ServerLog.Error("Exception while setting Copy text", ex);
        }
    }
    #endregion
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_support/MasterPage.master" AutoEventWireup="true" CodeFile="Loading.aspx.cs" Inherits="Loading" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="_support/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var timeout = 5000;
            var redirect = '';
            $.ajax({
                type: "POST",
                url: "Loading.aspx/GetBannerInfo",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    timeout = msg.d.Timeout;
                    redirect = msg.d.Redirect;
                    window.setTimeout(function () { window.location.href = redirect; }, timeout);
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="prefix_6 grid_12">
        <div class="content">
            <div class="banner">
                <div class="logo"> 
                  <h2><asp:Literal ID="LoginTitle" runat="server"></asp:Literal></h2>
                </div>
            </div>
            <p style="text-align:center;">
                <img src="_support/images/login1.gif" alt="New, Ours, Special" />
                <asp:Literal ID="UserInfo" runat="server"></asp:Literal>
            </p>
        </div>
        <div class="footer">
            <asp:Literal ID="FooterInfo" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>


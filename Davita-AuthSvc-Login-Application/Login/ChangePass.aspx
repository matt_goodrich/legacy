﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_support/MasterPage.master" AutoEventWireup="true" CodeFile="ChangePass.aspx.cs" Inherits="ChangePass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="prefix_6 grid_12">
        <div class="content">
            <div class="banner">
                <div class="logo"> 
                  <h2><asp:Literal ID="LoginTitle" runat="server"></asp:Literal></h2> 
                </div>
            </div>
            <asp:Panel DefaultButton="LoginButton" runat="server" ID="LoginFormPanel">
                <fieldset id="ChangePassFields">
                    <ol>
                        <li id="uList" runat="server">
                            <label runat="server" id="uLabel">Username:</label>
                            <asp:TextBox ID="userID" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label runat="server" id="pLabel">Current Password:</label>
                            <asp:TextBox ID="pass" runat="server" TextMode="Password" CssClass="input"></asp:TextBox>  
                        </li>
                        <li>
                            <label runat="server" id="npLabel">New Password:</label>
                            <asp:TextBox ID="newPass" runat="server" TextMode="Password" CssClass="input"></asp:TextBox>
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="* Password does not meet password criteria" ControlToValidate="newPass" Display="Dynamic" ValidationExpression="(?=^[A-Za-z])(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[_\\-~`^!@.=]{0,})(?=^[^ ]*$).{6,30}$" ></asp:RegularExpressionValidator>
                        </li>
                        <li>
                            <label runat="server" id="cpLabel">Confirm Password:</label>
                            <asp:TextBox ID="confirmPass" runat="server" TextMode="Password" CssClass="input"></asp:TextBox>  
                        </li>
                        <li class="send">
                            <asp:LinkButton ID="LoginButton" CssClass="button primary" runat="server" OnClick="LoginButton_OnClick">Change Password and Login</asp:LinkButton>                     
                        </li>
                    </ol>
                    <asp:Literal ID="ErrorLit" runat="server"></asp:Literal>
                    <div class="passwordPolicy">
                        <h3>DaVita Password Policy</h3>
                        <ul>
                            <li>Passwords must be between 6 and 30 characters.</li>
                            <li>Passwords must begin with an alphabetic character.</li>
                            <li>Passwords must contain at least 1 number.</li>
                            <li>Passwords cannot contain your username.</li>
                            <li>Passwords cannot contain a space.</li>
                            <li>Passwords may contain the following special characters: _ - ~ ` ^ ! @ . =</li>
                            <li>You can not use any of your past 3 passwords.</li>
                        </ul>
                    </div>
                </fieldset>
            </asp:Panel>            
            <div class="clear"></div>
        </div>
        <div class="footer">
            <asp:Literal ID="FooterInfo" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>


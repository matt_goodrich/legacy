﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_support/MasterPage.master" AutoEventWireup="true" CodeFile="NotAuthorized.aspx.cs" Inherits="NotAuthorized" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="prefix_6 grid_12">
        <div class="content">
            <div class="banner">
                <div class="logo"> 
                  <h2>Not Authorized</h2> 
                </div>
            </div>
            <fieldset style="border: none;">
                <ol>
                    <li>
                        <p>You do not have permission to access this resource.</p> 
                    </li>
                </ol>
            </fieldset>            
            <div class="clear"></div>
        </div>
        <div class="footer">
            <asp:Literal ID="FooterInfo" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>


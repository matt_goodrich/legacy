﻿/*******************************************************************************
* Name
*   Configuration Page Code Behind
*
* Identifier
*   Filename: config.aspx.cs
*   
* Purpose
*   This class contains the methods to provide configuration of the forms based
*   authentication with pingfederate, ENT Active Directory and the PC LDS.
*
********************************************************************************
*/

//Microsoft Libraries
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//External libraries
using Com.PingIdentity.Adapters.Pftoken.Util;
using Com.PingIdentity.PingFederate.SampleApp.SampleAppUtility;
using Utilities.Encryption;
using log4net;

public partial class config_config : System.Web.UI.Page
{
    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

    //Variables used to read the pingfederate-idp-config.props file
    public const String SAMPLE_PF_HOST = "hostPF";
    public const String SAMPLE_TOKEN_TYPE = "tokenType";
    public const String SAMPLE_USE_SSL = "useSSL";
    public const String SAMPLE_ACCOUNT_LINKING = "accountLinking";
    public const String SAMPLE_RAW_DATA = "rawData";
    public const String SAMPLE_ATTRS_NAMES_LIST = "attributeNamesList";
    public const String SAMPLE_PF_HTTP_PORT = "httpPort";
    public const String SAMPLE_PF_HTTPS_PORT = "httpsPort";

    IDictionary configProperties;

    #region PreInit
    /// <summary>
    /// This function is the first method to be executed during the page load.
    /// In order to apply the correct theme before the page is rendered, we must
    /// apply the theme early in the page lifecycle. 
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Always use Default Theme on this page
        Page.Theme = "Default";
    }
    #endregion
    
    /// <summary>
    /// This function is run on the click of the initial load of the page. This function populates
    /// the form fields based on the information saved in the web.config as well as the 
    /// pingfederate-idp-config.props file in the config directory.
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        ServerLog.DebugFormat("Config Page accessed by {0}", User.Identity.Name);

        //Add CSS style to Master Form
        this.Page.Form.Attributes.Add("class", "wide");

        //If we are not in a post back cycle, populate the form fields from the web.config and pingfederate-idp-config.props
        if (!IsPostBack)
        {
            //Load LDAP Information from the web.config
            LDAPCorp.Text = ConfigurationManager.AppSettings["LDAPConnectionString_CORP"];
            LDAPPath.Text = ConfigurationManager.AppSettings["LDAPPath_CORP"];
            LDAPCorpUser.Text = ConfigurationManager.AppSettings["CorpLDAPUser"];
            LDAPCorpPass.Attributes.Add("value", AESEncryption.Decrypt(ConfigurationManager.AppSettings["CorpLDAPPassword"], "PingAAA", "SymmetricAESEncryption", "SHA1", 2, "AaAIe2011*FtWL0l", 256));
            LDAPAdminUser.Text = ConfigurationManager.AppSettings["CorpADAdmin"];
            LDAPAdminPass.Attributes.Add("value", AESEncryption.Decrypt(ConfigurationManager.AppSettings["CorpADPassword"], "PingAAA", "SymmetricAESEncryption", "SHA1", 2, "AaAIe2011*FtWL0l", 256));
            PassDays.Text = ConfigurationManager.AppSettings["PasswordExpireReminderDays"];
            PassResetUrl.Text = ConfigurationManager.AppSettings["PasswordResetURL"];
            SkipADConfig.SelectedValue = ConfigurationManager.AppSettings["SkipAD"];
            ShowLoading.SelectedValue = ConfigurationManager.AppSettings["ShowBanner"];
            LoadingTime.Text = ConfigurationManager.AppSettings["BannerTime"];

            //Load the Ping Information from pingfederate-idp-config.props
            String idpConfig = ConfigurationManager.AppSettings["config-file"];
            String configPath = Context.Server.MapPath(idpConfig);

            // Read properties
            PropertyReader configPropertyReader = new PropertyReader();

            StreamReader configStreamReader = new StreamReader(configPath);
            configProperties = configPropertyReader.Load(configStreamReader);
            configStreamReader.Close();

            //Populate form fields
            PFHost.Text = (String)configProperties[SAMPLE_PF_HOST];
            //AdapterType.SelectedValue = (String)configProperties[SAMPLE_TOKEN_TYPE];
            //UseSSL.Checked = Convert.ToBoolean(configProperties[SAMPLE_USE_SSL]);
            //PFHttpPort.Text = (String)configProperties[SAMPLE_PF_HTTP_PORT];
            PFHttpsPort.Text = (String)configProperties[SAMPLE_PF_HTTPS_PORT];
        }
    }

    /// <summary>
    /// This function is run on the click of the "Save" button on the Ping Configuration section.
    /// This function adds all the properties to a local variable, and then iterates over them
    /// with a stream writer that is writing to the pingfederate-idp-config.props file.  If it 
    /// completes successfully an error message will indicate that to a user, and if it fails the
    /// user will get an error message.
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void SaveButton_OnClick(object sender, EventArgs e)
    {
        Dictionary<string, string> properties = new Dictionary<string, string>();
        properties.Add(SAMPLE_PF_HOST, (String.IsNullOrEmpty(PFHost.Text) ? (String)configProperties[SAMPLE_PF_HOST] : PFHost.Text));
        properties.Add(SAMPLE_TOKEN_TYPE, "OpenToken");
        properties.Add(SAMPLE_USE_SSL, "true");
        properties.Add(SAMPLE_PF_HTTP_PORT, "80");
        properties.Add(SAMPLE_PF_HTTPS_PORT, (String.IsNullOrEmpty(PFHttpsPort.Text) ? (String)configProperties[SAMPLE_PF_HTTPS_PORT] : PFHttpsPort.Text));
                        
        String idpConfig = ConfigurationManager.AppSettings["config-file"];
        String configPath = Context.Server.MapPath(idpConfig);

        StreamWriter propsWriter = null;

        try
        {
            propsWriter = new StreamWriter(configPath);
        }
        catch (Exception ex)
        {
            ServerLog.Error("Exception While Updating PingFederate config:", ex);
            PFErrorLit.Text = "<div id=\"fail\"><span class=\"ico_cancel\">Could not update configuration</span></div>";
        }

        foreach (KeyValuePair<string, string> kvp in properties)
        {
            String infoName = kvp.Key;
            String infoValue = kvp.Value;

            propsWriter.WriteLine(infoName + "=" + infoValue);
            ServerLog.DebugFormat("Writing Property: {0} , Value: {1}", infoName, infoValue);
        }

        propsWriter.Flush();
        propsWriter.Close();

        ServerLog.Debug("PingFederate configuration saved successfully");
        PFErrorLit.Text = "<div id=\"success\"><span class=\"ico_success\">Configuration Saved Successfully</span></div>";

    }

    /// <summary>
    /// This function is run on the click of the "Upload" button on the Ping Configuration section.
    /// This function checks to see if a file has been selected for upload, if a file has been 
    /// selected, it saves the file as "agent-config.txt" in the appropriate location, and returns
    /// a success message to the user. If no file was selected, an error message is returned to the
    /// user, and if there was a problem uploading the file, an error message is returned to the user.
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void AgentUpload_OnClick(object sender, EventArgs e)
    {
        ServerLog.Debug("Agent Config file being uploaded");
        //If the user selected a file
        if (AgentFileUpload.HasFile)
        {
            //Get the path of the agent-config.txt file
            string SaveLocation = Server.MapPath("agent-config.txt");
            try
            {
                //Save the new file as agent-config.txt in the correct location
                AgentFileUpload.SaveAs(SaveLocation);
                ServerLog.Debug("agent-config.txt file saved successfully");
                //Tell the user of the great success
                PFErrorLit.Text = "<div id=\"success\"><span class=\"ico_success\">The file was uploaded successfully</span></div>";
            }
            catch (Exception ex)
            {
                //Uh oh, there was an error, let the user know
                ServerLog.Error("Exception while saving agent-config.txt file: ", ex);
                PFErrorLit.Text = "<div id=\"fail\"><span class=\"ico_cancel\">There was an error uploading the file</span></div>";
            }
        }
        else
        {
            //The user did not specify a file, let them know.
            PFErrorLit.Text = "<div id=\"fail\"><span class=\"ico_cancel\">No File Specified</span></div>";
        }
    }

    /// <summary>
    /// This function is run on the click of the "Save" button on the LDAP Configuration section.
    /// This function beings by doing some error checking against all the inputs ensuring that all
    /// fields have values, and the two connection string fields start with "LDAP://". If the inputs
    /// are error free, we save the values to the web.config file.  The passwords are saved using
    /// AES 256 bit encryption.
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void SaveLDAPButton_OnClick(object sender, EventArgs e)
    {
        ServerLog.Debug("LDAP Configuration being saved");
        //Do our error checking
        string errors = "";
        if (String.IsNullOrEmpty(LDAPCorp.Text))
        {
            ServerLog.Debug("Admin Notified: Please enter an Internal LDAP Connection String"); 
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter an Internal LDAP Connection String</span></div>";
        }
        if (String.IsNullOrEmpty(LDAPPath.Text))
        {
            ServerLog.Debug("Admin Notified: Please enter an Internal LDAP Connection String Qualifier"); 
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter an LDAP Path</span></div>";
        }
        if (!LDAPCorp.Text.Contains("LDAP://"))
        {
            ServerLog.Debug("Admin Notified: The Internal LDAP Connection String specified does not contain \"LDAP://\", please enter a correct connection string"); 
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">The Internal LDAP Connection String specified does not contain \"LDAP://\", please enter a correct connection string</span></div>";
        }
        if (String.IsNullOrEmpty(LDAPCorpUser.Text))
        {
            ServerLog.Debug("Admin Notified: Please enter an Internal LDAP User Id");
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter an Internal LDAP User Id</span></div>";
        }
        if (String.IsNullOrEmpty(LDAPCorpPass.Text))
        {
            ServerLog.Debug("Admin Notified: Please enter an Internal LDAP Password"); 
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter an Internal LDAP Password</span></div>";
        }
        if (String.IsNullOrEmpty(LDAPAdminUser.Text))
        {
            ServerLog.Debug("Admin Notified: Please enter an Internal LDAP Admin User Id");
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter an Internal LDAP Admin User Id</span></div>";
        }
        if (String.IsNullOrEmpty(LDAPAdminPass.Text))
        {
            ServerLog.Debug("Admin Notified: Please enter an Internal LDAP Admin Password");
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter an Internal LDAP Admin Password</span></div>";
        }
        if (String.IsNullOrEmpty(PassDays.Text))
        {
            ServerLog.Debug("Admin Notified: Please enter a value for password expiration warnings to start"); 
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter a value for password expiration warnings to start</span></div>";
        }
        if (String.IsNullOrEmpty(PassResetUrl.Text))
        {
            ServerLog.Debug("Admin Notified: Please enter a value for the password reset redirect URL");
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter a value for Password Reset URL</span></div>";
        }
        if (String.IsNullOrEmpty(LoadingTime.Text))
        {
            ServerLog.Debug("Admin Notified: Please enter a value for Banner Time");
            errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter a value for Banner Time</span></div>";
        }
        //if (IsValidDouble(LoadingTime.Text))
        //{
        //    ServerLog.Debug("Admin Notified: Please enter a valid double (seconds) for Banner Time");
        //    errors += "<div id=\"fail\"><span class=\"ico_cancel\">Please enter a valid double (seconds) for Banner Time</span></div>";
        //}


        //If we have errors, show them to the user and stop processing
        if (errors.Length != 0)
        {
            LDAPErrorLit.Text = errors;
            return;
        }

        //If we are error free, save the settings to the web.config file
        Configuration myConfiguration = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
        myConfiguration.AppSettings.Settings["LDAPConnectionString_CORP"].Value = LDAPCorp.Text;
        myConfiguration.AppSettings.Settings["LDAPPath_CORP"].Value = LDAPPath.Text;
        myConfiguration.AppSettings.Settings["CorpLDAPUser"].Value = LDAPCorpUser.Text;
        myConfiguration.AppSettings.Settings["CorpLDAPPassword"].Value = AESEncryption.Encrypt(LDAPCorpPass.Text, "PingAAA", "SymmetricAESEncryption", "SHA1", 2, "AaAIe2011*FtWL0l", 256);
        myConfiguration.AppSettings.Settings["CorpADAdmin"].Value = LDAPAdminUser.Text;
        myConfiguration.AppSettings.Settings["CorpADPassword"].Value = AESEncryption.Encrypt(LDAPAdminPass.Text, "PingAAA", "SymmetricAESEncryption", "SHA1", 2, "AaAIe2011*FtWL0l", 256);
        myConfiguration.AppSettings.Settings["PasswordExpireReminderDays"].Value = PassDays.Text;
        myConfiguration.AppSettings.Settings["PasswordResetURL"].Value = PassResetUrl.Text;
        myConfiguration.AppSettings.Settings["SkipAD"].Value = SkipADConfig.SelectedValue;
        myConfiguration.AppSettings.Settings["ShowBanner"].Value = ShowLoading.SelectedValue;
        myConfiguration.AppSettings.Settings["BannerTime"].Value = LoadingTime.Text;

        try
        {
            myConfiguration.Save();

            //Tell the user of the great success
            LDAPErrorLit.Text = "<div id=\"success\"><span class=\"ico_success\">LDAP Configuration Saved Successfully</span></div>";
            ServerLog.Debug("LDAP Configuration Saved Successfully");
        }
        catch (Exception ex)
        {
            ServerLog.Error("Exception while saving LDAP Config to web.config file: ", ex);
            LDAPErrorLit.Text = "<div id=\"fail\"><span class=\"ico_cancel\">Error Saving Configuration</span></div>";
        }
    }

    private bool IsValidDouble(string value)
    {
        double d;
        return Double.TryParse(value, out d);
    }
}
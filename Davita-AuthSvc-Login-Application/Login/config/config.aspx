﻿<%@ Page Title="Configuration" Language="C#" MasterPageFile="~/_support/MasterPage.master" AutoEventWireup="true" CodeFile="config.aspx.cs" Inherits="config_config" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="grid_11">
        <div class="content">
            <div class="banner">
                <div class="logo11">
                    <h2>PingFederate Configuration</h2>
                </div>
            </div>
            <asp:Panel DefaultButton="SaveButton" runat="server" ID="LoginFormPanel">
                <fieldset>
                    <ol>
                        <li>
                            <label>PF Host Name: </label>
                            <asp:TextBox ID="PFHost" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label>PF HTTPS Port: </label>
                            <asp:TextBox ID="PFHttpsPort" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li class="send">
                            <asp:LinkButton ID="SaveButton" CssClass="button primary negative" runat="server" OnClick="SaveButton_OnClick"><span class="icon cog"></span>Save</asp:LinkButton>
                        </li>
                        <li>
                            <label>Agent Config</label>
                            <asp:FileUpload ID="AgentFileUpload" runat="server" CssClass="input" />
                        </li>
                        <li class="send">
                            <asp:LinkButton ID="AgentUpload" CssClass="button primary negative" runat="server" OnClick="AgentUpload_OnClick"><span class="icon uparrow"></span>Upload</asp:LinkButton>
                            <asp:Literal ID="PFErrorLit" runat="server"></asp:Literal>
                        </li>
                    </ol>
                </fieldset>
            </asp:Panel>            
            <div class="clear"></div>
        </div>
    </div>
    <div class="prefix_1 grid_11">
        <div class="content">
            <div class="banner">
                <div class="logo11">
                    <h2>LDAP Configuration</h2>
                </div>
            </div>
            <asp:Panel DefaultButton="SaveButton" runat="server" ID="Panel1">
                <fieldset>
                    <ol>
                        <li>
                            <label>ENT Conn. String:</label>
                            <asp:TextBox ID="LDAPCorp" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label>ENT LDAP Path:</label>
                            <asp:TextBox ID="LDAPPath" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label>ENT User Id:</label>
                            <asp:TextBox ID="LDAPCorpUser" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label>ENT Password:</label>
                            <asp:TextBox ID="LDAPCorpPass" runat="server" TextMode="Password" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label>ENT Admin Id:</label>
                            <asp:TextBox ID="LDAPAdminUser" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label>ENT Admin Pass:</label>
                            <asp:TextBox ID="LDAPAdminPass" runat="server" TextMode="Password" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label>Password Exp:</label>
                            <asp:TextBox ID="PassDays" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label>Password Reset:</label>
                            <asp:TextBox ID="PassResetUrl" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label>Skip AD:</label>
                            <asp:DropDownList ID="SkipADConfig" runat="server">
                                <asp:ListItem>False</asp:ListItem>
                                <asp:ListItem>True</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                        <li>
                            <label>Show Banner:</label>
                            <asp:DropDownList ID="ShowLoading" runat="server">
                                <asp:ListItem>False</asp:ListItem>
                                <asp:ListItem>True</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                        <li>
                            <label>Banner Time:</label>
                            <asp:TextBox ID="LoadingTime" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li class="send">
                            <asp:LinkButton ID="SaveLDAPButton" CssClass="button primary negative" runat="server" OnClick="SaveLDAPButton_OnClick"><span class="icon cog"></span>Save</asp:LinkButton>
                            <asp:Literal ID="LDAPErrorLit" runat="server"></asp:Literal>
                        </li>
                    </ol>
                </fieldset>
            </asp:Panel>            
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</asp:Content>


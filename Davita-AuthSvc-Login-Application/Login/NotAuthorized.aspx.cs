﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using log4net;

public partial class NotAuthorized : System.Web.UI.Page
{
    #region PreInit
    /// <summary>
    /// This function is the first method to be executed during the page load.
    /// In order to apply the correct theme before the page is rendered, we must
    /// apply the theme early in the page lifecycle. 
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Always use Default Theme on this page
        Page.Theme = "Default";
    }
    #endregion

    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

    protected void Page_Load(object sender, EventArgs e)
    {
        //Add CSS style to Master Form
        this.Page.Form.Attributes.Add("class", "wide");

        string ReturnURL = (Request.QueryString["ReturnUrl"] != null ? Request.QueryString["ReturnUrl"] : "");
        ReturnURL = Server.UrlDecode(ReturnURL);
        ServerLog.DebugFormat("User {0} does not have required rights to access {1}. Is in GrpSSO_Admin: {2}", User.Identity.Name, ReturnURL, User.IsInRole("DVAKeyQA\\GrpSSO_Admin"));
    }
}
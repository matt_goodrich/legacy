﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_support/MasterPage.master" AutoEventWireup="true" CodeFile="Logout.aspx.cs" Inherits="Logout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="prefix_6 grid_12">
        <div class="content">
            <div class="banner">
                <div class="logo"> 
                  <h2>Logout</h2> 
                </div>
            </div>
            <fieldset style="border: none;">
                <ol>
                    <li>
                        <p>You have successfully been logged out!</p>            
                    </li>
                </ol>
            </fieldset>
            <div class="clear"></div>
        </div>
    </div>
</asp:Content>


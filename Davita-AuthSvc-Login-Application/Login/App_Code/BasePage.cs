﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using log4net;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : Page
{
    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

    /// <summary>
    /// Initializes a new instance of the <see cref="BasePage"/> class.
    /// </summary>
	public BasePage()
	{
        this.PreInit += new EventHandler(BasePage_PreInit);
	}

    #region PreInit
    /// <summary>
    /// This function is the first method to be executed during the page load.
    /// In order to apply the correct theme before the page is rendered, we must
    /// apply the theme early in the page lifecycle. 
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void BasePage_PreInit(object sender, EventArgs e)
    {
        //Check if we have a Theme name saved in our session variables. Do further checking if so, apply the default if not
        if (Session["Theme"] != null)
        {
            ServerLog.Debug("Found theme in Session. Attempting to find theme: " + Session["Theme"]);
            //Check if the theme exists on the server. If so, apply the theme, if not apply the default.
            if (Directory.Exists(Server.MapPath("~/App_Themes/" + Session["Theme"])))
            {
                Page.Theme = Session["Theme"].ToString();
                ServerLog.Debug("Theme " + Page.Theme + " exists. Setting theme");
            }
            else
            {
                ServerLog.Debug("Theme " + Session["Theme"] + " not found in App_Themes. Setting default theme.");
                Page.Theme = "Default";
            }
        }
        else if (Request["PartnerSpId"] != null)
        {
            ServerLog.Debug("Found theme in URL. Attempting to find theme: " + Request["PartnerSpId"]);
            if (Directory.Exists(Server.MapPath("~/App_Themes/" + Request["PartnerSpId"].ToString().Replace(":", ""))))
            {
                Page.Theme = Request["PartnerSpId"].ToString().Replace(":", "");
                ServerLog.Debug("Theme " + Page.Theme + " exists. Setting theme");
            }
            else
            {
                ServerLog.Debug("Theme " + Request["PartnerSpId"] + " not found in App_Themes. Setting default theme.");
                Page.Theme = "Default";
            }
        }
        else
        {
            ServerLog.Debug("No theme information found. Setting default theme.");
            Page.Theme = "Default";
        }
    }
    #endregion

    #region OnLoad
    /// <summary>
    /// Handles the OnLoad event of the BasePage control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected override void OnLoad(EventArgs e)
    {
        //prevent the output of aspx page from being cached by the browser
        Response.AddHeader("Cache-Control", "no-cache");
        Response.AddHeader("Pragma", "no-cache");

        base.OnLoad(e);
    }
    #endregion
}
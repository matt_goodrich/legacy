﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LogHelper
/// </summary>
public class LogHelper
{
    public static void WriteToSession(string SessionID, string SessionValue)
    {
        HttpContext.Current.Session[SessionID] = SessionValue;
    }

    public static void ClearSessionData(string SessionID)
    {
        HttpContext.Current.Session[SessionID] = null;
    }
}
/*******************************************************************************
* Copyright (C) 2008 Ping Identity Corporation All rights reserved.
*
* This software is licensed under the Open Software License v2.1 (OSL v2.1).
*
* A copy of this license has been provided with the distribution of this
* software. Additionally, a copy of this license is available at:
* http://opensource.org/licenses/osl-2.1.php
*
******************************************************************************
*/
using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using opentoken;
using opentoken.util;
using Com.PingIdentity.PingFederate.SampleApp.SampleAppUtility;

using log4net;

namespace com.pingidentity.adapters.sampleapp.idp
{

    /// <summary>
    /// Summary description for OTIdPHttpModule
    /// </summary>
    public class OTIdPHttpModule : IHttpModule
    {
        //Logs
        private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

        private Links samlLinks;
        private String configPath = "";

        public OTIdPHttpModule()
        {
        }

        public String ModuleName
        {
            get { return "OTIdPHttpModule"; }
        }

        // In the Init function, register for HttpApplication 
        // events by adding your handlers.
        public void Init(HttpApplication application)
        {
            HttpContext context = application.Context;
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            //get properties from Idp config file
            String idpConfig = ConfigurationManager.AppSettings["config-file"];
            configPath = context.Server.MapPath("~/config/");
            if ((configPath.Contains("/config")))
            {
                configPath = context.Server.MapPath("~/config/");
            }
            samlLinks = new Links(context, configPath + idpConfig);

            application.PostAcquireRequestState += new EventHandler(Application_PostAcquireRequestState);
            application.PostMapRequestHandler += new EventHandler(Application_PostMapRequestHandler);
            application.BeginRequest +=
                (new EventHandler(this.Application_BeginRequest));
            application.EndRequest +=
                (new EventHandler(this.Application_EndRequest));
        }

        void Application_PostMapRequestHandler(object source, EventArgs e)
        {

            HttpApplication app = (HttpApplication)source;
            if (app.Context.Handler is IReadOnlySessionState || app.Context.Handler is IRequiresSessionState)
            {
                // no need to replace the current handler
                return;
            }

            // swap the current handler
            app.Context.Handler = new MyHttpHandler(app.Context.Handler);
        }



        void Application_PostAcquireRequestState(object source, EventArgs e)
        {
            HttpApplication app = (HttpApplication)source;
            MyHttpHandler resourceHttpHandler = HttpContext.Current.Handler as MyHttpHandler;
            if (resourceHttpHandler != null)
            {
                // set the original handler back
                HttpContext.Current.Handler = resourceHttpHandler.OriginalHandler;
            }

            // -> at this point session state should be available
            HttpSessionState session = app.Session;
            // Create HttpContext objects to access
            // request and response properties.
            HttpContext context = app.Context;
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            if (request["spentity"] != null)
            {
                session["Theme"] = request["spentity"].ToString().Replace(":", "");
                ServerLog.Debug("Adding theme " + request["spentity"].ToString().Replace(":", "") + " to session store");
            }

            if (samlLinks.configTable["tokenType"].Equals("OpenToken"))
            {
                String cmd = request["cmd"];
                switch (cmd)
                {
                    case "sso":
                        ServerLog.Debug("SSO Requested");
                        HandleIdPSSORequest(context, request, response, session);
                        break;

                    case "slo":
                        ServerLog.Debug("SLO Requested");
                        HandleIdPSLORequest(context, request, response, session);
                        break;

                    case "logout":
                        ServerLog.Debug("Logout Requested");
                        HandleLogoutRequest(context, request, response, session);
                        break;

                    default:
                        break;

                }

            }

        }



        private void Application_BeginRequest(Object source,
             EventArgs e)
        {

        }

        private void Application_EndRequest(Object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;
        }

        public void Dispose()
        {
        }

        private void HandleIdPSSORequest(HttpContext context, HttpRequest request, HttpResponse response, HttpSessionState session)
        {
            if (request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                ServerLog.Debug("Found existing user session");
                string strRedirect;
                // Check for forceAuthn
                if (forceAuthentication(request))
                {
                    ServerLog.Debug("Force Authentication is true. Prompting for re-authentication.");
                    session.Clear();
                    FormsAuthentication.SignOut();
                    strRedirect = "default.aspx"; 
                    if (request[Constants.RESUME_PATH] != null)
                    {
                        strRedirect += "?error=ForceAuthn is true&ReturnUrl=" + request.ApplicationPath + "/?cmd=sso%26" + Constants.RESUME_PATH + "=" + request[Constants.RESUME_PATH];
                    }
                    //strRedirect += "&cmd=sso&error=ForceAuthn is true";
                    response.Redirect(strRedirect, true);
                    return;
                }
                string cookiestr;
                cookiestr = request.Cookies[FormsAuthentication.FormsCookieName].Value;
                FormsAuthenticationTicket tkt = FormsAuthentication.Decrypt(cookiestr);
                MultiStringDictionary attributes;
                //if (session != null)
                //{
                    // Get attributes from session
                    //attributes = (MultiStringDictionary)session[Constants.USER_INFO];
                //}
                //else
                //{
                    attributes = new MultiStringDictionary();
                    attributes.Add(Constants.SUBJECT, tkt.Name);
                //}

                strRedirect = "";
                if (request[Constants.RESUME_PATH] != null)
                {
                    MultiStringDictionary attributesToSend = new MultiStringDictionary();
                    foreach (KeyValuePair<string, List<string>> pair in attributes)
                    {
                        string key = pair.Key;

                        foreach (string value in pair.Value)
                        {
                            attributesToSend.Add(key, value);
                        }
                    }
                    strRedirect = Links.hostPF + request[Constants.RESUME_PATH];
                    strRedirect = setOpenToken(context, strRedirect, attributesToSend);
                    response.Redirect(strRedirect, true);

                }
            }
            else
            {
                ServerLog.Debug("No existing user session. Prompting for authentication.");
                string strRedirect;
                strRedirect = "default.aspx";
                // Check for isPassive
                if (isPassive(request))
                {
                    strRedirect = Links.hostPF + request[Constants.RESUME_PATH];
                }
                else
                {
                    if (request[Constants.RESUME_PATH] != null)
                    {
                        strRedirect += "?ReturnUrl=" + request.ApplicationPath + "/?cmd=sso%26" + Constants.RESUME_PATH + "=" + request[Constants.RESUME_PATH];
                    }
                }
                response.Redirect(strRedirect, true);
            }
        }

        private void HandleIdPSLORequest(HttpContext context, HttpRequest request, HttpResponse response, HttpSessionState session)
        {
            if (request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                ServerLog.Debug("Found user session to logout");
                string cookiestr;
                cookiestr = request.Cookies[FormsAuthentication.FormsCookieName].Value;
                FormsAuthenticationTicket tkt = FormsAuthentication.Decrypt(cookiestr);
                MultiStringDictionary attributes;
                if (session != null)
                {
                    // Get attributes from session
                    attributes = (MultiStringDictionary)session[Constants.USER_INFO];
                }
                else
                {
                    attributes = new MultiStringDictionary();
                    attributes.Add(Constants.SUBJECT, tkt.Name);
                }

                session.Clear();
                FormsAuthentication.SignOut();
                string strRedirect;
                if (request[Constants.RESUME_PATH] != null)
                {
                    MultiStringDictionary attributesToSend = new MultiStringDictionary();
                    foreach (KeyValuePair<string, List<string>> pair in attributes)
                    {
                        string key = pair.Key;

                        foreach (string value in pair.Value)
                        {
                            attributesToSend.Add(key, value);
                        }
                    }
                    strRedirect = Links.hostPF + request[Constants.RESUME_PATH];
                    response.Redirect(strRedirect, true);
                }
            }
            else
            {
                ServerLog.Debug("No session found to logout");
                string strRedirect;
                strRedirect = "default.aspx?error=No local session found to logout.";
                response.Redirect(strRedirect, true);
            }
        }

        private void HandleLogoutRequest(HttpContext context, HttpRequest request, HttpResponse response, HttpSessionState session)
        {
            string BehaviorXMLFile = "";
            if (request["SPID"] != null)
            {
                ServerLog.Debug("Found theme \"" + request["SPID"] + "\" in request. Checking for custom logout behavior");
                //Check if the theme exists on the server. If so, apply the theme, if not apply the default.
                if (Directory.Exists(context.Server.MapPath("~/App_Themes/" + request["SPID"].Replace(":", ""))))
                {
                    ServerLog.Debug("Found theme in App_Themes. Loading Behavior.xml file.");
                    BehaviorXMLFile = context.Server.MapPath("~/App_Themes/" + request["SPID"].Replace(":", "") + "/Behavior.xml");
                }
                else
                {
                    ServerLog.Debug("Theme not found in App_Themes. Loading default Behavior.xml file.");
                    BehaviorXMLFile = context.Server.MapPath("~/App_Themes/Default/Behavior.xml");
                }
            }
            else
            {
                ServerLog.Debug("Theme not found in request. Loading default Behavior.xml file.");
                BehaviorXMLFile = context.Server.MapPath("~/App_Themes/Default/Behavior.xml");
            }

            XmlTextReader TextReader = new XmlTextReader(BehaviorXMLFile);
            XmlDocument TextDoc = new XmlDocument();
            TextDoc.Load(TextReader);
            TextReader.Close();
            XmlNode node = TextDoc.SelectSingleNode("//Redirect[@ID='DefaultLogout']");

            if (request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                ServerLog.Debug("Clearing user session and signing out the user");
                session.Clear();
                FormsAuthentication.SignOut();
            }
            ServerLog.Debug("Redirecting to logout URL: " + node.InnerText);
            response.Redirect(node.InnerText, true);
        }


        /// <summary>
        /// setOpenToken method uses the Java Integration Kit API to create an 
        /// instance of Idp Agent. The user attributes are added to this agent and 
        /// then the OpenToken is created as a cookie or query parameter
        /// </summary>
        /// <param name="context">current HttpContext</param>
        /// <param name="url">url to append OpenToken</param>
        /// <param name="userInfo">Hashtable of user attributes</param>
        /// <returns>URL with OpenToken</returns>
        private String setOpenToken(HttpContext context, String url, MultiStringDictionary userInfo)
        {
            String returnUrl = url;

            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            HttpSessionState session = context.Session;

            //read properties from Idp pfagent-properties file
            String propsPath = configPath + Constants.PFAGENT_PROPERTIES;

            // Create OpenToken Idp Agent
            Agent agent = new Agent(propsPath);

            UrlHelper urlHelper = new UrlHelper(url);
            agent.WriteToken(userInfo, response, urlHelper, false);

            return urlHelper.ToString();
        }

        /**
         * forceAuthentication method retrieves the ForceAuthn parameter from
         * request and returns the value in boolean.
         * @param request
         * 				current user request
         * @return boolean
         * 				flag to indicate if login is required
         */
        private bool forceAuthentication(HttpRequest request)
        {

            // Retrieve parameters from request
            String forceAuthn = request[Constants.FORCE_AUTH_PARAM];

            bool forceFlag = false;

            // Convert string parameters to boolean
            if (forceAuthn != null && forceAuthn.Equals("true"))
            {
                forceFlag = true;
            }
            return forceFlag;
        }

        /**
         * isPassive method retrieves the ForceAuthn parameter from
         * request and returns the value in boolean.
         * @param request
         * 				current user request
         * @return boolean
         * 				flag to indicate if login is required
         */
        private bool isPassive(HttpRequest request)
        {

            // Retrieve parameters from request
            String isPassive = request[Constants.IS_PASSIVE_PARAM];

            bool isPassiveFlag = false;

            // Convert string parameters to boolean
            if (isPassive != null && isPassive.Equals("true"))
            {
                isPassiveFlag = true;
            }
            return isPassiveFlag;
        }


        // a temp handler used to force the SessionStateModule to load session state
        public class MyHttpHandler : IHttpHandler, IRequiresSessionState
        {
            internal readonly IHttpHandler OriginalHandler;

            public MyHttpHandler(IHttpHandler originalHandler)
            {
                OriginalHandler = originalHandler;
            }

            public void ProcessRequest(HttpContext context)
            {
                // do not worry, ProcessRequest() will not be called, but let's be safe
                throw new InvalidOperationException("MyHttpHandler cannot process requests.");
            }

            public bool IsReusable
            {
                // IsReusable must be set to false since class has a member!
                get { return false; }
            }
        }

    }
}
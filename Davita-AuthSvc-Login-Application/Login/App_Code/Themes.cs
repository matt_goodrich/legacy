﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;

using log4net;

/// <summary>
/// Summary description for Themes
/// </summary>
public class Themes
{
    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

    XmlDocument TextDoc;
    XmlDocument BehaviorDoc;
    XmlNode node;

    /// <summary>
    /// This applies theme specific text and behavior to the page. 
    /// The text is stored in the Text.xml and Behavior.xml file associated with the current theme
    /// </summary>
    public Themes(string Theme)
    {
        ServerLog.Debug("Loading text for theme: " + Theme);
        XmlTextReader TextReader = new XmlTextReader(System.Web.HttpContext.Current.Server.MapPath("~/App_Themes/" + Theme + "/Text.xml"));
        TextDoc = new XmlDocument();
        TextDoc.Load(TextReader);
        TextReader.Close();

        ServerLog.Debug("Getting Banner Behavior for theme: " + Theme);
        XmlTextReader BehaviorReader = new XmlTextReader(System.Web.HttpContext.Current.Server.MapPath("~/App_Themes/" + Theme + "/Behavior.xml"));
        BehaviorDoc = new XmlDocument();
        BehaviorDoc.Load(BehaviorReader);
        BehaviorReader.Close();

        node = null;
    }

    #region Text
    /// <summary>
    /// Gets the footer info text.
    /// </summary>
    public string FooterInfoText
    {
        get
        {
            try
            {
                node = TextDoc.SelectSingleNode("//Literal[@ID='FooterInfo']");
                if (node != null)
                    return String.Format("<p>{0}</p>", node.InnerText);
            }
            catch (Exception ex)
            {
                ServerLog.Error("Exception while setting Footer text", ex);
            }
            return "";
        }
    }

    /// <summary>
    /// Gets the login title text.
    /// </summary>
    public string LoginTitleText
    {
        get
        {
            try
            {
                node = TextDoc.SelectSingleNode("//Literal[@ID='LoginTitle']");
                if (node != null)
                    return node.InnerText;
            }
            catch (Exception ex)
            {
                ServerLog.Error("Exception while getting Login Title text", ex);
            }
            return "";
        }
    }

    /// <summary>
    /// Gets the copy text.
    /// </summary>
    public string CopyText
    {
        get
        {
            try
            {
                node = TextDoc.SelectSingleNode("//Literal[@ID='Copy']");
                if (node != null)
                    return String.Format("<p>{0}</p>", node.InnerText);
            }
            catch (Exception ex)
            {
                ServerLog.Error("Exception while getting Copy text", ex);
            }
            return "";
        }
    }

    /// <summary>
    /// Gets the login button text.
    /// </summary>
    public string LoginButtonText
    {
        get
        {
            try
            {
                node = TextDoc.SelectSingleNode("//Literal[@ID='LoginButtonText']");
                if (node != null)
                    return node.InnerText;
            }
            catch (Exception ex)
            {
                ServerLog.Error("Exception while getting Login Button text", ex);
            }
            return "Login";
        }
    }
    #endregion

    #region Behavior
    /// <summary>
    /// Gets a value indicating whether [show banner for theme].
    /// </summary>
    /// <value>
    ///   <c>true</c> if [show banner for theme]; otherwise, <c>false</c>.
    /// </value>
    public bool RedirectToBannerBehavior
    {
        get
        {
            try
            {
                node = BehaviorDoc.SelectSingleNode("//Redirect[@ID='RedirectToBanner']");
                if (node != null)
                    return Convert.ToBoolean(node.InnerText);
            }
            catch (Exception ex)
            {
                ServerLog.Error("Exception while getting Banner Behavior", ex);
                return false;
            }
            return false;
        }
    }

    /// <summary>
    /// Gets the return to home URL.
    /// </summary>
    public string ReturnToHomeBehavior
    {
        get
        {
            try
            {
                node = BehaviorDoc.SelectSingleNode("//Redirect[@ID='ReturnToHome']");
                if (node != null)
                    return node.InnerText;
            }
            catch (Exception ex)
            {
                ServerLog.Error("Exception while getting Return to Home URL", ex);
            }
            return "";
        }
    }

    /// <summary>
    /// Gets the default logout URL.
    /// </summary>
    public string DefaultLogoutBehavior
    {
        get
        {
            try
            {
                node = BehaviorDoc.SelectSingleNode("//Redirect[@ID='DefaultLogout']");
                if (node != null)
                    return node.InnerText;
            }
            catch (Exception ex)
            {
                ServerLog.Error("Exception while getting Default Logout URL", ex);
            }
            return "";
        }
    }

    #endregion
}
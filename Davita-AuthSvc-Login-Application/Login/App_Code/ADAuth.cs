﻿/*******************************************************************************
* Name
*   Active Directory Authentication
*
* Identifier
*   Filename: ADAuth.cs
*   
* Purpose
*   This class contains the methods to provide forms based authentication
*   against Active Directory
*
******************************************************************************
*/

//Microsoft Libraries
using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using System.Net;
using System.Text;
using System.Web;

//External Libraries
using Utilities.Encryption;
using log4net;

/// <summary>
/// Class to handle communication with Active Directory
/// </summary>
public class ADAuth
{
    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");
    private static readonly ILog AuditLog = LogManager.GetLogger("AuditLog");

    public static DomainPolicy policy = new DomainPolicy(new DirectoryEntry(ConfigurationManager.AppSettings["LDAPConnectionString_CORP"] + "/" + ConfigurationManager.AppSettings["LDAPPath_CORP"], ConfigurationManager.AppSettings["CorpLDAPUser"], AESEncryption.Decrypt(ConfigurationManager.AppSettings["CorpLDAPPassword"], "PingAAA", "SymmetricAESEncryption", "SHA1", 2, "AaAIe2011*FtWL0l", 256), AuthenticationTypes.FastBind | AuthenticationTypes.Secure));
    const int UF_DONT_EXPIRE_PASSWD = 0x10000;
    /// <summary>
    /// This method finds a user in Active Directory. 
    /// The search is done based on the user ID supplied in the authentication 
    /// attempt. We search the directory based on the sAMAccountName
    /// attribute. If a user is found we return a SearchResult object with the 
    /// specified properties, if no user is found a null object is returned.
    /// </summary>
    /// <param name="userID">The user ID to search for</param>
    /// <param name="PropertiesToLoad">The AD attributes that need to be returned</param>
    /// <param name="error">This will return a string containing error messages generated</param>
    /// <returns>A SearchResult that is either null or contains a user</returns>
    public static SearchResult FindUser(string userID, List<string>PropertiesToLoad, out string error)
    {
        //Start by searching Active directory as specified in the web.config file in the AppSettings section
        DirectoryEntry v_searchRoot = new DirectoryEntry(ConfigurationManager.AppSettings["LDAPConnectionString_CORP"], ConfigurationManager.AppSettings["CorpLDAPUser"], AESEncryption.Decrypt(ConfigurationManager.AppSettings["CorpLDAPPassword"], "PingAAA", "SymmetricAESEncryption", "SHA1", 2, "AaAIe2011*FtWL0l", 256), AuthenticationTypes.FastBind | AuthenticationTypes.Secure);
        DirectorySearcher v_findUser = new DirectorySearcher(v_searchRoot, "(sAMAccountName=" + userID + ")");
        
        //Add additional properties we will need to validate a user can authenticate
        v_findUser.PropertiesToLoad.Add("sAMAccountName");
        v_findUser.PropertiesToLoad.Add("pwdLastSet");
        v_findUser.PropertiesToLoad.Add("userAccountControl");
        v_findUser.PropertiesToLoad.Add("disginguishedName");
        v_findUser.PropertiesToLoad.Add("accountExpires");
        v_findUser.PropertiesToLoad.Add("lockoutTime");
        
        //Add any additional attributes specified in the PropertiesToLoad data structure to the list of properties to be returned
        foreach (string str in PropertiesToLoad)
            v_findUser.PropertiesToLoad.Add(str);

        SearchResult v_result = null;
        error = "";
		
         //Find the individual user
        try
        {
            v_result = v_findUser.FindOne();

            if(v_result != null)
                ServerLog.Debug("Found user \"" + userID + "\" in \"" + v_searchRoot.Path + "\"");
            else
                ServerLog.Debug("User not found in \"" + v_searchRoot.Path + "\"");
        }
        catch (Exception ex)
        {
            error = "An unexpected error has occurred; please try again later (code 333).";
            ServerLog.Error("Exception while finding user in \"" + v_searchRoot.Path + "\"", ex);
        }

        v_searchRoot.Close();

        return v_result;
    }

    /// <summary>
    /// This function does the authentication for the specified user against 
    /// Active Directory.
    /// </summary>
    /// <param name="username">Username retrieved from initial LDAP query</param>
    /// <param name="pwd">Password as entered into the login form</param>
    /// <param name="error">This will return a string containing error messages generated</param>
    /// <returns>Boolean value of whether or not the user was authenticated</returns>
    public static bool LogonUser(string username, string pwd, out string error)
    {
        ServerLog.Debug("Authenticating user \"" + username + "\"");
        //Create a new DirectoryEntry object and connect as the user attempting to authenticate - 
        //this is connecting to the correct LDAP using a ternary function based on the external variable
        DirectoryEntry de = new DirectoryEntry(ConfigurationManager.AppSettings["LDAPConnectionString_CORP"], username, pwd, AuthenticationTypes.FastBind | AuthenticationTypes.Secure);
        error = "";
        try
        {
            if(de.Properties.Count > 0)
			{
				//This will force a bind/authentication
				Object obj = de.NativeObject;
				//If we do not throw an exception, the user has authenticated and we can return true
				de.Close();
			}
            LogHelper.WriteToSession("Subject", username);
            LogHelper.WriteToSession("Status", "Success");
            AuditLog.Debug("");
            return true;
        }
        catch (Exception ex)
        {
            //An exception was thrown meaning the user could not be authenticated, we can return false
            error = "The username or password you entered is incorrect (code 04)";
            LogHelper.WriteToSession("Subject", username);
            LogHelper.WriteToSession("Status", "Failure");
            AuditLog.Debug("");
            ServerLog.Error("Exception while authenticating user \"" + username + "\"", ex);
            de.Close();
            return false;
        }
    }

    /// <summary>
    /// Changes the password.
    /// </summary>
    /// <param name="userID">The user ID.</param>
    /// <param name="Password">The password.</param>
    /// <param name="NewPassword">The new password.</param>
    /// <returns></returns>
    public static bool ChangePassword(string userID, string Password, string NewPassword)
    {
        try
        {
            using (var context = new PrincipalContext(ContextType.Domain, null, ConfigurationManager.AppSettings["CorpLDAPUser"], AESEncryption.Decrypt(ConfigurationManager.AppSettings["CorpLDAPPassword"], "PingAAA", "SymmetricAESEncryption", "SHA1", 2, "AaAIe2011*FtWL0l", 256)))
            {
                using (var user = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userID))
                {
                    try
                    {
                        user.ChangePassword(Password, NewPassword);
                        ServerLog.DebugFormat("Password successfully changed for \"{0}\"", userID);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        ServerLog.Error("Exception caught while changing password", ex);
                        return false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ServerLog.Error(String.Format("Exception caught while finding user \"{0}\" to change password", userID), ex);
            return false;
        }
    }

    /// <summary>
    /// Resets the password.
    /// </summary>
    /// <param name="userID">The user ID.</param>
    /// <param name="NewPassword">The new password.</param>
    /// <returns></returns>
    public static bool ResetPassword(string userID, string Password, string NewPassword)
    {
        DirectoryEntry de = new DirectoryEntry(ConfigurationManager.AppSettings["LDAPConnectionString_CORP"], ConfigurationManager.AppSettings["CorpADAdmin"], AESEncryption.Decrypt(ConfigurationManager.AppSettings["CorpADPassword"], "PingAAA", "SymmetricAESEncryption", "SHA1", 2, "AaAIe2011*FtWL0l", 256), AuthenticationTypes.FastBind | AuthenticationTypes.Secure);

        DirectorySearcher v_findUser = new DirectorySearcher(de, "(sAMAccountName=" + userID + ")");

        //Add additional properties we will need to validate a user can authenticate
        v_findUser.PropertiesToLoad.Add("sAMAccountName");
        v_findUser.PropertiesToLoad.Add("badPwdCount");

        SearchResult v_result = null;

        //Find the individual user
        try
        {
            v_result = v_findUser.FindOne();

            if (v_result != null)
            {
                DirectoryEntry DE = v_result.GetDirectoryEntry();
                ServerLog.Debug("Found user \"" + userID + "\" in \"" + de.Path + "\"");

                string Error = "";
                int BadLogins = -1;
                int BadLoginsAfter = -2;

                if (DE.Properties.Contains("badPwdCount"))
                    BadLogins = Convert.ToInt32(DE.Properties["badPwdCount"].Value.ToString());

                LogonUser(userID, Password, out Error);

                DE.RefreshCache(new string[] { "badPwdCount" });

                if (DE.Properties.Contains("badPwdCount"))
                    BadLoginsAfter = Convert.ToInt32(DE.Properties["badPwdCount"].Value.ToString());

                ServerLog.DebugFormat("Attempting to authenticate user \"{0}\". Bad Logins Before: \"{1}\". Bad Logins After: \"{2}\".", userID, BadLogins, BadLoginsAfter);

                if (BadLogins == BadLoginsAfter)
                {
                    try
                    {
                        //DE.Invoke("SetPassword", new object[] { Password });
                        ServerLog.DebugFormat("Password successfully reset for \"{0}\"", userID);
                        de.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        ServerLog.Error(String.Format("Exception caught while finding user \"{0}\" to reset password", userID), ex);
                        return false;
                    }
                }
                else
                {
                    ServerLog.DebugFormat("Password not reset for \"{0}\", invalid current password", userID);
                    return false;
                }
            }
            else
                ServerLog.Debug("User not found in \"" + de.Path + "\"");
        }
        catch (Exception ex)
        {
            ServerLog.Error("Exception while finding user in \"" + de.Path + "\"", ex);
        }

        de.Close();

        return false;

        //try
        //{
        //    using (var context = new PrincipalContext(ContextType.Domain, null, ConfigurationManager.AppSettings["CorpADAdmin"], AESEncryption.Decrypt(ConfigurationManager.AppSettings["CorpADPassword"], "PingAAA", "SymmetricAESEncryption", "SHA1", 2, "AaAIe2011*FtWL0l", 256)))
        //    {
        //        ServerLog.DebugFormat("Connected to Server: {0}", context.ConnectedServer);
        //        using (var user = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userID))
        //        {
        //            try
        //            {
        //                //if (user.EmployeeId == employeeID)
        //                //{
        //                    string Error = "";
        //                    int BadLogins = -1;
        //                    int BadLoginsAfter = -2;
                            
        //                    DirectoryEntry DE = (DirectoryEntry)user.GetUnderlyingObject();
        //                    if (DE.Properties.Contains("badPwdCount"))
        //                        BadLogins = Convert.ToInt32(DE.Properties["badPwdCount"].Value.ToString());
                            
        //                    LogonUser(userID, Password, out Error);
                            
        //                    DE.RefreshCache(new string[] { "badPwdCount" });
        //                    if (DE.Properties.Contains("badPwdCount"))
        //                        BadLoginsAfter = Convert.ToInt32(DE.Properties["badPwdCount"].Value.ToString());

        //                    ServerLog.DebugFormat("Attempting to authenticate user \"{0}\". Bad Logins Before: \"{1}\". Bad Logins After: \"{2}\".", userID, BadLogins, BadLoginsAfter);
                            
        //                    if (BadLogins == user.BadLogonCount)
        //                    {
        //                        //user.SetPassword(NewPassword);
        //                        ServerLog.DebugFormat("Password successfully reset for \"{0}\"", userID);
        //                        return true;
        //                    }
        //                    else
        //                    {
        //                        ServerLog.DebugFormat("Password not reset for \"{0}\", invalid current password", userID);
        //                        return false;
        //                    }
        //                //}
        //                //else
        //                //{
        //                //    ServerLog.DebugFormat("Password not reset. Employee ID entered \"{0}\" does not match Employee ID in record \"{1}\"", employeeID, user.EmployeeId);
        //                //    return false;
        //                //}
        //            }
        //            catch (Exception ex)
        //            {
        //                ServerLog.Error("Exception caught while resetting password", ex);
        //                return false;
        //            }
        //        }
        //    }
        //}
        //catch (Exception ex)
        //{
        //    ServerLog.Error(String.Format("Exception caught while finding user \"{0}\" to reset password", userID), ex);
        //    return false;
        //}
    }

    /// <summary>
    /// This is a helper method that will return a DateTime of when the account is set to expire
    /// </summary>
    /// <param name="user">A directory entry of the user to get the account expiration for</param>
    /// <returns>A DateTime of when the account is set to expire</returns>
    public static DateTime GetAccountExpiration(DirectoryEntry user)
    {	
	    try
	    {
		    Int64 ldapdatetime = GetInt64(user, "accountExpires");
            if (!ldapdatetime.Equals(Int64.MaxValue))
            {
                ServerLog.Debug("User \"" + user.Username + "\" account expires at" + DateTime.FromFileTime(ldapdatetime));
                DateTime UTC = new DateTime(1601, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                if (DateTime.FromFileTime(ldapdatetime).ToUniversalTime() != UTC)
                {
                    ServerLog.Debug("Account Expires Date Not Equal to " + UTC.ToString() + " : Universal Time: " + DateTime.FromFileTime(ldapdatetime).ToUniversalTime());
                    return DateTime.FromFileTime(ldapdatetime);
                }
                else
                {
                    ServerLog.Debug("Account Expires Date Set to 1/1/1601");
                    return DateTime.Now.AddDays(180);
                }
            }
	    }
	    catch (Exception ex) 
        {
            ServerLog.Error("Exception while determining account expiration. Returning " + DateTime.Now.AddDays(180), ex);
        }
	    
        return DateTime.Now.AddDays(180);
    }

    /// <summary>
    /// This is a helper method that will return a Unicode byte array of the password
    /// </summary>
    /// <param name="password">The password to convert to a byte array</param>
    /// <returns>A byte array of the Unicode password</returns>
    private static byte[] GetPasswordData(string password)
    {
        string formattedPassword = String.Format("\"{0}\"", password);
        return (Encoding.Unicode.GetBytes(formattedPassword));
    }

    /// <summary>
    /// This is a helper method that will return a DateTime of when the password is set to
    /// expire based on the domain security policy. It will throw an error if the user does
    /// not have a password.
    /// </summary>
    /// <param name="user">A DirectoryEntry of the user we are checking the password for</param>
    /// <returns>A DateTime of when the password expires</returns>
    public static DateTime GetPasswordExpiration(DirectoryEntry user)
    {
        long ticks = GetInt64(user, "pwdLastSet");

        //User must change password at next login
        if (ticks == 0 || ticks == -1)
        {
            ServerLog.Debug("User \"" + user.Username + "\" has not set password or must change password at next login");
            return DateTime.MinValue;
        }

        //Get when the user last set their password
        DateTime pwdLastSet = DateTime.FromFileTime(ticks);

        //Use our policy class to determine when it will expire
        try
	    {
            ServerLog.Debug("User \"" + user.Username + "\" password expires at" + pwdLastSet.Add(policy.MaxPasswordAge));
		    return pwdLastSet.Add(policy.MaxPasswordAge);
	    }
	    catch (Exception ex)
	    {
            ServerLog.Error("Exception while determining password expiration. Returning " + pwdLastSet.AddDays(180), ex);
        	return pwdLastSet.AddDays(180);
	    }
    }

    /// <summary>
    /// This is a helper method that will return a TimeSpan of how long
    /// the user has until their password expires.
    /// </summary>
    /// <param name="user">A DirectoryEntry of the user we are checking the password for</param>
    /// <returns>A TimeSpan of the amount of time a user has until password expiration</returns>
    public static TimeSpan GetTimeLeft(DirectoryEntry user)
    {
        DateTime willExpire = GetPasswordExpiration(user);

        if (willExpire == DateTime.MaxValue)
            return TimeSpan.MaxValue;

        if (willExpire == DateTime.MinValue)
            return TimeSpan.MinValue;

        if (willExpire.CompareTo(DateTime.Now) > 0)
        {
            //the password has not expired
            //(pwdLast + MaxPwdAge)- Now = Time Left
            return willExpire.Subtract(DateTime.Now);
        }

        //the password has already expired
        return TimeSpan.MinValue;
    }

    /// <summary>
    /// This is a private helper method that will return a Unicode byte array of the password
    /// </summary>
    /// <param name="entry">The DirectoryEntry that has a connection to the PC LDS.</param>
    /// <param name="attr">The attribute in AD that we would like to get the Int64 from</param>
    /// <returns>An Int64 of the requested value</returns>
    private static Int64 GetInt64(DirectoryEntry entry, string attr)
    {
        //we will use the marshaling behavior of
        //the searcher
        DirectorySearcher ds = new DirectorySearcher(entry, String.Format("({0}=*)", attr), new string[] { attr }, System.DirectoryServices.SearchScope.Base);

        SearchResult sr = ds.FindOne();

        if (sr != null)
        {
            if (sr.Properties.Contains(attr))
            {
                return (Int64)sr.Properties[attr][0];
            }
        }
        return -1;
    }
}
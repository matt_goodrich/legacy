﻿/*******************************************************************************
* Name
*   Domain Policy
*
* Identifier
*   Filename: DomainPolicy.cs
*   
* Purpose
*   This class contains the methods to grab the domain security policy as it 
*   relates to users authenticating.
*
******************************************************************************
*/

using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Web;
using log4net;

[Flags]
public enum PasswordPolicy
{
    DOMAIN_PASSWORD_COMPLEX = 1,
    DOMAIN_PASSWORD_NO_ANON_CHANGE = 2,
    DOMAIN_PASSWORD_NO_CLEAR_CHANGE = 4,
    DOMAIN_LOCKOUT_ADMINS = 8,
    DOMAIN_PASSWORD_STORE_CLEARTEXT = 16,
    DOMAIN_REFUSE_PASSWORD_CHANGE = 32
}

/// <summary>
/// Summary description for DomainPolicy
/// </summary>
public class DomainPolicy
{
    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");

    ResultPropertyCollection attribs;

    /// <summary>
    /// This is the constructor. This is used to query Active Directory 
    /// for the domain security policy. It grabs a collection of attributes 
    /// that determine password policies.
    /// </summary>
    /// <param name="domainRoot">The root at which to search for the policies</param>
    public DomainPolicy(DirectoryEntry domainRoot)
    {
        string[] policyAttributes = new string[] {"maxPwdAge", "minPwdAge", "minPwdLength", "lockoutDuration", "lockOutObservationWindow", "lockoutThreshold", "pwdProperties", "pwdHistoryLength", "objectClass", "distinguishedName"};

        DirectorySearcher ds = new DirectorySearcher(domainRoot, "(objectClass=domainDNS)", policyAttributes, SearchScope.Base);
        SearchResult result;
        try
        {
            result = ds.FindOne();
            this.attribs = result.Properties;
        }
        catch (Exception ex) { ServerLog.Error("Exception while getting domain policy properties", ex); }
    }

    /// <summary>
    /// This is a helper function used to invert numbers.
    /// Intervals are stored as negative numbers.
    /// </summary>
    /// <param name="longInt">The value that needs to be inverted</param>
    /// <returns>The inverted value as a long</returns>
    private long GetAbsValue(object longInt)
    {
        return Math.Abs((long)longInt);
    }

    /// <summary>
    /// This "getter" determines the Maximum password
    /// age as per the password policy
    /// </summary>
    /// <returns>A Timespan representing the maximum password age</returns>
    public TimeSpan MaxPasswordAge
    {
        get
        {
            string val = "maxPwdAge";
            if (this.attribs != null)
            {
                if (this.attribs.Contains(val))
                {
                    long ticks = GetAbsValue(
                      this.attribs[val][0]
                      );

                    if (ticks > 0)
                        return TimeSpan.FromTicks(ticks);
                }
            }

            return TimeSpan.MaxValue;
        }
    }

    /// <summary>
    /// This "getter" returns the "pwdProperties" attribute
    /// from the Domain
    /// </summary>
    /// <returns>An enum representing the password policy</returns>
    public PasswordPolicy PasswordProperties
    {
        get
        {
            string val = "pwdProperties";
            //this should fail if not found
            return (PasswordPolicy)this.attribs[val][0];
        }
    }
}
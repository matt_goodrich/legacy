﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using log4net.Layout.Pattern;

namespace Log4NetExtender
{
    public class IPPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current != null)
            {
                writer.Write(HttpContext.Current.Request.UserHostAddress);
            }
        }
    }

    public class SubjectPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current.Session["Subject"] != null)
            {
                writer.Write(HttpContext.Current.Session["Subject"]);
            }
        }
    }

    public class UrlPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current != null)
                writer.Write(HttpContext.Current.Request.Url.AbsoluteUri);
        }
    }

    public class ConnectionIDPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current.Session["Theme"] != null)
            {
                writer.Write(HttpContext.Current.Session["Theme"]);
            }
        }
    }

    public class MachinePatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            writer.Write(Environment.MachineName);
        }
    }

    public class StatusPatternConverter : PatternLayoutConverter
    {
        protected override void Convert(TextWriter writer, log4net.Core.LoggingEvent loggingEvent)
        {
            if (HttpContext.Current.Session["Status"] != null)
            {
                writer.Write(HttpContext.Current.Session["Status"]);
            }
        }
    }
}
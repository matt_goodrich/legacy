﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/_support/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="prefix_6 grid_12">
        <div class="content">
            <div class="banner">
                <div class="logo"> 
                  <h2><asp:Literal ID="LoginTitle" runat="server"></asp:Literal></h2> 
                </div>
            </div>
            <asp:Panel DefaultButton="LoginButton" runat="server" ID="LoginFormPanel">
                <fieldset>
                    <ol>
                        <li id="uList" runat="server">
                            <label runat="server" id="uLabel">Username:</label>
                            <asp:TextBox ID="userID" runat="server" CssClass="input"></asp:TextBox>
                        </li>
                        <li>
                            <label runat="server" id="pLabel">Password:</label>
                            <asp:TextBox ID="pass" runat="server" TextMode="Password" CssClass="input"></asp:TextBox>  
                        </li>
                        <li>
                            <label runat="server" id="FPLabel">&nbsp;</label>
                            <asp:HyperLink ID="ChangePassLink" runat="server" CssClass="forgot">Change your password?</asp:HyperLink><br />
                            <asp:HyperLink ID="ForgotPassLink" runat="server" CssClass="forgot">Forgot your password?</asp:HyperLink><br />
                            <asp:HyperLink ID="ReturnToHome" runat="server" CssClass="forgot" Visible="false">Return to homepage >></asp:HyperLink>
                        </li>
                        <li id="useADList" runat="server">
                            <asp:CheckBox ID="UseAD" runat="server" Text="Use Active Directory" CssClass="CBL" />
                        </li>
                        <li class="send">
                            <asp:LinkButton ID="LoginButton" CssClass="button primary" runat="server" OnClick="LoginButton_OnClick">Login</asp:LinkButton>                     
                        </li>
                    </ol>
                    <asp:Literal ID="ErrorLit" runat="server"></asp:Literal>
                </fieldset>
            </asp:Panel>            
            <div class="clear"></div>
        </div>
        <div class="footer">
            <asp:Literal ID="FooterInfo" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>


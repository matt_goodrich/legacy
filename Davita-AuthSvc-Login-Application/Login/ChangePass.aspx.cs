﻿#region usings

    using System;
    using System.Collections.Generic;
    using System.DirectoryServices;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using log4net;

#endregion

/// <summary>
/// Code file for the ChangePass page.
/// </summary>
public partial class ChangePass : BasePage
{
    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");
    private static readonly ILog AuditLog = LogManager.GetLogger("AuditLog");

    public bool ErrorThrown = false;
    public bool Expired = false;

    #region ErrorCodes
    public const string UsernameRequired = "Please provide a username.";
    public const string PasswordRequired = "Please provide your current password.";
    public const string EmployeeIDRequired = "Please provide your employee ID.";
    public const string NewPasswordRequired = "Please provide your new password.";
    public const string ConfirmPasswordRequired = "Please confirm your new password.";
    public const string PasswordPolicy = "Password does not meet password policy.";
    public const string PasswordChangeFailed = "There was a problem encountered while changing your password.";
    public const string PasswordContainsUsername = "Password cannot contain your username.";
    public const string PasswordsDoNotMatch = "New password and confirmed password do not match.";
    public const string PasswordHasNotChanged = "New password cannot be the same as your current password.";
    public const string AcctExpired = "This account has expired (code 4444)";
    public const string AcctDisabled = "This account has been disabled (code 44444)";
    public const string AcctLocked = "Too many login attempts; please try again later (code 222)";
    #endregion

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        //Add CSS style to Master Form
        this.Page.Form.Attributes.Add("class", "wide");

        //Load text for theme
        Themes ThemeText = new Themes(Page.Theme);
        LoginTitle.Text = ThemeText.LoginTitleText;
        FooterInfo.Text = ThemeText.FooterInfoText;
        FooterInfo.Text += ThemeText.CopyText;

        if (!IsPostBack)
        {
            //Populate the username into the textbox
            if (Request.QueryString["Username"] != null)
            {
                userID.Text = Request.QueryString["Username"];
            }
        }
        if (Request.QueryString["Expired"] != null)
        {
            if (Request.QueryString["Expired"] == "true")
            {
                //pLabel.InnerText = "Employee ID";
                Expired = true;
            }
        }
        
    }

    /// <summary>
    /// Handles the OnClick event of the LoginButton control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    protected void LoginButton_OnClick(object sender, EventArgs e)
    {
        bool Success = false;

        if (String.IsNullOrEmpty(userID.Text))
            ThrowError(UsernameRequired);
        if (String.IsNullOrEmpty(pass.Text))
            ThrowError((Expired ? EmployeeIDRequired : PasswordRequired));
        if (String.IsNullOrEmpty(newPass.Text))
            ThrowError(NewPasswordRequired);
        if (String.IsNullOrEmpty(confirmPass.Text))
            ThrowError(ConfirmPasswordRequired);

        string strRegex = "(?=^[A-Za-z])(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[_\\-~`^!@.=]{0,})(?=^[^ ]*$).{6,30}$";
        Regex re = new Regex(strRegex);

        if (re.IsMatch(newPass.Text))
        {
            if (newPass.Text.Equals(confirmPass.Text))
            {
                if (!newPass.Text.Contains(userID.Text))
                {
                    if (!pass.Text.Equals(newPass.Text))
                    {
                        if (!ErrorThrown)
                        {
                            if (Request.QueryString["Expired"] != null && Request.QueryString["Expired"] == "true")
                                Success = ADAuth.ResetPassword(userID.Text, newPass.Text, pass.Text);
                            else
                                Success = ADAuth.ChangePassword(userID.Text, pass.Text, newPass.Text);
                        }
                    }
                    else
                        ThrowError(PasswordHasNotChanged);
                }
                else
                    ThrowError(PasswordContainsUsername);
            }
            else
                ThrowError(PasswordsDoNotMatch);
        }
        else
            ThrowError(PasswordPolicy);

        if (Success)
        {
            List<string> properties = new List<string>();  
            string error = "";

            //Find the user
            SearchResult v_result = ADAuth.FindUser(userID.Text, properties, out error);

            //If we returned an error
            if (!String.IsNullOrEmpty(error))
                ThrowError(error);

            //If the user was found
            if (v_result != null)
            {
                //Get The Directory Entry of the found user
                DirectoryEntry user = v_result.GetDirectoryEntry();


                //Check for Expired Account
                DateTime AcctExpire = ADAuth.GetAccountExpiration(user);
                if (DateTime.Compare(AcctExpire, DateTime.Now) <= 0)
                    ThrowError(AcctExpired);

                int uac = Convert.ToInt32(v_result.Properties["userAccountControl"][0]);

                const int ADS_UF_ACCOUNTDISABLE = 0x00000002;
                const int ADS_UF_LOCKOUT = 0x00000010;

                //Determine if Account is Disabled
                bool accountIsDisabled = (uac & ADS_UF_ACCOUNTDISABLE) == ADS_UF_ACCOUNTDISABLE;
                //Determine if Account is Locked
                bool accountIsLockedOut = (uac & ADS_UF_LOCKOUT) == ADS_UF_LOCKOUT;

                //Throw Error is Disabled
                if (accountIsDisabled)
                    ThrowError(AcctDisabled);

                //Throw Error if Locked
                if (accountIsLockedOut)
                    ThrowError(AcctLocked);
            }
            if (!ErrorThrown)
            {
                if (Request.QueryString["ReturnUrl"] != null)
                {
                    if (Request.QueryString["resume"] != null)
                    {
                        FormsAuthenticationTicket tkt;
                        string cookiestr;
                        HttpCookie ck;
                        //Generate a new ticket
                        tkt = new FormsAuthenticationTicket(1, userID.Text, DateTime.Now,
                            DateTime.Now.AddHours(8), false, "");
                        //Encrypt the cookie string
                        cookiestr = FormsAuthentication.Encrypt(tkt);
                        //Create a new cookie
                        ck = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);
                        ck.Path = FormsAuthentication.FormsCookiePath;
                        ck.Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf("."));
                        //Add the cookie
                        Response.Cookies.Add(ck);

                        ServerLog.Debug("Creating Forms Authentication Ticket and adding Cookie");

                        Response.Redirect(String.Format("{0}&resume={1}", Request.QueryString["ReturnUrl"], Request.QueryString["resume"]));
                    }
                    else
                        Response.Redirect(Request.QueryString["ReturnUrl"]);
                }
            }
        }
        else
            ThrowError(PasswordChangeFailed);
    }

    #region Modify UI
    /// <summary>
    /// This function sets the error message to display in the UI and then returns to render it
    /// </summary>
    /// <param name="ErrorText">The text to be rendered as an Error</param>
    public void ThrowError(string ErrorText)
    {
        ServerLog.DebugFormat("User Notified: " + ErrorText);
        if (!ErrorThrown)
        {
            ErrorLit.Text = String.Format("<div id=\"fail\"><span class=\"ico_cancel\">{0}</span></div>", ErrorText);
        }
        ErrorThrown = true;
        return;
    }
    #endregion
}
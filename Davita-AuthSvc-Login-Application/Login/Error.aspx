﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_support/MasterPage.master" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="prefix_6 grid_12">
        <div class="content">
            <div class="banner">
                <div class="logo"> 
                  <h2>Login Error</h2> 
                </div>
            </div>
            <fieldset style="border: none;">
                <ol>
                    <li>
                        <p>An error has occurred. Please contact the help desk to resolve the issue.</p>            
                    </li>
                </ol>
            </fieldset>
            <div class="clear"></div>
        </div>
    </div>
</asp:Content>


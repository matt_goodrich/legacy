﻿/*******************************************************************************
* Name
*   Login Page Code Behind
*
* Identifier
*   Filename: Default.aspx.cs
*   
* Purpose
*   This class contains the methods to provide forms based authentication
*   against Active Directory.
*
******************************************************************************
*/

//Microsoft Libraries
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.DirectoryServices;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

//External Libraries from PingIdentity
using Com.PingIdentity.PingFederate.SampleApp.SampleAppUtility;
using opentoken;
using opentoken.util;
using log4net;

public partial class _Default : BasePage
{
    //Logs
    private static readonly ILog ServerLog = LogManager.GetLogger("ServerLog");
    private static readonly ILog AuditLog = LogManager.GetLogger("AuditLog");

    Themes AppTheme;

    protected bool sessionExists = false;
    public Links samlLinks;
    public bool ErrorThrown = false;

    #region ErrorCodes
    public const string NoResource = "No resource requested. Please return to the application you were attempting to access and try again.";
    public const string NoUserId = "Please Provide Your Username (code 02)";
    public const string NoPass = "Please Provide Your Password (code 03)";
    public const string AcctExpired = "This account has expired (code 4444)";
    public const string PassExpired = "Please update your password now (code 7777)";
    public const string PassExprToday = "Your password expires today (code 885)";
    public const string ChangePassNow = "Please update your password now (code 884)";
    public const string AcctDisabled = "This account has been disabled (code 44444)";
    public const string AcctLocked = "Too many login attempts; please try again later (code 222)";
    public const string InvalidCred = "The username or password you entered is incorrect (code 04)";
    #endregion
 
    #region Page_Load
    /// <summary>
    /// This function is run on load of the page including post backs. 
    /// This method is being used to prevent the page from being cached,
    /// loading the needed configuration information, determining if the
    /// current user has already been authenticated, if the user is authenticated
    /// they will be redirected to the requested resource.
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        AppTheme = new Themes(Page.Theme);
        //get properties from Idp config file
        String idpConfig = ConfigurationManager.AppSettings["config-file"];
        String configPath = Context.Server.MapPath("config/" + idpConfig);
        
        samlLinks = new Links(Context, configPath);

	    ErrorThrown = false;

        //Add CSS style to Master Form
        this.Page.Form.Attributes.Add("class", "wide");

        if (!Page.IsPostBack)
        {
            userID.Focus();
        }

        //Load the Forgot Password Link from the Config
        ForgotPassLink.NavigateUrl = ConfigurationManager.AppSettings["PasswordResetURL"];

        if (Convert.ToBoolean(ConfigurationManager.AppSettings["SkipAD"]))
            useADList.Visible = true;
        else
        {
            useADList.Visible = false;
            UseAD.Checked = true;
        }

        //Load any Theme Independent Text now that the page is being rendered
        LoadText();

        //Check for the username that may have been saved into a cookie earlier
        if (Request.Cookies["LoginUName"] != null)
        {
            if (Request.Cookies["LoginUName"].Value.Trim().Length > 0)
            {
                ServerLog.Debug("Username found in request: " + Request.Cookies["LoginUName"].Value);
                //Populate the username into the Username Field
                userID.Text = Request.Cookies["LoginUName"].Value;
                //Hide the username label and input
                uLabel.Attributes.Add("style", "display:none;");
                userID.Attributes.Add("style", "display:none;");
                uList.Attributes.Add("style", "display:none;");

                if (!String.IsNullOrEmpty(AppTheme.ReturnToHomeBehavior))
                {
                    ReturnToHome.Visible = true;
                    ReturnToHome.NavigateUrl = AppTheme.ReturnToHomeBehavior;
                }
            }
        }

        //Check if we have a valid returnUrl, if not display an error.
        string strRedirect = Request.QueryString["ReturnUrl"];
        if (strRedirect == null) //If no resouce was requested
        {
            //Hide the form
            uLabel.Visible = false;
            userID.Visible = false;
            pLabel.Visible = false;
            pass.Visible = false;
            LoginButton.Visible = false;
            ForgotPassLink.Visible = false;
            ChangePassLink.Visible = false;
            ReturnToHome.Visible = false;
            useADList.Visible = false;
            
            //Generate an error message and return
            ErrorLit.Text = String.Format("<div id=\"warning\"><span class=\"ico_error\">{0}</span></div>", NoResource);
            return;
        }

        ChangePassLink.NavigateUrl = String.Format("ChangePass.aspx?ReturnUrl={0}&Username={1}", strRedirect, userID.Text);

        //Check if a curent session exists
        if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
        {
            ServerLog.Debug("User authentication still valid, redirecting");
            string cookiestr;
            //Get the name of the Forms Auth Cookie (Set in Web.Config)
            cookiestr = Request.Cookies[FormsAuthentication.FormsCookieName].Value;
            //Decrypt the Forms Auth Cookie and save it into a ticket
            FormsAuthenticationTicket tkt = FormsAuthentication.Decrypt(cookiestr);
            //Add the ticket to the session
            Session.Add("FORMAUTHTICKET", tkt);
            sessionExists = true;
            
            //User has already been authenticated, send them back to the SSO Server
            if (Request.QueryString["ReturnUrl"] != null && !Request.QueryString["ReturnUrl"].Contains("config.aspx"))
                Response.Redirect(Request.QueryString["ReturnUrl"], true);
            else if (Request.QueryString["ReturnUrl"].Contains("config.aspx"))
                Response.Redirect("NotAuthorized.aspx?ReturnUrl=" + Request.QueryString["ReturnUrl"], true);
        }
    }
    #endregion

    #region Login
    /// <summary>
    /// This function is run on the click of the "Login" button.
    /// This function begins by verifying the user ID and password
    /// has a valid length (> 0), returning an error if not. We 
    /// then determine what properties we would like retrieved from 
    /// Active Directory, then attempt to find the user. If the is 
    /// found, we authenticate them and if the authentication succeeds 
    /// we generate a cookie and redirect them to the requested resource. 
    /// If the user is not found we return an error message.
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void LoginButton_OnClick(object sender, EventArgs e)
    {
        //Setup some variables
        List<string> properties = new List<string>();  
        bool Authd = false;
        string error = "";

        //Declare Ping Data strucutre
        MultiStringDictionary userInfo = null;
       
        //Check that the user has entered a user ID
        if (String.IsNullOrEmpty(userID.Text))
            ThrowError(NoUserId);

        //Check that the user has entered a password
        if (String.IsNullOrEmpty(pass.Text))
            ThrowError(NoPass);            

        //Find the user
        SearchResult v_result = ADAuth.FindUser(userID.Text, properties, out error);

        //If we returned an error
        if (!String.IsNullOrEmpty(error))
            ThrowError(error);

        //If the user was found
        if (v_result != null)
        {
            //Get The Directory Entry of the found user
            DirectoryEntry user = v_result.GetDirectoryEntry();

            if (UseAD.Checked)
            {
                //Check for Expired Account
                DateTime AcctExpire = ADAuth.GetAccountExpiration(user);
                if (DateTime.Compare(AcctExpire, DateTime.Now) <= 0)
                    ThrowError(AcctExpired);

                //If the user has already been notified of password expiration
                if (Session["Skip"] == null)
                {
                    //Store the total Days left before password expiration
                    double daysLeft = Convert.ToDouble(ADAuth.GetTimeLeft(user).TotalDays);

                    ServerLog.Debug("Days left before password expiration for " + userID.Text + ": " + daysLeft);

                    //If password has already expired, ensure we do not return a bad value
                    if (daysLeft < 0)
                        daysLeft = 0;

                    //Determine if we are within the period of warning the user of looming password expiration
                    if (daysLeft <= Convert.ToInt32(ConfigurationManager.AppSettings["PasswordExpireReminderDays"]))
                    {
                        //If password has expired
                        if (daysLeft == 0)
                        {
                            LogHelper.WriteToSession("Subject", userID.Text);
                            LogHelper.WriteToSession("Status", "Failure");
                            AuditLog.Debug("");
                            ChangePassLink.NavigateUrl += (ChangePassLink.NavigateUrl.Contains("&Expired=true") ? "" : "&Expired=true");
                            ThrowError(PassExpired);
                        }
                        else if (daysLeft > 0 && daysLeft <= 1) //Password expires today
                            ThrowWarning(PassExprToday, true);
                        else //Password has not expired, but will soon
                            ThrowWarning(String.Format("Your password expires in {0} days (code 888).", Convert.ToInt32(daysLeft)), true);
                    }
                }

                //Get the date of the password expiration
                DateTime PwdExpire = ADAuth.GetPasswordExpiration(user);

                //Check if the password expired more than 180 days in the past
                if (DateTime.Compare(PwdExpire, DateTime.Now.AddDays(-180)) <= 0)
                {
                    ChangePassLink.NavigateUrl += (ChangePassLink.NavigateUrl.Contains("&Expired=true") ? "" : "&Expired=true");
                    ThrowError(ChangePassNow);
                }

                int uac = Convert.ToInt32(v_result.Properties["userAccountControl"][0]);

                const int ADS_UF_ACCOUNTDISABLE = 0x00000002;
                const int ADS_UF_LOCKOUT = 0x00000010;

                //Determine if Account is Disabled
                bool accountIsDisabled = (uac & ADS_UF_ACCOUNTDISABLE) == ADS_UF_ACCOUNTDISABLE;
                //Determine if Account is Locked
                bool accountIsLockedOut = (uac & ADS_UF_LOCKOUT) == ADS_UF_LOCKOUT;

                //Throw Error is Disabled
                if (accountIsDisabled)
                    ThrowError(AcctDisabled);

                //Throw Error if Locked
                if (accountIsLockedOut)
                    ThrowError(AcctLocked);
            }


            //Attempt to Authenticate the user
            if (!ErrorThrown)
            {
                if (UseAD.Checked)
                    Authd = ADAuth.LogonUser(v_result.Properties["sAMAccountName"][0].ToString(), pass.Text, out error);
                else
                {
                    Authd = true;
                    LogHelper.WriteToSession("Subject", userID.Text);
                    LogHelper.WriteToSession("Status", "Success");
                    AuditLog.Debug("");
                }
            }

            //Determine if we encountered an error during Authentication
            if (!String.IsNullOrEmpty(error))
                ThrowError(error);

            //If the user's credentials were correct, populate the MultiStringDictionary with the required attributes
            if (Authd)
            {
                ServerLog.Debug("User \"" + userID.Text + "\" successfully authenticated " + (UseAD.Checked ? "with password" : "without password"));
                userInfo = new MultiStringDictionary();
                userInfo.Add("authncontext", "PASSWORD");
                userInfo.Add("subject", v_result.Properties["sAMAccountName"][0].ToString());


                //Add the user information to the session
                Session.Add(Constants.USER_INFO, userInfo);
                FormsAuthenticationTicket tkt;
                string cookiestr;
                HttpCookie ck;
                //Generate a new ticket
                tkt = new FormsAuthenticationTicket(1, userID.Text, DateTime.Now,
                    DateTime.Now.AddHours(8), false, "");
                //Encrypt the cookie string
                cookiestr = FormsAuthentication.Encrypt(tkt);
                //Create a new cookie
                ck = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);
                ck.Path = FormsAuthentication.FormsCookiePath;
                ck.Domain = Request.Url.Host.Substring(Request.Url.Host.IndexOf("."));
                //Add the cookie
                Response.Cookies.Add(ck);
                ServerLog.Debug("Creating Forms Authentication Ticket and adding Cookie");

                //Redirect to the requested resource
                string strRedirect;
                strRedirect = Request.QueryString["ReturnUrl"];
                if (strRedirect == null) //If no resouce was requested
                    ThrowWarning(NoResource, false);

                HttpCookie cookie = new HttpCookie("LoginUName");
                cookie.Expires = DateTime.Now.AddMinutes(-1);
                Response.Cookies.Add(cookie);

                //Redirect to the requested resource
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["ShowBanner"]))
                {
                    if(AppTheme.RedirectToBannerBehavior)
                        Response.Redirect(String.Format("Loading.aspx?UserID={0}&Redirect={1}", userID.Text, HttpUtility.UrlEncode(strRedirect)));
                    else
                        Response.Redirect(strRedirect, true);
                }
                else
                    Response.Redirect(strRedirect, true);
            }
            else //Authentication Failed
                ThrowError(InvalidCred);
        }
        else //We didn't find the user
        {
            LogHelper.WriteToSession("Subject", userID.Text);
            LogHelper.WriteToSession("Status", "Failure");
            AuditLog.Debug("");
            ServerLog.Debug("User \"" + userID.Text + "\" not found");
            ThrowError(InvalidCred);
        }
         
    }
    #endregion

    #region Modify UI
    /// <summary>
    /// This function applies theme specific text to the page. 
    /// The text is stored in the Text.xml file associated with the current theme
    /// </summary>
    protected void LoadText()
    {
        LoginTitle.Text = AppTheme.LoginTitleText;
        FooterInfo.Text = AppTheme.FooterInfoText;
        FooterInfo.Text += AppTheme.CopyText;
        LoginButton.Text = AppTheme.LoginButtonText;
    }

    /// <summary>
    /// This function sets the error message to display in the UI and then returns to render it
    /// </summary>
    /// <param name="ErrorText">The text to be rendered as an Error</param>
    public void ThrowError(string ErrorText)
    {
        ServerLog.DebugFormat("User Notified: " + ErrorText);
        if (!ErrorThrown)
        {
            ErrorLit.Text = String.Format("<div id=\"fail\"><span class=\"ico_cancel\">{0}</span></div>", ErrorText);
        }
	    ErrorThrown = true;
        return;
    }

    /// <summary>
    /// This function sets the warning message to display in the UI and then returns to render it
    /// </summary>
    /// <param name="ErrorText">The text to be rendered as an Error</param>
    public void ThrowWarning(string WarnText, bool SkipSecond)
    {
        ServerLog.DebugFormat("User Notified: " + WarnText);
        //Allows user to re-authenticate after notice of their password nearing expiration
        if (SkipSecond)
            Session["Skip"] = true;
        if (!ErrorThrown)
        {
            ErrorLit.Text = String.Format("<div id=\"warning\"><span class=\"ico_error\">{0}</span></div>", WarnText);
        }
	    ErrorThrown = true;
        return;
    }
    #endregion
}
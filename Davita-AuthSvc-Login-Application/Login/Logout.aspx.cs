﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Logout : System.Web.UI.Page
{

    #region PreInit
    /// <summary>
    /// This function is the first method to be executed during the page load.
    /// In order to apply the correct theme before the page is rendered, we must
    /// apply the theme early in the page lifecycle. 
    /// </summary>
    /// <param name="sender">Unused in this context, but required as per ASP.NET rules</param>
    /// <param name="e">Unused in this context, but required as per ASP.NET rules</param>
    protected void Page_PreInit(object sender, EventArgs e)
    {
        //Always use Default Theme on this page
        Page.Theme = "Default";
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //Add CSS style to Master Form
        this.Page.Form.Attributes.Add("class", "wide");
    }
}
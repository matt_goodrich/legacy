﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;

public class PFError
{
    public DateTime ErrorTime { get; set; }
    public string TID { get; set; }
    public string ErrorText { get; set; }

    public void ParseErrorText(string ErrorString)
    {
        Regex errorParser = new Regex(@"(?<date>(\d|\-)+)\s(?<time>(\d|\:|,)+)\stid:(?<tid>\S+)\sERROR\s(?<errormessage>\S.+)\s");
        Match match = errorParser.Match(ErrorString);
        if (match.Length >= 2)
        {
            this.ErrorText = match.Groups["errormessage"].Value.Trim();
            this.TID = match.Groups["tid"].Value.Trim();
            this.ErrorTime = Convert.ToDateTime(String.Format("{0} {1}", match.Groups["date"].Value, match.Groups["time"].Value.Substring(0, match.Groups["time"].Value.IndexOf(","))));
        }
        else
        {
            errorParser = new Regex(@"(?<date>(\d|\-)+)\s(?<time>(\d|\:|,)+)\sERROR\s(?<errormessage>\S.+)\s");
            match = errorParser.Match(ErrorString);
            if (match.Length >= 2)
            {
                this.ErrorText = match.Groups["errormessage"].Value.Trim();
                this.TID = "NONE";
                this.ErrorTime = Convert.ToDateTime(String.Format("{0} {1}", match.Groups["date"].Value, match.Groups["time"].Value.Substring(0, match.Groups["time"].Value.IndexOf(","))));
            }
        }
    }
}

public class PFServerLogs
{
    public string LogFileName { get; set; }
    public List<PFError> Errors = new List<PFError>();

    public PFServerLogs(string LogFileName)
    {
        this.LogFileName = LogFileName;
    }

    public void AddError(string ErrorString)
    {
        PFError error = new PFError();
        error.ParseErrorText(ErrorString);

        Errors.Add(error);
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page 
{
    public List<PFServerLogs> Logs = new List<PFServerLogs>();

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Write(String.Format("Starting: {0:F}<br />", DateTime.Now));
        Response.Flush();

        string[] filePaths = Directory.GetFiles(@"D:\Library\Downloads\server.log", "server.log*");

        foreach (string str in filePaths)
        {
            PFServerLogs Log = new PFServerLogs(str);
            Response.Write(str + "<br />");
            Response.Flush();
            StreamReader reader = new StreamReader(str);
            string content = reader.ReadToEnd();
            reader.Close();

            Regex error = new Regex(@".+ERROR.+");
            MatchCollection errorMatches = error.Matches(content);
            foreach (Match m in errorMatches)
            {
                Log.AddError(m.Value);
                Response.Write(m.Value + "<br />");
                Response.Flush();
            }
        }

        Response.Write(String.Format("Finished: {0:F}", DateTime.Now));
        Response.Flush();

        GenerateTable();
    }

    protected void GenerateTable()
    {
        int NTLMErrorCount = 0;
        int BlazeAuthLevelCount = 0;
        int RequestProcessingCount = 0;
        int OtherErrorCount = 0;

        foreach (PFServerLogs log in Logs)
        {
            foreach (PFError error in log.Errors)
            {
                switch (error.ErrorText)
                {
                    case "[com.pingidentity.adapters.iwa.idp.IWAAuthenticationAdapter] NTLM Authentication Failed":
                        NTLMErrorCount++;
                        break;
                    case "[org.sourceid.saml20.domain.AttributeMapping] Problem with OGNL attribute mapping on blazeauthlevelauto.":
                        BlazeAuthLevelCount++;
                        break;
                    case "[org.sourceid.saml20.profiles.idp.HandleAuthnRequest] Exception occurred during request processing":
                        RequestProcessingCount++;
                        break;
                    default:
                        OtherErrorCount++;
                        break;
                }
            }
        }

        TableHeaderRow THR = new TableHeaderRow();
        TableHeaderCell THC1 = new TableHeaderCell();
        TableHeaderCell THC2 = new TableHeaderCell();
        
        THC1.Text = "Error";
        THC2.Text = "Count";
        
        THR.Cells.Add(THC1);
        THR.Cells.Add(THC2);
        
        StatsTable.Rows.Add(THR);

        TableRow row = new TableRow();

        TableCell NTLMError = new TableCell();
        NTLMError.Text = "[com.pingidentity.adapters.iwa.idp.IWAAuthenticationAdapter] NTLM Authentication Failed";
        row.Cells.Add(NTLMError);

        TableCell NTLMCount = new TableCell();
        NTLMCount.Text = NTLMErrorCount.ToString();
        row.Cells.Add(NTLMCount);

        StatsTable.Rows.Add(row);
        row = new TableRow();

        TableCell BlazeAuthError = new TableCell();
        BlazeAuthError.Text = "[org.sourceid.saml20.domain.AttributeMapping] Problem with OGNL attribute mapping on blazeauthlevelauto.";
        row.Cells.Add(BlazeAuthError);

        TableCell BlazeAuthCount = new TableCell();
        BlazeAuthCount.Text = BlazeAuthLevelCount.ToString();
        row.Cells.Add(BlazeAuthCount);

        StatsTable.Rows.Add(row);
        row = new TableRow();
        
        TableCell RequestProcError = new TableCell();
        RequestProcError.Text = "[org.sourceid.saml20.profiles.idp.HandleAuthnRequest] Exception occurred during request processing";
        row.Cells.Add(RequestProcError);

        TableCell RequestProcCount = new TableCell();
        RequestProcCount.Text = RequestProcessingCount.ToString();
        row.Cells.Add(RequestProcCount);

        StatsTable.Rows.Add(row);
        row = new TableRow();

        TableCell OtherError = new TableCell();
        OtherError.Text = "Others";
        row.Cells.Add(OtherError);

        TableCell OtherCount = new TableCell();
        OtherCount.Text = OtherErrorCount.ToString();
        row.Cells.Add(OtherCount);

        StatsTable.Rows.Add(row);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.IO;
using System.Text;
using Excel;

namespace ADSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream stream = File.Open("C:\\Users.xls", FileMode.Open, FileAccess.Read);
            IExcelDataReader xlsReader = ExcelReaderFactory.CreateBinaryReader(stream);
            DataSet result = xlsReader.AsDataSet();

            xlsReader.Close();
            stream.Close();

            using (StreamWriter file = new StreamWriter("C:\\UsersNew.csv"))
            {
                foreach (DataRow row in result.Tables[0].Rows)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        file.Write(row[i] + ",");
                    }
                    Console.WriteLine("Searching for \"" + row[0] + "\"");
                    file.Write(AD.GetUserID(row[0].ToString()) + Environment.NewLine);
                }
            }
            Console.ReadLine();
        }
    }

    class AD
    {
        public static string GetUserID(string employeeID)
        {
            DirectoryEntry root = new DirectoryEntry("LDAP://p01iaw001.ent.rt.csaa.com", "gergood@ent.rt.csaa.com", "WTF123@@");
            DirectorySearcher searcher = new DirectorySearcher(root, "(employeeNumber=" + employeeID + ")");
            searcher.PropertiesToLoad.Add("samaccountname");

            SearchResult result = null;

            try
            {
                result = searcher.FindOne();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return result.Properties["samaccountname"][0].ToString();
        }
    }
}

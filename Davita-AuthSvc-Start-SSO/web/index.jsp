<%-- 
    Document   : index
    Created on : Mar 9, 2012, 11:50:26 AM
    Author     : Matt
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%

    String host = request.getServerName();
    String SPID = "";
    String ACSIdx = "";
    
    String Username = "";
    if(request.getParameter("username") != null) {
        Username = request.getParameter("username");
        Cookie cookie = new Cookie("LoginUName", Username);
        cookie.setMaxAge(60);
        cookie.setPath("/");
        cookie.setDomain(".davita.com");
        response.addCookie(cookie);
    }
    
    String BuiltURL = "";
    
    if(request.getParameter("PartnerSpId") != null) {
        SPID = request.getParameter("PartnerSpId");
        BuiltURL += "PartnerSpId=" + SPID;
    }
    if(request.getParameter("ACSIdx") != null) {
        ACSIdx = request.getParameter("ACSIdx");
        BuiltURL += "&ACSIdx=" + ACSIdx;
    }
        
    response.sendRedirect("https://" + host + ":9031/idp/startSSO.ping?" + BuiltURL);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Start SSO</title>
    </head>
    <body>
    </body>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Excel;
using System.IO;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    public string loginChartString = "";
    public string loginChartString2 = "";

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_OnClick(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            string ticks = DateTime.Now.Ticks.ToString();
            FileUpload1.SaveAs(Server.MapPath("~/App_Data/") + ticks + FileUpload1.FileName);
            ParseFile(Server.MapPath("~/App_Data/") + ticks + FileUpload1.FileName);
        }
    }

    public void ParseFile(string filePath)
    {
        FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

        IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(stream);

        DataSet result = excelReader.AsDataSet();

        excelReader.Close();
        stream.Close();

        int idpRecords = 0;
        int spRecords = 0;

        int failCount = 0;

        int salesXCount = 0;
        int salesXSPCount = 0;

        int PASAdminCount = 0;
        int PASAdminSPCount = 0;

        int PASAppCount = 0;
        int PASAppSPCount = 0;

        int PASAppNoSvcCount = 0;
        int PASAppNoSvcSPCount = 0;

        int PASOpCount = 0;
        int PASOpSPCount = 0;

        int WebEndoCount = 0;
        int WebEndoSPCount = 0;

        int CUICount = 0;
        int CUISPCount = 0;

        int netPOSCount = 0;
        int netPOSSPCount = 0;

        int SalesForceCount = 0;

        List<DateTime> attemptTimes = new List<DateTime>();

        Literal1.Text = "<h1>" + result.Tables[0].TableName + "</h1>";

        foreach (DataRow row in result.Tables[0].Rows)
        {
            if (row[9].ToString().Trim().Equals("failure"))
                failCount++;

            switch (row[8].ToString().Trim())
            {
                case "IdP":
                    idpRecords++;
                    attemptTimes.Add(Convert.ToDateTime(row[0].ToString().Substring(0, row[0].ToString().Length - 4)));
                    switch (row[4].ToString().Trim())
                    {
                        case "https://salesx.csaaquote.com/agent/SSO":
                            row[4] = "SalesX";
                            salesXCount++;
                            break;
                        case "https://pas-prod-exigen:443/aaa-admin/do_auth":
                            row[4] = "PAS(Admin)";
                            PASAdminCount++;
                            break;
                        case "https://pas-prod-exigen.ent.rt.csaa.com:443/aaa-app/do_auth":
                        case "https://pas-prod-exigen:443/aaa-app/do_auth":
                            row[4] = "PAS(App)";
                            if (row[2].ToString().Trim() != "svc_healthchecks")
                                PASAppNoSvcCount++;
                            PASAppCount++;
                            break;
                        case "https://pas-prod-exigen.ent.rt.csaa.com:443/operational-reports-app/do_auth":
                            row[4] = "PAS(Op Reports)";
                            PASOpCount++;
                            break;
                        case "https://cssmcweblw71v.ent.rt.csaa.com:8443/TPresentation/SFDCGateway":
                            row[4] = "CUI";
                            CUICount++;
                            break;
                        case "https://newbusiness.westernunited.com/MulticoRater/Agent/SFDCIntegration.aspx":
                            row[4] = "netPOSitive";
                            netPOSCount++;
                            break;
                        case "https://www.westernunited.com/Endorsements/SFDCLoginAction.do":
                            row[4] = "WebEndo";
                            WebEndoCount++;
                            break;
                        case "":
                            row[4] = "SalesForce";
                            SalesForceCount++;
                            break;
                        default:
                            Response.Write("UNKNOWN: " + row[4] + "<br />");
                            break;
                    }
                    break;
                case "SP":
                    spRecords++;
                    switch (row[4].ToString().Trim())
                    {
                        case "https://salesx.csaaquote.com/agent/SSO":
                            row[4] = "SalesX";
                            salesXSPCount++;
                            break;
                        case "https://pas-prod-exigen:443/aaa-admin/do_auth":
                            row[4] = "PAS(Admin)";
                            PASAdminSPCount++;
                            break;
                        case "https://pas-prod-exigen.ent.rt.csaa.com:443/aaa-app/do_auth":
                        case "https://pas-prod-exigen:443/aaa-app/do_auth":
                            row[4] = "PAS(App)";
                            if (row[2].ToString().Trim() != "svc_healthchecks")
                                PASAppNoSvcSPCount++;
                            PASAppSPCount++;
                            break;
                        case "https://pas-prod-exigen.ent.rt.csaa.com:443/operational-reports-app/do_auth":
                            row[4] = "PAS(Op Reports)";
                            PASOpSPCount++;
                            break;
                        case "https://cssmcweblw71v.ent.rt.csaa.com:8443/TPresentation/SFDCGateway":
                            row[4] = "CUI";
                            CUISPCount++;
                            break;
                        case "https://newbusiness.westernunited.com/MulticoRater/Agent/SFDCIntegration.aspx":
                            row[4] = "netPOSitive";
                            netPOSSPCount++;
                            break;
                        case "https://www.westernunited.com/Endorsements/SFDCLoginAction.do":
                            row[4] = "WebEndo";
                            WebEndoSPCount++;
                            break;
                        case "":
                            row[4] = "SalesForce";
                            SalesForceCount++;
                            break;
                        default:
                            Response.Write("UNKNOWN: " + row[4] + "<br />");
                            break;
                    }
                    break;
            }
            
        }

        Literal1.Text += "<p><strong>Total Records:</strong>" + result.Tables[0].Rows.Count + "</p>";
        Literal1.Text += "<p><strong>Total IdP Records: </strong>" + idpRecords + "</p>";
        Literal1.Text += "<p><strong>Total SP Records: </strong>" + spRecords + "</p>";
        Literal1.Text += "<p><strong>Total Failures: </strong>" + failCount + "</p><br />";
        Literal1.Text += "<p><strong>SalesX Access Count (IdP): </strong>" + salesXCount + "</p>";
        Literal1.Text += "<p><strong>SalesX Access Count (SP): </strong>" + salesXSPCount + "</p><br />";
        Literal1.Text += "<p><strong>PAS (Admin) Access Count (IdP): </strong>" + PASAdminCount + "</p>";
        Literal1.Text += "<p><strong>PAS (Admin) Access Count (SP): </strong>" + PASAdminSPCount + "</p><br />";
        Literal1.Text += "<p><strong>PAS (App) Access Count (IdP): </strong>" + PASAppCount + "</p>";
        Literal1.Text += "<p><strong>PAS (App) Access Count (SP): </strong>" + PASAppSPCount + "</p><br />";
        Literal1.Text += "<p><strong>PAS (App - No Svc) Access Count (IdP): </strong>" + PASAppNoSvcCount + "</p>";
        Literal1.Text += "<p><strong>PAS (App - No Svc) Access Count (SP): </strong>" + PASAppNoSvcSPCount + "</p><br />";
        Literal1.Text += "<p><strong>PAS (Op Reports) Access Count (IdP): </strong>" + PASOpCount + "</p>";
        Literal1.Text += "<p><strong>PAS (Op Reports) Access Count (SP): </strong>" + PASOpSPCount + "</p><br />";
        Literal1.Text += "<p><strong>CUI Access Count (IdP): </strong>" + CUICount + "</p>";
        Literal1.Text += "<p><strong>CUI Access Count (SP): </strong>" + CUISPCount + "</p><br />";
        Literal1.Text += "<p><strong>netPOSitive Access Count (IdP): </strong>" + netPOSCount + "</p>";
        Literal1.Text += "<p><strong>netPOSitive Access Count (SP): </strong>" + netPOSSPCount + "</p><br />";
        Literal1.Text += "<p><strong>WebEndo Access Count (IdP): </strong>" + WebEndoCount + "</p>";
        Literal1.Text += "<p><strong>WebEndo Access Count (SP): </strong>" + WebEndoSPCount + "</p><br />";
        Literal1.Text += "<p><strong>SFDC Access Count: </strong>" + SalesForceCount + "</p><br />";
        
        Literal1.Text += "<p><strong>Total Access Count: </strong>" + (salesXCount + PASAdminCount + PASAppCount + PASOpCount + CUICount + netPOSCount + SalesForceCount + WebEndoCount) + "</p>";
        Literal1.Text += "<p><strong>Total Access Count (No Svc): </strong>" + (salesXCount + PASAdminCount + PASAppNoSvcCount + PASOpCount + CUICount + netPOSCount + SalesForceCount + WebEndoCount) + "</p>";

        int maxPerSecond = 0;
        int maxHour = 0;
        int maxMin = 0;
        int maxSec = 0;

        int currHour = 0;
        int curMin = 0;
        int curSec = 0;

        int perSecondCount = 0;

        for (int i = 0; i < attemptTimes.Count; i++)
        {
            if (attemptTimes[i].Hour == currHour)
            {
                if (attemptTimes[i].Minute == curMin)
                {
                    if (attemptTimes[i].Second == curSec)
                    {
                        perSecondCount++;
                    }
                    else
                    {
                        curSec = attemptTimes[i].Second;
                        if (perSecondCount > maxPerSecond)
                        {
                            maxPerSecond = perSecondCount;
                            maxSec = curSec;
                            maxMin = curMin;
                            maxHour = currHour;
                        }
                        perSecondCount = 0;
                    }
                }
                else
                {
                    curMin = attemptTimes[i].Minute;
                    if (perSecondCount > maxPerSecond)
                    {
                        maxPerSecond = perSecondCount;
                        maxSec = curSec;
                        maxMin = curMin;
                        maxHour = currHour;
                    }
                    perSecondCount = 0;
                }
            }
            else
            {
                currHour = attemptTimes[i].Hour;
                if (perSecondCount > maxPerSecond)
                {
                    maxPerSecond = perSecondCount;
                    maxSec = curSec;
                    maxMin = curMin;
                    maxHour = currHour;
                }
                perSecondCount = 0;
            }
        }

        TimeSpan ts = attemptTimes[attemptTimes.Count - 1].Subtract(attemptTimes[0]);

        double avgPerMinute = (double)ts.TotalMinutes / (double)attemptTimes.Count;

        Literal1.Text += "<p><strong>Max Requests Per Second: </strong>" + maxPerSecond + "@" + maxHour + ":" + maxMin + ":" + maxSec + "</p>";
        Literal1.Text += "<p><strong>Avg. Requests Per Minute: </strong>" + avgPerMinute + "</p>";

        for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < 60; j++)
            {
                int count = 0;
                foreach (DateTime dt in attemptTimes)
                {
                    if (dt.Hour == i)
                    {
                        if (dt.Minute == j)
                        {
                            count++;
                        }
                    }
                }
                loginChartString += count.ToString() + ", ";
            }
        }

        loginChartString = loginChartString.Substring(0, loginChartString.Length - 2);


        idpRecords = 0;
        spRecords = 0;
        
        failCount = 0;

        salesXCount = 0;
        salesXSPCount = 0;

        PASAdminCount = 0;
        PASAdminSPCount = 0;

        PASAppCount = 0;
        PASAppSPCount = 0;

        PASAppNoSvcCount = 0;
        PASAppNoSvcSPCount = 0;

        PASOpCount = 0;
        PASOpSPCount = 0;

        WebEndoCount = 0;
        WebEndoSPCount = 0;

        CUICount = 0;
        CUISPCount = 0;

        netPOSCount = 0;
        netPOSSPCount = 0;

        SalesForceCount = 0;

        attemptTimes = new List<DateTime>();

        Literal2.Text = "<h1>" + result.Tables[1].TableName + "</h1>";

        foreach (DataRow row in result.Tables[1].Rows)
        {
            if (row[9].ToString().Trim().Equals("failure"))
                failCount++;

            switch (row[8].ToString().Trim())
            {
                case "IdP":
                    idpRecords++;
                    attemptTimes.Add(Convert.ToDateTime(row[0].ToString().Substring(0, row[0].ToString().Length - 4)));
                    switch (row[4].ToString().Trim())
                    {
                        case "https://salesx.csaaquote.com/agent/SSO":
                            row[4] = "SalesX";
                            salesXCount++;
                            break;
                        case "https://pas-prod-exigen:443/aaa-admin/do_auth":
                            row[4] = "PAS(Admin)";
                            PASAdminCount++;
                            break;
                        case "https://pas-prod-exigen.ent.rt.csaa.com:443/aaa-app/do_auth":
                        case "https://pas-prod-exigen:443/aaa-app/do_auth":
                            row[4] = "PAS(App)";
                            if (row[2].ToString().Trim() != "svc_healthchecks")
                                PASAppNoSvcCount++;
                            PASAppCount++;

                            break;
                        case "https://pas-prod-exigen.ent.rt.csaa.com:443/operational-reports-app/do_auth":
                            row[4] = "PAS(Op Reports)";
                            PASOpCount++;
                            break;
                        case "https://cssmcweblw71v.ent.rt.csaa.com:8443/TPresentation/SFDCGateway":
                            row[4] = "CUI";
                            CUICount++;
                            break;
                        case "https://newbusiness.westernunited.com/MulticoRater/Agent/SFDCIntegration.aspx":
                            row[4] = "netPOSitive";
                            netPOSCount++;
                            break;
                        case "https://www.westernunited.com/Endorsements/SFDCLoginAction.do":
                            row[4] = "WebEndo";
                            WebEndoCount++;
                            break;
                        case "":
                            row[4] = "SalesForce";
                            SalesForceCount++;
                            break;
                        default:
                            Response.Write("UNKNOWN: " + row[4] + "<br />");
                            break;
                    }
                    break;
                case "SP":
                    spRecords++;
                    switch (row[4].ToString().Trim())
                    {
                        case "https://salesx.csaaquote.com/agent/SSO":
                            row[4] = "SalesX";
                            salesXSPCount++;
                            break;
                        case "https://pas-prod-exigen:443/aaa-admin/do_auth":
                            row[4] = "PAS(Admin)";
                            PASAdminSPCount++;
                            break;
                        case "https://pas-prod-exigen.ent.rt.csaa.com:443/aaa-app/do_auth":
                        case "https://pas-prod-exigen:443/aaa-app/do_auth":
                            row[4] = "PAS(App)";
                            if (row[2].ToString().Trim() != "svc_healthchecks")
                                PASAppNoSvcSPCount++;
                            PASAppSPCount++;
                            break;
                        case "https://pas-prod-exigen.ent.rt.csaa.com:443/operational-reports-app/do_auth":
                            row[4] = "PAS(Op Reports)";
                            PASOpSPCount++;
                            break;
                        case "https://cssmcweblw71v.ent.rt.csaa.com:8443/TPresentation/SFDCGateway":
                            row[4] = "CUI";
                            CUISPCount++;
                            break;
                        case "https://newbusiness.westernunited.com/MulticoRater/Agent/SFDCIntegration.aspx":
                            row[4] = "netPOSitive";
                            netPOSSPCount++;
                            break;
                        case "https://www.westernunited.com/Endorsements/SFDCLoginAction.do":
                            row[4] = "WebEndo";
                            WebEndoSPCount++;
                            break;
                        case "":
                            row[4] = "SalesForce";
                            SalesForceCount++;
                            break;
                        default:
                            Response.Write("UNKNOWN: " + row[4] + "<br />");
                            break;
                    }
                    break;
            }
            
        }

        Literal2.Text += "<p><strong>Total Records:</strong>" + result.Tables[1].Rows.Count + "</p>";
        Literal2.Text += "<p><strong>Total IdP Records: </strong>" + idpRecords + "</p>";
        Literal2.Text += "<p><strong>Total SP Records: </strong>" + spRecords + "</p>";
        Literal2.Text += "<p><strong>Total Failures: </strong>" + failCount + "</p><br />";
        Literal2.Text += "<p><strong>SalesX Access Count (IdP): </strong>" + salesXCount + "</p>";
        Literal2.Text += "<p><strong>SalesX Access Count (SP): </strong>" + salesXSPCount + "</p><br />";
        Literal2.Text += "<p><strong>PAS (Admin) Access Count (IdP): </strong>" + PASAdminCount + "</p>";
        Literal2.Text += "<p><strong>PAS (Admin) Access Count (SP): </strong>" + PASAdminSPCount + "</p><br />";
        Literal2.Text += "<p><strong>PAS (App) Access Count (IdP): </strong>" + PASAppCount + "</p>";
        Literal2.Text += "<p><strong>PAS (App) Access Count (SP): </strong>" + PASAppSPCount + "</p><br />";
        Literal2.Text += "<p><strong>PAS (App - No Svc) Access Count (IdP): </strong>" + PASAppNoSvcCount + "</p>";
        Literal2.Text += "<p><strong>PAS (App - No Svc) Access Count (SP): </strong>" + PASAppNoSvcSPCount + "</p><br />";
        Literal2.Text += "<p><strong>PAS (Op Reports) Access Count (IdP): </strong>" + PASOpCount + "</p>";
        Literal2.Text += "<p><strong>PAS (Op Reports) Access Count (SP): </strong>" + PASOpSPCount + "</p><br />";
        Literal2.Text += "<p><strong>CUI Access Count (IdP): </strong>" + CUICount + "</p>";
        Literal2.Text += "<p><strong>CUI Access Count (SP): </strong>" + CUISPCount + "</p><br />";
        Literal2.Text += "<p><strong>netPOSitive Access Count (IdP): </strong>" + netPOSCount + "</p>";
        Literal2.Text += "<p><strong>netPOSitive Access Count (SP): </strong>" + netPOSSPCount + "</p><br />";
        Literal2.Text += "<p><strong>WebEndo Access Count (IdP): </strong>" + WebEndoCount + "</p>";
        Literal2.Text += "<p><strong>WebEndo Access Count (SP): </strong>" + WebEndoSPCount + "</p><br />";
        Literal2.Text += "<p><strong>SFDC Access Count: </strong>" + SalesForceCount + "</p><br />";

        Literal2.Text += "<p><strong>Total Access Count: </strong>" + (salesXCount + PASAdminCount + PASAppCount + PASOpCount + CUICount + netPOSCount + SalesForceCount + WebEndoCount) + "</p>";
        Literal2.Text += "<p><strong>Total Access Count (No Svc): </strong>" + (salesXCount + PASAdminCount + PASAppNoSvcCount + PASOpCount + CUICount + netPOSCount + SalesForceCount + WebEndoCount) + "</p>";

        maxPerSecond = 0;
        maxHour = 0;
        maxMin = 0;
        maxSec = 0;

        currHour = 0;
        curMin = 0;
        curSec = 0;

        perSecondCount = 0;

        for (int i = 0; i < attemptTimes.Count; i++)
        {
            if (attemptTimes[i].Hour == currHour)
            {
                if (attemptTimes[i].Minute == curMin)
                {
                    if (attemptTimes[i].Second == curSec)
                    {
                        perSecondCount++;
                    }
                    else
                    {
                        curSec = attemptTimes[i].Second;
                        if (perSecondCount > maxPerSecond)
                        {
                            maxPerSecond = perSecondCount;
                            maxSec = curSec;
                            maxMin = curMin;
                            maxHour = currHour;
                        }
                        perSecondCount = 0;
                    }
                }
                else
                {
                    curMin = attemptTimes[i].Minute;
                    if (perSecondCount > maxPerSecond)
                    {
                        maxPerSecond = perSecondCount;
                        maxSec = curSec;
                        maxMin = curMin;
                        maxHour = currHour;
                    }
                    perSecondCount = 0;
                }
            }
            else
            {
                currHour = attemptTimes[i].Hour;
                if (perSecondCount > maxPerSecond)
                {
                    maxPerSecond = perSecondCount;
                    maxSec = curSec;
                    maxMin = curMin;
                    maxHour = currHour;
                }
                perSecondCount = 0;
            }
        }

        Literal2.Text += "<p><strong>Max Requests Per Second: </strong>" + maxPerSecond + "@" + maxHour + ":" + maxMin + ":" + maxSec + "</p>";

        ts = attemptTimes[attemptTimes.Count - 1].Subtract(attemptTimes[0]);

        avgPerMinute = (double)ts.TotalMinutes / (double)attemptTimes.Count;

        Literal2.Text += "<p><strong>Avg. Requests Per Minute: </strong>" + avgPerMinute + "</p>";

        for (int i = 0; i < 24; i++)
        {
            for (int j = 0; j < 60; j++)
            {
                int count = 0;
                foreach (DateTime dt in attemptTimes)
                {
                    if (dt.Hour == i)
                    {
                        if (dt.Minute == j)
                        {
                            count++;
                        }
                    }
                }
                loginChartString2 += count.ToString() + ", ";
            }
        }

        loginChartString2 = loginChartString2.Substring(0, loginChartString2.Length - 2);
        //GridView1.DataSource = result.Tables[0];
        //GridView1.DataBind();
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_support/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="_support/js/jquery.js" type="text/javascript"></script>
    <script src="_support/js/highcharts.js" type="text/javascript"></script>
    <script type="text/javascript">
        var chart;
        $(document).ready(function () {
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'chart1',
                    zoomType: 'x',
                    spacingRight: 20
                },
                title: {
                    text: 'Ping Federate Traffic'
                },
                subtitle: {
                    text: document.ontouchstart === undefined ?
							'Click and drag in the plot area to zoom in' :
							'Drag your finger over the plot to zoom in'
                },
                xAxis: {
                    type: 'datetime',
                   // maxZoom: 14 * 24 * 3600000, // fourteen days
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    title: {
                        text: 'Logins'
                    },
                    min: 0,
                    startOnTick: false,
                    showFirstLabel: false
                },
                tooltip: {
                    shared: true
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: [0, 0, 0, 300],
                            stops: [
									[0, Highcharts.getOptions().colors[0]],
									[1, 'rgba(2,0,0,0)']
								]
                        },
                        lineWidth: 1,
                        marker: {
                            enabled: false,
                            states: {
                                hover: {
                                    enabled: true,
                                    radius: 5
                                }
                            }
                        },
                        shadow: false,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        }
                    }
                },

                series: [{
                    type: 'area',
                    name: 'Logins',
                    pointInterval: 9600 * 6,
                    pointStart: Date.UTC(2011, 6, 8),
                    data: [<%=loginChartString %>]
                }]
            });


        });
				
		</script>
        <script type="text/javascript">
        var chart;
        $(document).ready(function () {
            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'chart2',
                    zoomType: 'x',
                    spacingRight: 20
                },
                title: {
                    text: 'Ping Federate Traffic'
                },
                subtitle: {
                    text: document.ontouchstart === undefined ?
							'Click and drag in the plot area to zoom in' :
							'Drag your finger over the plot to zoom in'
                },
                xAxis: {
                    type: 'datetime',
                   // maxZoom: 14 * 24 * 3600000, // fourteen days
                    title: {
                        text: null
                    }
                },
                yAxis: {
                    title: {
                        text: 'Logins'
                    },
                    min: 0,
                    startOnTick: false,
                    showFirstLabel: false
                },
                tooltip: {
                    shared: true
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: [0, 0, 0, 300],
                            stops: [
									[0, Highcharts.getOptions().colors[0]],
									[1, 'rgba(2,0,0,0)']
								]
                        },
                        lineWidth: 1,
                        marker: {
                            enabled: false,
                            states: {
                                hover: {
                                    enabled: true,
                                    radius: 5
                                }
                            }
                        },
                        shadow: false,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        }
                    }
                },

                series: [{
                    type: 'area',
                    name: 'Logins',
                    pointInterval: 9600 * 6,
                    pointStart: Date.UTC(2011, 6, 8),
                    data: [<%=loginChartString2 %>]
                }]
            });


        });
				
		</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:FileUpload ID="FileUpload1" runat="server" />
    <asp:Button ID="Button1" runat="server" Text="Upload File" OnClick="Button1_OnClick" />
    <hr />
    <asp:Panel ID="Panel1" runat="server">
        <div style="display:block; width:49%; float:left;">
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <div id="chart1"></div>
        </div>
        <div style="display:block; width:49%; float:left;">
        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
        <div id="chart2"></div>
        </div>
    </asp:Panel>
    
</asp:Content>


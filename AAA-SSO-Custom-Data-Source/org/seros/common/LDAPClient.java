/*******************************************************************************
* Name
*   LDAP Client
*
* Identifier
*   Filename: LDAPClient.java (org.seros.common.LDAPClient)
*   
* Purpose
*   This class acts as a Data Structure and interface between MultiLDAP and 
*   LDAPWorker and provides search capability.
*
******************************************************************************
*/

//Define package
package org.seros.common;

//Java Imports
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;

public class LDAPClient implements InitializingBean
{
    //Setup logging within this class
    private Log log = LogFactory.getLog(this.getClass());
    
    //Variables used to connect to an LDAP Directory
    private static final String DEFAULT_NAMING_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
    private long timeout = 5000;
    private String name;
    private String endpoint;
    private String username;
    private String password;
    private String baseDN;
    private String namingFactory = DEFAULT_NAMING_FACTORY;
    private boolean ssl = false;
    private long cacheTimeout = 60000;

    /**
    * This class provides thread-local variables. These variables differ from 
    * their normal counterparts in that each thread that accesses one (via its 
    * get or set method) has its own, independently initialized copy of the 
    * variable. ThreadLocal instances are typically private static fields in 
    * classes that wish to associate state with a thread (e.g., a user ID or 
    * Transaction ID). This is the worker associated with communicating with the
    * LDAPWorker class.
    *
    * @return      ThreadLocal<LDAPWorker>
    */
    protected ThreadLocal<LDAPWorker> worker = new ThreadLocal<LDAPWorker>()
    {
        @Override
        protected LDAPWorker initialValue()
        {
            log.debug("LDAPClient:ThreadLocal<LDAPWorker> worker:initialValue()");
            // This initializes one LDAP worker for each thread
            LDAPWorker worker = new LDAPWorker();
            worker.setTimeout(timeout);
            worker.setLdapURL(endpoint);
            worker.setLdapUser(username);
            worker.setBaseDN(baseDN);
            worker.setLdapPassword(password);
            worker.setNamingFactory(namingFactory);
            worker.setSsl(ssl);
            return worker;
        }
    };
    
    /**
    * This class provides thread-local variables. These variables differ from 
    * their normal counterparts in that each thread that accesses one (via its 
    * get or set method) has its own, independently initialized copy of the 
    * variable. ThreadLocal instances are typically private static fields in 
    * classes that wish to associate state with a thread (e.g., a user ID or 
    * Transaction ID). This is the worker associated with communicating with the
    * LDAPWorker class for the purposes of schema validation.
    *
    * @return      ThreadLocal<LDAPWorker>
    */
    protected ThreadLocal<LDAPWorker> schemaWorker = new ThreadLocal<LDAPWorker>()
    {
        @Override
        protected LDAPWorker initialValue()
        {
            log.debug("LDAPClient:ThreadLocal<LDAPWorker> schemaWorker:initialValue()");
            // This initializes one LDAP worker for each thread
            LDAPWorker Sworker = new LDAPWorker();
            Sworker.setTimeout(timeout);
            try
            {
                //Attempt to connect to the Schema partition
            	String objectCategory = endpoint.substring(0, endpoint.lastIndexOf('/'))+"/"+worker.get().getAttributes(username.substring((username.contains("CN=") ? 3 : 0), (username.contains("CN=") ? username.indexOf(',') : username.length()))).get("objectCategory").get(0).toString();
            	Sworker.setLdapURL(endpoint.substring(0, endpoint.lastIndexOf('/'))+"/"+objectCategory.substring(objectCategory.indexOf("CN=Schema")));
            }
            catch (Exception ex)
            {
            	log.trace("LDAPClient:ThreadLocal<LDAPWorker> schemaWorker:initialValue(): Exception Thrown: ", ex);
            }
            //Finish setting up LDAP Schema Worker
            Sworker.setLdapUser(username);
            Sworker.setBaseDN(baseDN);
            Sworker.setLdapPassword(password);
            Sworker.setNamingFactory(namingFactory);
            Sworker.setSsl(ssl);
            return Sworker;
        }
    };

    /**
    * Base version of the search method. This method connects to the LDAP 
    * Directory via the LDAPWorker class and returns all populated attribute
    * names and values for the specified user.
    *
    * @param    dn      UserPrincipalName/sAMAccountName to search for
    * @return      Map<String, String>
    * @exception   NamingException
    */
    public Map<String, String> search(final String dn) throws NamingException
    {
    	log.debug("LDAPClient:search(final String dn): dn = \"" + dn + "\"");
        //Setup map object to store attribute names and values
    	Map<String, String> attributes = new HashMap<String, String>();

        //Setup variable to store attribute name
        String attrName = "";
        //Setup an enumeration of the ID for each attribute with a value
        NamingEnumeration<String> ne = worker.get().getAttributes(dn).getIDs();
        //Iterate over all returned attributes
        while(ne != null && ne.hasMoreElements())
        {
            //Save the attribute name
            attrName = ne.next();
            //Setup a clean variable to store the attribute value
            String attrVal = "";

            log.debug("LDAPClient:search(final String dn): User \"" + dn + "\" returned populated attribute \"" + attrName + "\".");
            try
            {
                //Get attribute values including all values from multi-valued attributes
                NamingEnumeration<?> attrEnum = worker.get().getAttributes(dn).get(attrName).getAll();
                //Iterate attribute values
                while(attrEnum != null && ne.hasMoreElements())
                {
                    //Turn attributes into CSV (for all attributes, but purposed for Multi-Valued attributes)
                    attrVal += attrEnum.next() + ",";
                }
                log.debug("LDAPClient:search(final String dn): User \"" + dn + "\" returned populated Key:Values\"" + attrName + ":" + attrVal + "\".");
                //Remove the trailing Comma if we have a value length greater than 1
                if(attrVal.length() > 1)
                        attrVal = attrVal.substring(0, attrVal.length()-1);
            }
            catch (Exception ex)
            {
                //Something happened, attempt to get single value attribute
                attrVal = worker.get().getAttributes(dn).get(attrName).get().toString();
                log.debug("LDAPClient:search(final String dn): User \"" + dn + "\" returned non-multivalued Key:Value\"" + attrName + ":" + attrVal + "\".");
            }
            
            //Add the attribute name and value to our collection
            attributes.put(attrName, attrVal);
        }
        //Return attribute names and values
        return attributes;
    }
    
    /**
    * Base version of the search method. This method connects to the LDAP 
    * Directory via the LDAPWorker class and returns all populated attribute
    * names and values for the specified user.
    *
    * @param    dn      UserPrincipalName/sAMAccountName to search for
    * @param    attribs Collection of attributes to return 
    * @return      Map<String, String>
    * @exception   NamingException
    */
    public Map<String, String> search(final String dn, Set<String> attribs) throws NamingException
    {
    	log.debug("LDAPClient:search(final String dn, Set<String> attribs): dn = \"" + dn + "\", attribs = \"" + Arrays.toString(attribs.toArray()) + "\"");
        //Setup map object to store attribute names and values
    	Map<String, String> attributes = new HashMap<String, String>();

        //Setup variable to store attribute name
        String attrName = "";

        //Setup an enumeration of the ID for each attribute with a value
        NamingEnumeration<String> ne = worker.get().getAttributes(dn).getIDs();
        while(ne != null && ne.hasMoreElements())
        {
            //Save the attribute name
            attrName = ne.next();
            //Setup a clean variable to store the attribute value
            String attrVal = "";
        	
            try
            {
                //Check to see if we care about the attribute
                if (attribs.contains(attrName))
                {
                    log.debug("LDAPClient:search(final String dn, Set<String> attribs): User \"" + dn + "\" returned populated attribute \"" + attrName + "\".");
                    //Get attribute values including all values from multi-valued attributes
                    NamingEnumeration<?> attrEnum = worker.get().getAttributes(dn).get(attrName).getAll();
                    //Iterate attribute values
                    while(attrEnum != null && attrEnum.hasMoreElements())
                    {
                        //Turn attributes into CSV (for all attributes, but purposed for Multi-Valued attributes)
                        attrVal += attrEnum.next() + ",";
                    }
                    log.debug("LDAPClient:search(final String dn, Set<String> attribs): User \"" + dn + "\" returned populated Key:Values\"" + attrName + ":" + attrVal + "\".");
                    //Remove the trailing Comma if we have a value length greater than 1
                    if(attrVal.length() > 1)
                            attrVal = attrVal.substring(0, attrVal.length()-1);
                }
            }
            catch (Exception ex)
            {
                //Something happened, attempt to get single value attribute
                attrVal = worker.get().getAttributes(dn).get(attrName).get().toString();
                log.debug("LDAPClient:search(final String dn, Set<String> attribs): User \"" + dn + "\" returned non-multivalued Key:Value\"" + attrName + ":" + attrVal + "\".");
            }
            
            //Add the attribute name and value to our collection
            attributes.put(attrName, attrVal);
        }
        //Return attribute names and values
        return attributes;
    }
    
    /**
    * Method to determine if an attribute exists in the schema of the current
    * LDAPClient object.
    *
    * @param attrName The attribute name to search the schema for
    * @return      boolean
    * @exception   NamingException
    */
    public boolean AttributeExists(String attrName) throws NamingException
    {            
        return schemaWorker.get().SchemaAttributeExists(attrName);
    }

    /**
    * Method to set the timeout value for connecting to the LDAP Directory
    *
    * @param timeout    The timeout in milliseconds
    */
    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    /**
    * Method to set whether to use SSL to connect to the LDAP Directory
    *
    * @param ssl    Boolean value, true means use SSL, false means do not use SSL
    */
    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    /**
    * Method to set the Endpoint of the LDAP Directory. This is the LDAP URL
    *
    * @param endpoint    The LDAP URL of the Directory
    */
    public void setEndpoint(String endpoint)
    {
        this.endpoint = endpoint;
    }
    
    /**
    * Method to get the Endpoint of the LDAP Directory.
    *
    * @return  The endpoint of the LDAP Directory
    */
    public String getEndpoint()
    {
    	return this.endpoint;
    }

    /**
    * Method to set the username used to connect to the LDAP Directory
    *
    * @param username    The username used to connect to the LDAP Directory
    */
    public void setUsername(String username)
    {
        this.username = username;
    }
    
    /**
    * Method to get the username of the user being used to connect to the LDAP Directory.
    *
    * @return  The username used to connect to the LDAP Directory
    */
    public String getUsername()
    {
    	return this.username;
    }

    /**
    * Method to set the password used to connect to the LDAP Directory
    *
    * @param password    The password used to connect to the LDAP Directory
    */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
    * Method to set the base DN of the LDAP Directory
    *
    * @param baseDN    The base DN of the LDAP Directory
    */
    public void setBaseDN(String baseDN)
    {
        this.baseDN = baseDN;
    }
    
    /**
    * Method to set the name of the LDAP Directory. This is not used in connecting
    * to the directory, only a common name associated with the Directory
    *
    * @param name    The name of the LDAP Directory
    */
    public void setName(String name)
    {
    	this.name = name;
    }
    
    /**
    * Method to get the name of the LDAP Directory.
    *
    * @return  The name of the LDAP Directory
    */
    public String getName()
    {
    	return this.name;
    }

    /**
    * Method used to set the Naming Factory for the LDAP connection
    *
    * @param namingFactory    The naming factory to be used to connect to the LDAP Directory
    */
    public void setNamingFactory(String namingFactory)
    {
        this.namingFactory = namingFactory;
    }

    /**
    * Method used to set the cache timeout for the LDAP Directory
    *
    * @param namingFactory    The cache timeout value for the directory in milliseconds
    */
    public void setCacheTimeout(long cacheTimeout)
    {
        this.cacheTimeout = cacheTimeout;
    }

    /**
    * Invoked by a BeanFactory after it has set all bean properties supplied 
    * (and satisfied BeanFactoryAware and ApplicationContextAware).
    */
    @Override
    public void afterPropertiesSet() throws IllegalArgumentException
    {
        log.debug("LDAPClient:afterPropertiesSet()");
        //If no baseDN has been supplied
        if (baseDN == null)
        {
            //Attempt to extract DN from LDAP URL (endpoint)
            int lastSlash = endpoint.lastIndexOf('/');
            //If DN was found in LDAP URL (endpoint)
            if (lastSlash != -1)
            {
                //Grab the DN from the endpoint
                baseDN = endpoint.substring(lastSlash + 1);
                //Set the endpoint to be everything but the DN
                endpoint = endpoint.substring(0, lastSlash);
            }
        }
        //If no baseDN could be found
        if (baseDN == null)
        {
            //Throw exception
            throw new IllegalArgumentException("Missing baseDN");
        }
    }
}

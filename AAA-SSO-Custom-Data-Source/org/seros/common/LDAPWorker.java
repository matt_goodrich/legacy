/*******************************************************************************
* Name
*   LDAP Worker
*
* Identifier
*   Filename: LDAPWorker.java (org.seros.common.LDAPWorker)
*   
* Purpose
*   This class acts as a Data Structure and interface between MultiLDAP and 
*   LDAPWorker and provides search capability.
*
******************************************************************************
*/

//Define package
package org.seros.common;

//Java imports
import java.util.Hashtable;
import javax.naming.*;
import javax.naming.directory.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class LDAPWorker
{
    //Setup logging within this class
    private Log log = LogFactory.getLog(this.getClass());

    //Variables used to connect to an LDAP Directory
    private static final long DEFAULT_LDAP_RETRY_INTERVAL = 5 * 1000;
    private static final int MAX_RETRY = 3;
    private static final String[] attributeSet = new String[] { "dn" };
    private long timeout = 10000;
    private String ldapURL;
    private String ldapUser;
    private String ldapPassword;
    private String baseDN;
    private String namingFactory;
    private boolean ssl;
    private DirContext context;

    //Constructor
    public LDAPWorker()
    {
        //Do nothing :)
    }

    /**
    * Method to get the timeout of the LDAP Directory.
    *
    * @return  The timeout of the LDAP Directory
    */
    public long getTimeout()
    {
        return timeout;
    }

    /**
    * Method to set the timeout value for connecting to the LDAP Directory
    *
    * @param timeout    The timeout in milliseconds
    */
    public void setTimeout(long timeout)
    {
        this.timeout = timeout;
    }

    /**
    * Method to set the Endpoint of the LDAP Directory. This is the LDAP URL
    *
    * @param ldapURL    The LDAP URL of the Directory
    */
    public void setLdapURL(String ldapURL)
    {
        this.ldapURL = ldapURL;
    }

    /**
    * Method to set the username used to connect to the LDAP Directory
    *
    * @param ldapUser    The username used to connect to the LDAP Directory
    */
    public void setLdapUser(String ldapUser)
    {
        this.ldapUser = ldapUser;
    }

    /**
    * Method to set the password used to connect to the LDAP Directory
    *
    * @param ldapPassword    The password used to connect to the LDAP Directory
    */
    public void setLdapPassword(String ldapPassword)
    {
        this.ldapPassword = ldapPassword;
    }

    /**
    * Method to set the base DN of the LDAP Directory
    *
    * @param baseDN    The base DN of the LDAP Directory
    */
    public void setBaseDN(String baseDN)
    {
        this.baseDN = baseDN;
    }

    /**
    * Method used to set the Naming Factory for the LDAP connection
    *
    * @param namingFactory    The naming factory to be used to connect to the LDAP Directory
    */
    public void setNamingFactory(String namingFactory)
    {
        this.namingFactory = namingFactory;
    }

    /**
    * Method to set whether to use SSL to connect to the LDAP Directory
    *
    * @param ssl    Boolean value, true means use SSL, false means do not use SSL
    */
    public void setSsl(boolean ssl)
    {
        this.ssl = ssl;
    }

    /**
    * Method to connect to the specified LDAP Directory
    *
    * @exception NamingException 
    */
    private void connect() throws NamingException
    {
        log.debug("LDAPWorker:connect()");
        //Check for pre-existing connection and return since we are already connected
        if (context != null)
            return;

        //Setup a hashtable to store information about our connection
        Hashtable<String, Object> props = new Hashtable<String, Object>();
        props.put("com.sun.jndi.ldap.connect.timeout", String.valueOf(getTimeout()));
        //If we wish to use SSL
        if (ssl)
        {
            try
            {
                //Add the proper security security protocol to the hashtable
                props.put(Context.SECURITY_PROTOCOL, "ssl");
            }
            catch (Exception e)
            {
                log.trace("LDAPWorker:connect():Failed to initialize LDAP connection for SSL", e);
            }
        }
        //Add our naming factory
        props.put(Context.INITIAL_CONTEXT_FACTORY, namingFactory);
        //Add our LDAP URL
        props.put(Context.PROVIDER_URL, ldapURL);
        //Define that we wish to use simple authentication and provide our username and password
        props.put(Context.SECURITY_AUTHENTICATION, System.getProperty(Context.SECURITY_AUTHENTICATION, "simple"));
        props.put(Context.SECURITY_PRINCIPAL, ldapUser);
        props.put(Context.SECURITY_CREDENTIALS, ldapPassword);
        //Setup our context with our new connection
        context = new InitialDirContext(props);
    }

    /**
    * Method to disconnect from the LDAP Directory
    */
    private void disconnect()
    {
    	log.debug("LDAPWorker:disconnect()");
        //Check to ensure we have a context
        if (context != null)
        {
            try
            {
                //Attempt to close the context connection
                context.close();
            }
            catch (Exception e)
            {
                log.trace("LDAPWorker:disconnect():Failed to close the context", e);
            }
            context = null;
        }
    }

    /**
     * Search for the CN and return attributes from the first (and hopefully
     * only) match
     * 
     * @param username
     * @return Attributes
     * @throws NamingException
     */
    private Attributes getAttributesByUserName(String username) throws NamingException
    {
    	String validatedUsername = sanitize(username);
        log.debug("LDAPWorker:getAttributesByUserName(String username): username = \"" + validatedUsername + "\".");
        //Setup our search filter to work with AD-LDS and AD
        String searchFilter = "(|(cn=" + validatedUsername + ")(userPrincipalName=" + validatedUsername + ")(sAMAccountName=" + validatedUsername + "))";

        //Setup a new SearchControls object
        SearchControls srchControls = new SearchControls();

        //Pass null to return all attributes with values
        srchControls.setReturningAttributes(null);
        //Do a full subtree search
        srchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
    
        // Search LDAP and return only ONE result
        NamingEnumeration<SearchResult> srchResponse = context.search(baseDN,searchFilter, srchControls);

        //If we have results
        if (srchResponse.hasMore())
        {
            //Extract our search result from our search enumeration
            SearchResult result = srchResponse.next();
            
            //Disconnect, we have enverything we need
            disconnect();
            
            //Return the attribute set
            return result.getAttributes();
        }
        //We did not get any results, so disconnect
        disconnect();
        //Throw exception
        throw new NameNotFoundException("Search for \"" + username + "\" failed.");
    }
    
    /**
     * Search the schema partition for the specified attribute
     * 
     * @param attrName  Attribute name to search for
     * @return boolean
     * @throws NamingException
     */
    public boolean SchemaAttributeExists(String attrName) throws NamingException
    {
    	String validatedAttrname = sanitize(attrName);
        
        log.debug("LDAPWorker:SchemaAttributeExists(String attrName): attrName = \"" + validatedAttrname + "\".");
    	try
    	{
            //Connect and get a valid context
            connect();
    		
            //Setup our search filter
            String searchFilter = "(&(objectClass=attributeSchema)(lDAPDisplayName=" + validatedAttrname + "))";
    		
            //Setup a new SearchControls object
            SearchControls srchControls = new SearchControls();
    		
            //Pass null to return all attributes with values
            srchControls.setReturningAttributes(null);
            //Do a full subtree search
            srchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        
            // Search LDAP and return only ONE result
            NamingEnumeration<SearchResult> srchResponse = context.search(baseDN, searchFilter, srchControls);

            //If we have results
            if (srchResponse.hasMore())
            {
                //We found the attribute, disconnect and return true
            	disconnect();
                return true;
            }
            //We did not find the attribute
            disconnect();
            //Throw exception
            throw new NameNotFoundException("Search for attribute \"" + attrName + "\" failed");
    	}
    	catch (Throwable t)
        {
            //Something happened, do some cleanup
            disconnect();
            if (t instanceof NamingException)
            {
                throw (NamingException) t;
            }
            NamingException ex = new NamingException("Unable to retrieve attribute \"" + attrName + "\"");
            ex.setRootCause(t);
            throw ex;
        }
    }
    
    /**
     * Get the attributes for the specified user
     * 
     * @param dn  User to get the attributes for
     * @return Attributes
     * @throws NamingException
     */
    public Attributes getAttributes(String dn) throws NamingException
    {
    	log.debug("LDAPWorker:getAttributes(String dn): dn = \"" + dn + "\".");
        //Count in case we have problems connecting, go to our retry limit
        int count = 0;
        //Loop ALWAYS!
        while (true)
        {
            try
            {
                //Get a directory context by connecting
                connect();
                //Attempt to get the attributes
                Attributes attrs = context.getAttributes(dn);
                //If successful return attributes
                return attrs;
            }
            catch (NameNotFoundException ex)
            {
                //This is likely to be called, get attributes by username instead
                return getAttributesByUserName(dn);
            }
            catch (InvalidNameException ine)
            {
                //This is likely to be called, get attributes by username instead
                return getAttributesByUserName(dn);
            }
            catch (Throwable t)
            {
                //We really can't connect, try disconnecting
                disconnect();
                //If we have reached our max retry limit
                if (++count == MAX_RETRY)
                {
                    //Throw exception
                    if (t instanceof NamingException)
                    {
                        throw (NamingException) t;
                    }
                    NamingException ex = new NamingException("Unable to retrieve attributes");
                    ex.setRootCause(t);
                    throw ex;
                }
            }
            try
            {
                //Sleep the thread for the length of our retry interval
                Thread.sleep(DEFAULT_LDAP_RETRY_INTERVAL);
            }
            catch (InterruptedException ex)
            {
                //Something happened, to the logs it goes
                log.trace("LDAPWorker:getAttributes(String dn): Sleep Interrupted ", ex);
            }
        }
    }
    
    /**
    * Escapes any special chars (RFC 4515) from a string representing a
    * a search filter assertion value.
    *
    * @param input The input string.
    *
    * @return A assertion value string ready for insertion into a 
    *         search filter string.
    */
    public static String sanitize(final String input) {
        
        String s = "";
        
        for (int i=0; i< input.length(); i++) {
                char c = input.charAt(i);
                if (c == '*') {
                        // escape asterisk
                        s += "\\2a";
                }
                else if (c == '(') {
                        // escape left parenthesis
                        s += "\\28";
                }
                else if (c == ')') {
                        // escape right parenthesis
                        s += "\\29";
                }
                else if (c == '\\') {
                        // escape backslash
                        s += "\\5c";
                }
                else if (c == '\u0000') {
                        // escape NULL char
                        s += "\\00";
                }
                else if (c <= 0x7f) {
                        // regular 1-byte UTF-8 char
                        s += String.valueOf(c);
                }
                else if (c >= 0x080) { 
                        // higher-order 2, 3 and 4-byte UTF-8 chars
                        try {
                                byte[] utf8bytes = String.valueOf(c).getBytes("UTF8");
                                for (byte b: utf8bytes)
                                        s += String.format("\\%02x", b);
                        } catch (Exception e) {
                                // ignore
                        }
                }
        }
        return s;
    }
}

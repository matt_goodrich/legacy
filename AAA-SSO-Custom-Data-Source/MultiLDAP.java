/*******************************************************************************
* Name
*   Multi-LDAP - Ping Federate Custom Source SDK Driver
*
* Identifier
*   Filename: MultiLDAP.java
*   
* Purpose
*   This class acts as the interface between PingFederate and multiple LDAP
*   Data Stores. This class implements the methods that PingFederate will call
*   on the Data Stores screen as well as within an SP connector including 
*   attribute contract fulfillment.
*
******************************************************************************
*/

//Imports
import com.pingidentity.sources.ConfigurableDriver;
import com.pingidentity.sources.CustomDataSourceDriver;
import com.pingidentity.sources.CustomDataSourceDriverDescriptor;
import com.pingidentity.sources.gui.FilterFieldsGuiDescriptor;
import java.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.seros.common.LDAPClient;
import org.sourceid.saml20.adapter.conf.*;
import org.sourceid.saml20.adapter.gui.*;
import org.sourceid.saml20.adapter.gui.validation.ConfigurationValidator;
import org.sourceid.saml20.adapter.gui.validation.RowValidator;
import org.sourceid.saml20.adapter.gui.validation.ValidationException;
import org.sourceid.saml20.adapter.gui.validation.impl.RegExValidator;
import org.sourceid.saml20.adapter.gui.validation.impl.RequiredFieldValidator;

public class MultiLDAP implements ConfigurableDriver, CustomDataSourceDriver
{
    //Setup logging within this class
    private Log log = LogFactory.getLog(this.getClass());
	
    //Variables to store information on the LDAP Directories we will be connecting 
    //to and the attributes we require
    List<LDAPClient> ldaps = new ArrayList<LDAPClient>();
    List<String> attrs = new ArrayList<String>();

    //Final static variables that reference the names of UI elements in the Data 
    //Stores screen
    private static final String tableName = "LDAP Directories";
    private static final String columnOneName = "LDAP Name";
    private static final String columnTwoName = "LDAP URL";
    private static final String columnThreeName = "Username";
    private static final String columnFourName = "Password";
    private static final String columnFiveName = "Use SSL";
    private static final String AttrTableName = "Attributes";
    private static final String AttrColOneName = "AttributeName";

    /**
    * An adapter requires configuration fields in the PingFederate administrative 
    * console. The UI descriptor for custom data-source implementations works in 
    * much the same way as the same method does for adapters. An 
    * AdapterConfigurationGuiDescriptor is populated with objects that will 
    * appear as controls in the PingFederate administrative console when a 
    * custom data source is deployed. The AdapterConfigurationGuiDescriptor is 
    * passed into the constructor of the SourceDescriptor that is later returned 
    * from the getSourceDescriptor() method. This method is required by Ping.
    *
    * @return      CustomDataSourceDriverDescriptor
    */
    @Override
    public CustomDataSourceDriverDescriptor getSourceDescriptor()
    {	
        log.debug("MultiLDAP:getSourceDescriptor()");
        //Description text shown at the top of the Data-Stores configuration screen
        String description = "Please provide the details for configuring each LDAP connection.";

        //Build a new adapter
        AdapterConfigurationGuiDescriptor adapterConfGuiDesc = new AdapterConfigurationGuiDescriptor(description);
		
        //Build the table
        TableDescriptor table = new TableDescriptor(tableName, "");
        
        //Build Column 1 (LDAP Name) and mark it as required
        TextFieldDescriptor column1 = new TextFieldDescriptor(columnOneName, "");
        column1.addValidator(new RequiredFieldValidator());
        table.addRowField(column1);
        
        //Build Column 2 (LDAP URL) and mark it as required and validate it 
        //begins with "LDAP://" using a regular expression
        TextFieldDescriptor column2 = new TextFieldDescriptor(columnTwoName, "");
        column2.addValidator(new RequiredFieldValidator());
        column2.addValidator(new RegExValidator("^LDAP://.+"));
        table.addRowField(column2);
       
        //Build Column 3 (Username) and mark it as required
        TextFieldDescriptor column3 = new TextFieldDescriptor(columnThreeName, "");
        column3.addValidator(new RequiredFieldValidator());
        table.addRowField(column3);
        
        //Build Column 4 (Password) and mark it as required
        TextFieldDescriptor column4 = new TextFieldDescriptor(columnFourName, "", true);
        column4.addValidator(new RequiredFieldValidator());
        table.addRowField(column4);
        
        //Build Column 5 (SSL)
        table.addRowField(new CheckBoxFieldDescriptor(columnFiveName, ""));
        
        //Add a validator to be run when each row is saved
        table.addValidator(new RowValidator()
        {
            //Override validation function that recieves all values in row
            @Override
            public void validate(FieldList fieldsInRow) throws ValidationException
            {
                log.debug("MultiLDAP:Validate LDAP");
                //Setup a client to be used for testing the connection
                LDAPClient client = new LDAPClient();
                client.setName(fieldsInRow.getFieldValue(columnOneName));
                client.setEndpoint(fieldsInRow.getFieldValue(columnTwoName));
                client.setSsl(fieldsInRow.getBooleanFieldValue(columnFiveName));
                client.setUsername(fieldsInRow.getFieldValue(columnThreeName));
                client.setPassword(fieldsInRow.getFieldValue(columnFourName));
                client.setBaseDN("");
                //Test the connection and return error message/exception if test failed
                if(!testConnection(client)) 
                {
                    log.debug("MultiLDAP:Validate LDAP:Validation of \"" + fieldsInRow.getFieldValue(columnTwoName) + "\" Failed");
                    throw new ValidationException("Directory \"" + fieldsInRow.getFieldValue(columnTwoName) + "\" could not be contacted.");
                }
            }
        });
        
        //Add the table to the GUI
        adapterConfGuiDesc.addTable(table);
        
        //Setup second table for attribute list
        TableDescriptor AttrTableDesc = new TableDescriptor(AttrTableName, "");
        
        //Setup Column 1 (Attribute Name) and mark it as required
        TextFieldDescriptor AttrColumn1 = new TextFieldDescriptor(AttrColOneName, "");
        AttrColumn1.addValidator(new RequiredFieldValidator());
        AttrTableDesc.addRowField(AttrColumn1);
        
        //Add the table to the GUI
        adapterConfGuiDesc.addTable(AttrTableDesc);
        
        //Add a validator to the row to validate attribute exists
        adapterConfGuiDesc.addValidator(new ConfigurationValidator()
        {
            //Override validation function that recieves all values in row
            @Override
            public void validate(Configuration configuration) throws ValidationException
            {
                log.debug("MultiLDAP:Validate Attributes");
                //List to hold all LDAP Directories to check
            	List<LDAPClient> testList = new ArrayList<LDAPClient>();
            	
                //Extract the LDAP table from the configuration
                Table table = configuration.getTable(tableName);
                //Extract the attribute table from the configuration
                Table attrTable = configuration.getTable(AttrTableName);
                //Add each LDAP Directory to our List
                for (Row r : table.getRows())
                {
                    LDAPClient client = new LDAPClient();
                    client.setName(r.getFieldValue(columnOneName));
                    client.setEndpoint(r.getFieldValue(columnTwoName));
                    client.setSsl(r.getBooleanFieldValue(columnFiveName));
                    client.setUsername(r.getFieldValue(columnThreeName));
                    client.setPassword(r.getFieldValue(columnFourName));
                    client.setBaseDN("");
                    testList.add(client);
                }
                
                //Iterate over each LDAP Directory
                for (LDAPClient client : testList)
                {
                    //Iterate over each attribute
                    for (Row r : attrTable.getRows())
                    {
                        try
                        {
                            log.debug("MultiLDAP:Validate Attributes: Validating Attribute \"" + r.getFieldValue(AttrColOneName) + "\" in LDAP Directory \"" + client.getName() + "\".");
                            //Test if the current attribute exists in the current directory
                            if(!client.AttributeExists(r.getFieldValue(AttrColOneName)))
                            {
                                log.debug("MultiLDAP:Validate Attributes: Validatiion of Attribute \"" + r.getFieldValue(AttrColOneName) + "\" in LDAP Directory \"" + client.getName() + "\".");
                                throw new ValidationException("Attribute \"" + r.getFieldValue(AttrColOneName) + "\" could not be found.");
                            }
                        }
                        catch (Exception ex)
                        {
                            //Something happened, log error and throw Validation Exception
                            log.error(ex);
                            throw new ValidationException("Attribute \"" + r.getFieldValue(AttrColOneName) + "\" could not be found in " + client.getName() + " Directory.");
                        }
                    }
                }
            }
        });
        
        //Add Filter Capability so we can search in an SP connection
        FilterFieldsGuiDescriptor filter = new FilterFieldsGuiDescriptor();
        TextAreaFieldDescriptor textarea = new TextAreaFieldDescriptor("Filter", "", 5, 25);
        filter.addField(textarea);

        //Return the GUI
        return new CustomDataSourceDriverDescriptor(this,"Multi-LDAP",adapterConfGuiDesc, filter);
    }

    /**
    * A custom data source receives the configuration set in the PingFederate 
    * administrative console in the same way that an IdP adapter does. This 
    * method is required by Ping.
    * 
    * @param configuration The Configuration object that contains the information setup in the Data-Stores screen
    */
    @Override
    public void configure(Configuration configuration)
    {
        log.debug("MultiLDAP:configure(Configuration configuration)");
        //Get the LDAP Directory table
        Table tbl = configuration.getTable(tableName);

        //Iterate each row in the table
        for(Row r : tbl.getRows())
        {
            log.debug("MultiLDAP:configure(Configuration configuration): Adding LDAP \"" + r.getFieldValue(columnOneName) + "\".");
            //Create a new LDAPClient for each row and save it
            LDAPClient client = new LDAPClient();
            client.setName(r.getFieldValue(columnOneName));
            client.setEndpoint(r.getFieldValue(columnTwoName));
            client.setSsl(r.getBooleanFieldValue(columnFiveName));
            client.setUsername(r.getFieldValue(columnThreeName));
            client.setPassword(r.getFieldValue(columnFourName));
            client.setBaseDN("");
            ldaps.add(client);
        }

        //Get the attributes table
        Table TblAttrs = configuration.getTable(AttrTableName);

        //Iterate each row in the table
        for(Row r : TblAttrs.getRows())
        {
            log.debug("MultiLDAP:configure(Configuration configuration): Adding Attribute \"" + r.getFieldValue(AttrColOneName) + "\".");
            //Save the attribute
            attrs.add(r.getFieldValue(AttrColOneName));
        }
    }
    
    /**
    * When associating a custom data source with an IdP or SP connection, 
    * PingFederate tests connectivity to the data source by calling the 
    * testConnection() method. Your implementation of this method should perform 
    * the necessary steps to demonstrate a successful connection and return true. 
    * Return false if your implementation cannot communicate with the data store. 
    * A false result prevents an administrator from continuing with the 
    * data-source configuration. This method is required by Ping.
    * 
    * @return boolean
    */
    @Override
    public boolean testConnection()
    {
        log.debug("MultiLDAP:testConnection()");
        //Iterate each LDAP Directory and test the connection
        for(LDAPClient ldap : ldaps)
        {
            log.debug("MultiLDAP:testConnection(): Testing LDAP \"" + ldap.getName() + "\".");
            if(!testConnection(ldap))
            {
                log.debug("MultiLDAP:testConnection(): Testing of LDAP \"" + ldap.getName() + "\" failed.");
                return false;
            }
        }
        return true;
    }
    
    /**
    * This is an extension method of testConnection(). This method is used to
    * test connectivity to each LDAP Directory individually. This method is NOT 
    * required by Ping.
    * 
    * @param client The LDAPClient object to test
    * @return boolean
    */
    public boolean testConnection(LDAPClient client)
    {
        log.debug("MultiLDAP:testConnection(LDAPClient client)");
        try
        {
            //Attempt to connect to the directory and search for the account used to connect as we know that accoun exists in the Directory
            client.search(client.getUsername().substring((client.getUsername().contains("CN=") ? 3 : 0), (client.getUsername().contains("CN=") ? client.getUsername().indexOf(',') : client.getUsername().length())), new LinkedHashSet<String>());
            return true;
        }
        catch (Exception ex)
        {
            //Something happened, print stack trace and return false
            log.trace("MultiLDAP:testConnection(LDAPClient client): Error Testing Connection \"" + client.getName() + "\".", ex);
            return false;
        }
    }
    
    /**
    * PingFederate calls the getAvailableFields() method to determine the
    * available fields that could be returned from a query of this data source. 
    * These fields are displayed to the PingFederate administrator during the 
    * configuration of data-store lookup. The administrator can then select the 
    * attributes from the data source and map them to the adapter or attribute 
    * contract. PingFederate requires at least one field returned from this 
    * method. This method is required by Ping.
    * 
    * @return List<String>
    */
    @Override
    public List<String> getAvailableFields()
    {	
        //Return our saved list of attributes
        return attrs;
    }

    /**
    * When processing a connection using a custom data source, PingFederate 
    * calls the retrieveValues() method to perform the actual query for user 
    * attributes. This method receives a list of attribute names that should be 
    * populated with data. The method may also receive a filterConfiguration 
    * object containing criteria to use for selecting a specific record based on 
    * data during runtime. This method returns a map of name-value pairs. This 
    * map contains the collection of attribute names passed into the method and 
    * their corresponding values retrieved from the query. This method is 
    * required by Ping.
    * 
    * @param attributeNamesToFill   A collection of attribute names that require a value
    * @param filterConfiguration    The search criteria for the user to get the attribute values for
    * @return Map<String, Object>
    */
    @Override
    public java.util.Map<String,Object> retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration)
    {
        log.debug("MultiLDAP:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration)");
        //Map to store data to be returned
        Map<String,Object> map = new HashMap<String,Object>();
        //Map to store attribute names and values
        Map<String,String> Userattrs = new HashMap<String,String>();

        //Extract our filter and remove everything but the subject name
        String userName = filterConfiguration.getFieldValue("Filter").replace("(userPrincipalName=", "").replace(")", "");
        log.debug("MultiLDAP:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): Filter = \"" + userName + "\".");
        
        //Convert our set to a LinkedHashSet to remove duplicates
        Set<String> hs = new LinkedHashSet<String>(attributeNamesToFill);

        //Iterate each LDAP Directory to search for the user
        for(LDAPClient client : ldaps)
        {
            try 
            {
                //Search for the user and return the attributes requested
                Userattrs = client.search(userName, hs);
                //If we found the user, stop looking
                if(Userattrs.size() > 0) 
                {
                    log.debug("MultiLDAP:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): User \"" + userName + "\" found in LDAP \"" + client.getName() + "\".");
                    break;
                }
            }
            catch (Exception e) 
            {
                log.debug("MultiLDAP:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): User \"" + userName + "\" could not be found in LDAP \"" + client.getName() + "\".");
            }
        }
        
        //Iterate the attributes we need to return
        for(String str : attributeNamesToFill)
        {
            //Iterate the attributes returned from our search
            for(Map.Entry<String, String> e : Userattrs.entrySet())
            {
                //If the attribute we need to return matches the attribute we returned from our search
                if(str.equals(e.getKey())) 
                {
                    //Add the value to the map and map it to the name of the value we need to return
                    map.put(str, e.getValue());
                    log.debug("MultiLDAP:retrieveValues(Collection<String> attributeNamesToFill, SimpleFieldList filterConfiguration): Found attribute \"" + str + "\" for user \"" + userName + "\" with value \"" + e.getValue() + "\".");
                }
            }
        }
        
        //Return the values
        return map;
    }	
}
